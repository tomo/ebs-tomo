The sample stage represent the place the sample is located.

## Geometry

![Sample stage modelization](images/modelization-sample-stage.png)

<img src="images/axis-direction.svg" width="50%">

## Axis roles

The following axis role are provided.

To work properly with dedicated procedures or with Daiquiri, the following
references have to be used.

Some of them are not mandatory, depending on which kind of features to want to
use.

### `translation_x`

Translation on the beam (positive following the beam = in detector direction).

### `translation_y`

Lateral translation (positive on the left if you follow the beam.

### `translation_z`

Vertical translation (positive from bottom up).

### `rotation`

Rotation axis. By definition, at dial `rotation=0`, the `sample_x`
axis is aligned to the beam.

It can be clock-wise or counter-clock-wise. For that a metatdata have to
be set the axis in BLISS the following way.

```yaml
  - name: srot
    user_tag:
    - dial.clockwise

  - name: somega
    user_tag:
    - dial.anticlockwise
```

### `sample_u`

Translation over the rotation (same direction as `translation_x` when the rotation is at 0).

### `sample_v`

Translation over the rotation (same direction as `translation_y` when the rotation is at 0)

### `sample_x`

Virtual translation over the rotation (always following `translation_x`)

### `sample_y`

Virtual translation over the rotation (always following `translation_y`)

This roles have to be binded to the real axis used by the beamline.

## Focal position

The focal postion is located as x/y/z in this object.

See the [modelization of the beam](modelization_beam.md)
for more information.

## Detector center

The detector center is a parameter from the tomo sample stage.
It identifies the location of the detector at the sample stage coordinates.

See the [modelization of the beam](modelization_beam.md)
for more information.
