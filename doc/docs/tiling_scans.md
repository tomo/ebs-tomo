Tiling scans are provided.

![Reconstructed tiling scan](images/tiling-reconstruction.png)

## Tiling runner

As `TomoConfig` runner, the tiling scan provides an helper to scan a region of
the sample using the axis units and a detector.

![Tiling scan](images/tiling-scan.svg)

A trajectory is computed in `y` and `z` axis in order to acquire images
in the whole requested region. Some acquisition can overlap others.

According to the field of view and the requested region, this scan is processed
as a 2D, as a 1D or as a single `ct` scan.

## Tiling sequence

A dedicated tiling sequence is also provided.

It is designed to provide all at once sequence to display a view of the sample
stage, mostly with [Daiquiri](https://gitlab.esrf.fr/ui/daiquiri),
and the [tomo vis service](https://gitlab.esrf.fr/ui/tomovis).

![Reconstruction from a tiling sequence](images/tiling-sequence.png)

It provides dark and flat together with a tiling scans on the front (rotation at
0 deg) and the side (rotation at -90 deg) of the sample stage.

This sequence could change in the future in order to provide the expected data
for a proper reconstruction of the sample stage region.
