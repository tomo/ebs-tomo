Each `TomoDetector` provides a pixel size related to the sample (`sample_pixel_size`).

This pixel size can ever be provided by the user or be automatically computed.

A `sample_pixel_size_mode` property is provided to select one of this modes.

## Software architecture

![overview](images/tomo-detector-pixel-size.png)

A `TomoDetector` is aware of the existance of a lot of objects.

This objects are used to compute the auto pixel size.

## User pixel size

A user pixel size can be set manually to  each detectors.

For that:

- `sample_pixel_size_mode` have to be setup to `USER`
- And a value can be defined for `user_sample_pixel_size`

In this case `sample_pixel_size` with expose the user pixel size.

In this mode, the pixel size remains the same for any changes on
sample/source/detector/optics.

Only a change of the binning of the detector will update it.

## Auto pixel size

In this mode the sample pixel size is automatically computed based the
following modelization.

![modelization](images/pixel-size-geometry.png)

For that the following constants have to be tuned properly:

- **sx0**: the location of the focal position in the sample axis
    - This can be setup by defining **z1** (`tomo.sample_stage.source_distance`)
- **cx0**: the location of the focal position in the detector axis
    - This can be setup by defining **z1+z2** (`tomodetector.source_distance`)
    - This can be setup by defining **z2** (`tomodetector.sample_detector_distance`)
    - The detector `offset` related to the holder (TomoDetectors) could have to be fine tuned

The pixel size is computed based on the detector pixel size and binning,
plus the optic magnification plus a ratio based on the source/sample/detector
distances (z1, z2).

![modelization](images/pixel-size-math.png)

Finally `sample_pixel_size_mode` have to be set to `AUTO`, which switch
`sample_pixel_size` to the value of `auto_sample_pixel_size`.

In this mode, the pixel size is updated automatically during:

- sample axis displacements (event based)
- detector axis displacements (event based)
- tomo config parameter change change (event based)
- optic magnification change (event based)
- Lima detector changes (not event based)

## Beam tracker

For conic beam, the focal point and the detector are not necesserely
orthogonal to the translation of the sample stage.

This geometry is modelized in order to follow the beam when the sample stange
move.

![modelization](images/beam-tracker-modelization.png)

### Beam tracker

The `bean tracker` is an optional device which can be setup.

For now it can be setup with `sy_angle`, `sz_angle` in radian.

The `focal_position` is a sample stage parameter which have to be setup
independently.

### Tracked axis

A `tracked_translation_x` axis can be exposed from the `TomoConfig`.
It is a copy of `translation_x`, but when a motion is requested, the other
translations will be moved together (based on the beam tracked configuration)
to follow the beam.

### Detector center

The `detector center` is a parameter from the tomo sample stage.
It identifies the location of the detector at the sample stage coordinates.

This `detector center` is mandatory to display the image properly in Daiquiri and
to guess the location of the axis rotation. That is why this information is exposed
as scan metadata.

In the case of a conic beam, the value of the detector center depends on
the location of the sample stage in X, and follow this equation. The `beam tracker`
is responsible of the update of the value.

![equation](images/beam-tracker-math.png)

For non-conic beam or beamline which does not use sample stage translation in X,
this value can be set manually.
