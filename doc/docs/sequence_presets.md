## Sequence Presets ##

Hooks in tomo sequence. You can do some specific actions during preparation, before running sequence or after.

### How to create one ###
```
from tomo.sequence.presets import SequencePreset

class MySequencePreset(SequencePreset):

	def __init__(self, name, config):
		pass

	def prepare(self):
		# this part will be executed during sequence preparing

	def start(self):
		# this part will be executed before sequence running

	def stop(self):
		# this part will be executed after sequence running
		
		
# Configuration 
- name: mysequencepreset
  plugin: bliss
  class: MySequencePreset
  package: tomo.beamline.BM05.sequencepresets # module where you defined your sequence preset
  
  
#In your bliss session setup file#
your_tomo_sequence_object.add_sequence_preset(mysequencepreset, mysequencepresetname)
```
