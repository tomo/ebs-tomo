The tomo modelization is hold by the `TomoConfig` object.

This object is the state of everything we know about the tomo fullfield beamline.

In the other word, this is an abstraction on top of the hardware, allowing
to handle the same way multiple beamlines.

## Overview

![Overview modelization](images/modelization-overview.png)

## `TomoConfig`

This object is the main object.

It is linked to multiple specialized objects storing the configuration and the
state of the beamline.

It also stores the way to perform tomo scans. This includes the presets
specialized to each beamlines, and usually setup during the
session setup script.

## Sample stage

The sample stage represent the place the sample is located.

See the [modelization of the sample stage](modelization_sample_stage.md)
for more information.

## `TomoDetectors`

The object `TomoDetectors` whole everything we know about the detectors.

It is responsible of describing the available detectors of the beamline,
and the one which is active.

The component can be specialized to allow to mount detectors on the fly. Like
what is expected at ESRF BM18.

## `TomoDetector`

This component is responsible of providing a view of a specific detector
as it is localized into the beamline.

It is responsible to like together an optic and a detector, and the to provide
a "sample" pixel size (who much a motor have to move to shift the sample to 1
pixel in the detector)

## `Optic`

This component is responsible of handling the optic hardware and providing
a consistent API.

It exposes a magnification, like an axis provides a position. It can be changed
(to be tuned to an accurate value) or moved (to request the controller to select
the expected magnification).

This device provides a generic API to handle 3 kind of optics:

- Fixed magnification
- One to many magnifications
- One in a range of magnifications

## `Lima`

This is the default Lima device from BLISS.

## `TomoImaging`

Optionally, the `TomoConfig` can refer to a `TomoImaging`.

Now it's only an helper to provide feature for Daiquiri. Like taking projections,
dark and flat.

It have to be removed sooner or later.
