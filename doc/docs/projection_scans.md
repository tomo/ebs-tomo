## How to create new projection scan ##
Template:
```
class MyTomoRunner(TomoRunner):

    def __init__(self, config):
        super().__init__()
        self._projection_scan_name = "myprojname"                       
        self._scan = None

    def __call__(self, motor, start_pos, end_pos, tomo_n, expo_time, nloop, **kwargs):
        scan_info = kwargs.pop("scan_info", None)
        self.scan_info = ScanInfo.normalize(scan_info)

        header = kwargs.pop("header", {})
        run = kwargs.pop("run", True)
        save = kwargs.pop("save", True)

        # Add the user and scan meta data
        scan_info = self._add_metadata(
            motor, start_pos, end_pos, expo_time, tomo_n, latency_time, scan_info
        )

        self._scan = self._setup_scan(
            motor,
            start_pos,
            end_pos,
            tomo_n,
            expo_time,
            latency_time,
            header=header,
            save=save,
            scan_info=scan_info,
        )
        self._chain = self._scan.acq_chain

        self._setup_presets()

        if run:
            self._scan.run()

        return self._scan

    def _setup_scan(
        self,
        motor,
        start_pos,
        end_pos,
        tomo_n,
        expo_time,
        latency_time,
        header={},
        scan_info={},
        save=True,
    ):
        """
        Build  and return your projection scan
        ex:
            proj_scan = ascan(
                motor, start_pos, end_pos, tomo_n, expo_time, run=False, scan_info=scan_info
            )
        """
        log_info("tomo", "projection_scan() entering")

        proj_scan = myscan(
            motor, start_pos, end_pos, tomo_n, expo_time, run=False, scan_info=scan_info
        )

        log_info("tomo", "projection_scan() leaving")

        return proj_scan

    def _estimate_scan_duration(self, tomo_pars):
        """
        Compute and return scan duration
        """

    def _add_metadata(
        self, motor, start_pos, end_pos, expo_time, proj_n, latency_time, scan_info=None
    ):
        """
        Fill the scan_info dictionary with the scan meta data
        """
        scan_info = ScanInfo.normalize(scan_info)
        alias_name = "None"
        if setup_globals.ALIASES.get_alias(motor.name) is not None:
            alias_name = setup_globals.ALIASES.get_alias(motor.name)
        technique = scan_info.setdefault("technique", {})
        technique["proj"] = {
            "motor": ["rotation", motor.name, alias_name],
            "scan_type": "myscantype",          #can be STEP or CONTINUOUS for ex.
            "scan_range": end_pos - start_pos,
            "proj_n": proj_n,
            "exposure_time": expo_time,
            "exposure_time@units": "s",
        }
        return scan_info
```
