## Return Runner ##

Scan for images taken at the end when rotation returns to start angle.
By default, based on bliss pointscan.
A scan preset is automatically set to add image key value into image header for nexus compliance (see https://manual.nexusformat.org/classes/applications/NXtomo.html#nxtomo-entry-instrument-detector-image-key-field)

For shutter handling, you have the possiblity to use, as for flat scans, existing fast shutter preset or create your own.

```
#In your bliss session setup file#
from tomo.scan.presets.fast_shutter import FastShutterPreset

fastshutterpreset = FastShutterPreset(your_shutter_object)

your_tomo_config.add_scan_preset(['return_ref'], fastshutterpreset)
```

If you want to call return scan independantly from tomo, you have the possibility to extract it like this:

```
#In your bliss session setup file#
return_scan=your_tomo_config.get_runner('return_ref') 
``` 
