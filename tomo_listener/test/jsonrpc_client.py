import requests
import json

# client is on the bliss side. He knows when to send the request
# the following line should be embeed. I guess It won't add any extra dependancy on the bliss side


def scan_started(scan_number):
    return {
        "method": 'scan_started',
        "params": [scan_number, ],
        "jsonrpc": "2.0",
        "id": 0,
    }
    

def scan_ended(scan_number):
    return {
        "method": 'scan_ended',
        "params": [scan_number, ],
        "jsonrpc": "2.0",
        "id": 1,
    }


def sequence_started(saving_file, entry_name, scan_title, proposal_file, sample_file):
    return {
        "method": 'sequence_started',
        "params": [saving_file, scan_title, entry_name, proposal_file, sample_file],
        "jsonrpc": "2.0",
        "id": 2,
    }


def sequence_ended(saving_file, entry_name, suceed):
    return {
        "method": 'sequence_ended',
        "params": [saving_file, entry_name, suceed, ],
        "jsonrpc": "2.0",
        "id": 3,
    }


def main():
    url = "http://lbs191:4000/jsonrpc"
    timeout = 1.5

    response = requests.post(url,
                             json=sequence_started(saving_file='saving_file.h5',
                                                   scan_title='my scan title',
                                                   entry_name='entry0000',
                                                   proposal_file='ihpropfile.h5',
                                                   sample_file='ihsample.h5'),
                            timeout=timeout).json()
    response = requests.post(url,
                             json=scan_started(scan_number='0001'),
                             timeout=timeout).json()
    response = requests.post(url,
                             json=scan_ended(scan_number='0001'),
                             timeout=timeout).json()
    response = requests.post(url,
                             json=sequence_ended(saving_file='saving_file.h5',
                                                 entry_name='entry0000',
                                                 suceed=True),
                             timeout=timeout).json()


if __name__ == "__main__":
    main()
