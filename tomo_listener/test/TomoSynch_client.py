import time
import tango

beamline_name      = "id19"
bliss_session_name = "HRTOMO"
tango_device_name  = "//" + beamline_name + ":20000/" + beamline_name + "/tomo_listener/" + bliss_session_name
print (tango_device_name)

# connect to the tango device
device = tango.DeviceProxy(tango_device_name)

# waiting for a sequence to start
while device.State() != tango.DevState.MOVING:
    print ("sitting and waiting for a tomo sequence to start!")
    
# tomo sequence id running
# get sequence information
sequence_title       = device.SequenceTitle
sequence_scan_number = device.SequenceScanNumber
saving_file          = device.SavingFile
tomo_n               = device.TomoN
start_time           = device.StartTime

print(sequence_title)
print(sequence_scan_number)
print(saving_file)
print(tomo_n)
print(start_time)


# get scan information
#scan_number = device.ScanNumber
#scan_title  = device.ScanTitle

#print(scan_number)
#print(scan_title)

scan_number = -1  # default value when no scan was yet started
scan_title  = ''

# get information on the running tomo scans
while device.State() == tango.DevState.MOVING:
    time.sleep(1)
    if scan_number != device.ScanNumber:
        scan_number = device.ScanNumber
        scan_title  = device.ScanTitle

        print(scan_number)
        print(scan_title)

# tomo sequence done
end_time = device.EndTime   

# Did the tomo sequence finish properly?
if device.State() != tango.DevState.ON:
    # get the error information
    print (device.Status())
