import pytest
import gevent
from tomo import standard


def test_check_type_energy_pars(beacon):
    tomo_config = beacon.get("tomo_config")
    try:
        tomo_config.pars.energy = "10 keV"
    except Exception as exc:
        assert isinstance(exc, ValueError)

    tomo_config.pars.energy = 10.0
    assert tomo_config.pars.energy == 10.0

    tomo_config.pars.energy = 10
    assert tomo_config.pars.energy == 10.0


def test_y_step(beacon, default_session):
    tomo_config = beacon.get("tomo_config")
    tomo_config.set_active()
    gevent.sleep(1)
    assert tomo_config is not None

    with pytest.raises(ValueError):
        standard.Y_STEP()

    standard.Y_STEP(1.0)
    assert standard.Y_STEP() == 1.0
