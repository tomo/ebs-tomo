def test_pars(beacon):
    """Test that the synchronization of pars are right"""
    tomo_config = beacon.get("tomo_config")
    multitomo = beacon.get("multitomo")

    # tomo_config pars are shared
    tomo_config.pars.exposure_time = 10.0
    assert multitomo.pars.exposure_time == 10.0
    multitomo.pars.exposure_time = 11.0
    assert tomo_config.pars.exposure_time == 11.0

    # specialized pars are reachable
    assert multitomo.pars.shutter_time == 0
    multitomo.pars.shutter_time = 1.0
