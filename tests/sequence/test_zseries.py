import pytest
from ..utils import nxw_test_utils
from ..utils import lima_utils


def test_pars(beacon):
    """Test that the synchronization of pars are right"""
    tomo_config = beacon.get("tomo_config")
    fasttomo = beacon.get("fasttomo")
    tomo_zseries = beacon.get("tomo_zseries")

    # tomo_config pars are shared
    tomo_config.pars.exposure_time = 10.0
    assert tomo_zseries.pars.exposure_time == 10.0
    tomo_zseries.pars.exposure_time = 11.0
    assert tomo_config.pars.exposure_time == 11.0

    # sequence basic pars are shared
    fasttomo.pars.acquisition_position = 10.0
    assert tomo_zseries.pars.acquisition_position == 10.0
    tomo_zseries.pars.acquisition_position = 11.0
    assert fasttomo.pars.acquisition_position == 11.0

    # fulltomo pars are shared
    fasttomo.pars.rand_disp_y = 10.0
    assert tomo_zseries.pars.rand_disp_y == 10.0
    tomo_zseries.pars.rand_disp_y = 11.0
    assert fasttomo.pars.rand_disp_y == 11.0

    # specialized pars are reachable
    assert tomo_zseries.pars.delta_pos == 1.0
    tomo_zseries.pars.delta_pos = 10.0


def test_zseries_prepare_then_run(
    session,
    esrf_writer_session,
    nexus_writer_service,
    beacon,
    mocker,
    lima_simulator,
    setup_common_tomo_config,
):
    scan_factory = beacon.get("tomo_zseries")
    tomo_config = beacon.get("tomo_config")
    tomo_detector = beacon.get("tomo_detector")
    tomo_config.sample_stage.source_distance = 40000
    tomo_config.detectors.source_distance = 40000
    mg = beacon.get("mg")
    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()

    scan_factory.pars.scan_type = "STEP"
    scan_factory.pars.flat_n = 1  # speed up a bit the test
    scan_factory.pars.energy = 1
    scan_factory.basic_scan(
        0.1, 3, 0, 45, 4, 0.1, dataset_name="test_zseries", run=False
    )
    scan_factory.run()


def test_zseries_scan(
    session,
    esrf_writer_session,
    nexus_writer_service,
    beacon,
    mocker,
    lima_simulator,
    setup_common_tomo_config,
):
    """Setup a zseries scan in step mode

    Check that the basic scan record the expected data
    """
    scan_factory = beacon.get("tomo_zseries")
    tomo_config = beacon.get("tomo_config")
    tomo_detector = beacon.get("tomo_detector")
    tomo_config.sample_stage.source_distance = 40000
    tomo_config.detectors.source_distance = 40000
    mg = beacon.get("mg")
    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()

    scan_factory.pars.scan_type = "STEP"
    scan_factory.pars.flat_n = 1  # speed up a bit the test
    scan_factory.pars.energy = 1
    scan_factory.pars.dark_flat_for_each_scan = True

    scan_factory.basic_scan(0.1, 3, 0, 45, 4, 0.1, dataset_name="my_dataset")

    technique = scan_factory._sequence.scan_info["technique"]
    assert technique["subscans"]["scan0"]["type"] == "tomo:flat"
    assert technique["subscans"]["scan1"]["type"] == "tomo:step"
    assert technique["subscans"]["scan2"]["type"] == "tomo:return_ref"

    assert len(scan_factory._scans) == 3
    assert "flat" in scan_factory._scans[0].name
    assert "ascan" in scan_factory._scans[1].name
    assert "static" in scan_factory._scans[2].name

    ascan = scan_factory._scans[1]
    assert len(ascan.get_data()["axis:srot"]) == 5
    assert ascan.get_data()["axis:srot"][0] == 0
    assert ascan.get_data()["axis:srot"][-1] == 45
    view = ascan.get_data()["lima_simulator:image"]
    assert len(view) == 5


@pytest.fixture
def basic_zseries_per_dataset(
    beacon,
    esrf_writer_session,
    nexus_writer_service,
    mocker,
    lima_simulator,
    lima_simulator_small,
    lima_simulator_with_sino_roi,
    setup_common_tomo_config,
):
    """Run a zseries containing a sinogram"""
    scan_factory = beacon.get("tomo_zseries")
    tomo_config = beacon.get("tomo_config")
    tomo_detector = beacon.get("tomo_detector")
    tomo_config.sample_stage.source_distance = 40000
    tomo_config.detectors.source_distance = 40000
    mg = beacon.get("mg")
    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()
    mg.add(tomo_detector.detector.roi_profiles.counters.sino)
    mg.enable(tomo_detector.detector.roi_profiles.counters.sino.fullname)

    scan_factory.pars.activate_sinogram = True
    scan_factory.pars.scan_type = "STEP"
    scan_factory.pars.dark_at_start = True
    scan_factory.pars.flat_at_start = True
    scan_factory.pars.flat_n = 1  # speed up a bit the test
    scan_factory.pars.dark_n = 1  # speed up a bit the test
    scan_factory.pars.energy = 1
    scan_factory.pars.dark_flat_for_each_scan = True

    scan_factory.basic_scan(0.001, 3, 0, 45, 4, 0.1, dataset_name="my_dataset")

    nxw_test_utils.wait_scan_data_finished(
        scans=[scan_factory._scans[3]], writer=nexus_writer_service
    )
    lima_utils.wait_end_of_saving(lima_simulator)

    return scan_factory


@pytest.fixture
def basic_zseries_per_collection(
    beacon,
    esrf_writer_session,
    nexus_writer_service,
    mocker,
    lima_simulator,
    lima_simulator_small,
    lima_simulator_with_sino_roi,
    setup_common_tomo_config,
):
    """Run a zseries containing a sinogram"""
    scan_factory = beacon.get("tomo_zseries")
    tomo_config = beacon.get("tomo_config")
    tomo_detector = beacon.get("tomo_detector")
    tomo_config.sample_stage.source_distance = 40000
    tomo_config.detectors.source_distance = 40000
    mg = beacon.get("mg")
    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()
    mg.add(tomo_detector.detector.roi_profiles.counters.sino)
    mg.enable(tomo_detector.detector.roi_profiles.counters.sino.fullname)

    scan_factory.pars.activate_sinogram = True
    scan_factory.pars.dark_at_start = True
    scan_factory.pars.flat_at_start = True
    scan_factory.pars.scan_type = "STEP"
    scan_factory.pars.flat_n = 1  # speed up a bit the test
    scan_factory.pars.dark_n = 1  # speed up a bit the test
    scan_factory.pars.energy = 1
    scan_factory.pars.dark_flat_for_each_scan = True

    scan_factory.basic_scan(0.001, 3, 0, 45, 4, 0.1)

    nxw_test_utils.wait_scan_data_finished(
        scans=[scan_factory._scans[3]], writer=nexus_writer_service
    )
    lima_utils.wait_end_of_saving(lima_simulator)

    return scan_factory


def test_basic_zseries_per_dataset(
    basic_zseries_per_dataset, exportscan_service, lima_simulator
):
    """Setup a zseries scan in step mode with a sino profile roi

    Check that the basic scan record the expected data
    """
    scan_factory = basic_zseries_per_dataset

    assert len(scan_factory._scans) == 4
    assert "dark" in scan_factory._scans[0].name
    assert "flat" in scan_factory._scans[1].name
    assert "ascan" in scan_factory._scans[2].name
    assert "static" in scan_factory._scans[3].name

    sinogram_data = scan_factory._sequence.scan.get_data()["sinogram"]
    assert len(sinogram_data) == lima_simulator.image.width * 5
    exportscan_service.export("zseries_per_dataset", scan_factory._sequence)


def test_basic_zseries_per_collection(
    basic_zseries_per_collection, exportscan_service, lima_simulator
):
    """Setup a zseries scan in step mode with a sino profile roi

    Check that the basic scan record the expected data
    """
    scan_factory = basic_zseries_per_collection

    assert len(scan_factory._scans) == 4
    assert "dark" in scan_factory._scans[0].name
    assert "flat" in scan_factory._scans[1].name
    assert "ascan" in scan_factory._scans[2].name
    assert "static" in scan_factory._scans[3].name

    sinogram_data = scan_factory._sequence.scan.get_data()["sinogram"]
    assert len(sinogram_data) == lima_simulator.image.width * 5
    exportscan_service.export("zseries_per_collection", scan_factory._sequence)


def _test_nxtomomill(nxtomomill_service, basic_zseries_with_sinogram):
    scan_factory = basic_zseries_with_sinogram
    nxtomomill_service.convert(scan_factory._sequence)


def test_zseries_dark_flat_option(
    session,
    esrf_writer_session,
    nexus_writer_service,
    beacon,
    mocker,
    lima_simulator,
    setup_common_tomo_config,
):
    """Setup a zseries scan in step mode

    Check that the basic scan record the expected data
    """
    scan_factory = beacon.get("tomo_zseries")
    tomo_config = beacon.get("tomo_config")
    tomo_detector = beacon.get("tomo_detector")
    tomo_config.sample_stage.source_distance = 40000
    tomo_config.detectors.source_distance = 40000
    mg = beacon.get("mg")
    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()

    scan_factory.pars.scan_type = "STEP"
    scan_factory.pars.flat_n = 1  # speed up a bit the test
    scan_factory.pars.energy = 1
    scan_factory.pars.dark_at_start = True
    scan_factory.pars.flat_at_start = True
    scan_factory.pars.dark_at_end = True
    scan_factory.pars.flat_at_end = True

    scan_factory.pars.dark_flat_for_each_scan = False

    scan_factory.basic_scan(0.1, 3, 0, 45, 4, 0.1, dataset_name="my_dataset")

    technique = scan_factory._sequence.scan_info["technique"]
    subscans = [v["type"] for v in technique["subscans"].values()]
    assert subscans == [
        "tomo:step",
        "tomo:return_ref",
        "tomo:flat",
        "tomo:dark",
    ]

    scan_factory.pars.dark_flat_for_each_scan = True
    scan_factory.basic_scan(0.1, 3, 0, 45, 4, 0.1, dataset_name="my_dataset")

    technique = scan_factory._sequence.scan_info["technique"]
    subscans = [v["type"] for v in technique["subscans"].values()]
    assert subscans == [
        "tomo:dark",
        "tomo:flat",
        "tomo:step",
        "tomo:return_ref",
        "tomo:flat",
        "tomo:dark",
    ]


def test_estimation_time(
    beacon,
    session,
    lima_simulator,
    esrf_writer_session,
    setup_common_tomo_config,
):
    scan_factory = beacon.get("tomo_zseries")
    tomo_config = beacon.get("tomo_config")
    tomo_detector = beacon.get("tomo_detector")
    tomo_config.sample_stage.source_distance = 40000
    tomo_config.detectors.source_distance = 40000

    mg = beacon.get("mg")
    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()

    scan_factory.pars.scan_type = "STEP"
    scan_factory.pars.flat_n = 1
    scan_factory.pars.energy = 1
    scan_factory.pars.dark_at_start = True
    scan_factory.pars.flat_at_start = True
    scan_factory.pars.dark_at_end = True
    scan_factory.pars.flat_at_end = True
    scan_factory.pars.step_start_pos = 10
    scan_factory.pars.delta_pos = 0.1
    scan_factory.pars.nb_scans = 2
    scan_factory.pars.dark_flat_for_each_scan = False

    assert scan_factory.estimation_time() == pytest.approx(1050, abs=50)
