import pytest
import numpy
from silx.io import h5py_utils
from ..utils import nxw_test_utils
from ..utils import lima_utils
from tomo.helpers import sinogram_reader
from tomo.helpers import technique_model_reader


def test_pars(beacon):
    """Test that the synchronization of pars are right"""
    tomo_config = beacon.get("tomo_config")
    fasttomo = beacon.get("fasttomo")

    # tomo_config pars are shared
    tomo_config.pars.exposure_time = 10.0
    assert fasttomo.pars.exposure_time == 10.0
    fasttomo.pars.exposure_time = 11.0
    assert tomo_config.pars.exposure_time == 11.0


def test_basic_scan(
    session,
    esrf_writer_session,
    nexus_writer_service,
    beacon,
    mocker,
    lima_simulator,
    setup_common_tomo_config,
):
    """Setup a fasttomo scan in step mode

    Check that the basic scan record the expected data
    """
    fast_scan = beacon.get("fasttomo")
    tomo_detector = beacon.get("tomo_detector")
    mg = beacon.get("mg")
    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()

    tomo_detector.detector.acquisition.mode = "SINGLE"
    fast_scan.pars.scan_type = "STEP"
    fast_scan.pars.flat_n = 1  # speed up a bit the test
    fast_scan.pars.energy = 1
    fast_scan.pars.dark_at_start = False
    fast_scan.basic_scan(0, 45, 4, 0.1)

    technique = fast_scan._sequence.scan_info["technique"]
    assert technique["subscans"]["scan0"]["type"] == "tomo:flat"
    assert technique["subscans"]["scan1"]["type"] == "tomo:step"
    assert technique["subscans"]["scan2"]["type"] == "tomo:return_ref"
    assert technique["optic"]["optics_pixel_size"] == 0.1

    assert len(fast_scan._scans) == 3
    assert "flat" in fast_scan._scans[0].name
    assert "ascan" in fast_scan._scans[1].name
    assert "static" in fast_scan._scans[2].name

    ascan = fast_scan._scans[1]
    assert len(ascan.get_data()["axis:srot"]) == 5
    assert ascan.get_data()["axis:srot"][0] == 0
    assert ascan.get_data()["axis:srot"][-1] == 45
    view = ascan.get_data()["lima_simulator:image"]
    assert len(view) == 5
    filename = fast_scan._sequence.scan_info["filename"]
    with h5py_utils.File(filename) as h5:
        h5["2.1"][f"instrument/{tomo_detector.detector.name}/acq_parameters/acq_mode"][
            ()
        ].decode() == "SINGLE"
        h5["3.1"][f"instrument/{tomo_detector.detector.name}/acq_parameters/acq_mode"][
            ()
        ].decode() == "SINGLE"
        h5["4.1"][f"instrument/{tomo_detector.detector.name}/acq_parameters/acq_mode"][
            ()
        ].decode() == "SINGLE"


def test_basic_scan_with_extra_counters(
    session,
    esrf_writer_session,
    nexus_writer_service,
    beacon,
    mocker,
    lima_simulator,
    setup_common_tomo_config,
):
    """Setup a fasttomo scan in step mode with one extra counter (simulation counter)

    Check that the basic scan record the expected data
    """
    fast_scan = beacon.get("fasttomo")
    tomo_detector = beacon.get("tomo_detector")
    sim_ct_1 = beacon.get("sim_ct_1")
    mg = beacon.get("mg")
    mg.add(tomo_detector.detector.image)
    mg.add(sim_ct_1)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()

    fast_scan.pars.scan_type = "STEP"
    fast_scan.pars.flat_n = 1  # speed up a bit the test
    fast_scan.pars.dark_at_start = True
    fast_scan.pars.dark_n = 1
    fast_scan.basic_scan(0, 45, 4, 0.1)

    technique = fast_scan._sequence.scan_info["technique"]
    assert technique["subscans"]["scan0"]["type"] == "tomo:dark"
    assert technique["subscans"]["scan1"]["type"] == "tomo:flat"
    assert technique["subscans"]["scan2"]["type"] == "tomo:step"
    assert technique["subscans"]["scan3"]["type"] == "tomo:return_ref"

    assert len(fast_scan._scans) == 4
    assert "dark" in fast_scan._scans[0].name
    assert "flat" in fast_scan._scans[1].name
    assert "ascan" in fast_scan._scans[2].name
    assert "static" in fast_scan._scans[3].name

    dark_scan = fast_scan._scans[0]
    assert len(dark_scan.get_data()["*:sim_ct_1"]) == 1
    flat_scan = fast_scan._scans[1]
    assert len(flat_scan.get_data()["*:sim_ct_1"]) == 1
    return_scan = fast_scan._scans[3]
    assert len(return_scan.get_data()["*:sim_ct_1"]) == 1

    ascan = fast_scan._scans[2]
    assert len(ascan.get_data()["axis:srot"]) == 5
    assert ascan.get_data()["axis:srot"][0] == 0
    assert ascan.get_data()["axis:srot"][-1] == 45
    view = ascan.get_data()["lima_simulator:image"]
    assert len(view) == 5


def test_basic_scan_with_detector_in_accumulation(
    session,
    esrf_writer_session,
    nexus_writer_service,
    beacon,
    mocker,
    lima_simulator,
    setup_common_tomo_config,
):
    """Setup a fasttomo scan in step mode with one extra counter (simulation counter)

    Check that the basic scan record the expected data
    """
    fast_scan = beacon.get("fasttomo")
    tomo_detector = beacon.get("tomo_detector")
    mg = beacon.get("mg")
    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()

    tomo_detector.detector.acquisition.mode = "ACCUMULATION"
    fast_scan.pars.scan_type = "STEP"
    fast_scan.pars.flat_n = 1  # speed up a bit the test
    fast_scan.pars.dark_at_start = True
    fast_scan.pars.dark_n = 1
    fast_scan.basic_scan(0, 45, 4, 0.1)

    technique = fast_scan._sequence.scan_info["technique"]
    assert technique["subscans"]["scan0"]["type"] == "tomo:dark"
    assert technique["subscans"]["scan1"]["type"] == "tomo:flat"
    assert technique["subscans"]["scan2"]["type"] == "tomo:step"
    assert technique["subscans"]["scan3"]["type"] == "tomo:return_ref"

    assert tomo_detector.detector.acquisition.mode == "ACCUMULATION"

    assert len(fast_scan._scans) == 4
    assert "dark" in fast_scan._scans[0].name
    assert "flat" in fast_scan._scans[1].name
    assert "ascan" in fast_scan._scans[2].name
    assert "static" in fast_scan._scans[3].name

    ascan = fast_scan._scans[2]
    assert len(ascan.get_data()["axis:srot"]) == 5
    assert ascan.get_data()["axis:srot"][0] == 0
    assert ascan.get_data()["axis:srot"][-1] == 45
    view = ascan.get_data()["lima_simulator:image"]
    assert len(view) == 5
    filename = fast_scan._sequence.scan_info["filename"]
    with h5py_utils.File(filename) as h5:
        assert (
            h5["2.1"][
                f"instrument/{tomo_detector.detector.name}/acq_parameters/acq_mode"
            ][()].decode()
            == "ACCUMULATION"
        )
        assert (
            h5["3.1"][
                f"instrument/{tomo_detector.detector.name}/acq_parameters/acq_mode"
            ][()].decode()
            == "ACCUMULATION"
        )
        assert (
            h5["4.1"][
                f"instrument/{tomo_detector.detector.name}/acq_parameters/acq_mode"
            ][()].decode()
            == "ACCUMULATION"
        )
        assert (
            h5["5.1"][
                f"instrument/{tomo_detector.detector.name}/acq_parameters/acq_mode"
            ][()].decode()
            == "ACCUMULATION"
        )


def test_activate_sinogram(
    beacon,
    esrf_writer_session,
    nexus_writer_service,
    mocker,
    lima_simulator,
    lima_simulator_small,
    setup_common_tomo_config,
):
    """Setup a fasttomo scan in step mode with a sino profile roi

    Check that the basic scan record the expected data
    """
    fast_scan = beacon.get("fasttomo")
    tomo_detector = beacon.get("tomo_detector")
    mg = beacon.get("mgsino")
    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()

    tomo_detector.detector.acquisition.mode = "SINGLE"
    fast_scan.pars.scan_type = "STEP"
    fast_scan.pars.activate_sinogram = True
    fast_scan.pars.dark_at_start = True
    fast_scan.pars.flat_at_start = True
    fast_scan.pars.flat_n = 1  # speed up a bit the test
    fast_scan.pars.dark_n = 1  # speed up a bit the test
    fast_scan.pars.energy = 1
    fast_scan.basic_scan(0, 45, 4, 0.1)

    nxw_test_utils.wait_scan_data_finished(
        scans=[fast_scan._sequence], writer=nexus_writer_service
    )
    lima_utils.wait_end_of_saving(lima_simulator)

    assert len(fast_scan._scans) == 4
    assert "dark" in fast_scan._scans[0].name
    assert "flat" in fast_scan._scans[1].name
    assert "ascan" in fast_scan._scans[2].name

    sinogram_data = fast_scan._sequence.scan.get_data()["sinogram"]
    assert len(sinogram_data) == lima_simulator.image.width * 5


@pytest.fixture
def basic_scan_with_sinogram(
    beacon,
    esrf_writer_session,
    nexus_writer_service,
    mocker,
    lima_simulator,
    lima_simulator_small,
    lima_simulator_with_sino_roi,
    setup_common_tomo_config,
):
    """Run a scan containing a sinogram"""
    fast_scan = beacon.get("fasttomo")
    tomo_config = beacon.get("tomo_config")
    tomo_detector = beacon.get("tomo_detector")
    tomo_config.sample_stage.source_distance = 40000
    tomo_config.detectors.source_distance = 40000
    mg = beacon.get("mg")
    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()

    fast_scan.pars.scan_type = "STEP"
    fast_scan.pars.activate_sinogram = True
    fast_scan.pars.dark_at_start = True
    fast_scan.pars.flat_at_start = True
    fast_scan.pars.flat_n = 1  # speed up a bit the test
    fast_scan.pars.dark_n = 1  # speed up a bit the test
    fast_scan.pars.energy = 1
    fast_scan.basic_scan(0, 45, 4, 0.1)

    nxw_test_utils.wait_scan_data_finished(
        scans=[fast_scan._sequence], writer=nexus_writer_service
    )
    lima_utils.wait_end_of_saving(lima_simulator)

    return fast_scan


def test_basic_scan_with_sinogram(basic_scan_with_sinogram, lima_simulator):
    """Setup a fasttomo scan in step mode with a sino profile roi

    Check that the basic scan record the expected data
    """
    fast_scan = basic_scan_with_sinogram

    assert len(fast_scan._scans) == 4
    assert "dark" in fast_scan._scans[0].name
    assert "flat" in fast_scan._scans[1].name
    assert "ascan" in fast_scan._scans[2].name
    assert "static" in fast_scan._scans[3].name
    assert fast_scan._sequence.scan_info["plots"][0]["items"][0]["value"] == "sinogram"

    sinogram_data = fast_scan._sequence.scan.get_data()["sinogram"]
    assert len(sinogram_data) == lima_simulator.image.width * 5


def test_sinogram_reader_from_basic_scan(exportscan_service, basic_scan_with_sinogram):
    """Setup a fasttomo scan in step mode with a sino profile roi

    Check that the result is readable by the sinogram reader
    """
    fast_scan = basic_scan_with_sinogram
    sinogram_data = fast_scan._sequence.scan.get_data()["sinogram"]

    filename = fast_scan._sequence.scan_info["filename"]
    with h5py_utils.File(filename) as h5:
        reader = sinogram_reader.SinogramReader(h5, "1.1")
        assert h5["2.1/technique/image_key"][()] == 2
        assert h5["3.1/technique/image_key"][()] == 1
        assert h5["4.1/technique/image_key"][()] == 0
        assert h5["5.1/technique/image_key"][()] == -1
    assert len(reader.translation) == len(reader.rotation) == len(reader.sinogram)
    numpy.testing.assert_almost_equal(sinogram_data, reader.sinogram)
    exportscan_service.export("fulltomo", fast_scan._sequence)


def test_nxtomomill(nxtomomill_service, exportscan_service, basic_scan_with_sinogram):
    fast_scan = basic_scan_with_sinogram
    nxtomomill_service.convert(fast_scan._sequence)


def test_techique_reader(basic_scan_with_sinogram):
    fast_scan = basic_scan_with_sinogram
    filename = fast_scan._sequence.scan_info["filename"]
    with h5py_utils.File(filename) as h5:
        reader = technique_model_reader.TechniqueModelReader(h5, "1.1")
    assert reader.x_axis == (0.0, "mm")


def test_basic_scan_with_unvalid_frames_per_file(
    session,
    esrf_writer_session,
    nexus_writer_service,
    beacon,
    mocker,
    lima_simulator,
    setup_common_tomo_config,
):
    """Setup a fasttomo scan in step mode with one extra counter (simulation counter)

    Check that the basic scan record the expected data
    """
    fast_scan = beacon.get("fasttomo")
    tomo_detector = beacon.get("tomo_detector")
    mg = beacon.get("mg")
    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()

    fast_scan.pars.scan_type = "STEP"
    fast_scan.pars.flat_n = 1  # speed up a bit the test
    fast_scan.pars.dark_at_start = True
    fast_scan.pars.dark_n = 1
    tomo_detector.detector.saving.frames_per_file = 2
    tomo_detector.detector.saving.mode = 2
    fast_scan.basic_scan(0, 45, 4499, 0.1, run=False)

    assert tomo_detector.detector.saving.frames_per_file == int(4500 / 1024) + 1


def test_fulltomo_rand(
    session,
    esrf_writer_session,
    nexus_writer_service,
    beacon,
    mocker,
    lima_simulator,
    setup_common_tomo_config,
):
    """Setup a fasttomo scan in step mode

    Check that the basic scan record the expected data
    """
    fast_scan = beacon.get("test_fulltomo_rand")
    tomo_config = beacon.get("tomo_config")
    tomo_detector = beacon.get("tomo_detector")
    tomo_config.detectors.active_detector = tomo_detector

    tomo_detector.sample_pixel_size_mode = "user"
    tomo_detector.user_sample_pixel_size = 10

    mg = beacon.get("mg")
    tomo_detector.detector.image.roi = 0, 0, 4, 4

    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()

    tomo_detector.detector.acquisition.mode = "SINGLE"
    fast_scan.pars.scan_type = "STEP"
    fast_scan.pars.flat_n = 1  # speed up a bit the test
    fast_scan.pars.energy = 1
    fast_scan.pars.dark_at_start = False
    fast_scan.pars.flat_at_start = False
    fast_scan.pars.images_on_return = False
    fast_scan.pars.rand_disp_y = 1
    fast_scan.pars.rand_disp_z = 2
    nbpoints = 10
    fast_scan.basic_scan(0, 45, nbpoints - 1, 0.1)

    technique = fast_scan._sequence.scan_info["technique"]
    assert technique["subscans"]["scan0"]["type"] == "tomo:step"

    assert len(fast_scan._scans) == 1
    assert "ascan" in fast_scan._scans[0].name

    ascan = fast_scan._scans[0]
    assert ascan.scan_info["technique"]["proj"]["rand_disp_y"] == 1
    assert ascan.scan_info["technique"]["proj"]["rand_disp_z"] == 2
    assert len(ascan.get_data()["axis:srot"]) == nbpoints
    assert ascan.get_data()["axis:srot"][0] == 0
    assert ascan.get_data()["axis:srot"][-1] == 45
    assert len(ascan.get_data()["axis:test_fulltomo_rand_spy"]) == nbpoints
    assert len(ascan.get_data()["axis:test_fulltomo_rand_spz"]) == nbpoints

    # Check the random dispension
    spy_data = ascan.get_data()["axis:test_fulltomo_rand_spy"][:]
    spz_data = ascan.get_data()["axis:test_fulltomo_rand_spz"][:]
    numpy.testing.assert_array_less(spy_data, 10.001)
    numpy.testing.assert_array_less(-10.001, spy_data)
    numpy.testing.assert_array_less(spz_data, 20.001)
    numpy.testing.assert_array_less(-20.001, spz_data)


def test_dark_n_flat_n_0(
    session,
    esrf_writer_session,
    nexus_writer_service,
    beacon,
    mocker,
    lima_simulator,
    setup_common_tomo_config,
):
    """Setup a fasttomo scan in step mode with dark_n and flat_n set to 0

    Check that the basic scan does not record flat and dark images
    """
    fast_scan = beacon.get("fasttomo")
    tomo_detector = beacon.get("tomo_detector")
    mg = beacon.get("mg")
    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()

    tomo_detector.detector.acquisition.mode = "SINGLE"
    fast_scan.pars.scan_type = "STEP"
    fast_scan.pars.flat_n = 0
    fast_scan.pars.dark_n = 0
    fast_scan.pars.energy = 1
    fast_scan.pars.dark_at_start = True
    fast_scan.pars.flat_at_start = True
    fast_scan.pars.dark_at_end = False
    fast_scan.pars.flat_at_end = False
    fast_scan.pars.images_on_return = False

    with pytest.raises(ValueError) as exc_info:
        fast_scan.basic_scan(0, 45, 4, 0.1)


def test_fulltomo_estimation_time(
    session,
    esrf_writer_session,
    nexus_writer_service,
    beacon,
    mocker,
    lima_simulator,
    setup_common_tomo_config,
):
    fast_scan = beacon.get("fasttomo")
    tomo_detector = beacon.get("tomo_detector")
    mg = beacon.get("mg")
    mg.add(tomo_detector.detector.image)
    mg.enable(tomo_detector.detector.image.fullname)
    mg.set_active()

    tomo_detector.detector.acquisition.mode = "SINGLE"
    fast_scan.pars.scan_type = "STEP"
    fast_scan.pars.flat_n = 21
    fast_scan.pars.dark_n = 21
    fast_scan.pars.dark_at_start = True
    fast_scan.pars.flat_at_start = True
    fast_scan.pars.dark_at_end = False
    fast_scan.pars.flat_at_end = False
    fast_scan.pars.images_on_return = True

    assert fast_scan.estimation_time() == pytest.approx(536, abs=10)
