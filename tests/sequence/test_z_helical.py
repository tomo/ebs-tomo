def test_pars(beacon):
    """Test that the synchronization of pars are right"""
    tomo_config = beacon.get("tomo_config")
    z_helical = beacon.get("z_helical")

    # tomo_config pars are shared
    tomo_config.pars.exposure_time = 10.0
    assert z_helical.pars.exposure_time == 10.0
    z_helical.pars.exposure_time = 11.0
    assert tomo_config.pars.exposure_time == 11.0

    # specialized pars are reachable
    assert z_helical.pars.acc_margin == 0.0
    z_helical.pars.acc_margin = 1.0
