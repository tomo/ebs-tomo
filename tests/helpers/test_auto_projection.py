def test_exposure_time_sync_at_init(beacon):
    tomo_config = beacon.get("tomo_config")
    auto_projection = tomo_config.auto_projection
    assert auto_projection.exposure_time == tomo_config.pars.exposure_time


def test_exposure_time_sync_with_pars(beacon):
    tomo_config = beacon.get("tomo_config")
    auto_projection = tomo_config.auto_projection
    tomo_config.pars.exposure_time = 10
    assert auto_projection.exposure_time == 10


def test_exposure_time_update_pars(beacon):
    tomo_config = beacon.get("tomo_config")
    auto_projection = tomo_config.auto_projection
    auto_projection.exposure_time = 10
    assert tomo_config.pars.exposure_time == 10


def _test_auto_projection__get_by_name(session, beacon, mocker):
    """This functionality is not available for now"""
    tomoconfig = beacon.get("tomo_config")
    autoproj = tomoconfig.auto_projection
    autoproj2 = beacon.get(autoproj.name)
    assert autoproj is autoproj2
