from __future__ import annotations
import numpy
from tomo.helpers.detector_helper import normalize_image


def test_normal():
    data = numpy.array([[1, 2], [3, 4]])
    expected = numpy.array([[1, 2], [3, 4]])
    result = normalize_image(data, ("-z", "y"))
    numpy.testing.assert_allclose(result, expected)


def test_flipy():
    data = numpy.array([[1, 2], [3, 4]])
    expected = numpy.array([[2, 1], [4, 3]])
    result = normalize_image(data, ("-z", "-y"))
    numpy.testing.assert_allclose(result, expected)


def test_flipz():
    data = numpy.array([[1, 2], [3, 4]])
    expected = numpy.array([[3, 4], [1, 2]])
    result = normalize_image(data, ("z", "y"))
    numpy.testing.assert_allclose(result, expected)


def test_rot():
    data = numpy.array([[1, 2], [3, 4]])
    expected = numpy.array([[1, 3], [2, 4]])
    result = normalize_image(data, ("y", "-z"))
    numpy.testing.assert_allclose(result, expected)
