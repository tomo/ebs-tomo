from tomo.helpers import axis_utils


def test_clockwise_axis(beacon):
    axis = beacon.get("axis_cw")
    assert axis_utils.is_user_clockwise(axis) is True
    axis.sign = -1
    assert axis_utils.is_user_clockwise(axis) is False


def test_anticlockwise_axis(beacon):
    axis = beacon.get("axis_acw")
    assert axis_utils.is_user_clockwise(axis) is False
    axis.sign = -1
    assert axis_utils.is_user_clockwise(axis) is True


def test_unknown_axis(beacon):
    axis = beacon.get("sx")
    assert axis_utils.is_user_clockwise(axis) is True
    axis.sign = -1
    assert axis_utils.is_user_clockwise(axis) is False
