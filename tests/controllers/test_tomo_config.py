import gevent
import pytest
from bliss.common import event
from ..helpers.event_utils import EventListener


def test_creation(beacon):
    beacon.get("tomo_config")


def test_wrong_keys(beacon):
    with pytest.raises(RuntimeError) as exc_info:
        beacon.get("tomo_config_wrong_key")
    assert "transaltion_z" in exc_info.value.initial_cause.args[0]


@pytest.mark.parametrize(
    "data",
    [
        (1, 9, 0.01),
        (22, 2, 0.091666),
        (None, 2, None),
    ],
)
def test_sample_distance(beacon, lima_simulator_tango_server, data):
    source_sample_distance, detectors_distance, expected_px = data
    tomo_config = beacon.get("tomo_config")
    tomo_detector = beacon.get("tomo_detector")
    tomo_detector.tomo_detectors.source_distance = source_sample_distance
    tomo_detectors_axis = beacon.get("tomo_detectors_axis")

    tomo_config.sample_stage.source_distance = source_sample_distance
    tomo_detectors_axis.move(detectors_distance)
    ps = tomo_detector.auto_sample_pixel_size
    assert ps == pytest.approx(expected_px, abs=1e-3)


def test_moving(beacon):
    listener = EventListener()
    tomoconfig = beacon.get("tomo_config")

    try:
        event.connect(tomoconfig, "is_moving", listener)
        assert listener.values == []
        tomoconfig.rotation_axis.rmove(1, wait=False)
        # Wait for event received
        gevent.sleep(1)
    finally:
        event.disconnect(tomoconfig, "is_moving", listener)

    assert listener.event_count == 2
    assert listener.values == [True, False]


def test_auto_projection__enabled(session, beacon, mocker):
    tomoconfig = beacon.get("tomo_config")
    countrunner_mock = mocker.patch("tomo.scan.count_runner.CountRunner.__call__")
    tomoconfig.detectors.active_detector = tomoconfig.detectors.detectors[0]

    tomoconfig.set_active()
    tomoconfig.auto_projection.enabled = True
    try:
        tomoconfig.rotation_axis.rmove(1)
    finally:
        tomoconfig.auto_projection.enabled = False
    countrunner_mock.assert_called_once()


def test_auto_projection__not_active(session, beacon, mocker):
    tomoconfig = beacon.get("tomo_config")
    countrunner_mock = mocker.patch("tomo.scan.count_runner.CountRunner.__call__")
    tomoconfig.detectors.active_detector = tomoconfig.detectors.detectors[0]

    tomoconfig.auto_projection.enabled = True
    try:
        tomoconfig.rotation_axis.rmove(1)
    finally:
        tomoconfig.auto_projection.enabled = False
    countrunner_mock.assert_not_called()


def test_auto_projection__enabled_no_detector(session, beacon, mocker):
    tomoconfig = beacon.get("tomo_config")
    countrunner_mock = mocker.patch("tomo.scan.count_runner.CountRunner.__call__")
    endofmove_spy = mocker.spy(tomoconfig.auto_projection, "_end_of_move")

    tomoconfig.set_active()
    tomoconfig.detectors.active_detector = None

    tomoconfig.auto_projection.enabled = True
    try:
        tomoconfig.rotation_axis.rmove(1)
    finally:
        tomoconfig.auto_projection.enabled = False
    endofmove_spy.assert_called_once()
    countrunner_mock.assert_not_called()


def test_auto_projection__disabled(session, beacon, mocker):
    tomoconfig = beacon.get("tomo_config")
    countrunner_mock = mocker.patch("tomo.scan.count_runner.CountRunner.__call__")
    endofmove_spy = mocker.spy(tomoconfig.auto_projection, "_end_of_move")

    tomoconfig.set_active()
    tomoconfig.detectors.active_detector = tomoconfig.detectors.detectors[0]
    tomoconfig.auto_projection.enabled = False
    tomoconfig.rotation_axis.rmove(1)
    endofmove_spy.assert_called_once()
    countrunner_mock.assert_not_called()


def test_auto_projection__inhibited(session, beacon, mocker):
    tomoconfig = beacon.get("tomo_config")
    countrunner_mock = mocker.patch("tomo.scan.count_runner.CountRunner.__call__")
    tomoconfig.detectors.active_detector = tomoconfig.detectors.detectors[0]
    endofmove_spy = mocker.spy(tomoconfig.auto_projection, "_end_of_move")

    tomoconfig.set_active()
    tomoconfig.auto_projection.enabled = True
    try:
        with tomoconfig.auto_projection.inhibit():
            tomoconfig.rotation_axis.rmove(1)
    finally:
        tomoconfig.auto_projection.enabled = False
    endofmove_spy.assert_called_once()
    countrunner_mock.assert_not_called()


def test_source_sample_distance_changed(beacon):
    """Move the x_axis.

    Make sure a signal source_sample_distance was emitted and
    source_sample_distance property is up to date.
    """
    tomo_config = beacon.get("tomo_config")
    tomo_config.sample_stage.source_distance = 40000
    results = []

    def on_changed(value, signal=None, sender=None):
        nonlocal results
        results.append(value)

    event.connect(tomo_config.sample_stage, "source_distance", on_changed)

    gevent.sleep(0.1)
    tomo_config.x_axis.move(1)

    assert tomo_config.sample_stage.source_distance == pytest.approx(40001, abs=1e-3)
    assert len(results) > 0
    assert results[-1] == pytest.approx(40001, abs=1e-3), results
