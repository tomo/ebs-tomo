import pytest
from tomo.controllers.pusher import PusherState
from tomo.helpers.locking_helper import lock, unlock


def test_creation(beacon):
    beacon.get("test_pusher")


def test_move_in(beacon):
    pusher = beacon.get("test_pusher")
    axis = beacon.get("test_pusher_axis")
    axis.controller._set_home_pos(axis, 90.0)

    pusher.move_in()
    assert pusher.state == PusherState.IN_TOUCH
    assert axis.position > 45.0


def test_move_out(beacon):
    pusher = beacon.get("test_pusher")
    axis = beacon.get("test_pusher_axis")
    axis.controller._set_home_pos(axis, 90.0)

    pusher.move_in()
    pusher.move_out()
    assert pusher.state == PusherState.RETRACTED
    assert axis.position < 45.0


def test_locked_device(beacon):
    pusher = beacon.get("test_pusher_with_lock")
    axis = beacon.get("test_pusher_axis")
    axis.controller._set_home_pos(axis, 90.0)
    srot = beacon.get("test_pusher_srot")

    lock(srot, timeout=1)
    unlock(srot)

    pusher.move_in()

    # The device is own by the pusher
    with pytest.raises(Exception) as exc:
        lock(srot, timeout=1)
    assert "pusher" in str(exc.value)

    pusher.move_out()

    lock(srot, timeout=1)
    unlock(srot)
