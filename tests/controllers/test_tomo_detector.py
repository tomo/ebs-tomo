import pytest
import gevent
from bliss.common import event
from ..helpers.event_utils import EventListener
from bliss.testutils.rpc_controller import controller_in_another_process


def test_creation(beacon, lima_simulator_tango_server):
    detector = beacon.get("tomo_detector")
    tomo_config = beacon.get("tomo_config")
    tomo_config.sample_stage.source_distance = 40000
    detector.tomo_detectors.source_distance = 40000
    assert detector.detector is not None
    assert detector.optic is not None
    assert detector.optic.magnification == 10
    gevent.sleep(1)
    assert detector.sample_pixel_size == 0.1
    assert detector.field_of_view == pytest.approx(0.1024, rel=0.01)


def test_creation_without_optic(beacon, lima_simulator_tango_server):
    detector = beacon.get("tomo_detector_without_optic")
    tomo_config = beacon.get("tomo_config")
    detector._set_tomo_detectors(tomo_config.detectors)
    tomo_config.sample_stage.source_distance = 40000
    detector.tomo_detectors.source_distance = 40000
    assert detector.detector is not None
    assert detector.optic is None
    assert detector.sample_pixel_size == 1.0


def test_updated_magnification(beacon, lima_simulator_tango_server):
    detector = beacon.get("tomo_detector")
    tomo_config = beacon.get("tomo_config")
    tomo_config.sample_stage.source_distance = 40000
    detector.tomo_detectors.source_distance = 40000
    detector.optic.magnification = 100
    gevent.sleep(1)
    assert detector.sample_pixel_size == 0.01


def test_sample_pixel_size_event(beacon, lima_simulator_tango_server):
    detector = beacon.get("tomo_detector")
    tomo_config = beacon.get("tomo_config")
    tomo_config.sample_stage.source_distance = 40000
    detector.tomo_detectors.source_distance = 40000
    listener = EventListener()
    try:
        event.connect(detector, "sample_pixel_size", listener)
        detector.optic.magnification = 100
        # Wait for event received
        gevent.sleep(1)
    finally:
        event.disconnect(detector, "sample_pixel_size", listener)

    # No idea why this stuff is called more than 2 times sometimes (sounds to be  BLISS internal)
    assert listener.event_count >= 1
    assert listener.last_value == 0.01


def test_pixel_size_mode(beacon, lima_simulator_tango_server):
    """
    Check that switching from and to user/auto will properly update the sample
    pixel size.
    """
    detector = beacon.get("tomo_detector")
    detector.sample_pixel_size_mode = "user"
    assert detector.sample_pixel_size == detector.user_sample_pixel_size
    detector.user_sample_pixel_size = 10
    assert detector.sample_pixel_size == detector.user_sample_pixel_size
    detector.sample_pixel_size_mode = "auto"
    assert detector.sample_pixel_size == detector.auto_sample_pixel_size


def test_user_pixel_size_and_binning(beacon, lima_simulator_tango_server, mocker):
    """
    Check that switching from and to user/auto will properly update the sample
    pixel size.
    """
    detector = beacon.get("tomo_detector")
    tomo_config = beacon.get("tomo_config")
    assert tomo_config is not None
    detector.detector.image.binning = 2, 2
    detector.user_unbinned_sample_pixel_size = 10
    gevent.sleep(0.1)
    assert detector.user_sample_pixel_size == 20
    detector.user_sample_pixel_size = 10
    gevent.sleep(0.1)
    assert detector.user_unbinned_sample_pixel_size == 5


def test_info(beacon, lima_simulator_tango_server):
    """Make sure it returns something"""
    detector = beacon.get("tomo_detector")
    tomo_config = beacon.get("tomo_config")
    assert tomo_config is not None
    result = detector.__info__()
    assert isinstance(result, str)


def test_pixel_size__sample_detector_distance_changed(
    beacon, lima_simulator_tango_server
):
    detector = beacon.get("tomo_detector")
    tomo_config = beacon.get("tomo_config")
    tomo_config.sample_stage.source_distance = 40000
    detector.tomo_detectors.source_distance = 40000
    tomo_config.detectors.detector_axis.move(tomo_config.sample_stage.source_distance)
    assert detector.auto_sample_pixel_size == 0.05


def test_tomo_detector_with_detector_axis(beacon, lima_simulator_tango_server):
    detector = beacon.get("tomo_detector_with_detector_axis")
    tomo_config = beacon.get("tomo_config2")
    tomo_config.sample_stage.source_distance = 1000
    detector.source_distance = 1500
    detector.detector_axis.move(2500, wait=True)
    assert detector.source_distance == 4000
    assert detector.sample_detector_distance == 3000, str(
        detector._auto_sample_pixel_size_geometry
    )
    assert detector.auto_sample_pixel_size == 0.025


def test_pixel_size__source_sample_distance_changed(
    beacon, lima_simulator_tango_server
):
    detector = beacon.get("tomo_detector")
    tomo_config = beacon.get("tomo_config")
    tomo_config.sample_stage.source_distance = 30000
    tomo_config.detectors.source_distance = 30000
    tomo_config.detectors.detector_axis.move(10000, wait=True)
    gevent.sleep(0.1)
    assert detector.auto_sample_pixel_size == pytest.approx(0.075)


@pytest.fixture
def tomo_detector_in_another_process(beacon, beacon_host_port):
    beacon_host, beacon_port = beacon_host_port
    with controller_in_another_process(beacon_host, beacon_port, "tomo_detector") as c:
        yield c


def test_optic_change(
    beacon, lima_simulator_tango_server, tomo_detector_in_another_process
):
    """
    Get a tomo detector and change the optic.

    Make sure the pixel size is recomputed and another session
    is aware of the change.
    """
    detector = beacon.get("tomo_detector")
    remote_detector = tomo_detector_in_another_process
    tomo_config = beacon.get("tomo_config")
    tomo_config.sample_stage.source_distance = 1000
    detector.source_distance = 1500
    assert detector.sample_pixel_size == pytest.approx(0.0666, abs=0.0001)
    assert remote_detector is not None
    assert remote_detector._test_optic_name == "tomo_detector__optic"
    assert remote_detector.sample_pixel_size == pytest.approx(0.0666, abs=0.0001)

    # Change the state
    optic5 = beacon.get("tomo_detector__optic5")
    detector.optic = optic5

    # Change the state
    assert detector.sample_pixel_size == pytest.approx(0.1333, abs=0.0001)
    assert remote_detector._test_optic_name == "tomo_detector__optic5"
    assert remote_detector.sample_pixel_size == pytest.approx(0.1333, abs=0.0001)


def test_data_axes(beacon, lima_simulator_tango_server):
    detector = beacon.get("tomo_detector")
    tomo_config = beacon.get("tomo_config")
    optic = detector.optic
    lima = detector.detector
    assert tomo_config

    # the optic is flipped
    # that's the case when scientist dont want extra processing in the detector
    assert optic.image_flipping[0]
    assert detector.get_data_axes() == ("-z", "-y")

    # compensate the optic flip with lima processing
    # that's usually what scientist does
    lima.image.flip = [False, True]
    assert detector.get_data_axes() == ("-z", "y")

    # check z flip
    lima.image.flip = [True, True]
    assert detector.get_data_axes() == ("z", "y")
