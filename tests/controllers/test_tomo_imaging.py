import gevent


def test_creation(beacon, lima_simulator_tango_server):
    imaging = beacon.get("tomo_imaging")
    assert isinstance(imaging.__info__(), str)


def test_change_update_on_move(beacon, lima_simulator_tango_server, mocker):
    imaging = beacon.get("tomo_imaging")
    tomoconfig = imaging.tomoconfig

    imaging.update_on_move = True
    gevent.sleep(1)
    assert tomoconfig.auto_projection.enabled is True

    imaging.update_on_move = False
    gevent.sleep(1)
    assert tomoconfig.auto_projection.enabled is False


def test_take_proj(session, beacon, lima_simulator_tango_server):
    imaging = beacon.get("tomo_imaging")
    tomoconfig = imaging.tomoconfig
    tomoconfig.detectors.active_detector = tomoconfig.detectors.detectors[0]
    imaging.exposure_time = 0.5
    gevent.sleep(1)

    scan_info = None

    def scan_hook(s):
        nonlocal scan_info
        scan_info = s.scan_info

    imaging.take_proj(scan_hook=scan_hook)

    assert "technique" in scan_info
    assert "proj" in scan_info["technique"]
    assert scan_info["technique"]["proj"]["proj_n"] == 1
    assert scan_info["technique"]["proj"]["exposure_time"] == 0.5


def test_take_dark(session, beacon, lima_simulator_tango_server):
    imaging = beacon.get("tomo_imaging")
    tomoconfig = imaging.tomoconfig
    tomoconfig.detectors.active_detector = tomoconfig.detectors.detectors[0]
    imaging.exposure_time = 0.5
    gevent.sleep(1)

    scan_info = None

    def scan_hook(s):
        nonlocal scan_info
        scan_info = s.scan_info

    imaging.take_dark(scan_hook=scan_hook)

    assert "technique" in scan_info
    assert "dark" in scan_info["technique"]
    assert scan_info["technique"]["dark"]["dark_n"] == 1
    assert scan_info["technique"]["dark"]["exposure_time"] == 0.5


def test_take_flat(
    session, beacon, lima_simulator_tango_server, setup_common_tomo_config
):
    imaging = beacon.get("tomo_imaging")
    tomoconfig = imaging.tomoconfig
    tomoconfig.detectors.active_detector = tomoconfig.detectors.detectors[0]
    imaging.exposure_time = 0.5
    gevent.sleep(1)

    scan_info = None

    def scan_hook(s):
        nonlocal scan_info
        scan_info = s.scan_info

    imaging.take_flat(scan_hook=scan_hook)

    assert "technique" in scan_info
    assert "flat" in scan_info["technique"]
    assert scan_info["technique"]["flat"]["flat_n"] == 1
    assert scan_info["technique"]["flat"]["exposure_time"] == 0.5
