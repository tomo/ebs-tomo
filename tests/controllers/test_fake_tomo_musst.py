import gevent

from tomo.controllers.fake_musst import FakeMusst
from tomo.controllers.fake_tomo_musst import FscanProgram
from tomo.controllers.fake_musst import FakeMusstChannel


def test_fscan(mocker):
    """
    Launch a program using putget to set a var

    Test that the ATRIG was called according to this var
    """
    mock1 = mocker.Mock()

    countermock = mocker.Mock(spec_set=FakeMusstChannel)
    countermock.value.return_value = -2

    motormock = mocker.Mock(spec_set=FakeMusstChannel)
    motormock.value.return_value = -1

    def move_motor():
        for n in range(0, 11):
            motormock.value.return_value = n
            gevent.sleep(0.05)

    config = {
        "atrig": [mock1],
        "channels": [
            {"channel": 1, "obj": motormock, "type": "encoder"},
            {"channel": 2, "obj": countermock, "type": "cnt"},
        ],
    }
    musst = FakeMusst("m", config)
    musst.register_program("foo.mprg", FscanProgram)
    musst.upload_file("foo.mprg", "foo/foo")
    musst.putget("VAR SCANMODE 0")
    musst.putget("VAR TRIGSTART 0")
    musst.putget("VAR SCANDIR 1")
    musst.putget("VAR NPULSES 10")  # 0.5 / 0.05
    musst.putget("VAR ACCNB 1")
    delay = int(musst.get_timer_factor() * 0.05)  # 0.05 s
    musst.putget(f"VAR ACCTIME {delay}")

    gevent.spawn(move_motor)
    with gevent.Timeout(5):
        musst.run("FSCAN", wait=True)
    data = musst.get_data(3)
    assert data.shape[1] == 3
    assert data.shape[0] > 5
    assert data[0, 1] == 0
    assert data[-1, 1] == 10
    assert musst.STATE == FakeMusst.IDLE_STATE
