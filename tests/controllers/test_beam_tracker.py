import numpy
import pytest
import gevent


def test_creation(beacon):
    beacon.get("test_beam_tracker__tomo_config")


def test_correction(beacon):
    tomo_config = beacon.get("test_beam_tracker__tomo_config")
    beam_tracker = tomo_config.beam_tracker
    beam_tracker.sy_angle = numpy.pi * 0.25
    beam_tracker.sz_angle = numpy.pi * 0.125
    y, z = beam_tracker.compute_correction(0, 1)
    assert y == pytest.approx(1, abs=0.01)
    assert z == pytest.approx(0.414, abs=0.01)


def test_detector_center(beacon):
    """Make sure the detector center is updated after a change of
    the focal position or the beam correction"""
    tomo_config = beacon.get("test_beam_tracker__tomo_config")
    beam_tracker = tomo_config.beam_tracker
    sample_stage = tomo_config.sample_stage
    beam_tracker.sy_angle = numpy.pi * 0.25
    sample_stage.focal_position = 0, 0, 0
    assert sample_stage.detector_center[0] == pytest.approx(0, abs=0.01)
    sample_stage.x_axis.position = 1
    assert sample_stage.detector_center[0] == pytest.approx(1, abs=0.01)
    sample_stage.focal_position = -1, 0, 0
    gevent.sleep(0.5)
    assert sample_stage.detector_center[0] == pytest.approx(2, abs=0.01)
    beam_tracker.sy_angle = numpy.pi * 0.125
    assert sample_stage.detector_center[0] == pytest.approx(2 * 0.414, abs=0.01)
