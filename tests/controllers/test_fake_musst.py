import numpy
from tomo.controllers.fake_musst import FakeMusst
from tomo.controllers.fake_musst import FakeMusstProgram


class Prog(FakeMusstProgram):
    def _init(self):
        self.register_variable("NBTRIG", numpy.uint32)

    def PROG_P1(self):
        self.ATRIG()
        self.ATRIG()
        self.ATRIG()

    def PROG_P2(self):
        self.BTRIG(0)
        self.BTRIG(0)
        self.BTRIG(1)
        self.BTRIG(1)

    def PROG_P3(self):
        self.BTRIG()
        self.BTRIG()
        self.BTRIG()

    def PROG_P4(self):
        for _ in range(self.NBTRIG):
            self.ATRIG()


def test_musst_atrig(mocker):
    """
    Launch a program using ATRIG

    Test that devices was triggered with ATRIG
    """
    mock1 = mocker.Mock()
    mock2 = mocker.Mock()
    mock3 = mocker.Mock()

    config = {"atrig": [mock1], "btrig_up": [mock2], "btrig_down": [mock3]}
    musst = FakeMusst("m", config)
    musst.register_program("foo.mprg", Prog)
    musst.upload_file("foo.mprg", "foo/foo")
    musst.run("P1", wait=True)

    assert mock1.call_count == 3
    assert mock2.call_count == 0
    assert mock3.call_count == 0


def test_musst_btrig_up(mocker):
    """
    Launch a program using BTRIG creating a step UP

    Test that devices was triggered with BTRIG UP
    """
    mock1 = mocker.Mock()
    mock2 = mocker.Mock()
    mock3 = mocker.Mock()

    config = {"atrig": [mock1], "btrig_up": [mock2], "btrig_down": [mock3]}
    musst = FakeMusst("m", config)
    musst.register_program("foo.mprg", Prog)
    musst.upload_file("foo.mprg", "foo/foo")
    musst.run("P2", wait=True)

    assert mock1.call_count == 0
    assert mock2.call_count == 1
    assert mock3.call_count == 0


def test_musst_btrig_swap(mocker):
    """
    Launch a program using bare BTRIG

    Test that devices was triggered with BTRIG UP and DOWN
    """
    mock1 = mocker.Mock()
    mock2 = mocker.Mock()
    mock3 = mocker.Mock()

    config = {"atrig": [mock1], "btrig_up": [mock2], "btrig_down": [mock3]}
    musst = FakeMusst("m", config)
    musst.register_program("foo.mprg", Prog)
    musst.upload_file("foo.mprg", "foo/foo")
    musst.run("P3", wait=True)

    assert mock1.call_count == 0
    assert mock2.call_count == 2
    assert mock3.call_count == 1


def test_fixed_bit_arithmetic():
    """
    Create a program and subsdtract 1 from 0

    Test that there is an overflow in the result
    """
    prog = Prog(None)
    prog.NBTRIG -= 1
    assert prog.NBTRIG > 1
    assert prog.NBTRIG == 0xFFFFFFFF


def test_musst_set_var(mocker):
    """
    Launch a program using putget to set a var

    Test that the ATRIG was called according to this var
    """
    mock1 = mocker.Mock()

    config = {"atrig": [mock1]}
    musst = FakeMusst("m", config)
    musst.register_program("foo.mprg", Prog)
    musst.upload_file("foo.mprg", "foo/foo")
    musst.putget("VAR NBTRIG 3")
    musst.run("P4", wait=True)
    assert mock1.call_count == 3
