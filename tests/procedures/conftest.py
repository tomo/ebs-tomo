import pytest


@pytest.fixture
def no_text_block():
    """
    We have to be compatible with BLISS 2.0 and 2.1.

    So for now we have to use our own fixture.
    """
    try:
        from bliss.shell.pt import utils
    except ImportError:
        # BLISS < 2.1
        yield
        return

    if not hasattr(utils, "can_use_text_block"):
        # BLISS < 2.1
        yield
        return

    def always_false():
        return False

    old = utils.can_use_text_block
    utils.can_use_text_block = always_false
    yield
    utils.can_use_text_block = old
