import pytest
from tomo.scintillators import utils
from tomo.scintillators import optique_peter_ttss


def test_scintillator_correction():
    """Test a known geometry which was setup at BM05"""
    geometry = optique_peter_ttss.OptiquePeterTtss()

    tilt_corr_v_rad = 1.2e-03
    tilt_corr_h_rad = -2e-03

    results = geometry.tilt_hardware_correction(tilt_corr_v_rad, tilt_corr_h_rad)
    assert len(results) == 2

    corr1 = results[0].corrections
    assert corr1[0].role == "top_left"
    assert corr1[0].distance == pytest.approx(0.0795, abs=0.0001)
    assert corr1[1].role == "top_right"
    assert corr1[1].distance == pytest.approx(-0.0015, abs=0.0001)
    assert corr1[2].role == "bottom"
    assert corr1[2].distance == pytest.approx(-0.07797, abs=0.0001)

    corr2 = results[1].corrections
    assert corr2[0].role == "top_left"
    assert corr2[0].distance == pytest.approx(0.0, abs=0.0001)
    assert corr2[1].role == "top_right"
    assert corr2[1].distance == pytest.approx(-0.081, abs=0.0001)
    assert corr2[2].role == "bottom"
    assert corr2[2].distance == pytest.approx(-0.1575, abs=0.0001)

    print()
    utils.print_tilt_results(geometry, results)


@pytest.mark.parametrize(
    "test_input, expected",
    [
        ((0.0015, -0.0009), (0.27, -0.13, -0.14)),
        ((-0.0009, 0.0006), (-0.17, 0.07, 0.09)),
        ((0.001147, -0.0001323), (0.165218, -0.144587, -0.0206312)),
    ],
    ids=["align", "refine", "align2"],
)
def test_id16b_correction(test_input, expected):
    """Test few correction which was checked at ID16B to test regressions"""
    tilt_corr_v_rad = test_input[0]
    tilt_corr_h_rad = test_input[1]

    geometry = optique_peter_ttss.OptiquePeterTtss()
    results = geometry.tilt_hardware_correction(tilt_corr_v_rad, tilt_corr_h_rad)
    assert len(results) == 2

    screws = ["top_left", "top_right", "bottom"]
    correction_per_screws = {c.role: c for c in results[0].corrections}
    correction = [correction_per_screws[c].turns for c in screws]
    assert correction == pytest.approx(expected, abs=0.01)

    print()
    utils.print_tilt_results(geometry, results)
