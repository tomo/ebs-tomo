from tomo.scintillators import utils


def test_turns_clockwise():
    expected = utils.HumanReadableTurns(
        full_turns=1,
        half_turns=1,
        quarter_turns=1,
        eighth_turns=1,
        sixteenth_turns=1,
        orientation="clockwise",
    )
    assert utils.human_readable_turns(1.0 + 1 / 2 + 1 / 4 + 1 / 8 + 1 / 16) == expected


def test_turns_anticlockwise():
    expected = utils.HumanReadableTurns(
        full_turns=1,
        half_turns=1,
        quarter_turns=1,
        eighth_turns=1,
        sixteenth_turns=1,
        orientation="anticlockwise",
    )
    assert (
        utils.human_readable_turns(-(1.0 + 1 / 2 + 1 / 4 + 1 / 8 + 1 / 16)) == expected
    )


def test_no_turns():
    expected = utils.HumanReadableTurns(
        full_turns=0,
        half_turns=0,
        quarter_turns=0,
        eighth_turns=0,
        sixteenth_turns=0,
        orientation="",
    )
    assert utils.human_readable_turns(0.0) == expected
