import pytest
from tomo.optic.base_optic import OpticState


def test_creation(beacon):
    beacon.get("triplemic_optic")
    optic = beacon.get("triplemic_optic")
    assert optic.state == OpticState.READY
    assert optic.objective == 1
    assert optic.magnification == 5
    assert optic.available_magnifications == [5, 7.5, 10]


def test_magnification_set_user_magnification(beacon):
    optic = beacon.get("triplemic_optic")
    optic.magnification = 2
    assert optic.magnification == 2
    assert optic.objective == 1
    assert optic.available_magnifications == [2, 7.5, 10]

    optic.magnification = 3
    assert optic.magnification == 3
    assert optic.objective == 1
    assert optic.available_magnifications == [3, 7.5, 10]


def test_move_motor(beacon):
    optic = beacon.get("triplemic_optic")
    motor = beacon.get("triplemic_sel")
    motor.move(30)
    assert optic.objective == 3
    assert optic.magnification == 10
    assert optic.state == OpticState.READY


def test_move_objective(beacon):
    optic = beacon.get("triplemic_optic")
    motor = beacon.get("triplemic_sel")
    optic.objective = 3
    assert optic.objective == 3
    assert motor.position == 30
    assert optic.magnification == 10
    assert optic.state == OpticState.READY


def test_move_magnification(beacon):
    optic = beacon.get("triplemic_optic")
    motor = beacon.get("triplemic_sel")
    optic.move_magnification(10)
    assert optic.objective == 3
    assert motor.position == 30
    assert optic.magnification == 10
    assert optic.state == OpticState.READY


def test_move_wrong_pos(beacon):
    optic = beacon.get("triplemic_optic")
    motor = beacon.get("triplemic_sel")
    motor.move(-10)
    with pytest.raises(ValueError):
        optic.objective
    assert optic.magnification is None
    assert optic.state == OpticState.INVALID
