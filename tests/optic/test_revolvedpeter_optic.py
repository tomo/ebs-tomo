import pytest
import gevent
from bliss.common import event
from ..helpers.event_utils import EventListener
from tomo.optic.base_optic import OpticState
from tomo.optic.revolvedpeter_optic import RevolvedPeterOptic


@pytest.fixture
def factory(beacon):
    """
    Returns the tested optic by patching the Tango device proxy
    """

    def factory() -> RevolvedPeterOptic:
        optic = beacon.get("revolvedpeter_optic")
        return optic

    return factory


def test_creation(factory):
    """Check the state of the device at initialization"""
    optic = factory()
    assert optic._eyepiece_magnification == 2.0
    assert optic._objective_magnification is None
    assert optic.magnification == pytest.approx(2, abs=0.0001)
    assert optic.available_magnifications == []


def test_mounted_optics(factory):
    """Check the state of the device after manual mounting"""
    optic = factory()
    optic.set_mounted_objective(1, 10)
    optic.set_mounted_objective(2, 100)
    optic.set_mounted_objective(3, 250)
    assert optic.magnification == pytest.approx(20, abs=0.0001)
    assert optic.available_magnifications == [20, 200, 500]


def test_mounted_settings(factory):
    """Check that mounted objectives are restored if we reload the device"""
    optic = factory()
    optic.set_mounted_objective(1, 10)
    optic.set_mounted_objective(2, 100)
    optic.set_mounted_objective(3, 250)
    assert optic.available_magnifications == [20, 200, 500]
    id_optic = id(optic)
    optic = None

    optic2 = factory()
    assert optic2.available_magnifications == [20, 200, 500]
    assert id(optic2) != id_optic


def test_async_move(factory):
    """Check that objectives can be moved"""
    optic = factory()
    optic.set_mounted_objective(1, 10)
    optic.set_mounted_objective(2, 100)
    optic.set_mounted_objective(3, 250)
    optic.move_magnification(200, wait=False)
    assert optic.state == OpticState.MOVING
    optic.wait_move()
    assert optic.state == OpticState.READY
    assert optic.magnification == pytest.approx(200, abs=0.0001)


def test_magnification_event(factory):
    """Check that magnification can be listened"""
    optic = factory()
    optic.set_mounted_objective(1, 10)
    optic.set_mounted_objective(2, 100)
    optic.set_mounted_objective(3, 250)
    listener = EventListener()
    try:
        event.connect(optic, "magnification", listener)
        optic.move_magnification(200)
        # Wait for event received
        gevent.sleep(1)
    finally:
        event.disconnect(optic, "magnification", listener)

    # No idea why this stuff is called more than 2 times sometimes (sounds like it is BLISS internal)
    assert listener.event_count >= 1
    assert listener.last_value == pytest.approx(200, abs=0.0001)


def test_magnification_not_reachable(factory):
    """Check error when trying to move to an unavailable magnification"""
    optic = factory()
    optic.set_mounted_objective(1, 10)
    optic.set_mounted_objective(2, 100)
    optic.set_mounted_objective(3, 250)
    with pytest.raises(ValueError) as exc_info:
        optic.move_magnification(0.1)
    assert "0.1" in exc_info.value.args[0]
    assert "20" in exc_info.value.args[0]
