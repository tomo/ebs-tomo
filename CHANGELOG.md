# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.7.0]

### Added

- Added pusher device
- Added air bearing device
- Added locking for exclusive use of the srot and Lima detector device
- Added `lslock`, `force_unlock` to allow to manage manually the locks
- Added geometry for scintillator correction and few supported models
- Added procedure for `focus` (the `align` object still provide the same feature)
- Added `method` to the procedure `align_cor`
- Added a set of methods to `align_cor` which only estimate the center of rotation
- Added an optional pusher to the tomo sample stage
- Added an optional air bearing to the tomo sample stage
- Added support to Lima `acq_timeout` for a part of the tiling scans
- Added `data_axes` tomo_detector metadata, which only support flips for now
- Added deprecation warning for old API. Such API could be dropped the next version
- Added randomized displacements for step tomo scan
- Added `estimation_time` function for sequences
- Added setup to change detector pixel rate

### Changed

- Changed name of the procedure `align` into `align_cor`
- Changed automatic selection of maximum pixel size in `init_sequence()` into a sequence preset (PcoSetMaximumPixelRatePreset) to make it optional 
- Changed `CountRunner` to use sct instead. As result it uses the tomo/default chain settings.
- Changed TomoConfig `reference_position` into `parking_position`
- Changed order of the endding sequence scan the following `return, flat, dark` to match ID16A expectations
- Changed sequence `.pars` to merge all the different parameters into a `ProxyParam`

### Removed

- Removed compatibility code for BLISS 1.11
- Removed sequence `.basic_pars` which is now merged into `.pars`
- Removed sequence `.config_pars` which is now merged into `.pars`
- Removed sequence `.sequence_pars` which is now merged into `.pars`

## [2.6.0]

### Added

- Added support for fast shutter connected to a Ximea
- Added holotomo sequence
- Added `arscan` to handle random motion as an `ascan`
- Added ability to use or not a motor to take flats
- Added a new holotomo object for the management of the holo distances
- Added distance from the focal plan (source distance) for the sample and the detectors
- Added continuous scan for tiling
- Added supports to user tags `dial.clockwise` and `dial.anticlockwise`
- Added beam tracker controller
- Added reference position controller
- Added generic `procedure` which can be used together with Daiquiri
- Added new alignment based on `procedure`
- Added zseries parameter `dark_flat_for_each_scan`

### Changed

- Changed tiling sequence to only take a single flat (`intermediate_flat_scan` argument allow to fallback to the previous behavior)
- Changed `stabilization_time` into `settle_time` at different places
- Changed `YSTEP` only can display the displacement of a single motor
- Changed component name `TomoRefMot` into `FlatMotion`
- Changed `FlatMotion` meaning of motor configuration. Now this motors are only available for the motion
- Changed `FlatMotion` inheritance from `TomoParameters` to `BeaconObject`
- Changed `FlatMotion` motion storage as single list of named tuple
- Changed information displayed by default by `TomoConfig` and `TomoDetectors`
- Changed auto pixel size to take care of the sample location
- Changed auto pixel size to handle focal position of the sample and detector axis
- Changed the location of the computation of the auto pixel size in a dedicated object
- Changed the location of the computation of the user pixel size in a dedicated object
- Changed the yaml file structure of `TomoConfig`. Axes avec to be indented inside a `sample_stage:` block
- Changed auto proj inhibition by default during any tomo scans

### Removed

- Removed `FlatMotion.parameters`. The state is now private settings.
- Removed `TomoDetector.configured_optic`. The `.config` object can be used instead
- Removed `detector_x` from `TomoConfig`, now it always have to be defined inside `TomoDetectors`
- Removed `detector_x` from the metadata

### Fixed

- Fixed `TomoDetector.optic` synchronization with multiple BLISS sessions or Daiquiri

### Deprecated

- Deprecated `TomoDetector._user_optic_name`. It can be replaced by `TomoDetector.optic.name`
- Deprecated `TomoConfig.x_axis` and other axes. It is replaced by `TomoConfig.sample_stage.x_axis`

## [2.5.0]

### Added

- Added `detector_axis` to `TomoDetector`

## [2.4.0]

### Added

- Added `default_slow_single_chain` and `default_slow_accumulation_chain` to tomo config

### Changed

- Changed the runners location to `tomo.scan`
- Changed the controllers (like detector, detectors) location to `tomo.controllers`
- Changed the scan presets location to `tomo.scan.presets`
- Changed the tiling to use the tomo default chains

### Removed

- Removed `FakeShutter`. This can be replaced by a bliss controller `bliss.controllers.shutters.mockup`

## [2.3.0]

### Added

- Added `active_tomo_config` and metadata inside `instrument`

### Changed

### Fixed

## [2.2.0]

### Added

- Added `tomo_version` attribute to technique of the HDF5 tomo sequence
- Added `tomoconfig` with roles inside technique of the HDF5 tomo sequence
- Added `image_key` to the technique of every tomo scans

### Changed

### Fixed
