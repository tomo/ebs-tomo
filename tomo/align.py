import numpy as np
import gevent
from bliss.common.scans import ascan, pointscan, DEFAULT_CHAIN
from bliss.common.scans.ct import ct
from bliss.shell.getval import getval_yes_no
from tomo.controllers.tomo_config import TomoConfig
from tomo.fulltomo import FullFieldTomo

from tomo.chain_presets.image_no_saving import ImageNoSavingPreset, use_image_no_saving
from tomo.helpers.read_images import read_images
from bliss.common.plot import plotselect

from bliss.common.cleanup import (
    error_cleanup,
    cleanup,
    capture_exceptions,
    axis as cleanup_axis,
)
from bliss.common.utils import BOLD

from matplotlib import pyplot as plt
from bliss.scanning.scan_tools import peak
from bliss.controllers.lima.roi import Roi
from bliss.shell.standard import umv, mv, umvr, flint

import logging

_logger = logging.getLogger(__name__)


try:
    from nabu.estimation.translation import DetectorTranslationAlongBeam
    from nabu.estimation.focus import CameraFocus
    from nabu.estimation.tilt import CameraTilt
except ImportError:
    _logger.error("Error while importing nabu", exc_info=True)
    nabu = None


class Align:
    def __init__(self, name, config):
        self.__name = name
        sequence = config.get("sequence")
        if not isinstance(sequence, FullFieldTomo):
            raise TypeError(f"Expected FullFieldTomo but found {type(sequence)}")

        self.__config = config

        tomo_config = sequence.tomo_config
        if not isinstance(tomo_config, TomoConfig):
            raise TypeError(f"Expected TomoConfig but found {type(tomo_config)}")
        self._tomo_config = tomo_config

        self.sequence = sequence
        self.paper = self.__config.get("paper")

        self.slits_vgap = self.__config.get("slits_vgap")
        self.slits_hgap = self.__config.get("slits_hgap")
        self.detector_vtilt = self.__config.get("detector_vtilt")
        self.detector_htilt = self.__config.get("detector_htilt")

    @property
    def name(self):
        """
        A unique name for the Bliss object
        """
        return self.__name

    @property
    def config(self):
        """
        The configuration for Tomo alignment
        """
        return self.__config

    @property
    def tomo_config(self) -> TomoConfig:
        return self._tomo_config

    def __info__(self):
        info_str = f"{self.name} info:\n"
        if self.sequence is not None:
            info_str += f"  tomo            = {self.sequence.name} \n"
            info_str += f"  detector        = {self.sequence._detector.name}\n"
            info_str += f"  optic           = {self.sequence._detectors.get_optic(self.sequence._detector).description}\n"
            info_str += f"  pixel size      = {self._pixel_size()} \n"
            info_str += f"  exposure time   = {self.sequence.pars.exposure_time} \n\n"

            info_str += f"  focus motor     = {self.sequence._detectors.get_optic(self.sequence._detector).focus_motor.name} \n"
            info_str += f"  rotc motor      = {self.sequence._detectors.get_optic(self.sequence._detector).rotc_motor.name} \n"
            info_str += f"  lateral motor   = {self.sequence._y_axis.name} \n\n"
        else:
            info_str += "  tomo            = No Tomo object configured!\n"
        if self.paper is not None:
            info_str += f"  alignment paper = {self.paper.name} \n"
        else:
            info_str += "  alignment paper = No alignment pager object configured!\n"
        if self.slits_vgap is not None:
            info_str += f"  slits vgap      = {self.slits_vgap.name} \n"
        else:
            info_str += "  slits vgap      = No slits vgap object configured!\n"
        if self.slits_hgap is not None:
            info_str += f"  slits hgap      = {self.slits_hgap.name} \n"
        else:
            info_str += "  slits hgap      = No slits hgap object configured!\n"
        if self.detector_vtilt is not None:
            info_str += f"  detector vtilt  = {self.detector_vtilt.name} \n"
        else:
            info_str += "  detector vtilt  = No detector vtilt object configured!\n"
        if self.detector_htilt is not None:
            info_str += f"  detector htilt  = {self.detector_htilt.name} \n"
        else:
            info_str += "  detector htilt  = No detector htilt object configured!\n"
        return info_str

    def _pixel_size(self):
        # pixel size returned by tomo is in um
        pixel_size = self.sequence._detectors.get_image_pixel_size(
            self.sequence._detector
        )

        # pixel size for Nabu caluculations must be in mm
        pixel_size = pixel_size / 1000.0
        print(BOLD("\npixel_size in mm = %g\n" % pixel_size))

        return pixel_size

    # Detector focus alignment
    #

    def focus(self, save=False, plot=False, plot_images=False, dark_flat_images=True):

        try:
            projection = self.sequence._inpars.projection
        except AttributeError:
            projection = 1

        flat_image = None
        dark_image = None
        if dark_flat_images:
            with use_image_no_saving(DEFAULT_CHAIN, self.sequence._detector, not save):
                flat_runner = self.sequence._tomo_runners["flat"]
                flat_scan = flat_runner(
                    self.sequence.pars.exposure_time,
                    self.sequence.pars.flat_n,
                    self.sequence._detector,
                    projection=projection,
                    save=save,
                )
                flat_images = read_images(flat_scan, self.sequence._detector)
            flat_mean = np.array(
                np.mean(flat_images, axis=0), dtype=np.array(flat_images).dtype
            )
            flat_image = flat_mean

            with use_image_no_saving(DEFAULT_CHAIN, self.sequence._detector, not save):
                dark_runner = self.sequence._tomo_runners["dark"]
                dark_scan = dark_runner(
                    self.sequence.pars.exposure_time,
                    self.sequence.pars.dark_n,
                    self.sequence._detector,
                    save=save,
                )
                # read back the aquired images
                dark_images = read_images(dark_scan, self.sequence._detector)
            dark_mean = np.array(
                np.mean(dark_images, axis=0), dtype=np.array(dark_images).dtype
            )
            dark_image = dark_mean

        # prepare the focus scan parameters
        scan_pars = self.sequence._detectors.get_optic(
            self.sequence._detector
        ).focus_scan_parameters()

        scan_range = scan_pars["focus_scan_range"]
        scan_steps = scan_pars["focus_scan_steps"]
        focus_type = scan_pars["focus_type"]
        focus_motor = self.sequence._detectors.get_optic(
            self.sequence._detector
        ).focus_motor

        if (
            focus_type == "Rotation"
            and self.sequence._detectors.get_optic(
                self.sequence._detector
            ).magnification
            < 0.7
        ):
            scan_range /= 2
            scan_steps *= 2

        start = focus_motor.position - scan_range
        stop = focus_motor.position + scan_range

        # pixel size in mm
        pixel_size = self._pixel_size()

        if self.paper:
            # move in paper
            print("Move in paper")
            self.paper.IN()

        # Do the focus scan
        focus_scan = self.focus_scan(
            focus_motor,
            start,
            stop,
            scan_steps,
            self.sequence.pars.exposure_time,
            self.sequence._detector,
            save=save,
        )

        # read back the aquired images
        foc_images = read_images(focus_scan, self.sequence._detector)

        # convert to numpy array
        foc_images = np.array(foc_images)

        # read back the aquired positions
        foc_pos = focus_scan.streams[focus_motor.name][:]

        self.focus_calc(
            foc_images,
            flat_image,
            dark_image,
            foc_pos,
            focus_motor,
            self.sequence._detector,
            pixel_size,
            plot,
            plot_images,
            dark_flat_images,
        )

        ct(self.sequence.pars.exposure_time)
        print(f"Focus motor position = {focus_motor.position}")

        # Move out paper
        if self.paper and getval_yes_no("Move out paper?", default=True):
            self.paper.OUT()

    def focus_calc(
        self,
        foc_images,
        flat_image,
        dark_image,
        foc_pos,
        focus_motor,
        detector,
        pixel_size,
        plot=False,
        plot_images=False,
        dark_flat_images=True,
    ):
        if dark_flat_images:
            foc_images = np.divide(
                np.subtract(foc_images, dark_image), np.subtract(flat_image, dark_image)
            )

        if plot_images:
            f = flint()
            if dark_flat_images:
                p = f.get_plot(
                    "image",
                    name="dark_image",
                    unique_name="dark_image",
                    selected=True,
                    closeable=True,
                )

                p.set_data(dark_image)
                p.yaxis_direction = "down"
                p.side_histogram_displayed = False

                p = f.get_plot(
                    "image",
                    name="flat_image",
                    unique_name="flat_image",
                    selected=True,
                    closeable=True,
                )

                p.set_data(flat_image)
                p.yaxis_direction = "down"
                p.side_histogram_displayed = False

            p = f.get_plot(
                "stackview",
                name="projection_images",
                unique_name="projection_images",
                selected=True,
                closeable=True,
            )

            p.set_data(foc_images)
            p.yaxis_direction = "down"
            p.side_histogram_displayed = False

        focus_calc = CameraFocus()
        # enable calculation results plotting
        if plot is True:
            focus_calc.verbose = True

        focus_pos, focus_ind, tilts_vh = focus_calc.find_scintillator_tilt(
            foc_images, foc_pos
        )
        # print (focus_pos, focus_ind, tilts_vh)

        # tilts_corr_vh_deg = -np.rad2deg(np.arctan(tilts_vh / pixel_size))
        tilts_corr_vh_rad = -tilts_vh / pixel_size

        print(f"\nBest focus position:         {focus_pos}")
        print(f"Tilts vert. and hor. in rad: {tilts_corr_vh_rad}\n")

        if plot is True:

            def pause():
                plt.pause(1)

            g = gevent.spawn(pause)
        if getval_yes_no("Move to the best focus position?", default=True):
            # Always move to the best focus from the bottom
            mv(focus_motor, foc_pos[0])
            # move focus motor to maximum
            mv(focus_motor, focus_pos)

        # Kill the plot window when it was requested
        if plot is True:
            g.kill()
            # workaround for nabu plotting problem
            plt.close("all")

    def focus_scan(
        self,
        focus_motor,
        start,
        stop,
        steps,
        expo_time,
        detector,
        save=False,
    ):
        # prepare roi counters for statistics
        focus_roi = Roi(0, 0, detector.image.roi[2], detector.image.roi[3])

        det_rois = detector.roi_counters
        det_rois["focus_roi"] = focus_roi

        std = detector.roi_counters.counters.focus_roi_std
        counters = (std, detector.image)

        plotselect(std)

        if not save:
            savingpreset = ImageNoSavingPreset(detector)
            DEFAULT_CHAIN.add_preset(savingpreset)
        # clean-up: move back focus motor, delete roi counters, move out paper
        restore_list = (cleanup_axis.POS,)
        with cleanup(*[focus_motor], restore_list=restore_list):
            with capture_exceptions(raise_index=0) as capture:
                with capture():
                    # execute focus scan
                    scan = ascan(
                        focus_motor,
                        start,
                        stop,
                        steps,
                        expo_time,
                        counters,
                        title="focus scan",
                        save=save,
                    )

                    # get the position on the maximum of the standard deviation
                    peak()

                    # delete roi counters
                    del det_rois["focus_roi"]

                    if not save:
                        DEFAULT_CHAIN.remove_preset(savingpreset)

                    return scan

                # test if an error has occured
                if len(capture.failed) > 0:
                    # delete roi counters
                    del det_rois["focus_roi"]

                    if not save:
                        DEFAULT_CHAIN.remove_preset(savingpreset)

                    return None

    # Lateral alignment and camera tilt

    def align(
        self,
        save=False,
        plot=False,
        plot_images=False,
        use_fft_polar_method=False,
        questions_move=True,
    ):
        # move rotation axis to 0
        umv(self.sequence._rotation_axis, 0)

        # take reference image

        try:
            projection = self.sequence._inpars.projection
        except AttributeError:
            projection = 1

        flat_n = 1
        with use_image_no_saving(DEFAULT_CHAIN, self.sequence._detector, not save):
            flat_runner = self.sequence._tomo_runners["flat"]
            flat_scan = flat_runner(
                self.sequence.pars.exposure_time,
                flat_n,
                self.sequence._detector,
                projection=projection,
                save=save,
            )
            flat_images = read_images(flat_scan, self.sequence._detector)
        flat_image = flat_images[-1]

        # take dark image
        dark_n = 1
        with use_image_no_saving(DEFAULT_CHAIN, self.sequence._detector, not save):
            dark_runner = self.sequence._tomo_runners["dark"]
            dark_scan = dark_runner(
                self.sequence.pars.exposure_time,
                dark_n,
                self.sequence._detector,
                save=save,
            )
            # read back the aquired images
            dark_images = read_images(dark_scan, self.sequence._detector)
        dark_image = dark_images[-1]

        # pixel size in mm
        pixel_size = self._pixel_size()
        # do alignment scan
        al_scan = self.align_scan(
            self.sequence._rotation_axis,
            self.sequence.pars.exposure_time,
            self.sequence._detector,
            save=save,
        )

        # read back the aquired images
        al_images = read_images(al_scan, self.sequence._detector)

        # works only with on motor today!
        lateral_motor = self.sequence._y_axis
        rotc = self.sequence._detectors.get_optic(self.sequence._detector).rotc_motor

        source_sample_distance = self.sequence._tomo_config.sample_stage.source_distance
        sample_detector_distance = self.sequence._detectors.get_tomo_detector(
            self.sequence._detector
        ).sample_detector_distance

        lat_corr, camera_tilt = self.align_calc(
            al_images,
            dark_image,
            flat_image,
            lateral_motor,
            rotc,
            self.sequence._detector,
            pixel_size,
            source_sample_distance,
            sample_detector_distance,
            plot,
            plot_images,
            use_fft_polar_method,
            questions_move,
        )

        ct(self.sequence.pars.exposure_time)
        print(f"Lateral alignment motor position = {lateral_motor.position}")
        if rotc is not None:
            print(f"Camera tilt position {rotc.name} = {rotc.position}")

        return lat_corr, camera_tilt

    def correct_movement(self, d_t0, d_t1):
        if abs((d_t0 - d_t1) / d_t0) > 0.05:
            flag_not_moving = False
            sign = (d_t0 < 0) == (d_t1 < 0)
            if sign:
                if abs(d_t0) < abs(d_t1):
                    dire = -1
                else:
                    dire = 1
            else:
                dire = 1

            value_corrected = d_t1 - d_t0
            fraction_correct = abs(d_t0 / value_corrected) * dire
            new_corection = d_t1 * fraction_correct
        else:
            fraction_correct = 1
            flag_not_moving = True
            new_corection = 0

        return new_corection, fraction_correct, flag_not_moving

    def align_iter(self, yrot_precision=0.01, rotc_precision=0.001):
        lateral_motor = self.sequence._y_axis
        rotc = self.sequence._detectors.get_optic(self.sequence._detector).rotc_motor

        dyrot_t0, drotc_t0 = self.align(questions_move=False)
        if (abs(dyrot_t0) < yrot_precision) and (abs(dyrot_t0) < rotc_precision):
            flag_pass = True
        else:
            flag_pass = False

        if not flag_pass:
            umvr(lateral_motor, dyrot_t0, rotc, drotc_t0)

            i = 0
            f_rotc = 1.0
            f_yrot = 1.0

            while True:
                dyrot_t1, drotc_t1 = self.align(questions_move=False)

                if (abs(dyrot_t1) > yrot_precision) and (
                    abs(drotc_t1) > rotc_precision
                ):
                    umvr(lateral_motor, dyrot_t1, rotc, drotc_t1)
                elif abs(dyrot_t1) > yrot_precision:
                    umvr(lateral_motor, dyrot_t1)
                elif abs(drotc_t1) > rotc_precision:
                    umvr(rotc, drotc_t1)
                else:
                    break

                dyrot_t1 = f_yrot * dyrot_t1
                drotc_t1 = f_rotc * drotc_t1

                dyrot_t1, f_yrot, flag_not_moving_yrot = self.correct_movement(
                    dyrot_t0, dyrot_t1
                )
                drotc_t1, f_rotc, flag_not_moving_rotc = self.correct_movement(
                    drotc_t0, drotc_t1
                )

                dyrot_t0 = dyrot_t1
                drotc_t0 = drotc_t1

                i += 1

    def align_calc(
        self,
        al_images,
        dark_image,
        flat_image,
        lateral_motor,
        rotc_motor,
        detector,
        pixel_size,
        source_sample_distance,
        sample_detector_distance,
        plot=False,
        plot_images=False,
        use_fft_polar_method=False,
        questions_move=True,
    ):
        radio0 = (al_images[0].astype(float) - dark_image.astype(float)) / (
            flat_image.astype(float) - dark_image.astype(float)
        )
        radio180 = (al_images[1].astype(float) - dark_image.astype(float)) / (
            flat_image.astype(float) - dark_image.astype(float)
        )

        # flip the radio180 for the calculation
        radio180_flip = np.fliplr(radio180.copy())

        if plot_images:
            f = flint()
            p = f.get_plot(
                "image",
                name="dark_image",
                unique_name="dark_image",
                selected=True,
                closeable=True,
            )

            p.set_data(dark_image)
            p.yaxis_direction = "down"
            p.side_histogram_displayed = False

            p = f.get_plot(
                "image",
                name="flat_image",
                unique_name="flat_image",
                selected=True,
                closeable=True,
            )

            p.set_data(flat_image)
            p.yaxis_direction = "down"
            p.side_histogram_displayed = False

            p = f.get_plot(
                "image",
                name="projection_image_0",
                unique_name="projection_image_0",
                selected=True,
                closeable=True,
            )

            p.set_data(radio0)
            p.yaxis_direction = "down"
            p.side_histogram_displayed = False

            p = f.get_plot(
                "image",
                name="projection_image_180_flip",
                unique_name="projection_image_180_flip",
                selected=True,
                closeable=True,
            )

            p.set_data(radio180_flip)
            p.yaxis_direction = "down"
            p.side_histogram_displayed = False

        # plot_image(radio0)
        # plot_image(radio180_flip)

        # calculate the lateral correction for the rotation axis
        # and the camera tilt with line by line correlation
        tilt_calc = CameraTilt()
        # enable calculation results plotting
        if plot is True:
            tilt_calc.verbose = True

        method = "1d-correlation"
        if use_fft_polar_method:
            method = "fft-polar"
        pixel_cor, camera_tilt = tilt_calc.compute_angle(
            radio0, radio180_flip, method=method
        )
        print("CameraTilt: pixel_cor = %f" % pixel_cor)

        # distance from source to sample   = L1(mm)
        # distance from source to detector = L2(mm)
        # size of the image pixel          = s(mm/pixel)
        # dmot(mm) = L1/L2*s*pixel_cor
        # cor_factor = (
        # source_sample_distance
        # / (source_sample_distance + sample_detector_distance)
        # * pixel_size
        # )

        pos_cor = pixel_size * pixel_cor

        # pixel size in mm
        pixel_size = self._pixel_size()
        print(BOLD(f"Lateral alignment position correction in mm: {-pos_cor:>10.5f}"))
        print(
            BOLD(
                f"Camera tilt in deg (motor rotc: {rotc_motor.name}): {camera_tilt:35.5f}\n"
            )
        )

        if plot is True:

            def pause():
                plt.pause(1)

            g = gevent.spawn(pause)

        # if getval_yes_no(
        #    "Apply the lateral position correction and the camera tilt?", default=False):

        if questions_move:
            if getval_yes_no("Apply the lateral position correction?", default=False):
                # apply lateral correction
                umvr(lateral_motor, -pos_cor)

            if getval_yes_no("Apply the camera tilt?", default=False):
                # apply tilt correction
                try:
                    umvr(rotc_motor, camera_tilt)
                except Exception:
                    pass
        # Kill the plot window when it was requested
        if plot is True:
            # workaround for nabu plotting problem
            g.kill()
            plt.close("all")

        return -pos_cor, camera_tilt

    def align_scan(self, rotation_motor, expo_time, detector, save=False):
        if not save:
            savingpreset = ImageNoSavingPreset(detector)
            DEFAULT_CHAIN.add_preset(savingpreset)
        # clean-up: move back rot
        restore_list = (cleanup_axis.POS,)
        with cleanup(rotation_motor, restore_list=restore_list):
            with capture_exceptions(raise_index=0) as capture:
                with capture():
                    # scan rot
                    rot_pos_list = [0.0, 180.0]
                    scan = pointscan(
                        rotation_motor,
                        rot_pos_list,
                        expo_time,
                        detector.image,
                        title="align scan",
                        save=save,
                    )

                    if not save:
                        DEFAULT_CHAIN.remove_preset(savingpreset)

                    return scan

                # test if an error has occured
                if len(capture.failed) > 0:
                    if not save:
                        DEFAULT_CHAIN.remove_preset(savingpreset)

                    return None

    # Camera stage tilt alignment

    def alignxc(self, start=100, stop=800, steps=10, save=False, plot=False):
        # pixel size in mm
        pixel_size = self._pixel_size()

        # slit gap to use
        if pixel_size < 0.003:  # 3um
            slit_gap = 0.2
        else:
            slit_gap = 1.0

        tomo_detector = self.sequence.tomo_detector
        tomo_config = self.tomo_config
        detector_axis = tomo_detector.detector_axis
        if detector_axis is None:
            detector_axis = tomo_config.detectors.detector_axis
        if detector_axis is None:
            raise RuntimeError(
                f"No detector_axis found for {tomo_detector.detecor.name}"
            )

        lima = tomo_detector.detector
        alxc_scan = self.alignxc_scan(
            detector_axis,
            start,
            stop,
            steps,
            self.sequence.pars.exposure_time,
            lima,
            self.slits_vgap,
            self.slits_hgap,
            slit_gap,
            save=save,
        )
        # read back the aquired images
        alxc_images = read_images(alxc_scan, lima)
        # print(alxc_images)

        # read back the motor positions
        alxc_pos = alxc_scan.streams[detector_axis.name][:]
        # print(al_pos)

        # take dark image
        dark_n = 1
        dark_runner = self.sequence._tomo_runners["dark"]
        with use_image_no_saving(DEFAULT_CHAIN, lima, not save):
            dark_scan = dark_runner(
                self.sequence.pars.exposure_time, dark_n, lima, save=save
            )
        # read back the aquired images
        dark_images = read_images(dark_scan, lima)
        dark_image = dark_images[0]

        distanceMove = abs(stop - start)
        tilt_h, tilt_v = self.alignxc_calc(
            alxc_images, alxc_pos, dark_image, distanceMove, plot
        )

        self._print_alignxc_correction(tilt_h, tilt_v)

        # Kill the plot window when it was requested
        if plot is True:
            if getval_yes_no("Close the plot window?", default=True):
                # workaround for nabu plotting problem
                plt.close("all")

    def alignxc_calc(self, alxc_images, alxc_pos, dark_image, distanceMove, plot=False):
        # substract dark from images
        for i in alxc_images:
            i = i.astype(float) - dark_image.astype(float)
        # convert to numpy array
        al_array = np.array(alxc_images)

        # calculate XC tilt shifts
        # the scan positions and the pixel_size must have the same units!

        if nabu is None:
            raise RuntimeError("Nabu is not installed")

        tr_calc = DetectorTranslationAlongBeam()
        # enable calculation results plotting
        if plot is True:
            tr_calc.verbose = True

        shifts_v, shifts_h, _ = tr_calc.find_shift(
            al_array, alxc_pos, return_shifts=True, use_adjacent_imgs=True
        )
        print(shifts_v, shifts_h)
        pixel_size = self._pixel_size()
        # pixel size should be im mm
        tilt_v = np.arctan(shifts_v * pixel_size)
        tilt_h = np.arctan(shifts_h * pixel_size)
        return tilt_h, tilt_v

    def _print_alignxc_correction(self, tilt_h: float, tilt_v: float):
        print()
        print(f"Vertical   tilt in deg (thy): {np.rad2deg(tilt_v)}")
        print(f"Horizontal tilt in deg (thz): {np.rad2deg(tilt_h)}")
        print()

    def alignxc_scan(
        self,
        xc,
        start,
        stop,
        steps,
        expo_time,
        detector,
        svg,
        shg,
        slit_gap,
        save=False,
    ):
        # save current slit positions
        vg_pos = svg.position
        hg_pos = shg.position
        print(f"Current slit gap positions: vertical={vg_pos}, horizontal={hg_pos}")
        print(f"Current xc positions:       xc={xc.position}")

        if not save:
            savingpreset = ImageNoSavingPreset(detector)
            DEFAULT_CHAIN.add_preset(savingpreset)
        # clean-up: move back xc motor, open_slits
        restore_list = (cleanup_axis.POS,)
        with use_image_no_saving(DEFAULT_CHAIN, detector, not save):
            with error_cleanup(xc, svg, shg, restore_list=restore_list):
                with capture_exceptions(raise_index=0) as capture:
                    with capture():
                        # close the slits
                        umv(svg, slit_gap, shg, slit_gap)

                        # scan xc
                        scan = ascan(
                            xc,
                            start,
                            stop,
                            steps,
                            expo_time,
                            detector.image,
                            title="align xc scan",
                            save=save,
                        )

                        # open the slits
                        umv(svg, vg_pos, shg, hg_pos)

                        if not save:
                            DEFAULT_CHAIN.remove_preset(savingpreset)
                        # return images
                        return scan

                    # test if an error has occured
                    if len(capture.failed) > 0:
                        if not save:
                            DEFAULT_CHAIN.remove_preset(savingpreset)

                        return None
