""" Sequences

.. autosummary::
   :toctree:

    presets

    multitomo
    pcotomo
    tiling
"""
