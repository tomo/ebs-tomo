from __future__ import annotations

import typing

from bliss import setup_globals
from bliss.common.axis import Axis
from bliss.common.cleanup import axis as cleanup_axis
from bliss.common.cleanup import cleanup, capture_exceptions
from bliss.shell.standard import umv, countdown
from fscan.fscantools import FScanParamBase

from tomo.sequence.presets.inhibit_auto_projection import InhibitAutoProjection
from tomo.sequencebasic import cleanup_sequence
from tomo.fulltomo import FullFieldTomo
from tomo.helpers.proxy_param import ProxyParam
from tomo.helpers import nexuswriter_utils
from bliss.physics.trajectory import LinearTrajectory


class HoloTomoPars(FScanParamBase):
    """
    Class to create holotomo sequence parameters

    Arguments:
        use_beam_tracker: Use the beam tracker to move the sample along the x-axis
    """

    DEFAULT = {
        "use_beam_tracker": True,
    }
    LISTVAL = {}
    NOSETTINGS = []

    def __init__(self, name):
        FScanParamBase.__init__(
            self,
            name,
            HoloTomoPars.DEFAULT,
            value_list=HoloTomoPars.LISTVAL,
            no_settings=HoloTomoPars.NOSETTINGS,
        )


class HoloTomo(FullFieldTomo):
    """
    Setup and run an holotomo sequence as a BLISS object.

    .. code-block:: yaml

        name: holotomo
        plugin: bliss
        class: HoloTomo
        package: tomo.sequence.holotomo

        tomo_config: $tomo_config
        tomo: $full_tomo
    """

    def __init__(self, name:str, config: dict[str, typing.Any]):
        self.name: str = name + "_sequence"
        """Bliss object name"""

        self.tomo = config["tomo"]
        """
        Sequence executed after each holotomo movement
        """

        FullFieldTomo.__init__(self, self.name, config)

        inhibitautoproj = InhibitAutoProjection(self.tomo._tomo_config)
        self.add_sequence_preset(inhibitautoproj)

    def _create_parameters(self):
        """
        Create pars attribute containing zseriesm fulltomo and config pars
        """
        holo_pars = HoloTomoPars(self.name)
        # NOTE: tomo_config.pars is already part of self.tomo.pars
        return ProxyParam(self.tomo.pars, holo_pars)

    def _x_axis(self) -> Axis:
        tomo_config = self.tomo_config
        if self.pars.use_beam_tracker:
            x_axis = tomo_config.tracked_x_axis
            if x_axis is None:
                raise RuntimeError("No tracked translation x axis defined in the setup")
        else:
            x_axis = tomo_config.sample_stage.x_axis
        return x_axis

    def add_metadata(self, scan_info):
        """
        Fill the scan_info dictionary with the basic tomo meta data
        and update it with specific meta data related to holotomo
        """
        super().add_metadata(scan_info)
        tomo_config = self.tomo_config
        holotomo = tomo_config.holotomo
        pars = self.pars

        x_axis = self._x_axis()

        scan_meta = scan_info["technique"].setdefault("scan", {})

        scan_meta["nb_distances"] = holotomo.nb_distances
        scan_meta["use_beam_tracker"] = pars.use_beam_tracker
        scan_meta["sequence"] = "tomo:holotomo"
        scan_meta["x_axis"] = nexuswriter_utils.hdf5_device_names(x_axis)

    @cleanup_sequence
    def run(self, user_info=None):
        """Run the sequence in blocking mode."""
        tomo_config = self.tomo_config

        # Dump locally everything needed from holotomo object
        # Because this object could change during the scan
        holotomo = tomo_config.holotomo
        assert holotomo is not None
        settle_time = holotomo.settle_time
        positions = [d.position for d in holotomo.distances]
        x_axis = self._x_axis()

        if self.pars.return_to_start_pos:
            restore_list = (cleanup_axis.POS,)
        else:
            restore_list = ()

        with capture_exceptions(raise_index=0) as capture:
            with self._run_context(capture):
                with cleanup(x_axis, restore_list=restore_list):
                    base_name = self._sequence_name
                    for distance_id, position in enumerate(positions):
                        umv(x_axis, position)

                        if settle_time != 0.0:
                            countdown(settle_time, message="Waiting settle time...")

                        self._sequence_name = f"{base_name}_distance{distance_id + 1}"
                        # Already locked, so we have to disable the lock
                        super().run(user_info, _lock=False)

    def estimation_time(self) -> float:
        """
        Estimation time of the sequence (in seconds).
        """
        fullfield_duration = self.tomo.estimation_time()

        tomo_config = self.tomo_config
        holotomo = tomo_config.holotomo
        assert holotomo is not None
        settle_time = holotomo.settle_time
        nb_distances = holotomo.nb_distances
        positions = [d.position for d in holotomo.distances]

        # Assume the x axis is the slowest
        # Use the real axis instead of the tracked axis
        x_axis = tomo_config.sample_stage.x_axis
        positions.insert(0, x_axis.position)
        motion_duration = 0
        for i in range(len(positions) - 1):
            lt = LinearTrajectory(
                positions[i], positions[i + 1], x_axis.velocity, x_axis.acceleration
            )
            motion_duration += lt.duration

        return (fullfield_duration + settle_time) * nb_distances + motion_duration

    def full_turn_scan(
        self,
        dataset_name=None,
        collection_name=None,
        start_pos=None,
        scan_info=None,
        run=True,
    ):
        """
        Build and run full field tomo of 360 angular range
        """
        if dataset_name is not None:
            setup_globals.newdataset(dataset_name)
        if collection_name is not None:
            setup_globals.newcollection(collection_name)
        if start_pos is not None:
            self.pars.start_pos = start_pos
        self.pars.range = 360

        self._setup_sequence("tomo:holotomo", run=run, scan_info=scan_info)

    def half_turn_scan(
        self,
        dataset_name=None,
        collection_name=None,
        start_pos=None,
        scan_info=None,
        run=True,
    ):
        """
        Build and run full field tomo of 180 angular range
        """
        if dataset_name is not None:
            setup_globals.newdataset(dataset_name)
        if collection_name is not None:
            setup_globals.newcollection(collection_name)
        if start_pos is not None:
            self.pars.start_pos = start_pos
        self.pars.range = 180

        self._setup_sequence("tomo:holotomo", run=run, scan_info=scan_info)

    def basic_scan(
        self,
        start_pos,
        end_pos,
        tomo_n,
        expo_time,
        dataset_name=None,
        collection_name=None,
        scan_info=None,
        run=True,
    ):
        """
        Build and run full field tomo using given parameters
        """

        if dataset_name is not None:
            setup_globals.newdataset(dataset_name)
        if collection_name is not None:
            setup_globals.newcollection(collection_name)
        if start_pos is not None:
            self.pars.start_pos = start_pos
        if end_pos is not None:
            self.pars.range = end_pos - self.pars.start_pos
        if tomo_n is not None:
            self.pars.tomo_n = tomo_n
        if expo_time is not None:
            self.pars.exposure_time = expo_time

        self._setup_sequence("tomo:holotomo", run=run, scan_info=scan_info)
