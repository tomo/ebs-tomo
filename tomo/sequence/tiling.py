from __future__ import annotations
import contextlib
import typing
import pint
import numbers
from bliss.controllers.lima.lima_base import Lima
from bliss.common import cleanup
from bliss.common.utils import typecheck
from bliss.scanning.group import Sequence
from bliss.physics.trajectory import LinearTrajectory
from tomo.controllers.tomo_config import TomoConfig
from tomo.scan.presets.move_axis import MoveAxisPreset
from tomo.helpers.axis_utils import is_user_clockwise
from tomo.helpers.nexuswriter_utils import hdf5_axis_name
from tomo.scan.tiling_runner import TilingLateralMotorMode
from tomo.helpers.locking_helper import lock_context
from tomo.helpers.locked_axis_motion_hook import LockedAxisMotionHook


NFRAMES_PER_FILE = 5
"""Max number of frames per file for the tiling scans"""


class _YAxisInfo(typing.NamedTuple):
    axis: object
    start: pint.Quantity
    stop: pint.Quantity
    sample_y_axis_name: str | None
    y_axis_name: str
    interval: int


def _get_y_axis_info(
    ystart: numbers.Real,
    ystop: numbers.Real,
    lateral_motor_mode: TilingLateralMotorMode,
    tomoconfig: TomoConfig,
    tomo_detector,
    real_motor=False,
):
    """
    Arguments:
        real_motor: If true try not to use a pseudo motor
                    This only have meaning for estimation time
                    Else pseudo will not have velocity/acceleration
    """
    tiling_runner = tomoconfig.get_runner("tiling")

    axis, start, stop, interval = tiling_runner.get_y_trajectory_info(
        tomo_detector,
        float(ystart),
        float(ystop),
        lateral_motor_mode,
        real_motor=real_motor,
    )
    if lateral_motor_mode == TilingLateralMotorMode.OVER_ROT:
        is_sample_y_moving = interval != 0
        is_y_moving = False
    elif lateral_motor_mode == TilingLateralMotorMode.UNDER_ROT:
        is_sample_y_moving = False
        is_y_moving = interval != 0
    else:
        raise ValueError(f"Unsupported lateral motor mode: {lateral_motor_mode}")
    return _YAxisInfo(
        axis=axis,
        start=start,
        stop=stop,
        sample_y_axis_name=hdf5_axis_name(tomoconfig.sample_y_axis, is_sample_y_moving),
        y_axis_name=hdf5_axis_name(tomoconfig.y_axis, is_y_moving),
        interval=interval,
    )


@contextlib.contextmanager
def save_nframes_per_file(detector: Lima, nframes: int):
    """Context setting the max number of frames per file for given detector"""
    previous_frames_per_file = detector.saving.frames_per_file
    previous_saving_mode = detector.saving.mode
    try:
        detector.saving.frames_per_file = nframes
        detector.saving.mode = "ONE_FILE_PER_N_FRAMES"
        yield
    finally:
        detector.saving.frames_per_file = previous_frames_per_file
        detector.saving.mode = previous_saving_mode


@contextlib.contextmanager
def run_independent_presets(scan):
    """Execute the independent presets part of the scan (if ones).

    FIXME: This have to be properly handled by BLISS.
    """
    presets = getattr(scan, "_independent_presets", None)
    try:
        if presets is not None:
            presets.prepare(None)
            try:
                presets.start(None)
            except BaseException:
                presets.stop(None)
                raise
        yield
    finally:
        if presets is not None:
            presets.stop(None)


@typecheck
def tiling_estimation_time(
    xstart: numbers.Real,
    xstop: numbers.Real,
    ystart: numbers.Real,
    ystop: numbers.Real,
    zstart: numbers.Real,
    zstop: numbers.Real,
    detector: Lima,
    expo_time: numbers.Real,
    tomoconfig: TomoConfig,
    n_dark: int,
    n_flat: int,
    sleep_time: typing.Optional[numbers.Real] = None,
    lateral_motor_mode="over_rot",
    restore_motor_positions: bool = False,
    continuous: bool = False,
    speed_ratio: float = 1.0,
    intermediate_flat_scan: bool = False,
):
    lateral_motor_mode = TilingLateralMotorMode(lateral_motor_mode.upper())
    tomo_detector = tomoconfig.detectors.get_tomo_detector(detector)
    assert tomo_detector is not None

    # Get axis names information
    tiling_runner = tomoconfig.get_runner("tiling")
    side_info = _get_y_axis_info(
        xstart, xstop, lateral_motor_mode, tomoconfig, tomo_detector, real_motor=True
    )
    front_info = _get_y_axis_info(
        ystart, ystop, lateral_motor_mode, tomoconfig, tomo_detector, real_motor=True
    )

    z_axis, start_z, stop_z, interval_z = tiling_runner.get_z_trajectory_info(
        tomo_detector, float(zstart), float(zstop)
    )

    single_acq_time = float(expo_time)
    if sleep_time is not None:
        single_acq_time += float(sleep_time)

    acq_time = (n_dark + n_dark) * single_acq_time

    if not continuous:
        proj = 0
        proj += (side_info.interval + 1) * (interval_z + 1)
        proj += (front_info.interval + 1) * (interval_z + 1)
        acq_time += proj * single_acq_time

    def estimate_move(axis, pfrom, pto, velocity=None):
        if isinstance(pfrom, pint.Quantity):
            pfrom = pfrom.to(axis.unit).magnitude
        if isinstance(pto, pint.Quantity):
            pto = pto.to(axis.unit).magnitude
        if velocity is None:
            velocity = axis.velocity
        lt = LinearTrajectory(
            pi=pfrom,
            pf=pto,
            velocity=velocity,
            acceleration=axis.acceleration,
            ti=0,
        )
        return lt.instant(pto)

    move_time = 0
    if not continuous:
        step = (stop_z - start_z) / (interval_z + 1)
        move_time += (
            estimate_move(z_axis, start_z, start_z + step)
            * interval_z
            * ((side_info.interval + 1) + (front_info.interval + 1))
        )
    else:

        def get_continuous_velocity(z_axis):
            px = tomo_detector.sample_pixel_size
            vmax = z_axis.velocity
            v1px = 1000 * px / expo_time
            if v1px < vmax:
                vmin = v1px
            else:
                vmin = vmax * 0.1
            return vmin + (vmax - vmin) * speed_ratio

        velocity = get_continuous_velocity(z_axis)
        # FIXME: There is a missing acceleration distance
        move_time += estimate_move(z_axis, start_z, stop_z, velocity) * (
            (side_info.interval + 1) + (front_info.interval + 1)
        )

    step = (front_info.stop - front_info.start) / (front_info.interval + 1)
    move_time += (
        estimate_move(front_info.axis, front_info.start, front_info.start + step)
        * front_info.interval
    )
    step = (side_info.stop - side_info.start) / (side_info.interval + 1)
    move_time += (
        estimate_move(side_info.axis, side_info.start, side_info.start + step)
        * side_info.interval
    )

    r_axis = tomoconfig.rotation_axis
    rot_time = estimate_move(r_axis, -90, 0)

    flat_runner = tomoconfig.get_runner("flat")
    flat_time = (
        flat_runner.estimation_time(expo_time=float(expo_time), flat_n=n_flat) * 2
    )

    # FIXME: Take into account time to move z/y at start
    start_time = estimate_move(r_axis, r_axis.position, -90)

    # FIXME: Take into account time to restore z/y position
    restore_time = estimate_move(r_axis, 0, r_axis.position)

    return rot_time + acq_time + flat_time + move_time + start_time + restore_time


@typecheck
def tiling(
    xstart: numbers.Real,
    xstop: numbers.Real,
    ystart: numbers.Real,
    ystop: numbers.Real,
    zstart: numbers.Real,
    zstop: numbers.Real,
    detector: Lima,
    expo_time: numbers.Real,
    tomoconfig: TomoConfig,
    n_dark: int = 3,
    n_flat: int = 3,
    sleep_time: typing.Optional[numbers.Real] = None,
    lateral_motor_mode="over_rot",
    restore_motor_positions: bool = False,
    continuous: bool = False,
    speed_ratio: float = 1.0,
    intermediate_flat_scan: bool = False,
):
    """
    Scan a tomo sample stage using full field acquisition as tiled images.

    The sequence contains:

    - Few dark
    - Few flat
    - A 2D scan at rotation -90 (at this angle scanning sy is like scanning sx at rot 0)
    - Few flat
    - A 2D scan at rotation 0

    Arguments:
        xstart: X position to start in absolute coordinate of the sample stage (y-axis at -rot90)
        xstop: X position to stop in absolute coordinate of the sample stage (y-axis at -rot90)
        ystart: Y position to start in absolute coordinate of the sample stage
        ystop: Y position to stop in absolute coordinate of the sample stage
        zstart: Z position to start in absolute coordinate of the sample stage
        zstop: Z position to stop in absolute coordinate of the sample stage
        detector: Lima detector to use
        expo_time: Integration time for the Lima detector
        sleep_time: Time to wait between motor move before taking a new acquisition
        tomoconfig: Description of the tomo sample stage
        lateral_motor_mode: One of 'under_rot' or 'over_rot', this define which motor will be
                involved to scan laterally
        restore_motor_positions: If true (default save and restore motor position
                                 at the end of the scan.
    """
    lateral_motor_mode = TilingLateralMotorMode(lateral_motor_mode.upper())
    tomo_detector = tomoconfig.detectors.get_tomo_detector(detector)
    assert tomo_detector is not None

    sample_stage = tomoconfig.sample_stage

    # Get front and side angles
    srot = tomoconfig.rotation_axis
    min_angle, max_angle = min(srot.limits), max(srot.limits)
    if min_angle <= 0 <= max_angle:
        front_angle = 0
    elif min_angle % 360 == 0:
        front_angle = min_angle
    else:
        front_angle = 360 * (min_angle // 360 + 1)
        if front_angle > max_angle:
            raise RuntimeError(
                "The current rotation limits do not allow a tiling scan: Front scan forbidden"
            )

    direction_sign = 1 if is_user_clockwise(srot) else -1
    for angle_direction in (-90, 90):
        side_angle = front_angle + direction_sign * angle_direction
        if min_angle <= side_angle <= max_angle:
            break
    else:
        raise RuntimeError(
            "The current rotation limits do not allow a tiling scan: Side scan forbidden"
        )

    front_rotation = MoveAxisPreset(srot, front_angle, disable_locking=True)
    side_rotation = MoveAxisPreset(srot, side_angle, disable_locking=True)
    is_left_viewpoint = direction_sign * side_angle > direction_sign * front_angle
    if is_left_viewpoint:
        xstart, xstop = -xstop, -xstart

    # Get axis names information
    tiling_runner = tomoconfig.get_runner("tiling")
    side_info = _get_y_axis_info(
        xstart, xstop, lateral_motor_mode, tomoconfig, tomo_detector
    )
    front_info = _get_y_axis_info(
        ystart, ystop, lateral_motor_mode, tomoconfig, tomo_detector
    )

    _, _, _, interval_z = tiling_runner.get_z_trajectory_info(
        tomo_detector, zstart, zstop
    )
    is_z_moving = interval_z != 0
    z_axis_name = hdf5_axis_name(tomoconfig.z_axis, is_z_moving)

    subscans = {
        "scan1": {"type": "tomo:dark"},
        "scan2": {"type": "tomo:flat"},
        "scan3": {
            "type": f"tomo:tiling:{'left' if is_left_viewpoint else 'right'}",
            "vertical_range": [zstart, zstop],
            "vertical_range@units": "mm",
            "vertical_nb_frames": interval_z + 1,
            "horizontal_range": [xstart, xstop],
            "horizontal_range@units": "mm",
            "horizontal_nb_frames": side_info.interval + 1,
            "y_axis_name": side_info.y_axis_name,
            "z_axis_name": z_axis_name,
            "continuous": continuous,
        },
    }
    scan_number = 4
    if intermediate_flat_scan:
        subscans["scan4"] = {"type": "tomo:flat"}
        scan_number += 1

    subscans[f"scan{scan_number}"] = {
        "type": "tomo:tiling:front",
        "vertical_range": [zstart, zstop],
        "vertical_range@units": "mm",
        "vertical_nb_frames": interval_z + 1,
        "horizontal_range": [ystart, ystop],
        "horizontal_range@units": "mm",
        "horizontal_nb_frames": front_info.interval + 1,
        "y_axis_name": front_info.y_axis_name,
        "z_axis_name": z_axis_name,
        "continuous": continuous,
    }

    if side_info.sample_y_axis_name is not None:
        subscans["scan3"]["sample_y_axis_name"] = side_info.sample_y_axis_name
    if front_info.sample_y_axis_name is not None:
        subscans[f"scan{scan_number}"][
            "sample_y_axis_name"
        ] = front_info.sample_y_axis_name

    technique = {
        "rotation_axis_name": hdf5_axis_name(tomoconfig.rotation_axis, False),
        "rotation_offset": tomoconfig.rotation_axis.offset,
        "rotation_offset@units": "deg",
        "rotation_sign": tomoconfig.rotation_axis.sign,
        "exposure_time": expo_time,
        "exposure_time@units": "s",
        "detector_name": detector.name,
        "detector_size": tomo_detector.actual_size,
        "sample_pixel_size": tomo_detector.sample_pixel_size,
        "sample_pixel_size@units": "um",
        "subscans": subscans,
    }
    detector_center = sample_stage.detector_center
    if detector_center[0]:
        technique["detector_center_y"] = detector_center[0]
        technique["detector_center_y@units"] = sample_stage.y_axis.unit
    if detector_center[1]:
        technique["detector_center_z"] = detector_center[1]
        technique["detector_center_z@units"] = sample_stage.z_axis.unit

    scan_info = {"tomotype": "tilingseq", "technique": technique}
    sequence = Sequence(title="tiling sequence", scan_info=scan_info)

    if restore_motor_positions:
        involved_motors = [
            tomoconfig.rotation_axis,
            tomoconfig.y_axis,
            tomoconfig.z_axis,
        ]
        if tomoconfig.sample_u_axis is not None:
            involved_motors += [tomoconfig.sample_u_axis]
        if tomoconfig.sample_v_axis is not None:
            involved_motors += [tomoconfig.sample_v_axis]
        restore_motors_context = cleanup.cleanup(
            *involved_motors, restore_list=(cleanup.axis.POS,)
        )
    else:
        restore_motors_context = None

    with contextlib.ExitStack() as stack:
        stack.enter_context(tomoconfig.auto_projection.inhibit())
        stack.enter_context(
            lock_context(
                tomo_detector.detector, tomoconfig.rotation_axis, owner="tomotiling"
            )
        )
        for mh in tomoconfig.rotation_axis.motion_hooks:
            # The rotation was locked by the sequence, we have to
            # disable it for LockedAxisMotionHook because it is not reentrant
            if isinstance(mh, LockedAxisMotionHook):
                stack.enter_context(mh.disabled_context())

        if restore_motors_context is not None:
            stack.enter_context(restore_motors_context)

        with sequence.sequence_context() as scan_seq:
            dark_runner = tomoconfig.get_runner("dark")
            # FIXME: Passing technique here is a workaround to make it available early in the file
            # See https://gitlab.esrf.fr/bliss/bliss/-/issues/3390
            with dark_runner.disabled_locking_detector_context():
                scan = dark_runner(
                    expo_time,
                    n_dark,
                    detector,
                    run=False,
                    # NOTE: Make sure to clock the technique, else it deadlock
                    # Because the runner will change the content of the technique
                    scan_info={"technique": dict(technique)},
                )
                scan_seq.add_and_run(scan)

            flat_runner = tomoconfig.get_runner("flat")
            with flat_runner.disabled_locking_detector_context():
                scan = flat_runner(expo_time, n_flat, detector, run=False)
                scan_seq.add_and_run(scan)

            tiling_runner = tomoconfig.get_runner("tiling")
            with tiling_runner.disabled_locking_detector_context():
                with save_nframes_per_file(detector, NFRAMES_PER_FILE):
                    scan = tiling_runner(
                        expo_time=expo_time,
                        tomo_detector=tomo_detector,
                        zstart=zstart,
                        zstop=zstop,
                        ystart=xstart,
                        ystop=xstop,
                        sleep_time=sleep_time,
                        lateral_motor_mode=lateral_motor_mode,
                        presets=[side_rotation],
                        continuous=continuous,
                        speed_ratio=speed_ratio,
                        run=False,
                    )
                    with run_independent_presets(scan):
                        scan_seq.add_and_run(scan)

            if intermediate_flat_scan:
                flat_runner = tomoconfig.get_runner("flat")
                with flat_runner.disabled_locking_detector_context():
                    scan = flat_runner(expo_time, n_flat, detector, run=False)
                    scan_seq.add_and_run(scan)

            tiling_runner = tomoconfig.get_runner("tiling")
            with tiling_runner.disabled_locking_detector_context():
                with save_nframes_per_file(detector, NFRAMES_PER_FILE):
                    scan = tiling_runner(
                        expo_time=expo_time,
                        tomo_detector=tomo_detector,
                        zstart=zstart,
                        zstop=zstop,
                        ystart=ystart,
                        ystop=ystop,
                        sleep_time=sleep_time,
                        presets=[front_rotation],
                        lateral_motor_mode=lateral_motor_mode,
                        continuous=continuous,
                        speed_ratio=speed_ratio,
                        run=False,
                    )
                    with run_independent_presets(scan):
                        scan_seq.add_and_run(scan)

    return sequence.scan
