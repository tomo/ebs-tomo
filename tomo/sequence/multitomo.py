from bliss.common.scans import ascan
from bliss import current_session
from bliss import setup_globals
from bliss.common.logtools import log_info, log_debug, log_error
from fscan.fscantools import FScanParamBase, FScanModeBase
from tomo.sequencebasic import SequenceBasic, SequenceBasicPars, ScanType
from tomo.helpers.proxy_param import ProxyParam
from bliss.scanning.scan_info import ScanInfo
from bliss.controllers.motors.elmo import Elmo
from bliss.controllers.motors.elmo_whistle import Elmo_whistle
from bliss.controllers.motors.icepap import Icepap
from tomo.sequence.presets.inhibit_auto_projection import InhibitAutoProjection
from bliss.common.cleanup import cleanup, axis as cleanup_axis, capture_exceptions
from bliss.shell.dialog.helpers import dialog
from tomo.sequence.presets.user_info_preset import UserInfoPreset

SequenceTriggerMode = FScanModeBase("SequenceTriggerMode", "INTERNAL", "EXTERNAL")
LoopMode = FScanModeBase("LoopMode", "INTERNAL", "EXTERNAL", "LIMA_READY")

ShutterMode = FScanModeBase("ShutterMode", "SYNCHRONIZED", "SOFT")


class MultiTurnsTomoPars(SequenceBasicPars):
    DEFAULT = dict(
        **SequenceBasicPars.DEFAULT,
        **{
            "sequence_trigger_mode": SequenceTriggerMode.INTERNAL,
            "ntomo": 1,
            "tomo_loop": 1,
            "waiting_turns": 0,
            "start_turns": 0,
            "shutter_mode": ShutterMode.SYNCHRONIZED,
            "shutter_time": 0,
            "loop_mode": LoopMode.INTERNAL,
        }
    )
    LISTVAL = dict(
        **SequenceBasicPars.LISTVAL,
        **{
            "loop_mode": LoopMode.values,
            "shutter_mode": ShutterMode.values,
            "sequence_trigger_mode": SequenceTriggerMode.values,
        }
    )
    OBJKEYS = [*SequenceBasicPars.OBJKEYS]
    NOSETTINGS = [*SequenceBasicPars.NOSETTINGS]

    def _validate_loop_mode(self, value):
        return LoopMode.get(value, "loop_mode")

    def _validate_shutter_mode(self, value):
        return ShutterMode.get(value, "shutter_mode")


class MultiTurnsTomo(SequenceBasic):
    """
    Class to handle multiple turns tomo acquisition

    **Attributes**

    name : str
        The Bliss object name
    """

    def __init__(self, name, config):
        self.name = name + "_sequence"
        super().__init__(name, config)
        self._sequence_name = "multitomo:basic"
        inhibitautoproj = InhibitAutoProjection(self._tomo_config)
        self.add_sequence_preset(inhibitautoproj)

    def _create_parameters(self):
        tomo_config = self.tomo_config
        sequence_pars = MultiTurnsTomoPars(self.name)
        return ProxyParam(tomo_config.pars, sequence_pars)

    def validate(self):
        """
        Check parameters coherency
        Estimate sequence acquisition time
        """

        self._inpars.end_pos = self.pars.start_pos + self.pars.range
        self._inpars.field_of_view = "Full"

        if self.pars.half_acquisition:
            self._inpars.field_of_view = "Half"

        darks_in_sequence = [
            self._scans[i] for i, name in enumerate(self._scans_names) if name == "dark"
        ]
        flats_in_sequence = [
            self._scans[i] for i, name in enumerate(self._scans_names) if name == "flat"
        ]
        returns_in_sequence = [
            self._scans[i]
            for i, name in enumerate(self._scans_names)
            if name == "return_ref"
        ]
        projs_in_sequence = [self._scans[i] for i, name in enumerate(self._scans_names)]

        nb_proj_scan = len(projs_in_sequence)
        nb_dark_scan = len(darks_in_sequence)
        nb_flat_scan = len(flats_in_sequence)
        nb_return_scan = len(returns_in_sequence)

        proj_time = nb_proj_scan * self._inpars.proj_time
        dark_time = nb_dark_scan * self.pars.dark_n * self.pars.exposure_time
        flat_runner = self.get_runner("flat")
        flat_time = nb_flat_scan * flat_runner._estimate_scan_duration(self._inpars)

        if self.pars.return_images_aligned_to_flats:
            disp = self.pars.flat_on * self.pars.range / self.pars.tomo_n
        else:
            disp = 90
        return_runner = self.get_runner("return_ref")
        return_time = nb_return_scan * return_runner._estimate_scan_duration(
            self._inpars
        )

        self._inpars.scan_time = dark_time + flat_time + proj_time + return_time

    def add_metadata(self, scan_info):
        """
        Fill the scan_info dictionary with the basic tomo meta data
        Add lateral motor name to tomo meta data
        """
        super().add_metadata(scan_info)
        scan_info["technique"]["scan"]["sequence"] = "tomo:multitomo"
        scan_info["technique"]["scan"]["nb_turns"] = self.pars.tomo_loop
        scan_info["technique"]["scan"]["waiting_turns"] = self.pars.waiting_turns
        scan_info["technique"]["scan"]["start_turns"] = self.pars.start_turns
        scan_info["technique"]["scan"]["loop_mode"] = self.pars.loop_mode
        scan_info["technique"]["scan"]["shutter_mode"] = self.pars.shutter_mode

    def projection_scan(
        self, motor, start_pos, end_pos, tomo_n, expo_time, scan_info=None, run=True
    ):
        """
        Get runner and build scan
        Return projection scan object
        """
        runner = self.get_runner("MULTITURNS")

        if self.pars.shutter_mode == "SOFT":
            runner.pars.shutter_time = 0
        else:
            runner.pars.shutter_time = self.pars.shutter_time
        latency_time = self.pars.latency_time
        tomo_loop = self.pars.tomo_loop
        waiting_angle = self.pars.waiting_turns * (end_pos - start_pos)
        start_turns = self.pars.start_turns * 360
        start_pos += start_turns
        end_pos = start_pos + self.pars.range * self.pars.ntomo
        tomo_n = self.pars.tomo_n * self.pars.ntomo
        loop_mode = self.pars.loop_mode
        runner.pars.start_trig_mode = self.pars.sequence_trigger_mode
        tomo_scan = runner(
            motor,
            start_pos,
            end_pos,
            tomo_n,
            expo_time,
            tomo_loop,
            self.pars.range,
            waiting_angle=waiting_angle,
            loop_mode=loop_mode,
            latency_time=latency_time,
            scan_info=scan_info,
            run=False,
        )

        self._inpars.proj_time = runner._estimate_scan_duration(self._inpars)

        userinfopreset = UserInfoPreset(runner.pars.start_trig_mode)
        tomo_scan.add_preset(userinfopreset)

        if run:
            tomo_scan.run()

        return tomo_scan

    def full_turn_scan(
        self,
        ntomo=None,
        tomo_loop=None,
        dataset_name=None,
        collection_name=None,
        start_pos=None,
        waiting_turns=None,
        loop_mode=None,
        start_turns=None,
        scan_info=None,
        run=True,
    ):
        """
        Build and run full field tomo of 360 angular range
        """
        if dataset_name is not None:
            setup_globals.newdataset(dataset_name)
        if collection_name is not None:
            setup_globals.newcollection(collection_name)
        if start_pos is not None:
            self.pars.start_pos = start_pos
        if ntomo is not None:
            self.pars.ntomo = ntomo
        if tomo_loop is not None:
            self.pars.tomo_loop = tomo_loop
        if waiting_turns is not None:
            self.pars.waiting_turns = waiting_turns
        if loop_mode is not None:
            self.pars.loop_mode = loop_mode
        if start_turns is not None:
            self.pars.start_turns = start_turns

        self.pars.range = 360

        self._setup_sequence("multitomo:fullturn", run=run, scan_info=scan_info)

    def half_turn_scan(
        self,
        ntomo=None,
        tomo_loop=None,
        dataset_name=None,
        collection_name=None,
        start_pos=None,
        waiting_turns=None,
        loop_mode=None,
        start_turns=None,
        scan_info=None,
        run=True,
    ):
        """
        Build and run full field tomo of 180 angular range
        """
        if dataset_name is not None:
            setup_globals.newdataset(dataset_name)
        if collection_name is not None:
            setup_globals.newcollection(collection_name)
        if start_pos is not None:
            self.pars.start_pos = start_pos
        if ntomo is not None:
            self.pars.ntomo = ntomo
        if tomo_loop is not None:
            self.pars.tomo_loop = tomo_loop
        if waiting_turns is not None:
            self.pars.waiting_turns = waiting_turns
        if loop_mode is not None:
            self.pars.loop_mode = loop_mode
        if start_turns is not None:
            self.pars.start_turns = start_turns

        self.pars.range = 180

        self._setup_sequence("multitomo:halfturn", run=run, scan_info=scan_info)

    def basic_scan(
        self,
        ntomo=None,
        tomo_loop=None,
        start_pos=None,
        end_pos=None,
        tomo_n=None,
        expo_time=None,
        dataset_name=None,
        collection_name=None,
        waiting_turns=None,
        loop_mode=None,
        start_turns=None,
        scan_info=None,
        run=True,
    ):
        """
        Build and run full field tomo using given parameters
        """

        if dataset_name is not None:
            setup_globals.newdataset(dataset_name)
        if collection_name is not None:
            setup_globals.newcollection(collection_name)
        if start_pos is not None:
            self.pars.start_pos = start_pos
        if end_pos is not None:
            self.pars.range = self.pars.start_pos + end_pos
        if tomo_n is not None:
            self.pars.tomo_n = tomo_n
        if expo_time is not None:
            self.pars.exposure_time = expo_time
        if ntomo is not None:
            self.pars.ntomo = ntomo
        if tomo_loop is not None:
            self.pars.tomo_loop = tomo_loop
        if waiting_turns is not None:
            self.pars.waiting_turns = waiting_turns
        if loop_mode is not None:
            self.pars.loop_mode = loop_mode
        if start_turns is not None:
            self.pars.start_turns = start_turns

        self._setup_sequence("multitomo:basic", run=run, scan_info=scan_info)

    def build_sequence(self):
        """
        Build full field tomo sequence according to scan option
        parameters
        """
        if self.pars.dark_at_start:
            self.add_dark()
        if self.pars.flat_at_start:
            self.add_flat()
        if self.pars.projection_groups:
            self.add_projections_group(
                self.pars.start_pos,
                self.pars.start_pos + self.pars.range,
                self.pars.tomo_n,
                self.pars.exposure_time,
                flat_on=self.pars.flat_on,
            )
        else:
            self.add_proj(
                self.pars.start_pos,
                self.pars.start_pos + self.pars.range,
                self.pars.tomo_n,
                self.pars.exposure_time,
            )
        if self.pars.images_on_return:
            self.add_return()
        if self.pars.flat_at_end:
            self.add_flat()
        if self.pars.dark_at_end:
            self.add_dark()

    def _send_icat_metadata(self):
        scan_saving = current_session.scan_saving
        scan = self.get_runner("MULTITURNS")._fscanloop
        acc_disp = scan.inpars.acc_disp
        scan_saving.dataset.write_metadata_field("TOMOAcquisition_accel_disp", str(acc_disp))
        super()._send_icat_metadata()

    def _check_acc_turns(self):
        runner = self.get_runner("MULTITURNS")
        fscanloop = runner._fscanloop
        fscanloop.pars.motor = self._rotation_axis
        fscanloop.pars.step_size = self.pars.range / self.pars.tomo_n
        fscanloop.pars.acq_time = self.pars.exposure_time
        fscanloop.pars.step_time = fscanloop.pars.acq_time
        fscanloop.pars.acc_margin = 0
        fscanloop.validate()
        min_acc_turns = int(fscanloop.inpars.acc_disp / 360) + 1
        return min_acc_turns

    def set_min_acc_turns(self):
        min_acc_turns = self._check_acc_turns()
        self.pars.start_turns = min_acc_turns


# Setup the standard BLISS `menu`
@dialog(MultiTurnsTomo.__name__, "setup")
def _setup(sequence):
    from tomo.utils import setup

    setup(sequence)
