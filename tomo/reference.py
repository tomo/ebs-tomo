# Compatibility with tomo <= 2.3.1

from tomo.controllers.reference import TomoRefMot  # noqa
from bliss.common import deprecation

deprecation.deprecated_warning(
    "Class",
    "tomo.reference.TomoRefMot",
    replacement="tomo.controllers.flat_motion.FlatMotion",
    since_version="2.3.1",
)
