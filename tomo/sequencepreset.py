"""
Backward compatibility.

The module was moved to `tomo.sequence.presets.base` the 2022-08-12.

This will be removed sooner or later.
"""
from .sequence.presets.base import SequencePreset  # noqa
from bliss.common import deprecation

deprecation.deprecated_warning(
    "Class",
    "tomo.sequencepreset.SequencePreset",
    replacement="tomo.sequence.presets.base.SequencePreset",
    since_version="2.3.1",
)
