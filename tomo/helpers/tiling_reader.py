from __future__ import annotations
from contextlib import contextmanager
import functools
import logging
import pathlib
import re
from typing import Generator, NamedTuple, Optional, Tuple

import h5py
import numpy
import pint
from tomo.helpers import flatfield_correction

_logger = logging.getLogger(__name__)


def h5file_ro(filename: str | pathlib.Path):
    """Try to open hdf5 file without file locking"""
    try:
        return h5py.File(filename, mode="r", locking=False)
    except TypeError:
        return h5py.File(filename, mode="r")


class TilingInfo(NamedTuple):
    viewpoint: str  # "front", "left", "right"
    nb_frames: int
    horizontal_range: Tuple[float, float]
    """Horizontal range taking care of the detector center"""
    vertical_range: Tuple[float, float]
    """Vertical range taking care of the detector center"""
    sample_y_axis_name: str | None
    y_axis_name: str
    z_axis_name: str


def _read_as_quantity(dataset: h5py.Dataset, units: str) -> float:
    return pint.Quantity(dataset[()], dataset.attrs["units"]).to(units).magnitude


def _read_range_as_quantity(dataset: h5py.Dataset, units: str) -> [float, float]:
    return pint.Quantity(dataset[()], dataset.attrs["units"]).to(units).magnitude


def _read_tiling_info(
    group: h5py.Group, detector_center_y: float, detector_center_z: float
) -> TilingInfo:
    """Read tiling scan information from technique/subscans/ subgroup"""
    hrange = _read_range_as_quantity(group["horizontal_range"], "mm")
    horizontal_range = min(hrange) - detector_center_y, max(hrange) - detector_center_y

    vrange = _read_range_as_quantity(group["vertical_range"], "mm")
    vertical_range = min(vrange) - detector_center_z, max(vrange) - detector_center_z

    nb_frames = group["horizontal_nb_frames"][()] * group["vertical_nb_frames"][()]

    if "sample_y_axis_name" in group:
        sample_y_axis_name = group["sample_y_axis_name"].asstr()[()]
    else:
        sample_y_axis_name = None
    y_axis_name = group["y_axis_name"].asstr()[()]
    z_axis_name = group["z_axis_name"].asstr()[()]

    viewpoint = group["type"].asstr()[()].split(":")[-1]

    return TilingInfo(
        viewpoint,
        nb_frames,
        horizontal_range,
        vertical_range,
        sample_y_axis_name,
        y_axis_name,
        z_axis_name,
    )


class TechniqueInfo(NamedTuple):
    detector_name: str
    detector_size: numpy.ndarray
    pixel_size: float
    rotation_offset: float
    rotation_sign: int
    front: TilingInfo
    side: TilingInfo
    detector_center_y: float
    detector_center_z: float


def _read_technique_info(technique_group: h5py.Group) -> TechniqueInfo:
    """Read information from technique group"""
    pixel_size = _read_as_quantity(technique_group["sample_pixel_size"], "mm")
    rotation_offset = _read_as_quantity(technique_group["rotation_offset"], "deg")
    rotation_sign = technique_group["rotation_sign"][()]
    if "detector_center_y" in technique_group:
        detector_center_y = _read_as_quantity(
            technique_group["detector_center_y"], "mm"
        )
    else:
        detector_center_y = 0
    if "detector_center_z" in technique_group:
        detector_center_z = _read_as_quantity(
            technique_group["detector_center_z"], "mm"
        )
    else:
        detector_center_z = 0

    tiling_info = {}
    for scan_info in technique_group["subscans"].values():
        scan_type = scan_info["type"].asstr()[()]
        if scan_type.startswith("tomo:tiling:"):
            tiling_info[scan_type.split(":")[-1]] = _read_tiling_info(
                scan_info, detector_center_y, detector_center_z
            )

    side_viewpoint = "right" if "right" in tiling_info else "left"

    return TechniqueInfo(
        detector_name=technique_group["detector_name"].asstr()[()],
        detector_size=technique_group["detector_size"][()],
        pixel_size=pixel_size,
        rotation_offset=rotation_offset,
        rotation_sign=rotation_sign,
        front=tiling_info["front"],
        side=tiling_info[side_viewpoint],
        detector_center_y=detector_center_y,
        detector_center_z=detector_center_z,
    )


class LimaScanImageReader:
    """Class to iterate over images in Lima files during acquisition"""

    def __init__(self, scan_folder: pathlib.Path, detector_name: str, nb_frames: int):
        self.__scan_folder = scan_folder
        self.__detector_name = detector_name
        self.__nb_frames = nb_frames
        self.__current_file_index = 0
        self.__last_slice = -1
        self.__last_index = -1

    @staticmethod
    def scan_folder_from_group(group: h5py.Group) -> pathlib.Path:
        scan_number = int(group.name.split("/")[-1].split(".")[0])
        return pathlib.Path(group.file.filename).parent / f"scan{scan_number:04}"

    @property
    @contextmanager
    def __current_file(self) -> Generator[h5py.File, None, None]:
        """Context manager for current HDF5 file"""
        filename = f"{self.__detector_name}_{self.__current_file_index:04}.h5"
        with h5file_ro(self.__scan_folder / filename) as h5file:
            # Make sure file is complete before getting the first slice
            end_time = h5file["/entry_0000/end_time"].asstr()[()]
            if not end_time:
                raise RuntimeError(f"File {filename} is not finished")
            yield h5file

    @staticmethod
    def __instrument_group(entry: h5py.Group) -> Optional[h5py.Group]:
        for entity in entry.values():
            if (
                isinstance(entity, h5py.Group)
                and entity.attrs.get("NX_class").lower() == "nxinstrument"
            ):
                return entity
        raise RuntimeError(
            f"Cannot find NXinstrument group in {entry.file.filename}::{entry.name}"
        )

    def __read_slice_from_current_file(self) -> Optional[numpy.ndarray]:
        """Try to get next slice from current file.

        Returns None if the next slice is in the next file.
        """
        with self.__current_file as h5file:
            instrument = self.__instrument_group(h5file["/entry_0000"])
            dataset = instrument[f"{self.__detector_name}/data"]
            this_slice = self.__last_slice + 1
            if this_slice >= len(dataset):
                return None  # Go to next file
            data = dataset[this_slice]
        self.__last_slice = this_slice
        return data

    def __getitem__(self, index: int) -> numpy.ndarray:
        """Inefficient random access"""
        if index < 0 or index >= self.__nb_frames:
            raise IndexError()

        if index <= self.__last_index:
            # Poor implementation: restart from the beginning
            self.__current_file_index = 0
            self.__last_slice = -1
            self.__last_index = -1

        # Seek to previous image
        for i in range(index - (self.__last_index + 1)):
            self.next_image()
        return self.next_image()

    def next_image(self) -> numpy.ndarray:
        """Iterate over the image of the scan"""
        if self.__last_index >= self.__nb_frames:
            raise StopIteration()

        data = self.__read_slice_from_current_file()
        if data is None:  # Go to next file in the serie
            self.__current_file_index += 1
            self.__last_slice = -1
            data = self.__read_slice_from_current_file()
            if data is None:
                raise RuntimeError(
                    f"Cannot read first slice from next file {self.__current_file_index}"
                )
        self.__last_index += 1
        return data


class CorrectedImage(NamedTuple):
    image: numpy.ndarray
    horizontal_pos: float
    vertical_pos: float


class TiledPlane:
    """Accessor of a single 2D tiling scan"""

    def __init__(self, reader: TilingScanReader, name: str):
        self.__reader = reader
        self.__name = name
        self.__info: TilingInfo = getattr(self.__reader.info, self.__name)
        with self.__scan_group() as scan_group:
            scan_folder = LimaScanImageReader.scan_folder_from_group(scan_group)

        self.__lima_reader = LimaScanImageReader(
            scan_folder, self.__reader.info.detector_name, self.nb_images
        )

    @property
    def reader(self) -> TilingScanReader:
        return self.__reader

    @contextmanager
    def __scan_group(self) -> Generator[h5py.Group, None, None]:
        with self.reader.scan_group(self.__name) as scan_group:
            yield scan_group

    @property
    def detector_size(self) -> numpy.ndarray:
        return self.reader.info.detector_size

    @property
    def pixel_size(self) -> float:
        return self.reader.info.pixel_size

    @property
    def nb_images(self) -> int:
        """Expected number of projection images in the stack"""
        return self.__info.nb_frames

    @property
    def horizontal_range(self) -> Tuple[float, float]:
        """Range of horizontal locations of the whole tiled image (in millimeter)"""
        return self.__info.horizontal_range

    @property
    def vertical_range(self) -> Tuple[float, float]:
        """Range of vertical locations of the whole tiled image (in millimeter)"""
        return self.__info.vertical_range

    @property
    def rotation_offset(self) -> pint.Quantity:
        """Offset between dial and user rotation (in degrees)"""
        return self.__reader.info.rotation_offset

    @property
    def rotation_sign(self) -> int:
        """Orientation of the user rotation relative to dial rotation

        Values: 1 if same directions, -1 if opposite directions
        """
        return self.__reader.info.rotation_sign

    @property
    def viewpoint(self) -> str:
        """Viewpoint of the tiling scan: 'front', 'left' or 'right'"""
        return self.__info.viewpoint

    def __motor_position_in_mm(self, motor_name: str, index: int) -> float:
        with self.__scan_group() as scan_group:
            if motor_name in scan_group["instrument/positioners"].keys():
                dataset = scan_group[f"instrument/positioners/{motor_name}"]
            else:
                dataset = scan_group[f"instrument/{motor_name}/data"]
            value = dataset[index] if dataset.shape != () else dataset[()]
            units = dataset.attrs["units"]

        return pint.Quantity(value, units).to("mm").magnitude

    def position(self, index: int) -> tuple[float, float]:
        """Return (y, z) location of given projection image (in millimeter)"""
        if self.__info.sample_y_axis_name is not None:
            sample_y = self.__motor_position_in_mm(
                self.__info.sample_y_axis_name, index
            )
        else:
            sample_y = 0
        y = self.__motor_position_in_mm(self.__info.y_axis_name, index)
        z = self.__motor_position_in_mm(self.__info.z_axis_name, index)
        detcy = self.__reader.info.detector_center_y
        detcz = self.__reader.info.detector_center_z
        return sample_y + y - detcy, z - detcz

    def image(self, index: int, dtype: numpy.DType = numpy.float32) -> numpy.ndarray:
        """Return requested projection image"""
        return self.__lima_reader[index].astype(dtype)

    def corrected_image(
        self, index: int, dtype: numpy.DType = numpy.float32
    ) -> CorrectedImage:
        """Return requested flat-field corrected image and position"""
        dark = self.reader.dark(dtype)
        corrected_flat = self.reader.corrected_flat(self.__name, dtype)
        with numpy.errstate(divide="ignore"):
            image = (self.image(index, dtype) - dark) / corrected_flat
        # Make sure everything looks like a valid count
        image[numpy.logical_not(numpy.isfinite(image))] = 0
        return CorrectedImage(image, *self.position(index))


class TilingScanReader:
    """Connector to a master HDF5 file containing tiling scan sequence.

    This is designed to work with the NexusWriter. Other writer could generate
    a totally different data structure.

    This is meant to work during the acquisition, but accessing some information
    might raise exceptions until it is available in the file.
    """

    def __init__(self, filename: str, sequence_path: str):
        self.__filename = filename

        self.__sequence_path = sequence_path
        if "_" in sequence_path:
            path_prefix = sequence_path.rsplit("_", 1)[0] + "_"
        else:
            path_prefix = ""
        self.__path_prefix = path_prefix
        self.__sequence_scan_index = int(
            sequence_path[len(path_prefix) :].split(".", 1)[0]
        )

    @property
    def filename(self) -> str:
        return self.__filename

    @property
    def sequence_path(self) -> str:
        return self.__sequence_path

    @property
    def is_finished(self) -> bool:
        """True if scan sequence is finished, False otherwise."""
        try:
            with self.__h5file() as h5file:
                if h5file[self.sequence_path + "/end_time"][()]:
                    return True
        except BaseException:
            pass
        return False

    @contextmanager
    def __h5file(self) -> Generator[h5py.File, None, None]:
        with h5file_ro(self.filename) as h5file:
            yield h5file

    @property
    @functools.lru_cache(None)
    def info(self) -> TechniqueInfo:
        """Information about the tiling scans.

        It tries to access the scan containing the "technique" group and
        raises an exception if this scan is not finished.
        """
        with self._finished_scan_group("technique") as scan_group:
            return _read_technique_info(scan_group["technique"])

    @functools.lru_cache()
    def __get_scan_indices(self) -> dict[str, int]:
        """Returns scan type to scan index mapping"""
        indices = {}
        flat_indices = []
        with self.__h5file() as h5file:
            sequence_group = h5file[self.sequence_path]
            subscans_group = sequence_group["technique/subscans"]
            for key, group in subscans_group.items():
                match = re.fullmatch(r"scan(?P<index>\d+)", key)
                if match is None or not isinstance(group, h5py.Group):
                    _logger.info(f"Subscan {key} ignored")
                    continue
                # Offset to account for the sequence scan
                scan_index = int(match["index"]) + self.__sequence_scan_index

                scan_type_group = group.get("type")
                if not isinstance(scan_type_group, h5py.Dataset):
                    _logger.info(f"Subscan {key} ignored, no type found")
                    continue

                # Convert from scan type stored in hdf5 to kind used here
                scan_kind = {
                    "tomo:dark": "dark",
                    "tomo:flat": "flat",
                    "tomo:tiling:front": "front",
                    "tomo:tiling:left": "side",
                    "tomo:tiling:right": "side",
                }.get(scan_type_group.asstr()[()])
                if scan_kind is None:
                    _logger.info(f"Subscan {key} ignored, unknown type")
                    continue

                if scan_kind == "flat":
                    flat_indices.append(scan_index)
                else:
                    indices[scan_kind] = scan_index

        # Use dark scan to store "technique"
        # to workaround scan_info being written at the end of the sequence
        # https://gitlab.esrf.fr/bliss/bliss/-/issues/3390
        indices["technique"] = indices["dark"]

        # Set flat indices last to support 0, 1 or 2 flat scans
        indices["flat_side"] = flat_indices[0] if flat_indices else None
        indices["flat_front"] = flat_indices[-1] if flat_indices else None
        return indices

    @functools.lru_cache(None)
    def __get_scan_path(self, kind: str) -> str:
        """Return scan path of given sub scan name"""
        scan_number = self.__get_scan_indices()[kind]
        return f"{self.__path_prefix}{scan_number}.1"

    @contextmanager
    def scan_group(self, kind: str) -> Generator[h5py.Group, None, None]:
        """Context providing the HDF5 group of the given scan."""
        with self.__h5file() as h5file:
            yield h5file[self.__get_scan_path(kind)]

    @contextmanager
    def _finished_scan_group(self, kind: str) -> Generator[h5py.Group, None, None]:
        """ "Context providing the HDF5 group of the given scan.

        Check that the scan is finished and raise an exception if not.
        """
        with self.scan_group(kind) as scan:
            end_time = scan["end_time"][()]
            if not end_time:
                raise RuntimeError("Scan not finished")
            yield scan

    def __get_tiled_plane(self, name: str) -> TiledPlane:
        # Make sure scan has started
        try:
            with self.scan_group(name) as scan_group:
                start_time = scan_group["start_time"].asstr()[()]
        except Exception as e:
            _logger.warning(f"Tiling scan {name} not started")
            raise e
        if not start_time:
            raise RuntimeError(f"Tiling scan {name} not started")
        return TiledPlane(self, name)

    @property
    @functools.lru_cache(None)
    def front(self) -> TiledPlane:
        """Access front tiling scan.

        This can only be retrieved once the scan has started.
        """
        return self.__get_tiled_plane("front")

    @property
    @functools.lru_cache(None)
    def side(self) -> TiledPlane:
        """Access side tiling scan.

        This can only be retrieved once the scan has started.
        """
        return self.__get_tiled_plane("side")

    @functools.lru_cache(None)
    def dark(self, dtype: numpy.dtype) -> numpy.ndarray:
        """Dark used for flat field correction"""
        with self._finished_scan_group("dark") as dark_group:
            darks = dark_group[f"measurement/{self.info.detector_name}"][()]
        return flatfield_correction.compute_dark(darks, dtype=dtype)

    @functools.lru_cache(None)
    def flat(self, name: str, dtype=numpy.dtype) -> numpy.ndarray:
        """Flat used for flat field correction"""
        with self._finished_scan_group(f"flat_{name}") as flat_group:
            flats = flat_group[f"measurement/{self.info.detector_name}"][()]
        return flatfield_correction.compute_flat(flats, inplace=True, dtype=dtype)

    @functools.lru_cache(None)
    def corrected_flat(self, name: str, dtype: numpy.dtype) -> numpy.ndarray:
        """Flat-field correction denominator: (flat - dark) for given tiling scan"""
        return self.flat(name, dtype) - self.dark(dtype)
