from typing import Optional
import contextlib

from bliss.common.measurementgroup import get_active as get_active_mg
from bliss.common.measurementgroup import MeasurementGroup
from bliss.scanning.toolbox import get_all_counters


@contextlib.contextmanager
def use_counters_with_fscan(fscanrunner, *counter_args):
    """
    Setup an active MG to use, and patch the fscan master config if needed.

    This changes are restored at the context exit.
    """
    previous_mg = get_active_mg()
    config_counters = fscanrunner.master.config.get("counters", {}).get("measgroup")
    try:
        mg = MeasurementGroup(
            f"fscanrunner_mg:{id(fscanrunner)}",
            {"counters": get_all_counters(counter_args)},
        )
        if config_counters is not None:
            fscanrunner.master.config.get["counters"]["measgroup"] = mg
        else:
            mg.set_active()
        yield
    finally:
        if config_counters is not None:
            fscanrunner.master.config.get["counters"]["measgroup"] = config_counters
        else:
            previous_mg.set_active()
    return fscanrunner.scan


def fscan_as_bliss_scan(
    fscanrunner,
    *counter_args,
    scan_info: Optional[dict] = None,
    run: bool = True,
    title: Optional[str] = None,
):
    """Allow to use a fscanrunner as a BLISS scan.

    Common BLISS scan attributes was added, and a list of counters can be
    specified.

    Returns a default BLISS scan.
    """
    if title:
        if scan_info is None:
            scan_info = {}
        scan_info["title"] = title
    with use_counters_with_fscan(fscanrunner, *counter_args):
        fscanrunner.prepare(scan_info=scan_info)
    scan = fscanrunner.scan
    if run:
        scan.run()
    return scan
