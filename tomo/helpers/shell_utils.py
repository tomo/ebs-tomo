import sys


def is_in_shell():
    """The process was launched by a shell.

    It means it's not Daiquiri server.
    """
    webshell = (
        "/bin/bliss" in sys.argv[0]
        and "--server" in sys.argv
        and "--no-tmux" not in sys.argv
    )
    # BLISS <= 2.0
    shell = "start_bliss_repl" in sys.argv[0] or (
        "/bin/bliss" in sys.argv[0] and "--no-tmux" in sys.argv
    )
    # BLISS >= 2.1
    shell = shell or "bliss/shell/main" in sys.argv[0]
    return webshell or shell
