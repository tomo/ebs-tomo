import contextlib
from typing import Optional, List
from bliss.common.scans import DEFAULT_CHAIN
from bliss.controllers.lima.lima_base import Lima
from bliss.scanning.chain import AcquisitionChain


class TomoDefaultChain:
    """Setup the default chain according to the detectors and the scans.

    For now it uses the configuration from the active tomo config.
    """

    def __init__(self):
        from ..globals import ACTIVE_TOMOCONFIG

        tomoconfig = ACTIVE_TOMOCONFIG.deref_active_object()

        self._default_single_chain: AcquisitionChain = tomoconfig.default_single_chain
        self._default_acc_chain: AcquisitionChain = tomoconfig.default_acc_chain
        self._slow_single_chain: AcquisitionChain = tomoconfig.default_slow_single_chain
        self._slow_acc_chain: AcquisitionChain = tomoconfig.default_slow_acc_chain

    def _apply_acq_mode(self, detector: Lima):
        from ..globals import ACTIVE_TOMOCONFIG

        tomoconfig = ACTIVE_TOMOCONFIG.deref_active_object()
        if tomoconfig is None:
            return None
        tomo_detector = tomoconfig.detectors.get_tomo_detector(detector)
        tomo_detector.apply_acq_mode()

    @contextlib.contextmanager
    def setup_detectors_in_default_chain(
        self, detectors: List[Lima], slow_scan: bool = False
    ):
        """
        Context to setup the default chain based on default settings.

        The default chain is restored at the termination.

        Arguments:
            detectors: List of detectors to setup
            slow_scan: If the scan is slow, use the slow configuration, else fallback to the default
        """
        previous_settings = DEFAULT_CHAIN._settings

        def get_patch_setting(device, chains) -> Optional[dict]:
            for chain in chains:
                if chain is None:
                    continue
                for config in chain["chain_config"]:
                    if config["device"] is device:
                        return config
            return None

        if slow_scan:
            single_chain = [self._slow_single_chain, self._default_single_chain]
            acc_chain = [self._slow_acc_chain, self._default_acc_chain]
        else:
            single_chain = [self._default_single_chain]
            acc_chain = [self._default_acc_chain]

        patches = []
        for detector in detectors:
            self._apply_acq_mode(detector)
            if detector.acquisition.mode == "ACCUMULATION":
                patch = get_patch_setting(detector, acc_chain)
                if patch is None:
                    raise ValueError(
                        "Detector '%s' is in accumulation mode, but no accumulation settings was found in any chain settings",
                        detector.name,
                    )
            else:
                patch = get_patch_setting(detector, single_chain)
            if patch is not None:
                patches.append(patch)
        if patches != []:
            DEFAULT_CHAIN.set_settings(patches)

        try:
            yield
        finally:
            DEFAULT_CHAIN._settings = previous_settings
