from bliss.common.logtools import log_warning
from bliss.common.axis import Axis


def is_user_clockwise(axis: Axis) -> bool:
    """True if the axis is clockwise. Else assume it is antoclockwise.

    If the dial/user sign of the axis changes, the result will change too.

    It is based on user tags `dial.clockwise` and `dial.anticlockwise` which
    can be specified in the beacon configuration files. If not specified, an
    axis is consisdered as clockwise.

    .. code-block:: yaml

        - name: my_axis
            unit: mm
            user_tag:
            - dial.clockwise
    """
    c = axis.config
    user_tags = c.get("user_tag", None, [])
    clockwise_tag = "dial.clockwise" in user_tags
    anticlockwise_tag = "dial.anticlockwise" in user_tags
    if clockwise_tag and anticlockwise_tag:
        log_warning(
            axis,
            "Only a single dial.clockwise or dial.anticlockwise have to be defined",
        )
    inverted = axis.sign < 0
    clockwise = clockwise_tag or not anticlockwise_tag
    return clockwise != inverted
