import numpy as np
import fabio

from bliss import current_session
from bliss.config.settings import SimpleSetting, QueueSetting
from bliss.common.scans import ct
from bliss.common.logtools import log_info


class ImageCorrection:
    def __init__(self, name, config):
        self.__name = name
        self.__config = config

        self.__dark_image = None
        self.__flat_image = None
        self.__flat_current = SimpleSetting("flat_current", default_value=None)
        self.__image_roi = QueueSetting("image_roi")

        self.__tomo = None

    @property
    def name(self):
        """
        A unique name for the Bliss object
        """
        return self.__name

    @property
    def config(self):
        """
        The configuration of Ar Gas Valve
        """
        return self.__config

    def set_tomo(self, tomo):
        self.__tomo = tomo
        self.__tomo.init_sequence()

    def verify_active_detector(func):
        def f(self, *args, **kwargs):
            last_detector = self.__tomo._detector
            current_detector = self.__tomo.get_controller_lima()
            # if detector changes, reset dark and flat images
            if current_detector != last_detector:
                log_info(self, "dark and flat images are going to be erased")
                self.__tomo._detector = current_detector
                self.reset()
            return func(self, *args, **kwargs)

        return f

    def verify_tomo(func):
        def f(self, *args, **kwargs):
            if self.__tomo is None:
                raise RuntimeError(
                    "Tomo not set. Please enter tomo object to use with set_tomo()"
                )
            return func(self, *args, **kwargs)

        return f

    @property
    def detector(self):
        if self.__tomo is not None:
            return self.__tomo._detector

    @property
    def dark_is_on(self):
        return self.detector.processing.use_background

    @property
    def flat_is_on(self):
        return self.detector.processing.use_flatfield

    @property
    def dark_image(self):
        if self.detector.processing.background != "":
            return self.detector.processing.background
        else:
            raise Exception(
                "No dark image has been recorded. Please do image_corr.take_dark() first"
            )

    @property
    def flat_image(self):
        if self.detector.processing.flatfield != "":
            return self.detector.processing.flatfield
        else:
            raise Exception(
                "No flat image has been recorded. Please do image_corr.take_flat() first"
            )

    @property
    def flat_current(self):
        if self.__flat_current.get() is not None and self.flat_image != "":
            return self.__flat_current.get()
        else:
            raise Exception(
                "No flat image has been recorded. Please do image_corr.take_flat() first"
            )

    @property
    def image_roi(self):
        if self.__image_roi.get() != []:
            return self.__image_roi.get()
        else:
            raise Exception(
                "No flat or dark image has been recorded. Please do image_corr.take_flat() or image_corr.take_dark() first"
            )

    @verify_active_detector
    @verify_tomo
    def reset(self):
        self.__tomo._detector.processing.background = ""
        self.__tomo._detector.processing.flatfield = ""
        self.__tomo._detector.processing.use_background = False
        self.__tomo._detector.processing.use_flatfield = False
        self.__image_roi.set(None)

    @verify_active_detector
    @verify_tomo
    def take_dark(self):
        self.__dark_image = None
        exposure_time = self.__tomo.pars.exposure_time

        file_format = self.__tomo._detector.saving.file_format
        self.__tomo._detector.saving.file_format = "EDF"

        try:
            self.dark_off()

            # take dark image
            dark_runner = self.__tomo.get_runner("dark")
            dark_scan = dark_runner(exposure_time, self.__tomo.pars.dark_n, save=True)
            dark_images = dark_scan.streams[self.detector.name + ":image"][:]
            dark_mean = np.array(
                np.mean(dark_images, axis=0), dtype=np.array(dark_images).dtype
            )

            image_path = (
                current_session.scan_saving.get_path()
                + "/"
                + "scan"
                + f"{current_session.scans[-1].scan_number}"
                + "/"
                + self.__tomo._detector.proxy.saving_prefix
                + "mean"
                + self.__tomo._detector.proxy.saving_suffix
            )
            image = fabio.edfimage.EdfImage(data=dark_mean)
            image.write(image_path)

            image_path = (
                self.__tomo._detector.proxy.saving_directory
                + "/"
                + self.__tomo._detector.proxy.saving_prefix
                + "mean"
                + self.__tomo._detector.proxy.saving_suffix
            )

            self.__dark_image = image_path

        except BaseException:
            self.__tomo._detector.saving.file_format = file_format
            raise

        self.__image_roi.set(self.__tomo._detector.image.roi)
        self.__tomo._detector.saving.file_format = file_format

        self.__tomo._detector.processing.background = self.__dark_image
        print(f"Background image = {self.__dark_image}")

    @verify_active_detector
    @verify_tomo
    def take_flat(self):
        self.__flat_image = None
        exposure_time = self.__tomo.pars.exposure_time

        file_format = self.__tomo._detector.saving.file_format
        self.__tomo._detector.saving.file_format = "EDF"

        try:
            self.flat_off()

            # take flat image
            # update reference motor position to current values
            self.__tomo._reference.update_in_beam_position()
            flat_runner = self.__tomo.get_runner("flat")
            flat_scan = flat_runner(exposure_time, self.__tomo.pars.flat_n, save=True)
            # shopen()
            # self.__tomo._reference.move_out()
            # flat_scan = sct(exposure_time)
            self.__flat_current.set(flat_scan.streams["machinfo:current"][-1])
            # self.__tomo._reference.move_in()
            flat_images = flat_scan.streams[self.detector.name + ":image"][:]
            flat_mean = np.array(
                np.mean(flat_images, axis=0), dtype=np.array(flat_images).dtype
            )

            image_path = (
                current_session.scan_saving.get_path()
                + "/"
                + "scan"
                + f"{current_session.scans[-1].scan_number}"
                + "/"
                + self.__tomo._detector.proxy.saving_prefix
                + "mean"
                + self.__tomo._detector.proxy.saving_suffix
            )
            image = fabio.edfimage.EdfImage(data=flat_mean)
            image.write(image_path)

            image_path = (
                self.__tomo._detector.proxy.saving_directory
                + "/"
                + self.__tomo._detector.proxy.saving_prefix
                + "mean"
                + self.__tomo._detector.proxy.saving_suffix
            )

            self.__flat_image = image_path
            self.flat_off()
        except BaseException:
            self.__tomo._detector.saving.file_format = file_format
            raise

        self.__image_roi.set(self.__tomo._detector.image.roi)
        self.__tomo._detector.saving.file_format = file_format

        self.__tomo._detector.processing.flatfield = self.__flat_image
        print(f"Flatfield  image = {self.__flat_image}")

    @verify_active_detector
    @verify_tomo
    def dark_on(self):
        if (
            self.__tomo._detector.processing.background is None
            or self.__tomo._detector.processing.background == ""
        ):
            raise RuntimeError("Cannot switch on! Missing image for dark")

        self.__tomo._detector.processing.use_background = True

    @verify_active_detector
    @verify_tomo
    def dark_off(self):
        self.__tomo._detector.processing.use_background = False

        # be sure that it is applied
        ct(self.__tomo.pars.exposure_time)

    @verify_active_detector
    @verify_tomo
    def flat_on(self, normalize=True):
        if (
            self.__tomo._detector.processing.flatfield is None
            or self.__tomo._detector.processing.flatfield == ""
        ):
            raise RuntimeError("Cannot switch on! Missing image for flat")

        # set normaization
        # start
        self.__tomo._detector.processing.flatfield_normalize = normalize
        self.__tomo._detector.processing.use_flatfield = True

    @verify_active_detector
    @verify_tomo
    def flat_off(self):
        self.__tomo._detector.processing.flatfield_normalize = False
        self.__tomo._detector.processing.use_flatfield = False

        # be sure that it is applied
        ct(self.__tomo.pars.exposure_time)

    @verify_active_detector
    def __info__(self):
        background = "ON"
        flatfield = "ON"

        if self.__tomo is None:
            info_str = "tomo       : Not yet given. Please use set_tomo()\n"
            return info_str
        info_str = f"tomo       : {self.__tomo.name}\n"
        info_str += f"detector   : {self.__tomo._detector.name}\n"
        info_str += f"dark_image : {self.__tomo._detector.processing.background}\n"
        info_str += f"flat_image : {self.__tomo._detector.processing.flatfield}\n"

        if self.__tomo._detector.processing.use_background is False:
            background = "OFF"
        if self.__tomo._detector.processing.use_flatfield is False:
            flatfield = "OFF"
        info_str += f"use_background_substraction : {background}\n"
        info_str += f"use_flatfield               : {flatfield }\n"
        return info_str
