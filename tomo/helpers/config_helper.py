from bliss.config import static


def register_object(obj):
    """Allow to register an object into the cache in order to let it available.

    Mostly for Daiquiri usage.
    """
    config = static.get_config()
    config._name2instance[obj.name] = obj
