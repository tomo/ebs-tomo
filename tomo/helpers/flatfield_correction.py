from __future__ import annotations
import numpy


def compute_dark(
    darks: list[numpy.NdArray] | numpy.NdArray,
    dtype: numpy.DType | None = None,
) -> numpy.NdArray:
    """Compute a dark from a stack of darks"""
    res = numpy.mean(numpy.array(darks, copy=False), axis=0)
    if dtype is not None:
        res = res.astype(dtype)
    return res


def compute_flat(
    flats: list[numpy.NdArray] | numpy.NdArray,
    dtype: numpy.DType | None = None,
    inplace: bool = False,
) -> numpy.NdArray:
    """Compute a flat from a stack of flats

    Arguments:
        inplace: If true, the `flats` can be read/write instead of allocating extra memory.
    """
    res = numpy.median(numpy.array(flats, copy=False), axis=0, overwrite_input=inplace)
    if dtype is not None:
        res = res.astype(dtype)
    return res


def compute_proj(
    flats: list[numpy.NdArray] | numpy.NdArray,
    dtype: numpy.DType | None = None,
    inplace: bool = False,
) -> numpy.NdArray:
    """Compute a proj from a stack of projections

    Arguments:
        inplace: If true, the `flats` can be read/write instead of allocating extra memory.
    """
    res = numpy.median(numpy.array(flats, copy=False), axis=0, overwrite_input=inplace)
    if dtype is not None:
        res = res.astype(dtype)
    return res
