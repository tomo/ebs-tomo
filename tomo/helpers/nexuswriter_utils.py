from __future__ import annotations
import typing
from bliss import setup_globals
from bliss.common.axis import Axis


def hdf5_axis_name(axis: Axis | None, involved_in_scan):
    """
    Returns the name of the axis as it is/will be stored in the HDF5 file.

    This is not handled by BLISS.
    """
    if axis is None:
        return None

    if involved_in_scan:
        return axis.name

    # If the axis is not involved in the scan, BLISS will send it's metadata
    # to the writer as alias, always
    axis_name = axis.name
    alias_name = setup_globals.ALIASES.get_alias(axis_name)
    if alias_name is not None:
        return alias_name
    return axis_name


def hdf5_device_names(device) -> typing.List[str]:
    """
    Returns a list of valid names for this device.

    If an axis is not involved in a scan, BLISS will send it's metadata
    to the writer as an alias, always. But of the device is part of the scan
    it's not always the case.

    Returns:
        The result will contain the BLISS name, and optionally an alias.
    """
    if hasattr(device, "original_name"):
        # It's an alias
        return [device.original_name, device.name]
    else:
        name = device.name
        alias_name = setup_globals.ALIASES.get_alias(name)
        if alias_name is not None:
            return [name, alias_name]
        return [name]
