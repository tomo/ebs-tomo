from __future__ import annotations
import typing


class TechniqueModelReader:
    """Connector to a master HDF5 file containing a sinogram sequence.

    Allows to retrieve the actual model values part of the tomo modelization.
    """

    def __init__(self, h5obj, sequence_path):
        sequence = h5obj[sequence_path]
        self.__positioners = sequence["instrument/positioners"]
        self.__model = sequence["technique/tomoconfig"]
        self.__optic = sequence["technique/optic"]

        self.detector_center_y: typing.Tuple[
            float, str
        ] | None = self._get_detector_center_y()
        self.detector_center_z: typing.Tuple[
            float, str
        ] | None = self._get_detector_center_z()
        self.x_axis: typing.Tuple[float, str] | None = self._get_axis_position(
            "translation_x"
        )
        self.y_axis: typing.Tuple[float, str] | None = self._get_axis_position(
            "translation_y"
        )
        self.sample_x_axis: typing.Tuple[float, str] | None = self._get_axis_position(
            "sample_x"
        )
        self.sample_y_axis: typing.Tuple[float, str] | None = self._get_axis_position(
            "sample_y"
        )
        self.sample_pixel_size: typing.Tuple[
            float, str
        ] | None = self._get_sample_pixel_size()

        # Make sure there is no more link to the HDF5
        self.__positioners = None
        self.__model = None
        self.__optic = None

    def _get_detector_center_y(self) -> typing.Tuple[float, str] | None:
        """Returns the detector_center_y metadata."""
        dataset = self.__model.get("detector_center_y")
        if dataset is None:
            return None
        pos = dataset[()]
        units = dataset.attrs["units"]
        return pos, units

    def _get_detector_center_z(self) -> typing.Tuple[float, str] | None:
        """Returns the detector_center_z metadata."""
        dataset = self.__model.get("detector_center_z")
        if dataset is None:
            return None
        pos = dataset[()]
        units = dataset.attrs["units"]
        return pos, units

    def _get_sample_pixel_size(self) -> typing.Tuple[float, str] | None:
        """Returns the sample pixel size."""
        dataset = self.__optic.get("sample_pixel_size")
        if dataset is None:
            return None
        pos = dataset[()]
        units = dataset.attrs["units"]
        return pos, units

    def _get_axis_position(self, role_name: str) -> typing.Tuple[float, str] | None:
        """Returns the position and the unit from a role name.

        If the role is not found, returns None.
        """
        motor_name = self.__model.get(role_name)
        if motor_name is None:
            return None
        motor_names = motor_name.asstr()[()]
        for name in motor_names:
            dataset = self.__positioners.get(name)
            if dataset is not None:
                break
        else:
            KeyError(
                "Device role '%s' using names %s was not found."
                % (role_name, motor_names)
            )
        pos = dataset[()]
        units = dataset.attrs["units"]
        return pos, units
