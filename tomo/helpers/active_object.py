import logging
import functools
from bliss.common.proxy import Proxy
from bliss import current_session
from bliss.config import settings
from bliss.config.conductor.client import get_redis_proxy

from bliss.config import static


_logger = logging.getLogger(__name__)


class ActiveObject(Proxy):
    """Create an active object, like the measurement groups

    This can be used the following way:

    .. code-block::

        from bliss.common.measurementgroup import MeasurementGroup

        ACTIVE_MG = ActiveObject(
            object_class=MeasurementGroup,
            object_key="active_measurementgroup",
            object_label="measurement group",
        )

    """

    def __init__(self, object_class, object_key, object_label):
        object.__setattr__(self, "__object_class__", object_class)
        object.__setattr__(self, "__object_label__", object_label)
        object.__setattr__(self, "__object_key__", object_key)
        factory = functools.partial(
            self.__get_active_object, object_class, object_key, object_label
        )
        super().__init__(factory)

    @property
    def __class__(self):
        return self.__object_class__

    def __info__(self):
        if self.__wrapped__ is None:
            return f"No active {self.__object_label__} defined"
        else:
            return self.__wrapped__.__info__()

    def deref_active_object(self):
        """Returns the real object exposed by this proxy"""
        try:
            return self.__wrapped__
        except Exception:
            return None

    def set_active_object(self, named_obj):
        """Define the object exposed by this ActiveObject"""
        if named_obj is not None:
            if not hasattr(named_obj, "name"):
                _logger.error(
                    "Object '%s' don't have a name",
                    named_obj,
                )
                return
            name = named_obj.name
            if not isinstance(named_obj, self.__object_class__):
                _logger.error(
                    "Object '%s' is not of kind '%s' for active %s",
                    name,
                    self.__object_class__,
                    self.__object_label__,
                )
                return
        else:
            name = ""
        try:
            session_name = current_session.name
        except Exception:
            _logger.error("No active BLISS session. Redis storage skipped.")
        else:
            active_name = settings.SimpleSetting(
                f"{session_name}:{self.__object_key__}",
                connection=get_redis_proxy(caching=True),
            )
            active_name.set(name)

    @staticmethod
    def __get_active_object(object_class, object_key, object_label):
        """
        Read the active object from BLISS.

        Reference to self was remove to avoid recursive loops when debugging at
        initialization.
        """
        try:
            session_name = current_session.name
        except Exception:
            _logger.error("No active BLISS session. Redis storage skipped.")
            obj_name = None
        else:
            active_name = settings.SimpleSetting(
                f"{session_name}:{object_key}",
                connection=get_redis_proxy(caching=True),
            )
            obj_name = active_name.get()
        if obj_name == "" or obj_name is None:
            return None

        try:
            obj = static.get_config().get(obj_name)
        except Exception:
            _logger.error(
                "Object name '%s' can't be created for active %s",
                obj_name,
                object_label,
                exc_info=True,
            )
            return None

        if obj is None:
            return None

        if not isinstance(obj, object_class):
            _logger.error(
                "Object name '%s' is not of kind '%s' for active %s",
                obj_name,
                object_class,
                object_label,
            )
            return None
        return obj
