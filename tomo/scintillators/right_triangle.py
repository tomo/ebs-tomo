from __future__ import annotations
import typing
from typing_extensions import TypedDict
import numpy
from .base_scintillator import BaseScintillator, Correction, ScrewCorrection


class _ScintillatorAxisGeometryDict(TypedDict):
    screw_distance_mm: float
    """Distance of the screw to tune from the fixed screw"""

    screw_pitch_mm: float
    """Nominal pitch of the screw in mm/turn"""


class _ScintillatorGeometryDict(TypedDict):
    """Description of the geometry of a scintillator"""

    top_right: _ScintillatorAxisGeometryDict
    """Screw for the correction of the horizontal tilt -> the correction is applied on the z-axis"""

    bottom_left: _ScintillatorAxisGeometryDict
    """Screw for the correction of the vertical tilt -> the correction is applied on the y-axis"""


class RightTriangleScintillator(BaseScintillator):
    """Scintillator geometry based on a right triangle.

    - 1) A fixed screw is at the top left corner.
    - 2) A tunable screw is at top right
    - 3) A tunable screw is at botton left

    .. code-block::

        (1)--------(2)
         |      _,-'
         |  _,-'
        (3)'

    Based on the distance of the screw, and the kind of screws, we can provide a
    correction of the tilt.

    It can be defined in the optic description the following way.

    .. code-block:: yaml

        - name:  tomo_detector__optic
          plugin: bliss
          class: FixedmagOptic
          package: tomo.optic.fixedmag_optic

          scintillator_geometry:
             model: right_triangle
             top_right:
                # Top right screw
                # Correction of the horizontal tilt
                screw_distance_mm: 28.65
                screw_pitch_mm: 0.25
             bottom_left:
                # Bottom left screw
                # Correction of the vertical tilt
                screw_distance_mm: 9
                screw_pitch_mm: 0.25
    """
    def __init__(self, config: dict[typing.Any, typing.Any]):
        self._geometry: _ScintillatorGeometryDict = self._cast(config)

    def _cast(self, config: dict[typing.Any, typing.Any]) -> _ScintillatorGeometryDict:
        try:
            from pydantic import BaseModel, ConfigDict
        except Exception:
            # pydantic is not there
            return config  # type: ignore

        class Holder(BaseModel):
            model_config = ConfigDict(arbitrary_types_allowed=True)
            description: _ScintillatorGeometryDict

        return Holder(description=typing.cast(_ScintillatorGeometryDict, config)).description

    def tilt_hardware_correction(
        self,
        tilt_corr_v_rad: float,
        tilt_corr_h_rad: float,
    ) -> list[Correction]:
        """Convert a physical correction into the a hardware based correction."""

        def calculate_tilt_distance_and_turns(
            axis_geometry: _ScintillatorAxisGeometryDict, tilt_rad: float
        ) -> tuple[float, float]:
            """
            Calculate the lateral displacement/turn to fix the tilt.

            Parameters:
                axis_geometry: Geometry to correct the axis
                angle_rad: The rotation angle in radians, representing the tilt of the scintillator calculated.

            Returns:
                A tuple with displacement_distance (the horizontal displacement caused by the tilt, relative to
                the scintillator original position) and the number of turns of screw.
            """
            screw_distance_mm = axis_geometry["screw_distance_mm"]
            screw_pitch_mm = axis_geometry["screw_pitch_mm"]
            tan_angle = numpy.tan(tilt_rad)
            displacement_distance = screw_distance_mm * tan_angle
            num_turns = displacement_distance / screw_pitch_mm
            return displacement_distance, num_turns

        rz_dist, rz_turns = calculate_tilt_distance_and_turns(
            self._geometry["top_right"], tilt_corr_v_rad
        )
        ry_dist, ry_turns = calculate_tilt_distance_and_turns(
            self._geometry["bottom_left"], tilt_corr_h_rad
        )

        return [
            Correction(
                description="Default correction",
                corrections=[
                    ScrewCorrection(
                        role="bottom_left",
                        distance=ry_dist,
                        turns=ry_turns,
                    ),
                    ScrewCorrection(
                        role="top_right",
                        distance=rz_dist,
                        turns=rz_turns,
                    ),
                ]
            )
        ]
