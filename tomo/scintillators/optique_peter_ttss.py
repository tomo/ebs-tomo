from __future__ import annotations
import numpy
from .base_scintillator import BaseScintillator, Correction, ScrewCorrection


def _apply_tilt(vector, tilt_around_vertical_axis, tilt_around_horizontal_axis):
    """
    Applies two successive rotations to a vector with the given tilts around
    vertical and horizontal axes.
    """

    # tilt around vertical axs
    rotation_matrix_Z = numpy.array(
        [
            [numpy.cos(tilt_around_vertical_axis), -numpy.sin(tilt_around_vertical_axis), 0],
            [numpy.sin(tilt_around_vertical_axis), numpy.cos(tilt_around_vertical_axis), 0],
            [0, 0, 1],
        ]
    )
    # tilt around horizontal axs
    rotation_matrix_Y = numpy.array(
        [
            [
                numpy.cos(tilt_around_horizontal_axis),
                0,
                numpy.sin(tilt_around_horizontal_axis),
            ],
            [0, 1, 0],
            [
                -numpy.sin(tilt_around_horizontal_axis),
                0,
                numpy.cos(tilt_around_horizontal_axis),
            ],
        ]
    )
    tilt_array = numpy.dot(rotation_matrix_Z, vector.T).T
    tilt_array = numpy.dot(rotation_matrix_Y, tilt_array.T).T
    return tilt_array


class OptiquePeterTtss(BaseScintillator):
    """
    Triple tiltable scintillator support produced by Optique Peter.

    It provides a rotative holder for 3 scintillators.

    The whole holder is tiltable with 3 screws.

    Only this mounting geometry is supported for now.

    - 1) At the top left
    - 2) At the top right
    - 3) At the bottom

    .. code-block::

        (1)   (2)
            o
           (3)
    """

    SCREW_PITCH = 0.25
    """In mm/turn"""

    SCREW_SCREW_DIST = 67.5
    """Distance between 2 screws"""

    SCREW_SCREW_HEIGHT = 58.5
    """Height between 2 orizontal screws and the third one"""

    def __init__(self):
        pass

    def tilt_hardware_correction(
        self,
        tilt_corr_v_rad: float,
        tilt_corr_h_rad: float,
    ) -> list[Correction]:
        """Convert a physical correction into the a hardware based correction."""

        # Geometry of the scintillators and support
        # lets draw the scintillators and support as if they were aligned in X-Y

        # cercle conscrit: R**2 = S/(2 sin(a) sin(b) sin(c))
        circle_screw_radius = numpy.sqrt(
            (self.SCREW_SCREW_DIST * self.SCREW_SCREW_HEIGHT / 2) / (2 * numpy.sin(numpy.pi / 3) ** 3)
        )

        screw_1_good = numpy.array(
            [
                0,
                circle_screw_radius * numpy.sin(numpy.pi / 3),
                circle_screw_radius * numpy.cos(numpy.pi / 3),
            ]
        )
        screw_2_good = numpy.array(
            [
                0,
                -circle_screw_radius * numpy.sin(numpy.pi / 3),
                circle_screw_radius * numpy.cos(numpy.pi / 3),
            ]
        )
        screw_3_good = numpy.array([0, 0, -circle_screw_radius])

        screw_1_tilt = _apply_tilt(
            screw_1_good, tilt_corr_v_rad, tilt_corr_h_rad
        )
        screw_2_tilt = _apply_tilt(
            screw_2_good, tilt_corr_v_rad, tilt_corr_h_rad
        )
        screw_3_tilt = _apply_tilt(
            screw_3_good, tilt_corr_v_rad, tilt_corr_h_rad
        )

        # let's say that the screwing distance is approximatively the distance
        # between the two points in good position and after tilt
        # for the sign, if xtilt<xgood --> screw, if  xtilt>xgood --> unscrew
        dist_screw_1 = numpy.sign(screw_1_good[0] - screw_1_tilt[0]) * numpy.linalg.norm(
            screw_1_tilt - screw_1_good
        )
        dist_screw_2 = numpy.sign(screw_2_good[0] - screw_2_tilt[0]) * numpy.linalg.norm(
            screw_2_tilt - screw_2_good
        )
        dist_screw_3 = numpy.sign(screw_3_good[0] - screw_3_tilt[0]) * numpy.linalg.norm(
            screw_3_tilt - screw_3_good
        )

        corr1 = Correction(
            description="Default correction",
            corrections=[
                ScrewCorrection(
                    role="top_left",
                    distance=dist_screw_1,
                    turns=dist_screw_1 / self.SCREW_PITCH,
                ),
                ScrewCorrection(
                    role="top_right",
                    distance=dist_screw_2,
                    turns=dist_screw_2 / self.SCREW_PITCH,
                ),
                ScrewCorrection(
                    role="bottom",
                    distance=dist_screw_3,
                    turns=dist_screw_3 / self.SCREW_PITCH,
                ),
            ]
        )

        max_dist_screw = numpy.max([dist_screw_1, dist_screw_2, dist_screw_3])
        dist_screw_1 = dist_screw_1 - max_dist_screw
        dist_screw_2 = dist_screw_2 - max_dist_screw
        dist_screw_3 = dist_screw_3 - max_dist_screw

        corr2 = Correction(
            description="If 'tight' means that you initialize the screws position by tighting all screws and then unscrewing all of them by 1/2 turn",
            corrections=[
                ScrewCorrection(
                    role="top_left",
                    distance=dist_screw_1,
                    turns=dist_screw_1 / self.SCREW_PITCH,
                ),
                ScrewCorrection(
                    role="top_right",
                    distance=dist_screw_2,
                    turns=dist_screw_2 / self.SCREW_PITCH,
                ),
                ScrewCorrection(
                    role="bottom",
                    distance=dist_screw_3,
                    turns=dist_screw_3 / self.SCREW_PITCH,
                ),
            ]
        )

        return [corr1, corr2]
