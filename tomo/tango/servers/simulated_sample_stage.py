from __future__ import annotations

import numpy
import h5py
import os.path
import logging

try:
    from skimage.transform import resize
except ImportError:
    resize = None


SPARSIFY = True
"""If true sparsify the data before rendering it"""

_logger = logging.getLogger(__name__)
_logger.setLevel(logging.INFO)


def voxel_density_skimage(angle, intensity, oversampled=1):
    """Reference implementation of the voxel density using radon transformation.

    It is only used for test.
    """
    import skimage.transform

    size = int(oversampled * 4)
    image = numpy.zeros((size, size), float)
    start = int(size / 2 - oversampled)
    end = int(size / 2 + oversampled)
    image[start:end, start:end] = 1
    angles = [angle * 180 / numpy.pi]
    proj = skimage.transform.radon(
        image, circle=False, theta=angles, preserve_range=True
    )
    proj = proj[:, 0]
    center = proj.shape[0] / 2
    start = int(center - size / 2)
    end = int(center + size / 2)
    proj = proj[start:end]
    return proj * intensity


def voxel_density_shape(angle, intensity, oversampled=1):
    """Fast radon transformation of a slice of a voxel.

    It is based on the expected shape, which is a trapezoid for a voxel.
    """

    def isocele_trapezoid(size, half_base, half_top, vtop):
        half_size = size / 2
        if half_top < 0:
            half_top = 0
        points = [
            (0, 0),
            (half_size - half_base - 0.00001, 0),
            (half_size - half_top, vtop),
            (half_size + half_top, vtop),
            (half_size + half_base + 0.00001, 0),
            (size, 0),
        ]
        points2 = numpy.array(points).T
        return numpy.interp(numpy.arange(size), points2[0], points2[1])

    # Could be faster but it's already fine
    corners = numpy.array([0, numpy.pi * 0.5, numpy.pi, numpy.pi * 1.5])
    corners = corners + angle
    corners = numpy.sin(corners) + numpy.cos(corners)
    corners = numpy.sort(corners)
    half_base = numpy.abs(corners[0])
    half_top = numpy.abs(corners[1])

    pi4 = numpy.pi / 4
    pi2 = numpy.pi / 2
    a = ((angle - pi4) % pi2) - pi4

    intensity = intensity << 3
    # Intensity is = pixelsize/cos(angle)
    # But have to be computed properly
    coef = 1 / numpy.cos(a)
    intensity = coef * intensity

    array = isocele_trapezoid(
        4 * oversampled, oversampled * half_base, oversampled * half_top, intensity
    )
    return array


class DenseModel:
    def __init__(self, data):
        self.data = data

    def voxel_density(self, angle, intensity, oversampled=1):
        """Compute a radon transformation of a slice of a voxel.

        Arguments:
            angle: Angle around vertical axis of the voxel, in radian
            intensity: Intensity of the voxel
            oversampled: Ratio on number of output bins
        """
        return voxel_density_shape(angle, intensity, oversampled)

    def paint(self, sample_stage, image, gain):
        """Radon transformation of a voxel model projected into a screen"""
        height, width = image.shape
        angle = numpy.deg2rad(sample_stage.rot)
        y = sample_stage.y
        z = sample_stage.z
        sampu = sample_stage.sampu
        sampv = sample_stage.sampv
        detector_y = sample_stage.detector_y
        zoom = sample_stage.zoom

        data = self.data
        zsize, ysize, xsize = data.shape

        yy, xx = numpy.ogrid[0:ysize, 0:xsize]
        xproj = (
            (
                y
                - detector_y
                + (xx - xsize / 2 + sampu) * numpy.cos(angle)
                - (yy - ysize / 2 + sampv) * numpy.sin(angle)
            )
            * zoom
        ).flatten()

        oversampled = 4
        voxel_density = self.voxel_density(
            angle, intensity=1, oversampled=((oversampled + 2) * zoom)
        )
        prev_iz = None
        for h in range(height):
            iz = int(zsize / 2 - z + (h - height / 2) / zoom)
            if 0 <= iz < data.shape[0]:
                if prev_iz != iz:
                    density = data[iz].flatten()
                    proj, _ = numpy.histogram(
                        xproj * oversampled,
                        bins=width * oversampled,
                        range=(-width * oversampled * 0.5, width * oversampled * 0.5),
                        weights=density,
                    )
                    conv = numpy.convolve(proj, voxel_density, mode="same")
                    prev_iz = iz
                # TODO: This loop could be removed
                for i in range(oversampled):
                    image[height - 1 - h] += conv[i::oversampled] * gain


class SparseModel:
    def __init__(self, data):
        self._zsize, self._ysize, self._xsize = data.shape
        self._size = max(self._ysize, self._xsize)

        from .quad_tree_image import SparseVoxel

        self.data = SparseVoxel(data)

        self._oversampled = 2
        self._voxel_density = {}

    def _preprocess_voxel_density(self, angle, zoom):
        for i in range(0, 10):
            size = 1 << i
            voxel_density = voxel_density_shape(
                angle, size, self._oversampled * size / 2 * zoom
            )
            self._voxel_density[size] = voxel_density

    def _xproj_line(self, sample_stage, angle, quadimage, output, gain):
        width = output.shape[0]
        angle = numpy.deg2rad(sample_stage.rot)
        y = sample_stage.y
        z = sample_stage.z
        sampu = sample_stage.sampu
        sampv = sample_stage.sampv
        detector_y = sample_stage.detector_y
        zoom = sample_stage.zoom
        data = self.data
        zsize, ysize, xsize = data.shape
        oversampled = self._oversampled

        for block in quadimage.blocks:
            if block.pixel_size not in [1, 2, 4, 8, 16, 32, 64, 128, 256]:
                continue
            if block.nb_pixels == 0:
                continue
            yy, xx = block.pixel_center_y, block.pixel_center_x
            xproj = (
                (
                    y
                    - detector_y
                    + (xx - xsize / 2 + sampv) * numpy.cos(angle)
                    - (yy - ysize / 2 + sampu) * numpy.sin(angle)
                )
                * zoom
            ).flatten()

            # density = block.pixel_intensity
            density = numpy.ones((block.nb_pixels,))
            proj, _ = numpy.histogram(
                xproj * oversampled,
                bins=width * oversampled,
                range=(-width * oversampled * 0.5, width * oversampled * 0.5),
                weights=density,
            )
            voxel_density = self._voxel_density[block.pixel_size]
            conv = numpy.convolve(proj, voxel_density, mode="same")

            if oversampled > 1:
                conv.shape = -1, oversampled
                conv = numpy.average(conv, axis=1)
            if conv.shape[0] > output.shape[0]:
                c = conv.shape[0] // 2
                s = output.shape[0] // 2
                conv = conv[c - s : c + s]
            output += conv * gain

    def paint(self, sample_stage, image, gain):
        """Radon transformation of a voxel model projected into a screen"""
        height = image.shape[0]
        angle = numpy.deg2rad(sample_stage.rot)
        z = sample_stage.z
        zoom = sample_stage.zoom
        data = self.data
        zsize = data.shape[0]

        self._preprocess_voxel_density(angle, zoom)

        prev_iz = None
        for h in range(height):
            iz = int(zsize / 2 - z + (h - height / 2) / zoom)
            if 0 <= iz < self.data.shape[0]:
                if prev_iz != iz:
                    quadimage = self.data[iz]
                    self._xproj_line(
                        sample_stage, angle, quadimage, image[height - 1 - h], gain
                    )

        return image


class SimulatedSampleStage:
    def __init__(self):
        self._model = None
        self.mode = "xray"
        self.detector_distance = -1

        # Sample stage status
        self.sx: float = 0
        self.sy: float = 0
        self.sz: float = 0

        # Position of the focal plan
        self.sx0: float = 0
        self.sy0: float = 0
        self.sz0: float = 0

        # Angle from the focal plan to compute the real y based on x
        self.sy_angle: float = 0
        # Angle from the focal plan to compute the real z based on x
        self.sz_angle: float = 0

        self.srot: float = 0
        self.srot_dir: int = 1
        self.model_flipx: bool = False
        self.model_flipy: bool = False
        self.sampu: float = 0
        self.sampv: float = 0

        # Detector status
        self.detector_y: float = 0
        self.internal_detector_size: tuple[int, int] = 128, 128
        """Internal detector"""
        self.detector_size: tuple[int, int] = 128, 128
        """Resulting rescaled image"""
        self.detector_magnification: float = 1

        # Shutter status
        self.is_shutter_open: bool = True

        self._gain: float = 1
        self._dark: numpy.NdArray | None = None
        self._beam: numpy.NdArray | None = None

    @property
    def distance_ratio(self) -> float:
        dd = self.detector_distance
        if dd < 0.0:
            return 1.0
        if self.x == 0:
            return 0
        return dd / self.x

    @property
    def x(self) -> float:
        return self.sx - self.sx0

    @property
    def y(self) -> float:
        return self.sy - self.sy0 - self.x * numpy.tan(self.sy_angle)

    @property
    def z(self) -> float:
        return self.sz - self.sz0 - self.x * numpy.tan(self.sz_angle)

    @property
    def rot(self) -> float:
        return self.srot * self.srot_dir

    @property
    def zoom(self):
        """Normalize the zoom to a power of 2"""
        zoom = self.detector_magnification * self.distance_ratio
        if zoom is None:
            return 1.0
        if zoom <= 0:
            return 0.1
        if zoom < 1:
            return zoom
        return int(zoom)

    def _load_dataset(self, filename, kind):
        if filename is None or filename == "":
            return None
        filename, path = filename.split("::", 1)
        if not os.path.isfile(filename):
            _logger.error("Filename %s not found", filename)
            return None

        with h5py.File(filename, "r") as h5obj:
            if path not in h5obj:
                _logger.error("Dataset %s::%s not found", filename, path)
                return None
            data = h5obj[path][...]
            _logger.info("%s from filename %s loaded", kind, filename)
            return data

    def load_filenames(self, filename, dark_filename=None, beam_filename=None):
        with h5py.File(filename, "r") as h5obj:
            self.load_h5(h5obj)
        _logger.info("Data from filename %s loaded", filename)

        self._dark = self._load_dataset(dark_filename, "Dark data")
        self._beam = self._load_dataset(beam_filename, "Beam data")
        if self._beam is not None:
            self._gain = self._beam[0, 0] * 0.5

    def load_h5(self, h5obj):
        self._model = self._create_model(h5obj)

    def _create_model(self, h5obj) -> SparseModel | DenseModel:
        data = h5obj["model"][...]
        if self.model_flipx:
            _logger.info("Flip data in X")
            data = data[:, :, ::-1]
        if self.model_flipy:
            _logger.info("Flip data in Y")
            data = data[:, ::-1, :]
        if SPARSIFY:
            return SparseModel(data)
        else:
            return DenseModel(data)

    def compute_internal_image(self, expo_time):
        height, width = self.internal_detector_size
        image = numpy.zeros((height, width), dtype=numpy.float32)

        if not self.is_shutter_open:
            return image
        if self._model is None:
            return image

        self._model.paint(self, image, 1)

        if self._beam is not None:
            i0 = self._beam[0 : image.shape[0], 0 : image.shape[1]]
        else:
            i0 = 10000

        _logger.info("Min/max %s %s", numpy.min(image), numpy.max(image))
        # This constants should be tuned based on the sample
        data = 10000 * numpy.exp(-image / 2000)
        image = data * i0 * expo_time

        if self._dark is not None:
            image += self._dark[0 : image.shape[0], 0 : image.shape[1]]

        return image

    def compute_image(self, expo_time=0.1):
        image = self.compute_internal_image(expo_time)
        if self.internal_detector_size != self.detector_size:
            if resize is None:
                raise RuntimeError("scikit-image is not installed")
            height, width = self.detector_size
            image = resize(image, (height, width), anti_aliasing=True)
        return image
