from __future__ import annotations

import redis
import threading
import pickle
import logging
import os
import time
from bliss.common.shutter import BaseShutterState


_logger = logging.getLogger(__name__)

# _logger.setLevel(logging.DEBUG)


def find_address():
    if "BEACON_HOST" in os.environ:
        host = os.environ["BEACON_HOST"].split(":")[0]
    else:
        host = "localhost"

    if "BEACON_REDIS_PORT" in os.environ:
        port = int(os.environ["BEACON_REDIS_PORT"])
    else:
        port = 25001

    return host, port


class RemoteBliss:
    """Helper to follow the changes of the BLISS devices.

    All the device properties have to be subscribed before starting the process.

    It uses pubsub from Redis to synchronize the state of the BLISS session.

    This using a thread and Redis.
    """

    def __init__(self, host, port, **kwargs):
        self.__redis: redis.Redis | None = None
        self.__params = host, port, kwargs
        self.__thread = threading.Thread(target=self.__listen)
        self.__pending_subs = []
        self.__settings_events = {}
        self.__values = {}

    def __update(self, channel, value):
        if isinstance(channel, bytes):
            channel = channel.decode()
        _logger.debug("Publish %s %s (%s)", channel, value, type(value))
        self.__values[channel] = value

    def __listen(self):
        """
        Threaded method.

        All the redis access have to be done in the same thread
        """
        host, port, kwargs = self.__params
        self.__redis = redis.Redis(host=host, port=port, **kwargs)
        pubsub = self.__redis.pubsub()

        for channel_name in self.__pending_subs:
            pubsub.subscribe(channel_name)
            if channel_name.endswith(".dial_position"):
                # Hack to know the position before any move
                name = channel_name.split(".")[1]
                try:
                    pos2 = self.__redis.hget(f"axis.{name}", "dial_position")
                    pos = float(pos2)
                except TypeError:
                    pass
                else:
                    self.__values[channel_name] = pos

        for event in self.__settings_events.values():
            name, attr = event
            try:
                # Read the actual state
                value = self.__get_from_settings(name, attr)
                self.__values[f"{name}:{attr}"] = value
            except Exception:
                _logger.error(
                    "Error while reading %s:%s from settings", name, attr, exc_info=True
                )

        _logger.debug("Initial state %s", self.__values)

        try:
            while True:
                result = pubsub.get_message(timeout=1)
                # response = pubsub.parse_response(block=True)
                # result = pubsub.handle_response(response)
                if result is not None:
                    if result["type"] == "message":
                        data = result.get("data")
                        channel = result.get("channel")
                        try:
                            data = pickle.loads(data)
                        except Exception:
                            _logger.error(
                                "Error while decoding channel %a",
                                channel,
                                exc_info=True,
                            )
                        else:
                            if hasattr(data, "value"):
                                value = data.value
                                if channel.startswith(b"__EVENT__:"):
                                    name = channel.split(b":", 1)[1].decode()
                                    for v in value:
                                        event = self.__settings_events.get((channel, v))
                                        if event is not None:
                                            setting_name, key = event
                                            publish_name = f"{name}:{key}"
                                            value2 = self.__get_from_settings(
                                                setting_name, key
                                            )
                                            self.__update(publish_name, value2)
                                else:
                                    self.__update(channel, data.value)
                            else:
                                _logger.error(
                                    "Unexpected object kind %a from channel %a",
                                    type(data),
                                    channel,
                                )
        finally:
            pubsub.unsubscribe()
            self.__redis = None
            self.__pubsub = None

    def __get_from_settings(self, channel, key):
        value = self.__redis.hget(f"{channel}:settings", key)
        if value is not None:
            value = pickle.loads(value)
        return value

    def subscribe(self, redis_channel_name: str):
        if self.__thread.is_alive():
            raise RuntimeError("Subscriptions have to be done before the start")
        if redis_channel_name not in self.__pending_subs:
            self.__pending_subs.append(redis_channel_name)

    def subscribe_from_settings(self, name: str, attr: str):
        self.subscribe(f"__EVENT__:{name}")
        self.__settings_events[(b"__EVENT__:" + name.encode(), attr)] = (name, attr)

    def subscribe_shutter(self, name: str, state=False):
        if state:
            self.subscribe_from_settings(name, "state")

    def subscribe_optic(self, name: str, magnification=False):
        if magnification:
            self.subscribe_from_settings(name, "magnification")

    def subscribe_axis(self, name: str, state=False, dial_position=False):
        if state:
            self.subscribe(f"axis.{name}.state")
        if dial_position:
            self.subscribe(f"axis.{name}.dial_position")

    def start(self):
        """Start listening"""
        self.__thread.start()

    def __raise_if_stoped(self):
        if not self.__thread.is_alive():
            raise RuntimeError("BlissRemoteBliss was not started")

    def get_shutter_state(
        self, name: str, default: BaseShutterState
    ) -> BaseShutterState:
        self.__raise_if_stoped()
        return self.__values.get(f"{name}:state", default)

    def get_optic_magnification(self, name: str, default: float) -> float:
        self.__raise_if_stoped()
        for _ in range(10):
            optic = self.__values.get(f"{name}:magnification", default)
            if optic is not None:
                break
            # Optic can be None in between 2 good states
            time.sleep(0.1)
        return optic

    def get_axis_state(self, name: str, default):
        self.__raise_if_stoped()
        return self.__values.get(f"axis.{name}.state", default)

    def get_axis_dial_position(self, name: str, default: float) -> float:
        self.__raise_if_stoped()
        value = self.__values.get(f"axis.{name}.dial_position", default)
        if value is None:
            value = default
        return value
