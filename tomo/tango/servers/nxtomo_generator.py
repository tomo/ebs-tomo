import h5py
import numpy
import enum


class NXtomoSpec(enum.IntEnum):
    """Specification from NXtomo"""

    Y_INDEX = 1
    X_INDEX = 2
    PROJECTION = 0
    FLAT_FIELD = 1
    DARK_FIELD = 2
    INVALID = 3


class GeneratorFromNxTomo:
    """Generator of a full field tomo data from a NXtomo file

    https://manual.nexusformat.org/classes/applications/NXtomo.html

    Arguments:
        nxtomo_filename: Name of the nexus HDF5 file
    """

    def __init__(self):
        self.__detector_size = None
        self.__flat_cursor = -1
        self.__dark_cursor = -1

    def load_nxtomo_filename(self, filename, entry_name="entry0000"):
        h5 = h5py.File(filename, "r")
        self.load_nxtomo_h5(h5, entry_name=entry_name)

    def load_nxtomo_h5(self, h5, entry_name="entry0000"):
        count_time = h5[f"/{entry_name}/instrument/detector/count_time"][...]
        angles = h5[f"/{entry_name}/sample/rotation_angle"][...]
        images = h5[f"/{entry_name}/instrument/detector/data"]
        image_keys = h5[f"/{entry_name}/instrument/detector/image_key"][...]
        assert images.ndim == 3
        assert image_keys.ndim == 1

        self.__detector_shape = images.shape[-2:]

        self.__count_time = count_time
        self.__images = images
        self.__flat_indexes = numpy.where(image_keys == NXtomoSpec.FLAT_FIELD)[0]
        self.__dark_indexes = numpy.where(image_keys == NXtomoSpec.DARK_FIELD)[0]
        self.__projection_indexes = numpy.where(image_keys == NXtomoSpec.PROJECTION)[0]
        self.__projection_angles = angles[self.__projection_indexes]

    @property
    def detector_shape(self):
        """Returns the shape of the detector.

        Usually order such as height is the first, then width.

        It's the numpy shape of the projections/dark/flat
        """
        return self.__detector_shape

    def _get_image(self, image_id, exposure):
        """Returns an image id corrected with a requested exposure"""
        image = self.__images[image_id]
        count_time = self.__count_time[image_id]
        return image / count_time * exposure

    def get_projection(self, angle: float, exposure: float):
        """Returns a projection at the closes requested angle

        Arguments:
            angle: In degree
            exposure: In second
        """
        projection_id = numpy.argmin(abs(self.__projection_angles - angle))
        image_id = self.__projection_indexes[projection_id]
        return self._get_image(image_id, exposure)

    def get_dark(self, exposure: float):
        """Returns one of the provided flat image

        Arguments:
            exposure: In second
        """
        self.__dark_cursor += 1
        flat_index = self.__dark_cursor % len(self.__dark_indexes)
        image_id = self.__dark_indexes[flat_index]
        return self._get_image(image_id, exposure)

    def get_flat(self, exposure: float):
        """Returns one of the provided flat image

        Arguments:
            exposure: In second
        """
        self.__flat_cursor += 1
        flat_index = self.__flat_cursor % len(self.__flat_indexes)
        image_id = self.__flat_indexes[flat_index]
        return self._get_image(image_id, exposure)
