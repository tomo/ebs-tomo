# Compatibility with tomo <= 2.3.1

from tomo.controllers.tomo_detectors import TomoDetectors  # noqa
from tomo.controllers.tomo_detectors import TomoDetectorsState  # noqa
from bliss.common import deprecation

deprecation.deprecated_warning(
    "Module",
    "tomo.tomo_detectors",
    replacement="tomo.controllers.tomo_detectors",
    since_version="2.3.1",
)
