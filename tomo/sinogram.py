import numpy
from bliss import global_map
from bliss.scanning.chain import AcquisitionChannel, AcquisitionChain
from bliss.scanning.acquisition.calc import CalcChannelAcquisitionSlave, CalcHook
from bliss.scanning.group import GroupingMaster, GroupingSlave, ScanGroup, StatePreset
from tomo.scan.presets.image_spectrum import ImageSpectrumPreset
from tomo.scan.presets.projection_spectrum import ProjectionSpectrumPreset


class TomoSinogram:
    """
    Handle sinogram acquisition during a tomo sequence.

    This sinogram is connected to a dedicated Lima profile roi.

    This corresponds to one image slice taken at each rotation angle.
    """

    def __init__(
        self,
        name,
        detector,
        roi_name,
    ):
        self.log_name = name + ".add_sinogram"
        global_map.register(self, tag=self.log_name)
        self._detector = detector
        self._roi_name = roi_name
        self._rotation_axis = None
        self._angle_start = None
        self._angle_stop = None
        self._nb_angles = None

    @property
    def roi_name(self):
        return self._roi_name

    def setup_counters(self, mg):
        detector = self._detector
        if len(detector.roi_profiles.get_rois()) == 0:
            detector.roi_profiles[self.roi_name] = [
                0,
                detector.image.height / 2,
                detector.image.width,
                1,
            ]

        try:
            mg.add(detector.roi_profiles.counters.sino)
        except Exception:  # nosec
            pass
        mg.enable(detector.roi_profiles.counters.sino.fullname)

        roi = detector.roi_profiles.get(self.roi_name)
        if (
            roi.x + roi.width > detector.image.width
            or roi.y + roi.height > detector.image.height
        ):
            raise RuntimeError(
                f"roi profile {self.roi_name} does not fit into image roi"
            )

    def setup_meta(
        self,
        rotation_axis,
        angle_start,
        angle_stop,
        nb_angles,
    ):
        self._rotation_axis = rotation_axis
        self._angle_start = angle_start
        self._angle_stop = angle_stop
        self._nb_angles = nb_angles

    def add_sinogram_plot(self, scan_info):
        """
        Configure sinogram display by constructing a scatter plot.

        - X axis corresponds to image pixel indices in width.
        - Y axis corresponds to rotation angle.
        """
        roi_width = self._detector.roi_profiles[self._roi_name].width

        # Create a data group for the sinograms
        scan_info.set_channel_meta(
            "rotation",
            start=self._angle_start,
            stop=self._angle_stop,
            points=self._nb_angles * roi_width,
            axis_points=self._nb_angles,
            axis_id=1,
            axis_kind="forth",
            group="sinogram",
        )
        scan_info.set_channel_meta(
            "translation",
            start=0.5,
            stop=roi_width - 0.5,
            points=self._nb_angles * roi_width,
            axis_points=roi_width,
            axis_id=0,
            axis_kind="forth",
            group="sinogram",
        )
        scan_info.set_channel_meta("sinogram", group="sinogram")

        # Define a default plot
        scan_info.add_scatter_plot(x="translation", y="rotation", value="sinogram")

    def setup_sequence(
        self,
        sequence,
        nb_images,
        dark_scan,
        flat_scan,
        proj_scans,
    ):
        """
        Add custom data channels to sequence bliss object:
        - to receive roi profiles data from dark, flat and projection scans
        - to emit sinogram data
        Configure a CalcChannelAcquisitionSlave object:
        - to emit roi profiles data after flatfield correction
        - to resize sinogram data to see sinogram image in hdf5 file
        """
        detector = self._detector
        rotation_axis = self._rotation_axis
        counters = [
            c for c in detector.roi_profiles.counters if c.roi_name == self._roi_name
        ]
        if len(counters) == 0:
            raise RuntimeError(
                "No counter associated to the ROI profile name %s", self._roi_name
            )
        counter = counters[0]

        sequence.add_custom_channel(
            AcquisitionChannel("dark_spectrum", numpy.float32, counter.shape)
        )
        sequence.add_custom_channel(
            AcquisitionChannel("flat_spectrum", numpy.float32, counter.shape)
        )
        sequence.add_custom_channel(
            AcquisitionChannel("proj_spectrum", numpy.float32, counter.shape)
        )
        sequence.add_custom_channel(
            AcquisitionChannel("rotation", numpy.float32, (), unit="degree")
        )
        sequence.add_custom_channel(
            AcquisitionChannel("translation", numpy.float32, (), unit="px")
        )
        corrected_channel = AcquisitionChannel(
            "corrected_spectrum", numpy.float32, counter.shape
        )
        sinogram_channel = AcquisitionChannel("sinogram", numpy.float32, ())

        ### TEMPORARY SOLUTION TO BUILD SINOGRAM, NEED TO FIND A SOLUTION TO AVOID DUPLICATED CODE FROM GROUP.PY MODULE
        sequence.group_acq_master = GroupingMaster()
        chain = AcquisitionChain()
        chain.add(sequence.group_acq_master)

        sequence.group_custom_slave = GroupingSlave(
            "custom_channels", sequence.custom_channels.values()
        )
        chain.add(sequence.group_acq_master, sequence.group_custom_slave)

        sino_calc = SinoCalc(
            sequence.custom_channels["dark_spectrum"],
            sequence.custom_channels["flat_spectrum"],
            sequence.custom_channels["proj_spectrum"],
            sequence.custom_channels["rotation"],
            sequence.custom_channels["translation"],
            nb_images,
        )
        calc_sino_acq = CalcChannelAcquisitionSlave(
            "calc_sino_acq",
            [sequence.group_custom_slave],
            sino_calc,
            [sinogram_channel, corrected_channel],
        )
        chain.add(sequence.group_acq_master, calc_sino_acq)

        sequence._scan = ScanGroup(
            chain, sequence.title, save=True, scan_info=sequence._scan_info
        )
        sequence._scan.add_preset(StatePreset(sequence))

        dark_preset = ImageSpectrumPreset(detector, sequence, "dark_spectrum")
        dark_scan.add_preset(dark_preset)
        flat_preset = ImageSpectrumPreset(detector, sequence, "flat_spectrum")
        flat_scan.add_preset(flat_preset)
        proj_preset = ProjectionSpectrumPreset(
            detector,
            rotation_axis,
            sequence,
            "proj_spectrum",
            "translation",
            "rotation",
        )
        for scan in proj_scans:
            scan.add_preset(proj_preset)


class SinoCalc(CalcHook):
    """
    Class used for sinogram data emission

    Flatfield correction is applied on each roi profiles data coming
    projection scans before being emitted through "sinogram" sequence
    channel

    Sinogram data are also reformatted to be interpreted as an image and
    be stored in hdf5 data file

    **Attributes**:

    dark_channel: bliss object
        Channel containing roi profile data of dark images
    ref_channel: bliss object
        Channel containing roi profile data of ref images
    proj_channel: bliss object
        Channel containing roi profile data of projection images
    rotation_channel: bliss object
        Channel containing rotation position values
    translation_channel: bliss object
        Channel containing image pixel indices values
    dark_data: numpy array
        Array to store roi profile data of dark images
    ref_data: numpy array
        Array to store roi profile data of ref images
    rotation_data: numpy array
        Array to store rotation position values
    translation_data: numpy array
        Array to store image pixel indices values
    image_corr: numpy array
        Array to store roi profile data of projection images after flatfield correction
    proj_per_turn: int
        Number of projections in one tomo turn
    turn: int
        Current tomo turn (incremented at each turn in case of multi tomo)
    """

    def __init__(
        self,
        dark_channel,
        ref_channel,
        proj_channel,
        rotation_channel,
        translation_channel,
        proj_per_turn,
    ):
        self.dark_channel = dark_channel
        self.ref_channel = ref_channel
        self.proj_channel = proj_channel
        self.rotation_channel = rotation_channel
        self.translation_channel = translation_channel
        self.dark_data = numpy.array([])
        self.ref_data = numpy.array([])
        self.rotation_data = numpy.array([])
        self.translation_data = numpy.array([])
        self.image_corr = numpy.array([])
        self.proj_per_turn = proj_per_turn
        self.turn = 1

    def compute(self, sender, data_dict):
        """
        Store roi profiles data of dark and flat
        Receive roi profiles data from projection scans and apply flatfield
        correction
        Emit corrected data to 'sinogram' channel
        Reshape 'sinogram' channel as an image and emit new data to
        'corrected_spectrum' channel
        """

        dark_data = data_dict.get(self.dark_channel.name)
        if dark_data is not None:
            self.dark_data = numpy.append(self.dark_data, dark_data)

        ref_data = data_dict.get(self.ref_channel.name)
        if ref_data is not None:
            self.ref_data = numpy.append(self.ref_data, ref_data)

        rotation_data = data_dict.get(self.rotation_channel.name)
        if rotation_data is not None:
            self.rotation_data = numpy.append(self.rotation_data, rotation_data)

        translation_data = data_dict.get(self.translation_channel.name)
        if translation_data is not None:
            self.translation_data = numpy.append(
                self.translation_data, translation_data
            )

        roi_width = int(self.dark_channel.shape[0])

        proj_data = data_dict.get(self.proj_channel.name)
        if proj_data is not None:
            if len(self.dark_data) != roi_width and len(self.ref_data) != roi_width:
                nb_dark = int(len(self.dark_data) / roi_width)
                self.dark_data = numpy.array(
                    numpy.mean(self.dark_data.reshape(nb_dark, roi_width)[0:1], axis=0)
                )
                nb_ref = int(len(self.ref_data) / roi_width)
                self.ref_data = numpy.array(
                    numpy.mean(self.ref_data.reshape(nb_ref, roi_width)[0:1], axis=0)
                )

            image_corr = (proj_data - self.dark_data) / (self.ref_data - self.dark_data)
            image_corr[numpy.logical_not(numpy.isfinite(image_corr))] = 0
            self.image_corr = numpy.append(self.image_corr, image_corr)
            if len(self.image_corr) >= roi_width * self.proj_per_turn * self.turn:
                image2d_corr = self.image_corr[
                    (self.turn - 1)
                    * self.proj_per_turn
                    * roi_width : self.turn
                    * self.proj_per_turn
                    * roi_width
                ]
                image2d_corr = image2d_corr.reshape(self.proj_per_turn, roi_width)
                self.turn += 1
            else:
                image2d_corr = None
            sinogram_data = image_corr.flatten().astype(numpy.float32)
            if image2d_corr is not None:
                image2d_corr = image2d_corr.astype(numpy.float32)

            return {"corrected_spectrum": image2d_corr, "sinogram": sinogram_data}
