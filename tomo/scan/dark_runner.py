from bliss.common.logtools import log_info, log_error
from bliss.scanning.toolbox import ChainBuilder
from bliss.controllers.lima.lima_base import Lima
from bliss.scanning.scan_info import ScanInfo
from bliss.common.scans import loopscan
from bliss.common.cleanup import capture_exceptions
from tomo.scan.tomo_runner import TomoRunner
from tomo.scan.presets.common_header import CommonHeaderPreset
from tomo.constants import NxTomoImageKey


class DarkRunner(TomoRunner):
    """
    Class used to build and run scan to acquire images with beam
    shutter closed (dark images)
    Built as a layer of loopscan
    """

    def __init__(self, single_chain=None, acc_chain=None):
        super().__init__(single_chain=single_chain, acc_chain=acc_chain)
        self._detectors = None

    def __call__(
        self,
        expo_time,
        dark_n,
        *counters,
        scan_info=None,
        header=None,
        run=True,
        save=True,
    ):
        """
        Build and run scan by given parameters directly like
        loopscan
        Some specific metadata are filled in scan info
        Scan and chain configured presets are added on scan
        """
        if header is None:
            header = {}

        # Add the dark scan meta data
        scan_info = self._add_metadata(expo_time, dark_n, scan_info)

        self._scan = self._setup_scan(
            expo_time, dark_n, *counters, header=header, save=save, scan_info=scan_info
        )

        self._setup_presets()
        if run:
            with capture_exceptions(raise_index=0) as capture:
                with capture():
                    self._scan.run()
                # test if an error has occurred
                if len(capture.failed) > 0:
                    log_error(
                        "tomo", "A problem occured during the dark scan, scan aborted"
                    )
                    log_error("tomo", capture.exception_infos)
                    log_error("tomo", "\n")
        return self._scan

    def _setup_scan(
        self, expo_time, dark_n, *counters, header={}, save=True, scan_info={}
    ):
        """
        Build and return scan
        Take a list of given counters by user or use those which are
        enabled in ACTIVE_MG
        Handle accumulation mode on detectors
        """
        log_info("tomo", "dark_scan() setup entering")

        # dark images to be taken?
        if dark_n > 0:
            # scan title
            title = "dark images"

            # add image identification to the common image header
            header["image_key"] = str(NxTomoImageKey.DARK_FIELD.value)

            counters = list(counters)
            builder = ChainBuilder(counters)

            detectors = list()
            for node in builder.get_nodes_by_controller_type(Lima):
                detectors.append(node.controller)
            self._detectors = detectors

            # scan for dark images
            with self._setup_detectors_in_default_chain(detectors):
                dark_scan = loopscan(
                    dark_n,
                    expo_time,
                    *counters,
                    name=title,
                    title=title,
                    save=save,
                    run=False,
                    scan_info=scan_info,
                )

            # add common header preset
            header_preset = CommonHeaderPreset(*detectors, header=header)
            dark_scan.add_preset(header_preset)

        log_info("tomo", "dark_scan() setup done")
        return dark_scan

    def _estimate_scan_duration(self, tomo_pars):
        """
        Estimate scan time by taking into account image acquisition time
        for each image
        """
        return tomo_pars.dark_n * tomo_pars.exposure_time

    def _add_metadata(self, expo_time, dark_n, scan_info=None):
        """
        Fill the scan_info dictionary with the scan meta data
        """
        scan_info = ScanInfo.normalize(scan_info)
        technique = scan_info.setdefault("technique", {})
        technique["dark"] = {
            "dark_n": dark_n,
            "exposure_time": expo_time,
            "exposure_time@units": "s",
        }
        technique["image_key"] = NxTomoImageKey.DARK_FIELD.value
        return scan_info
