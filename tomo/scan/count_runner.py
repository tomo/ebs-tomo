from bliss.common.logtools import log_info, log_error
from bliss.common.scans.ct import sct
from bliss.scanning.toolbox import ChainBuilder
from bliss.controllers.lima.lima_base import Lima
from bliss.scanning.scan_info import ScanInfo
from bliss.common.cleanup import capture_exceptions
from tomo.scan.presets.common_header import CommonHeaderPreset
from tomo.constants import NxTomoImageKey
from tomo.scan.tomo_runner import TomoRunner


class CountRunner(TomoRunner):
    """
    Class used to acquire an image with beam shutter open.
    Built as a layer of ImageRunner
    """

    def __init__(self, single_chain=None, acc_chain=None):
        super().__init__(single_chain=single_chain, acc_chain=acc_chain)
        self._detectors = None

    def __call__(
        self,
        expo_time,
        *detectors,
        scan_info=None,
        header=None,
        run=True,
        save=True,
    ):
        """
        Build and run scan by given parameters directly like
        bliss common scans
        Some specific metadata are filled in scan info
        Scan and chain configured presets are added on scan
        """
        if header is None:
            header = {}

        # Add the dark scan meta data
        scan_info = self._add_metadata(expo_time, scan_info)

        with self._setup_detectors_in_default_chain(detectors):
            self._scan = self._setup_scan(
                expo_time, *detectors, header=header, save=save, scan_info=scan_info
            )

        self._setup_presets()
        if run:
            with capture_exceptions(raise_index=0) as capture:
                with capture():
                    self._scan.run()
                # test if an error has occurred
                if len(capture.failed) > 0:
                    log_error(
                        "tomo", "A problem occurred during the count scan, scan aborted"
                    )
                    log_error("tomo", capture.exception_infos)
                    log_error("tomo", "\n")
        return self._scan

    def _setup_scan(self, expo_time, *detectors, header={}, save=True, scan_info={}):
        """
        Build and return scan
        Take a list of given detectors by user or use those which are
        enabled in ACTIVE_MG
        """
        log_info("tomo", "proj_scan() setup entering")

        # scan title
        title = "count images"

        # add image identification to the common image header
        header["image_key"] = str(NxTomoImageKey.PROJECTION.value)

        detectors = list(detectors)
        if len(detectors) == 0:
            builder = ChainBuilder([])

            detectors = list()
            for node in builder.get_nodes_by_controller_type(Lima):
                detectors.append(node.controller)
        self._detectors = detectors

        with capture_exceptions(raise_index=0) as capture:
            with capture():
                # scan for proj images
                proj_scan = sct(
                    expo_time,
                    *detectors,
                    title=title,
                    save=save,
                    scan_info=scan_info,
                    run=False,
                )

                # add common header preset
                header_preset = CommonHeaderPreset(*detectors, header=header)
                proj_scan.add_preset(header_preset)

                # test if an error has occured
                if len(capture.failed) > 0:
                    log_error(
                        "tomo",
                        "A problem occured during the count scan, scan aborted",
                    )
                    log_error("tomo", capture.exception_infos)
                    log_error("tomo", "\n")

        log_info("tomo", "proj_scan() setup done")
        return proj_scan

    def _estimate_scan_duration(self, tomo_pars):
        """
        Estimate scan time by taking into account image acquisition time
        for each image
        """
        return tomo_pars.tomo_n * tomo_pars.exposure_time

    def _add_metadata(self, expo_time, scan_info=None):
        """
        Fill the scan_info dictionary with the scan meta data
        """
        scan_info = ScanInfo.normalize(scan_info)
        technique = scan_info.setdefault("technique", {})
        technique["proj"] = {
            "proj_n": 1,
            "exposure_time": expo_time,
            "exposure_time@units": "s",
        }
        technique["image_key"] = NxTomoImageKey.PROJECTION.value
        return scan_info
