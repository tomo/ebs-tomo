import logging

from bliss.common.logtools import log_info
from bliss.scanning.scan_info import ScanInfo
from bliss import current_session
from tomo.scan.tomo_runner import TomoRunner
from tomo.constants import NxTomoImageKey
from fscan.fscantools import FScanParamBase

_logger = logging.getLogger(__name__)


class InterlacedRunner(TomoRunner):
    """
    Class used to build and run scan covering all angular space by
    executed scan in 2 pass
    During the first pass, only
    one acquistion is performed every two points, during the second pass,
    missing angles are acquired
    Built as a layer of finterlaced
    """

    def __init__(self, config):
        super().__init__()
        self._projection_scan_name = "finterlaced"
        self._fscan_config = config
        self._finterlaced = config.get_runner(self._projection_scan_name)
        self.pars: FScanParamBase = self._finterlaced.pars
        """finterlaced parameters"""
        self._scan = None

    def __call__(
        self,
        motor,
        start_pos,
        end_pos,
        tomo_n,
        expo_time,
        scan_info=None,
        header=None,
        run=True,
        save=True,
    ):
        """
        Build and run scan by given parameters directly like finterlaced or
        bliss common scans
        Some specific metadata are filled in scan info
        Scan and chain configured presets are added on scan
        """
        self.scan_info = ScanInfo.normalize(scan_info)
        if header is None:
            header = {}

        # Add the user and scan meta data
        scan_info = self._add_metadata(
            motor, start_pos, end_pos, expo_time, tomo_n, scan_info
        )

        self._setup_detectors_from_active_mg()

        self._scan = self._setup_scan(
            motor,
            start_pos,
            end_pos,
            tomo_n,
            expo_time,
            header=header,
            save=save,
            scan_info=scan_info,
        )
        self._chain = self._scan.acq_chain

        self._setup_presets()

        if run:
            self._scan.run()

        return self._scan

    def _setup_scan(
        self,
        motor,
        start_pos,
        end_pos,
        tomo_n,
        expo_time,
        header={},
        scan_info={},
        save=True,
    ):
        """
        Build and return finterlaced
        """
        log_info("tomo", "projection_scan() entering")

        finterlaced = self._finterlaced
        finterlaced.pars.motor = motor
        finterlaced.pars.start_pos = start_pos
        finterlaced.pars.acq_size = (end_pos - start_pos) / tomo_n
        finterlaced.pars.acq_time = expo_time
        finterlaced.pars.npoints = tomo_n
        finterlaced.prepare(scan_info)
        proj_scan = finterlaced.scan

        log_info("tomo", "projection_scan() leaving")

        return proj_scan

    def _estimate_scan_duration(self, tomo_pars):
        """
        Get scan time already estimated in finterlaced.validate()
        """
        finterlaced = self._finterlaced
        finterlaced.validate()
        return finterlaced.inpars.scan_time

    def _add_metadata(
        self, motor, start_pos, end_pos, expo_time, proj_n, scan_info=None
    ):
        """
        Fill the scan_info dictionary with the scan meta data
        """
        scan_info = ScanInfo.normalize(scan_info)
        alias_name = "None"
        ALIASES = current_session.env_dict["ALIASES"]

        if ALIASES.get_alias(motor.name) is not None:
            alias_name = ALIASES.get_alias(motor.name)
        technique = scan_info.setdefault("technique", {})
        technique["image_key"] = NxTomoImageKey.PROJECTION.value
        technique["proj"] = {
            "motor": ["rotation", motor.name, alias_name],
            "scan_type": "INTERLACED",
            "scan_range": end_pos - start_pos,
            "proj_n": proj_n,
            "exposure_time": expo_time,
            "exposure_time@units": "s",
        }
        return scan_info

    def get_scan_preset_list(self):
        """
        Return list of configured scan presets
        """
        # get the scan presets on the tomo runner
        preset_list = super().get_scan_preset_list()
        # get the scan presets on the fscan
        for i in self._finterlaced._scan_presets.keys():
            preset_list.append(f"{self._projection_scan_name}:{i}")
        return preset_list

    def get_chain_preset_list(self):
        """
        Return list of configured chain presets
        """
        # get the chain presets on the tomo runner
        preset_list = super().get_chain_preset_list()
        # get the chain presets on the fscan
        for i in self._finterlaced._chain_presets.keys():
            preset_list.append(f"{self._projection_scan_name}:{i}")
        return preset_list
