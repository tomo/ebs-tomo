from bliss.scanning.toolbox import ChainBuilder
from bliss.controllers.lima.lima_base import Lima
from bliss.controllers.lima.limatools import LimaTakeDisplay
from bliss.scanning.scan import Scan
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.scan_info import ScanInfo
from bliss.scanning.acquisition.timer import SoftwareTimerMaster
from bliss.controllers.counter import CounterController
from tomo.scan.tomo_runner import TomoRunner


class ImageRunner(TomoRunner):
    """
    Class used to build and run scan with one detector
    """

    def __call__(
        self,
        expo_time,
        nbframes,
        *counters,
        scan_info=None,
        title="image_scan",
        lima_acq_params=None,
        lima_ctrl_params=None,
        run=True,
        save=True,
    ):
        """
        Build and run scan by given parameters directly like
        bliss common scans
        Scan and chain configured presets are added on scan
        """
        scan_info = ScanInfo.normalize(scan_info)
        if lima_acq_params is None:
            lima_acq_params = {}
        if lima_ctrl_params is None:
            lima_ctrl_params = {}

        self._scan = self._setup_scan(
            expo_time,
            nbframes,
            *counters,
            title=title,
            lima_acq_params=lima_acq_params,
            lima_ctrl_params=lima_ctrl_params,
            save=save,
            scan_info=scan_info,
        )
        self._setup_presets()
        if run:
            self._scan.run()

        return self._scan

    def _setup_scan(
        self,
        expotime,
        nbframes,
        *counters,
        title=None,
        lima_acq_params={},
        lima_ctrl_params={},
        scan_info={},
        save=True,
    ):
        """
        Build and return scan
        Detector acquisition is triggered by soft
        All counters (roi, image, ...) on detector are taken into account
        as all other counters enabled in ACTIVE_MG
        """
        lima_acq = {
            "acq_nb_frames": nbframes,
            "acq_expo_time": expotime,
            "acq_trigger_mode": "INTERNAL_TRIGGER",
            # The right way to do that in BLISS 2.0 is False/False
            "prepare_once": False,
            "start_once": False,
        }
        lima_ctrl = {}

        # merge all acquisition and controller related parameters
        lima_acq.update(lima_acq_params)
        lima_ctrl.update(lima_ctrl_params)

        # build the acquisition chain
        chain = AcquisitionChain()

        timer = SoftwareTimerMaster(expotime, npoints=1)

        counters = [counter for counter in counters]
        builder = ChainBuilder(counters)

        limadevs = list()
        for node in builder.get_nodes_by_controller_type(Lima):
            limadevs.append(node.controller)
            lima_acq.update({"acq_mode": node.controller.acquisition.mode})

            node.set_parameters(acq_params=lima_acq, ctrl_params=lima_ctrl)

            for child_node in node.children:
                lima_children_params = {"count_time": expotime, "npoints": nbframes}
                child_node.set_parameters(acq_params=lima_children_params)

            chain.add(timer, node)

        for node in builder.get_nodes_by_controller_type(CounterController):
            if not isinstance(node.controller, Lima):
                node.set_parameters(acq_params={"count_time": expotime, "npoints": 0})
                chain.add(timer, node)

        scan_info.update({"type": "image_scan"})

        scan = Scan(
            chain,
            scan_info=scan_info,
            name=title,
            save=save,
            scan_progress=LimaTakeDisplay(),
        )

        return scan
