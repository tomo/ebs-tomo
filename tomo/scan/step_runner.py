from __future__ import annotations
from typing import Optional, Tuple, Sequence
import numpy
import logging

from bliss.common.logtools import log_info, log_error
from bliss.common.axis import Axis
from bliss.scanning.toolbox import ChainBuilder
from bliss.controllers.lima.lima_base import Lima
from bliss.scanning.scan_info import ScanInfo
from bliss import current_session
from bliss.common.cleanup import error_cleanup, capture_exceptions
from bliss.common.cleanup import axis as cleanup_axis
from tomo.scan.tomo_runner import TomoRunner
from tomo.constants import NxTomoImageKey
from tomo.helpers.nexuswriter_utils import hdf5_device_names


from bliss.common.scans.step_by_step import lookupscan
from bliss.common.utils import typecheck
from bliss.common.types import (
    _int,
    _float,
    _countables,
    Scannable,
)


_logger = logging.getLogger(__name__)


@typecheck
def arscan(
    motor: Scannable,
    start: _float,
    stop: _float,
    intervals: _int,
    count_time: _float,
    *counter_args: _countables,
    randaxes: Optional[Sequence[Tuple[Scannable, _float]]] = None,
    scan_type: Optional[str] = None,
    name: Optional[str] = None,
    title: Optional[str] = None,
    save: bool = True,
    save_images: Optional[bool] = None,
    sleep_time: Optional[_float] = None,
    stab_time: Optional[_float] = None,
    run: bool = True,
    return_scan: bool = True,
    scan_info: Optional[dict] = None,
    restore_motor_positions: bool = False,
):
    motor_tuple_list = [(motor, start, stop)]
    npoints = intervals + 1

    scan_info = ScanInfo.normalize(scan_info)

    motors_positions = list()
    title_list = list()
    starts_list = list()
    stops_list = list()
    scan_axes = set()
    for m_tup in motor_tuple_list:
        mot = m_tup[0]
        if mot in scan_axes:
            raise ValueError(f"Duplicated axis {mot.name}")
        scan_axes.add(mot)
        d = mot._set_position if scan_type == "dscan" else 0
        start = m_tup[1] + d
        stop = m_tup[2] + d
        title_list.extend(
            (
                mot.name,
                mot.axis_rounder(start),
                mot.axis_rounder(stop),
            )
        )
        motors_positions.append((mot, numpy.linspace(start, stop, npoints)))
        starts_list.append(start)
        stops_list.append(stop)

    if randaxes:
        for (ax, delta) in randaxes:
            # generate random positions
            pos = numpy.random.uniform(low=-delta, high=delta, size=npoints)

            # force pos=0 at first point and last point
            pos[0] = 0
            pos[-1] = 0

            # force pos=0 at middle point if npoints is odd
            if npoints % 2:
                pos[int((npoints - 1) / 2)] = 0

            motors_positions.append((ax, pos))

    # Specify a default plot if it is not already the case
    if not scan_info.has_default_curve_plot():
        mot = motor_tuple_list[0][0]
        scan_info.add_curve_plot(x=f"axis:{mot.name}")

    scan_info["start"] = starts_list
    scan_info["stop"] = stops_list

    for motor, start, stop in motor_tuple_list:
        d = motor.position if scan_type == "dscan" else 0
        scan_info.set_channel_meta(
            f"axis:{motor.name}", start=start + d, stop=stop + d, points=npoints
        )

    scan_params = dict()
    scan_params["start"] = starts_list
    scan_params["stop"] = stops_list

    # scan type is forced to be either aNscan or dNscan
    if scan_type == "dscan":
        scan_type = (
            f"d{len(title_list)//3}scan" if len(title_list) // 3 > 1 else "dscan"
        )
    else:
        scan_type = (
            f"a{len(title_list)//3}scan" if len(title_list) // 3 > 1 else "ascan"
        )

    if not name:
        name = scan_type

    if not title:
        args = [scan_type.replace("d", "a")]
        args += title_list
        args += [intervals, count_time]
        template = " ".join(["{{{0}}}".format(i) for i in range(len(args))])
        title = template.format(*args)

    return lookupscan(
        motors_positions,
        count_time,
        *counter_args,
        save=save,
        save_images=save_images,
        run=run,
        title=title,
        name=name,
        scan_type=scan_type,
        sleep_time=sleep_time,
        stab_time=stab_time,
        return_scan=return_scan,
        scan_info=scan_info,
        scan_params=scan_params,
        restore_motor_positions=restore_motor_positions,
    )


class StepRunner(TomoRunner):
    """
    Class used to build and run step by step scan
    Built as a layer of bliss standard ascan
    """

    def __init__(self, default_single_chain=None, default_accumulation_chain=None):
        super().__init__(
            single_chain=default_single_chain, acc_chain=default_accumulation_chain
        )
        self._projection_scan_name = "ascan"
        self._motor = None
        self._start_pos = None
        self._end_pos = None
        self._tomo_n = None
        self._expo_time = None
        self._detectors = None

    def __call__(
        self,
        motor: Axis | None,
        start_pos,
        end_pos,
        tomo_n,
        expo_time,
        latency_time=None,
        rand_y_axis: Axis | None = None,
        rand_z_axis: Axis | None = None,
        rand_disp_y: float = 0,
        rand_disp_z: float = 0,
        rand_disp_y_px: float = 0,
        rand_disp_z_px: float = 0,
        scan_info=None,
        header=None,
        run=True,
        save=True,
    ):
        """
        Build and run scan by given parameters directly like bliss
        standard ascan or arscan.

        Some specific metadata are filled in scan info

        Scan and chain configured presets are added on scan

        Arguments:
            rand_y_axis: Optional scan to horizontally move the sample randomly between each steps
            rand_disp_y: Dispersion for the motion of `rand_y_axis` (in axis unit)
            rand_z_axis: Optional scan to vertically move the sample randomly between each steps
            rand_disp_z: Dispersion for the motion of `rand_z_axis` (in axis unit)
        """
        self.scan_info = ScanInfo.normalize(scan_info)
        if header is None:
            header = {}

        # Add the user and scan meta data
        scan_info = self._add_metadata(
            motor,
            start_pos,
            end_pos,
            expo_time,
            tomo_n,
            latency_time,
            scan_info,
            rand_y_axis=rand_y_axis,
            rand_z_axis=rand_z_axis,
            rand_disp_y_px=rand_disp_y_px,
            rand_disp_z_px=rand_disp_z_px,
        )

        self._scan = self._setup_scan(
            motor,
            start_pos,
            end_pos,
            tomo_n,
            expo_time,
            latency_time,
            header=header,
            save=save,
            scan_info=scan_info,
            rand_y_axis=rand_y_axis,
            rand_z_axis=rand_z_axis,
            rand_disp_y=rand_disp_y,
            rand_disp_z=rand_disp_z,
        )
        self._chain = self._scan.acq_chain

        self._setup_presets()

        if run:
            # on error go back to initial position
            restore_list = (cleanup_axis.POS,)
            with error_cleanup(motor, restore_list=restore_list):
                with capture_exceptions(raise_index=0) as capture:
                    with capture():
                        self._scan.run()
                    # test if an error has occured
                    if len(capture.failed) > 0:
                        log_error(
                            "tomo",
                            "A problem occured during the step scan, scan aborted",
                        )
                        log_error("tomo", capture.exception_infos)
                        log_error("tomo", "\n")

        return self._scan

    def _setup_scan(
        self,
        motor,
        start_pos,
        end_pos,
        tomo_n,
        expo_time,
        latency_time,
        header={},
        scan_info={},
        save=True,
        rand_y_axis: Axis | None = None,
        rand_z_axis: Axis | None = None,
        rand_disp_y: float = 0,
        rand_disp_z: float = 0,
    ):
        """
        Build and return ascan
        """
        log_info("tomo", "projection_scan() entering")

        randaxes = []
        if rand_y_axis is not None:
            if rand_disp_y != 0:
                randaxes.append((rand_y_axis, rand_disp_y))
            else:
                print(f"No rand_disp_y. Motor {rand_y_axis.name} ignored")
        if rand_z_axis is not None:
            if rand_disp_z != 0:
                randaxes.append((rand_z_axis, rand_disp_z))
            else:
                print(f"No rand_disp_z. Motor {rand_z_axis.name} ignored")

        self._motor = motor
        self._start_pos = start_pos
        self._end_pos = end_pos
        self._tomo_n = tomo_n
        self._expo_time = expo_time
        self._latency_time = latency_time

        builder = ChainBuilder([])
        self._detectors = []
        for node in builder.get_nodes_by_controller_type(Lima):
            self._detectors.append(node.controller)

        with self._setup_detectors_in_default_chain(self._detectors):
            proj_scan = arscan(
                motor,
                start_pos,
                end_pos,
                tomo_n,
                expo_time,
                randaxes=randaxes,
                sleep_time=latency_time,
                run=False,
                scan_info=scan_info,
            )

        log_info("tomo", "projection_scan() leaving")

        return proj_scan

    def _estimate_scan_duration(self, tomo_pars):
        """
        Estimate scan time by taking into account motor displacement and image
        acquisition time for each angle
        """
        step = (self._end_pos - self._start_pos) / self._tomo_n
        return (
            self.mot_disp_time(self._motor, step, self._motor.velocity) * self._tomo_n
            + (self._tomo_n + 1) * self._expo_time
            + self._latency_time * self._tomo_n
        )

    def _add_metadata(
        self,
        motor,
        start_pos,
        end_pos,
        expo_time,
        proj_n,
        latency_time,
        scan_info=None,
        rand_y_axis: Axis | None = None,
        rand_z_axis: Axis | None = None,
        rand_disp_y_px: float = 0,
        rand_disp_z_px: float = 0,
    ):
        """
        Fill the scan_info dictionary with the scan meta data
        """
        scan_info = ScanInfo.normalize(scan_info)
        alias_name = "None"
        ALIASES = current_session.env_dict["ALIASES"]

        if ALIASES.get_alias(motor.name) is not None:
            alias_name = ALIASES.get_alias(motor.name)
        technique = scan_info.setdefault("technique", {})
        technique["image_key"] = NxTomoImageKey.PROJECTION.value
        technique["proj"] = {
            "motor": ["rotation", motor.name, alias_name],
            "scan_type": "STEP",
            "scan_range": end_pos - start_pos,
            "proj_n": proj_n + 1,
            "exposure_time": expo_time,
            "exposure_time@units": "s",
            "latency_time": latency_time,
            "latency_time@units": "s",
        }
        if rand_y_axis is not None:
            technique["proj"]["rand_y_axis"] = hdf5_device_names(rand_y_axis)
            technique["proj"]["rand_disp_y"] = rand_disp_y_px
        if rand_z_axis is not None:
            technique["proj"]["rand_z_axis"] = hdf5_device_names(rand_z_axis)
            technique["proj"]["rand_disp_z"] = rand_disp_z_px

        return scan_info
