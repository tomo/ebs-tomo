import weakref
from bliss.scanning.scan import ScanPreset


class InhibitAutoProjectionPreset(ScanPreset):
    """Inhibit the auto projection provided by the specified TomoConfig"""

    def __init__(self, tomoconfig):
        from tomo.controllers.tomo_config import TomoConfig

        assert isinstance(tomoconfig, TomoConfig)
        super(InhibitAutoProjectionPreset, self).__init__()
        self.__tomoconfig = weakref.ref(tomoconfig)
        self.__context = None

    @property
    def _tomoconfig(self):
        return self.__tomoconfig()

    def prepare(self, scan):
        assert self.__context is None
        tomoconfig = self._tomoconfig
        self.__context = tomoconfig.auto_projection.inhibit()
        self.__context.__enter__()

    def stop(self, scan):
        self.__context.__exit__(None, None, None)
        self.__context = None
