import logging
from bliss.scanning.scan import ScanPreset

_logger = logging.getLogger(__name__)


class CommonHeaderPreset(ScanPreset):
    """
    Class used by reference, projection, dark and static images to set
    image header.

    **Attributes**

    detectors : Bliss controller object
        list of detectors on which image header must be set
    header : dict
        contains info that will appear in image header
    """

    def __init__(self, *detectors, header):
        super().__init__()
        self.detectors = detectors
        self.header = header

    def prepare(self, scan):
        """
        Reset detector image header if header has been provided.
        Set detector image header with header provided.
        """
        if not self.header:
            self.reset_image_header()
        else:
            self.image_header(self.header)

    def image_header(self, header):
        """
        Set saving common header of detector proxy with header attribute (dictionary)
        """
        header_list = list()
        for key, value in header.items():
            temp = key + "=" + value
            header_list.append(temp)

        for detector in self.detectors:
            detector.proxy.saving_common_header = header_list

    def reset_image_header(self):
        """
        Reset saving common header of detector proxy
        """
        for detector in self.detectors:
            detector.proxy.resetCommonHeader()

    def start(self, scan):
        pass

    def stop(self, scan):
        """
        Reset detector image header
        """
        self.reset_image_header()
