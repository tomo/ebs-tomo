import logging
from bliss.scanning.scan import ScanPreset
from bliss.scanning.acquisition.lima import LimaAcquisitionMaster

_logger = logging.getLogger(__name__)


class DarkShutterPreset(ScanPreset):
    """
    Class used to close beam shutter before taking dark images
    and reopen it after acquisition.

    **Attributes**

    shutter : beam shutter object
        Beam shutter to be used
    sh_was_open : boolean
        flag used to check if shutter is opened before closing it
        in order to reopens it after acquisition if it is the case
    """

    def __init__(self, shutter=None, open_shutter=True):
        super().__init__()
        self.shutter = shutter
        self.open_shutter = open_shutter

    def prepare(self, scan):
        """
        Close shutter
        """

        self.restore_params = {}
        self.restore_shutter_mode = None

        dets = [
            node.device
            for node in scan.acq_chain.nodes_list
            if isinstance(node, LimaAcquisitionMaster)
        ]
        for det in dets:
            if det.proxy.lima_type == "Frelon":
                det.shutter.mode = "MANUAL"
                det.shutter.close_time = 0.0
            elif det.proxy.lima_type == "Ximea":
                proxy = det._get_proxy("Ximea")
                self.restore_params[proxy] = ("gpo_mode", proxy.gpo_mode)
                proxy.gpo_mode = "OFF"
                self.restore_shutter_mode = self.shutter.mode
                self.shutter.mode = self.shutter.MANUAL

        print(f"\nPreparing shutter for {scan.name}\n")
        print(f"Closing the dark shutter")

        self.shutter.close()

    def start(self, scan):
        pass

    def stop(self, scan):
        """
        Open shutter
        """
        if self.open_shutter:
            print(f"Opening the dark shutter")
            self.shutter.open()

        for obj, (attr, value) in self.restore_params.items():
            setattr(obj, attr, value)

        if self.restore_shutter_mode is not None:
            self.shutter.mode = self.restore_shutter_mode
