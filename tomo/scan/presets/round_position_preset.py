import logging
import time
from bliss.scanning.scan import ScanPreset
from bliss.shell.standard import mv

_logger = logging.getLogger(__name__)


class RoundPositionPreset(ScanPreset):
    """
    Class used to normalize current rotation position between 0 and 360 degrees

    **Attributes**

    rotation_axis: Bliss axis object used to rotate sample
    """

    def __init__(self, rotation_axis):
        super().__init__()
        self._rotation_axis = rotation_axis

    def stop(self, scan):
        mv(self._rotation_axis, round(self._rotation_axis.position / 360) * 360)
        self._rotation_axis.position = 0
        self._rotation_axis.dial = 0
