import logging
from bliss.scanning.scan import ScanPreset

_logger = logging.getLogger(__name__)


class MusstTimerConfigPreset(ScanPreset):
    """
    Class used by helical sequence to adapt musst timer clock config to
    each tomo scan.

    For the moment, helical sequence is composed of 3 scans:

    - one basic 360 tomo scan
    - one helical scan (rotation and z axis move continuously)
    - one basic 360 tomo scan

    Because scan time is not the same between basic and helical scans and
    with current sequence structure, we need this preset to setup musst
    timer clock according to tomo scan
    """

    def __init__(self, musst):
        super().__init__()
        self.musst = musst
        """Bliss object used to control musst"""
        self.musst_TMRCFG = musst.TMRCFG[0]
        """musst timer clock config value"""

    def prepare(self, scan):
        """
        Set musst timer clock config with stored value
        """
        self.musst.TMRCFG = self.musst_TMRCFG
