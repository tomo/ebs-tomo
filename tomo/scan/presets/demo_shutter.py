import logging
from bliss.scanning.scan import ScanPreset

_logger = logging.getLogger(__name__)


class DemoShutterPreset(ScanPreset):
    """
    Class used to simulate fast shutter opening before taking reference, static or projection images
    and closing after acquisition.
    """

    def prepare(self, scan):
        """
        Open shutter
        """
        print("PREPARE SHUTTER")

    def start(self, scan):
        """
        Open shutter
        """
        print("OPEN SHUTTER")

    def stop(self, scan):
        """
        Close shutter
        """
        print("CLOSE SHUTTER")
