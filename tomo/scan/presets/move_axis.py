import logging
import numbers
import gevent
import contextlib
from bliss.scanning.scan import ScanPreset
from bliss.common.standard import move
from bliss.common.protocols import Scannable
from bliss import is_bliss_shell
from bliss.shell.standard import umv
from tomo.helpers.locked_axis_motion_hook import LockedAxisMotionHook

_logger = logging.getLogger(__name__)


class MoveAxisPreset(ScanPreset):
    """
    Preset to move axis before scanning.

    Arguments:
        sleep_time: Time to wait after the move, for hardware stabilization
        restore_position: If true, the motor move back at it's original position
                          after the scan. Default is False
        disable_locking: If `True`, disable `LockedAxisMotionHook` from the axis
                         before the motion. The default if `False`.
    """

    def __init__(
        self,
        axis1=None,
        pos1=None,
        axis2=None,
        pos2=None,
        axis3=None,
        pos3=None,
        sleep_time=None,
        restore_position=False,
        disable_locking: bool = False,
    ):
        self._mv_args = []
        self._restore_args = []

        def _define_axis(axis, pos):
            if axis is None and pos is None:
                return
            if axis is None:
                raise ValueError("Axis none but pos defined. Argument skipped")
            if pos is None:
                raise ValueError("Pos none but axis defined. Argument skipped")
            if not isinstance(axis, Scannable):
                raise TypeError("A scannable is expected")
            if not isinstance(pos, numbers.Number):
                raise TypeError("A number is expected")
            self._mv_args.extend([axis, pos])
            self._restore_args.extend([axis, axis.position])

        _define_axis(axis1, pos1)
        _define_axis(axis2, pos2)
        _define_axis(axis3, pos3)
        self._sleep_time = sleep_time
        self._restore_position = restore_position
        self._disable_locking = disable_locking

    def _do_motion(self, *args):
        if is_bliss_shell():
            mv = umv
        else:
            mv = move
        with contextlib.ExitStack() as stack:
            if self._disable_locking:
                for i in range(0, len(args), 2):
                    axis = args[i]
                    for mh in axis.motion_hooks:
                        # The axis was locked, we have to disable it for
                        # LockedAxisMotionHook because it is not reentrant
                        if isinstance(mh, LockedAxisMotionHook):
                            stack.enter_context(mh.disabled_context())
            mv(*self._mv_args)

    def prepare(self, scan):
        self._do_motion(*self._mv_args)
        if self._sleep_time is not None:
            gevent.sleep(self._sleep_time)

    def start(self, scan):
        pass

    def stop(self, scan):
        if self._restore_position:
            self._do_motion(*self._restore_args)
            if self._sleep_time is not None:
                gevent.sleep(self._sleep_time)
