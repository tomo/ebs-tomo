from __future__ import annotations

import os
import contextlib
from bliss.common.hook import MotionHook
from bliss.config.conductor.connection import Connection
from tomo.helpers.locking_helper import lock, unlock, AlreadyLockedDevices
from bliss.scanning.scan import Scan, ScanPreset
from bliss.scanning.acquisition.lima import LimaAcquisitionMaster
from bliss.controllers.lima.lima_base import Lima


class LockedLimaDetectorsPreset(ScanPreset):
    """Lock the detectors contained in the scan

    If the scans devices are already locked, the scan fail at prepare.
    """

    def __init__(self, owner: str):
        MotionHook.__init__(self)
        self._owner = owner
        self._connection: Connection | None = None
        self._enabled = True
        self._locked_devices: list[object] = []

    @property
    def enabled(self) -> bool:
        return self._enabled

    @enabled.setter
    def enabled(self, enabled: bool):
        self._enabled = enabled

    @contextlib.contextmanager
    def disabled_context(self):
        """Disable the preset during a context"""
        prev = self.enabled
        self.enabled = False
        try:
            yield
        finally:
            self.enabled = prev

    def _devices(self, scan: Scan) -> list[Lima]:
        return [
            acq_obj.device
            for acq_obj in scan.acq_chain.nodes_list
            if isinstance(acq_obj, LimaAcquisitionMaster)
        ]

    def prepare(self, scan: Scan):
        if not self._enabled:
            return
        if self._connection is None:
            self._connection = Connection()
            self._connection.set_client_name(f"{self._owner},pid:{os.getpid()}")
        try:
            self._locked_devices = self._devices(scan)
            lock(*self._locked_devices, connection=self._connection, timeout=3)
        except AlreadyLockedDevices:
            self._connection.close()
            self._connection = None
            raise

    def start(self, scan: Scan):
        pass

    def stop(self, scan: Scan):
        if self._connection is not None:
            try:
                unlock(*self._locked_devices, timeout=3)
            finally:
                self._locked_devices = []
                self._connection.close()
                self._connection = None
