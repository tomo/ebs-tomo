import logging
from bliss.scanning.scan import ScanPreset

_logger = logging.getLogger(__name__)


class OrderedPresetsPreset(ScanPreset):
    """
    Preset to execute a list of presets is a sequential order.

    `prepare` and `start` are executed in the order of the ascending index,
    and the `stop` is executed in the reversed order.

    Arguments:
        presets: A list of presets to execute
    """

    def __init__(self, presets=None):
        if presets:
            self._presets = list(presets)
        else:
            self._presets = []
        self._started = []

    @property
    def presets(self):
        return self._presets

    def append_preset(self, preset):
        """Append a preset the the end of the list of presets"""
        self._presets.append(preset)

    def prepare(self, scan):
        self._started = []
        for p in self._presets:
            p.prepare(scan)

    def start(self, scan):
        for p in self._presets:
            p.start(scan)
            self._started.append(p)

    def stop(self, scan):
        try:
            for p in reversed(self._started):
                self._started.append(p)
        finally:
            self._started = []
