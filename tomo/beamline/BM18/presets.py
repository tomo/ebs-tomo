from bliss.scanning.acquisition.lima import LimaAcquisitionMaster
from bliss.scanning.scan import ScanPreset
from bliss.scanning.chain import ChainPreset, ChainIterationPreset


class OpiomPreset(ScanPreset):
    """
    Class used to handle opiom during tomo acquisition

    ***Attributes***
    mux : Bliss controller object
        multiplexer based on opiom card
    tomo_station : string
        name of tomo station used to retrieve related opiom mode
    mode : string
        name of opiom mode associated to tomo station and a given
    """

    def __init__(self, *multiplexer):
        self.mux = multiplexer[0]
        self.mux2 = multiplexer[1]

    def prepare(self, scan):
        camera = [
            node.device
            for node in scan.acq_chain.nodes_list
            if type(node) == LimaAcquisitionMaster
        ]
        if len(camera) == 0:
            raise RuntimeError(
                "no detector is enabled in measurement group, please activate one with tomoccdselect()"
            )
        if len(camera) > 1:
            raise RuntimeError(
                "several detectors are enabled in measurement group, please make sure only one is active"
            )
        camera = camera[0]

        if scan.scan_info['type'] == "ascan":
            return
        elif camera.camera_type.lower() == "basler":
            self.mux.switch("SOURCE", "MUSST_GATE")
            self.mux2.switch("SOURCE", "MUSST_GATE")
        else:
            self.mux.switch("SOURCE", "MUSST_TRIG")
            self.mux2.switch("SOURCE", "MUSST_TRIG")               


class DefaultChainOpiomPreset(ChainPreset):
    """
    Class used for all bliss common scans (ct, ascan, timescan,...) to switch opiom output according to detector

    ***Attributes***
    mux : Bliss controller object
        multiplexer based on opiom card
    detectors_optic : tomo controller object
        links each tomo detector to its optic
    tomo_station : string
        name of tomo station used to retrieve related musst card
    """

    def __init__(self, *multiplexer):
        self.mux = multiplexer[0]
        self.mux2 = multiplexer[1]

    def prepare(self, acq_chain):
        camera = [
            node.device
            for node in acq_chain.nodes_list
            if type(node) == LimaAcquisitionMaster
        ]
        if len(camera) == 0:
            return
            raise RuntimeError(
                "no detector is enabled in measurement group, please activate one with tomoccdselect()"
            )
        if len(camera) > 1:
            raise RuntimeError(
                "several detectors are enabled in measurement group, please make sure only one is active"
            )

        camera = camera[0]

        if (
            camera.camera_type.lower() == "basler"
        ):
            self.mux.switch("SOURCE", "MUSST_GATE")
            self.mux2.switch("SOURCE", "MUSST_GATE")
        else:
            if camera.acquisition.mode == "ACCUMULATION":
                self.mux.switch("SOURCE", "MUSST_TRIG")
                self.mux2.switch("SOURCE", "MUSST_TRIG") 
            else:
                self.mux.switch("SOURCE", "MUSST_GATE")
                self.mux2.switch("SOURCE", "MUSST_GATE") 
