from tomo.sequence.presets.base import SequencePreset
from tomo.standard import update_off, update_on, update_is_on
from bliss.scanning.toolbox import ChainBuilder
from bliss.controllers.lima.lima_base import Lima
from tomo.sequencebasic import ScanType
from bliss import setup_globals, current_session
from bliss.config.settings import SimpleSetting
from bliss.common.measurementgroup import get_active as get_active_mg


class UpdateOnOffPreset(SequencePreset):
    def __init__(self, name, config):
        self.update_was_on = None
        super().__init__(name, config)

    def start(self):
        self.update_was_on = False
        if update_is_on():
            self.update_was_on = True
            update_off()

    def stop(self):
        if self.update_was_on:
            update_on()


class ImageCorrOnOffPreset(SequencePreset):
    def __init__(self, name, config):
        super().__init__(name, config)
        self.image_corr = config.get("image_corr")

    def prepare(self):
        self.image_corr_was_on = {}
        self.detectors = []

        builder = ChainBuilder([])
        for node in builder.get_nodes_by_controller_type(Lima):
            self.detectors.append(node.controller)
            self.image_corr_was_on[node.controller] = False
            if (
                node.controller.processing.use_background
                or node.controller.processing.use_flatfield
            ):
                self.image_corr_was_on[node.controller] = True
                self.image_corr.flat_off()
                self.image_corr.dark_off()

    def stop(self):
        for detector in self.detectors:
            if self.image_corr_was_on[detector]:
                self.image_corr.dark_on()
                self.image_corr.flat_on()


class HDF5SavingCheckPreset(SequencePreset):
    def __init__(self, name, config):
        super().__init__(name, config)

    def prepare(self):
        self.saving_format_was_edf = {}
        self.detectors = []

        builder = ChainBuilder([])
        for node in builder.get_nodes_by_controller_type(Lima):
            self.detectors.append(node.controller)
            self.saving_format_was_edf[node.controller] = False
            if node.controller.saving.file_format == "EDF":
                self.saving_format_was_edf[node.controller] = True
                node.controller.saving.file_format = "HDF5"

    def stop(self):
        for detector in self.detectors:
            if self.saving_format_was_edf[detector]:
                detector.saving.file_format = "EDF"


class FastShutterSequencePreset(SequencePreset):
    """
    Class used to open fast shutter before taking reference, static or projection images
    and close it after acquisition.

    ***Attributes***
    shutter : Tomo shutter object
        contains methods to control shutters
    """

    def __init__(self, tomo, shutter):
        self.tomo = tomo
        self.shutter = shutter

    def prepare(self):
        """
        Opens shutter if shutter controlled by detector
        """
        self.soft_shutter = False

        det = self.tomo._detector

        if det is not None and det.camera_type == "Frelon":
            if det.camera.image_mode == "FULL FRAME":
                det.shutter.mode = "AUTO_FRAME"
                det.shutter.close_time = self.shutter.closing_time
            else:
                self.soft_shutter = True
                det.shutter.mode = "MANUAL"
                det.shutter.close_time = 0.0
        elif self.shutter is not None:
            self.soft_shutter = True

    def start(self):
        """
        Opens shutter if shutter controlled by soft
        """
        if self.soft_shutter:
            print("Opening shutter")
            self.shutter.open()

    def stop(self):
        """
        Closes shutter if shutter controlled by soft
        """
        if self.soft_shutter:
            print("Closing shutter")
            self.shutter.close()


class SzStatusCheckPreset(SequencePreset):
    def __init__(self, name, config):
        self.axis = config.get("axis")
        super().__init__(name, config)

    def start(self):
        if not self.axis.state.READY:
            self.axis.rmove(-0.001)
            self.axis.rmove(0.001)


class AccMarginPreset(SequencePreset):
    def __init__(self, name, config):
        tomo = config.get("tomo")
        self.tomo_pars = tomo.pars
        self.cont_scan = tomo.get_runner("CONTINUOUS")
        super().__init__(name, config)

    def prepare(self):
        if self.tomo_pars.scan_type == "CONTINUOUS":
            step_size = self.tomo_pars.range / self.tomo_pars.tomo_n
            self.cont_scan.pars.acc_margin = step_size


class AccMarginPreset(SequencePreset):
    def __init__(self, fscan_config, sequence):
        self._fscan = fscan_config.get_runner("fscan")
        self._sequence = sequence
    def prepare(self):
        builder = ChainBuilder([])
        for node in builder.get_nodes_by_controller_type(Lima):
            det = node.controller
        self._fscan.pars.motor = self._sequence.tomo_config.rotation_axis
        self._fscan.pars.start_pos = self._sequence.pars.start_pos
        self._fscan.pars.acq_time = self._sequence.pars.exposure_time
        self._fscan.pars.npoints = self._sequence.pars.tomo_n
        self._fscan.pars.step_size = self._sequence.pars.range / self._sequence.pars.tomo_n
        self._fscan.pars.step_time = 0
        self._fscan.pars.latency_time = self._sequence.pars.latency_time
        if det.camera_type.lower() == "pco":
            self._fscan.validate()
            if self._fscan.inpars.acc_time >= 3:
                self._fscan.pars.acc_margin = round(
                    (
                        (
                            2.9
                            - self._fscan.inpars.speed
                            / self._fscan.inpars.motor.acceleration
                        )
                        * self._fscan.inpars.speed
                    ),
                    3,
                )
        if det.camera_type.lower() == "iris_15":
            self._fscan.pars.acc_margin = self._fscan.pars.step_size
            self._fscan.validate()
            det.camera.acq_timeout = self._fscan.inpars.acc_time+5


class AccMarginHelicalPreset(SequencePreset):
    def __init__(self, fscan_config, sequence):
        self._fscan = fscan_config.get_runner("fscan")
        self._f2scan = fscan_config.get_runner("f2scan")
        self._sequence = sequence

    def prepare(self):
        builder = ChainBuilder([])
        for node in builder.get_nodes_by_controller_type(Lima):
            det = node.controller
        self._f2scan.pars.motor = self._sequence.tomo_config.rotation_axis
        self._f2scan.pars.slave_motor = self._sequence.tomo_config.z_axis
        self._f2scan.pars.start_pos = self._sequence.pars.start_pos
        self._f2scan.pars.acq_time = self._sequence.pars.exposure_time
        self._f2scan.pars.npoints = self._sequence.pars.tomo_n
        self._f2scan.pars.step_size = self._sequence.pars.range / self._sequence.pars.tomo_n
        self._f2scan.pars.step_time = 0
        self._f2scan.pars.slave_start_pos = self._sequence.pars.slave_start_pos
        self._f2scan.pars.slave_step_size = self._sequence.pars.slave_step_size / self._sequence.pars.tomo_n
        self._fscan.pars.acc_margin = self._f2scan.pars.step_size
        self._f2scan.pars.acc_margin = self._f2scan.pars.step_size
        self._f2scan.pars.latency_time = self._sequence.pars.latency_time
        self._f2scan.validate()
        acc_margin = self._f2scan.master.mot_master.master["undershoot_start_margin"]
        acc_time = self._f2scan.inpars.speed / self._f2scan.pars.motor.acceleration + acc_margin / self._f2scan.inpars.speed
        if det.camera_type.lower() == "pco":
            if acc_time >= 3:
                self._fscan.pars.acc_margin = 0
                self._f2scan.pars.acc_margin = 0
                self._f2scan.pars.slave_acc_margin = 0
        if det.camera_type.lower() == "iris_15":
            det.camera.acq_timeout = acc_time+5


class MetadataPreset(SequencePreset):
    def __init__(self):
        self.scan_saving = current_session.scan_saving

    def prepare(self):
        current_session.icat_metadata.resetup()
        setup_globals.decoheror_metadata()

    def stop(self):
        if (
            "_" in self.scan_saving.dataset_name
            and self.scan_saving.dataset_name.split("_")[-1].isnumeric()
        ):
            dataset_name = "_".join(self.scan_saving.dataset_name.split("_")[:-1])
        else:
            dataset_name = self.scan_saving.dataset_name

        current_session.scan_saving.enddataset()

        current_session.scan_saving.newdataset(dataset_name)


class MusstMaxAccuracySequencePreset(SequencePreset):
    """
    Class used to open fast shutter before taking reference, static or projection images
    and close it after acquisition.

    ***Attributes***
    shutter : Tomo shutter object
        contains methods to control shutters
    """

    def __init__(self, musst):
        self.musst = musst

    def prepare(self):
        """
        Opens shutter if shutter controlled by detector
        """
        self.musst.TMRCFG = "50MHZ"


class FrontEndPreset(SequencePreset):
    def __init__(self, machinfo, frontend):
        self.frontend = frontend
        self.machinfo = machinfo

    def prepare(self):
        beam_status = self.machinfo.proxy.mode
        delivery_mode = (
            "delivery" in self.machinfo.all_information.get("SR_Operator_Mesg").lower()
        )
        if not self.frontend.is_open and beam_status == 2 and delivery_mode and self.machinfo.counters.current.value > 5:
            self.frontend.open()

class RemoveBackwardDispPreset(SequencePreset):
    def __init__(self, sequence):
        self.fscanloop = sequence.get_runner("MULTITURNS")._fscanloop
        self.sequence = sequence
        self.pars = sequence.pars
        self.motor = sequence._rotation_axis

    def prepare(self):
        self.sequence.init_sequence()
        if (
            hasattr(self.sequence._detector.camera, "cam_type")
            and "dimax" in self.sequence._detector.camera.cam_type.lower()
        ):
            self.sequence.set_min_acc_turns()
        fscanloop = self.fscanloop
        fscanloop.pars.motor = self.motor
        fscanloop.pars.step_size = self.pars.range / self.pars.tomo_n
        fscanloop.pars.acq_time = self.pars.exposure_time
        fscanloop.pars.step_time = fscanloop.pars.acq_time
        fscanloop.pars.acc_margin = 0
        fscanloop.validate()
        fscanloop.pars.acc_margin = (
            self.pars.start_pos
            + self.pars.start_turns * 360
            - fscanloop.inpars.acc_disp
        )
        if (
            hasattr(self.sequence._detector.camera, "cam_type")
            and "dimax" not in self.sequence._detector.camera.cam_type.lower()
        ):
            self.sequence.pars.start_turns = fscanloop.inpars.acc_disp / 360


class HalfAcquiPreset(SequencePreset):
    def __init__(self, sequence):
        self.sequence = sequence

    def prepare(self):
        if self.sequence._y_axis.position != 0:
            self.sequence.pars.half_acquisition = True
        else:
            self.sequence.pars.half_acquisition = False


class SetMinAccTurnsPreset(SequencePreset):
    def __init__(self, sequence):
        self.sequence = sequence

    def prepare(self):
        self.sequence.set_min_acc_turns()


class CheckAccPreset(SequencePreset):
    def prepare(self):
        builder = ChainBuilder([])
        for node in builder.get_nodes_by_controller_type(Lima):
            det = node.controller
            det_mode = SimpleSetting("mode_" + det.name, default_value="SINGLE")
            if det.acquisition.mode != det_mode.get():
                det.acquisition.mode = det_mode.get()


class DeactivateRoiCountersPreset(SequencePreset):
    def prepare(self):
        builder = ChainBuilder([])
        self.ldet = []
        self.lroicounters = []
        for node in builder.get_nodes_by_controller_type(Lima):
            det = node.controller
            self.ldet.append(det)
            for i, roi in enumerate(list(det.roi_counters.get_rois())):
                name = list(det.roi_counters.keys())[i]
                self.lroicounters.append((name, roi))
            det.roi_counters.clear()

    def stop(self):
        for i, det in enumerate(self.ldet):
            for roi_counters in self.lroicounters:
                det.roi_counters[roi_counters[0]] = roi_counters[1]


class AttCommentPreset(SequencePreset):

    def prepare(self):
        setup_globals.att_comment(preview_comment=False)
