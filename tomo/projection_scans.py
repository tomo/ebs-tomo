# Compatibility with tomo <= 2.3.1

from tomo.scan.continuous_runner import ContinuousRunner  # noqa
from tomo.scan.sweep_runner import SweepRunner  # noqa
from tomo.scan.interlaced_runner import InterlacedRunner  # noqa
from tomo.scan.step_runner import StepRunner  # noqa
from tomo.scan.helical_runner import HelicalRunner  # noqa
from tomo.scan.continuous_2d_runner import Continuous2DRunner  # noqa
from tomo.scan.mesh_runner import MeshRunner  # noqa
from tomo.scan.sweep_2d_runner import Sweep2DRunner  # noqa
from bliss.common import deprecation

deprecation.deprecated_warning(
    "Module",
    "tomo.projection_scans",
    replacement="tomo.scan.{runner_name}",
    since_version="2.3.1",
)
