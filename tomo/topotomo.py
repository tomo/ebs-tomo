import os
import time
import numpy as np
from bliss.common.scans import ascan
from bliss import current_session
from bliss import setup_globals
from bliss.common.cleanup import cleanup, axis as cleanup_axis, capture_exceptions
from bliss.common.logtools import log_info, log_debug, log_error, log_warning
from bliss.shell.standard import umv, mv
from tomo.sequencebasic import SequenceBasic, SequenceBasicPars, ScanType
from bliss.scanning.scan_info import ScanInfo
from tomo.helpers.proxy_param import ProxyParam
from tomo.sequencebasic import cleanup_sequence
from bliss.scanning.group import Sequence
from tomo.scan.presets.common_header import CommonHeaderPreset


class TopoTomoPars(SequenceBasicPars):
    """
    Class to create topotomo sequence parameters

    **Parameters**:

    nested_start_pos: int
        scan start position for nested axis
    nested_end_pos: int
        scan stop position for nested axis
    nested_npoints: int
        scan number of points for nested axis
    """

    DEFAULT = dict(
        **SequenceBasicPars.DEFAULT,
        **{"nested_start_pos": 0.0, "nested_end_pos": 100.0, "nested_npoints": 100},
    )


class TopoTomo(SequenceBasic):
    """
    Class to handle topotomo acquisition

    Compare to fasttomo, this sequence uses a step motor to trigger at each projection angle a nested motor that will be scanned

    .. code-block:: yaml

        name: mrtopo_tomo
        plugin: bliss
        class: TopoTomo
        package: tomo.topotomo

        tomo_config: $mrtomo_config
        nested_axis: $mryrot
    """

    def __init__(self, name, config):
        self.name = name + "_sequence"
        """Bliss object name"""
        super().__init__(name, config)
        self._nested_axis = config["nested_axis"]

    def _create_parameters(self):
        """
        Create pars attribute containing sequence, basic and config pars
        """
        tomo_config = self.tomo_config
        sequence_pars = TopoTomoPars(self.name)
        return ProxyParam(tomo_config.pars, sequence_pars)

    def validate(self):
        """
        Check parameters coherency
        Estimate sequence acquisition time
        """

        self._inpars.end_pos = self.pars.nested_start_pos + self.pars.nested_npoints
        self._inpars.field_of_view = "Full"

        darks_in_sequence = [
            self._scans[i] for i, name in enumerate(self._scans_names) if name == "dark"
        ]
        flats_in_sequence = [
            self._scans[i] for i, name in enumerate(self._scans_names) if name == "flat"
        ]
        returns_in_sequence = [
            self._scans[i]
            for i, name in enumerate(self._scans_names)
            if name == "return_ref"
        ]
        projs_in_sequence = [
            self._scans[i]
            for i, name in enumerate(self._scans_names)
            if name in ScanType.values
        ]

        nb_proj_scan = len(projs_in_sequence)
        nb_dark_scan = len(darks_in_sequence)
        nb_flat_scan = len(flats_in_sequence)
        nb_return_scan = len(returns_in_sequence)

        proj_time = nb_proj_scan * self._inpars.proj_time
        dark_time = nb_dark_scan * self.pars.dark_n * self.pars.exposure_time
        flat_runner = self.get_runner("flat")
        flat_time = nb_flat_scan * flat_runner._estimate_scan_duration(self._inpars)

        if self.pars.return_images_aligned_to_flats:
            disp = self.pars.flat_on * self.pars.range / self.pars.tomo_n
        else:
            disp = 90
        return_runner = self.get_runner("return_ref")
        return_time = nb_return_scan * return_runner._estimate_scan_duration(
            self._inpars
        )

        self._inpars.scan_time = dark_time + flat_time + proj_time + return_time

    def add_metadata(self, scan_info):
        """
        Fill the `scan_info` dictionary with topotomo related metadata.

        - nested axis name and alias name if one exists
        - nested axis scan start position
        - nested axis scan stop position
        - nested axis scan number of points
        """
        super().add_metadata(scan_info)
        scan_info["technique"]["scan"]["sequence"] = "tomo:topotomo"
        scan_info["technique"]["scan"]["nested_start_pos"] = self.pars.nested_start_pos
        scan_info["technique"]["scan"]["nested_end_pos"] = self.pars.nested_end_pos
        scan_info["technique"]["scan"]["nested_npoints"] = self.pars.nested_npoints
        motor_name = self._nested_axis.name
        alias_name = "None"
        if setup_globals.ALIASES.get_alias(self._nested_axis.name) is not None:
            alias_name = setup_globals.ALIASES.get_alias(self._nested_axis.name)
        scan_info["technique"]["scan"]["motor"].extend(
            ["nested_axis", motor_name, alias_name]
        )

    def add_projections_group(
        self,
        tomo_start_pos=None,
        tomo_end_pos=None,
        tomo_n=None,
        expo_time=None,
        nested_start_pos=None,
        nested_end_pos=None,
        nested_npoints=None,
        flat_on=None,
        proj_scan=None,
    ):
        """
        Divide tomo acquisition into several projection groups according
        to flat_on parameter
        One projection group is one projection scan followed by flat
        images
        flat_on indicates the number of projections after which flat
        images must be taken
        If number of projection groups deduced from tomo_n and flat_on
        is not an integer, an additional group with a reduced number of
        projections is created
        """
        if flat_on is None:
            flat_on = self.pars.flat_on

        if tomo_n is None:
            tomo_n = self.pars.tomo_n

        if tomo_n < flat_on:
            flat_on = tomo_n

        nb_groups = int(tomo_n / flat_on)
        groups = [tomo_start_pos]
        step = (tomo_end_pos - tomo_start_pos) / tomo_n

        for i in range(nb_groups):
            groups.append(groups[-1] + step * flat_on)

        for i in range(0, len(groups) - 1):
            self.add_proj_group(
                tomo_start_pos=groups[i],
                tomo_end_pos=groups[i + 1],
                tomo_n=flat_on,
                expo_time=expo_time,
                nested_start_pos=nested_start_pos,
                nested_end_pos=nested_end_pos,
                nested_npoints=nested_npoints,
                proj_scan=proj_scan,
            )

        if tomo_n % flat_on != 0:
            self.add_proj_group(
                tomo_start_pos=groups[-1],
                tomo_end_pos=tomo_end_pos,
                tomo_n=tomo_n % flat_on,
                expo_time=expo_time,
                nested_start_pos=nested_start_pos,
                nested_end_pos=nested_end_pos,
                nested_npoints=nested_npoints,
                proj_scan=proj_scan,
            )

    def add_proj_group(
        self,
        tomo_start_pos=None,
        tomo_end_pos=None,
        tomo_n=None,
        expo_time=None,
        nested_start_pos=None,
        nested_end_pos=None,
        nested_npoints=None,
        proj_scan=None,
    ):
        """
        Set the runner that sequence must use to acquire projection
        images of one projection group
        If proj_scan is not specified, default projection runner
        corresponding to scan type parameter will be used
        Add projection scan to the sequence
        Add image common header preset to projection scan
        Add flat scan to the sequence
        """
        if proj_scan is not None:
            self.add_helper_scan(self.pars.scan_type, proj_scan)
        else:
            proj_scan = self.projection_scan(
                self._nested_axis,
                nested_start_pos,
                nested_end_pos,
                nested_npoints,
                self._rotation_axis,
                tomo_start_pos,
                (tomo_end_pos - tomo_start_pos) / tomo_n,
                tomo_n,
                expo_time,
                run=False,
            )
            self.add_helper_scan(self.pars.scan_type, proj_scan)
        detector = self._detector
        header = {}
        header["image_key"] = "0"
        header_preset = CommonHeaderPreset(detector, header=header)
        proj_scan.add_preset(header_preset)
        self._inpars.nb_proj_groups += 1
        self._inpars.start_proj_groups.append(tomo_start_pos)
        self._inpars.end_proj_groups.append(tomo_end_pos)
        self._inpars.points_proj_groups.append(tomo_n)
        end_proj = sum(self._inpars.points_proj_groups)
        start_proj = end_proj - self._inpars.points_proj_groups[-1]
        title = f"projections {int(start_proj)} - {int(end_proj)}"
        proj_scan.scan_info.update({"title": title})
        self._inpars.projection = end_proj
        self.add_flat()

    def add_proj(
        self,
        tomo_start_pos=None,
        tomo_end_pos=None,
        tomo_n=None,
        expo_time=None,
        nested_start_pos=None,
        nested_end_pos=None,
        nested_npoints=None,
        proj_scan=None,
    ):
        """
        Set the runner that sequence must use to acquire projection
        images
        If proj_scan is not specified, default projection runner
        corresponding to scan type parameter will be used
        Add projection scan to the sequence
        Add image common header preset to projection scan
        """
        detector = self._detector
        header = {}
        header["image_key"] = "0"
        header_preset = CommonHeaderPreset(detector, header=header)

        title = f"projections 0 - {int(tomo_n)}"
        self._inpars.projection = tomo_n

        if proj_scan is not None:
            proj_scan.add_preset(header_preset)
            self.add_helper_scan(self.pars.scan_type, proj_scan)
        else:
            proj_scan = self.projection_scan(
                self._nested_axis,
                nested_start_pos,
                nested_end_pos,
                nested_npoints,
                self._rotation_axis,
                tomo_start_pos,
                (tomo_end_pos - tomo_start_pos) / tomo_n,
                tomo_n,
                expo_time,
                run=False,
            )
            proj_scan.add_preset(header_preset)
            self.add_helper_scan(self.pars.scan_type, proj_scan)

        proj_scan.scan_info.update({"title": title})

    def projection_scan(
        self,
        fast_motor,
        fast_start_pos,
        fast_end_pos,
        fast_npoints,
        slow_motor,
        slow_start_pos,
        slow_step_size,
        slow_npoints,
        expo_time,
        scan_info=None,
        run=True,
    ):
        """
        Get runner and build scan corresponding to tomo scan type parameter
        Return projection scan object
        """

        if self.pars.scan_type not in ScanType.values:
            raise ValueError(f"{self.pars.scan_type} is not a recognized type of scan")

        if self.pars.scan_type == ScanType.STEP:
            runner = self.get_runner("MESH")
        elif self.pars.scan_type == ScanType.INTERLACED:
            runner = self.get_runner(self.pars.scan_type)
        else:
            runner = self.get_runner(self.pars.scan_type + "2D")

        if self.pars.scan_type == ScanType.CONTINUOUS:
            latency_time = self.pars.latency_time
            tomo_scan = runner(
                fast_motor,
                fast_start_pos,
                fast_end_pos,
                fast_npoints,
                slow_motor,
                slow_start_pos,
                slow_step_size,
                slow_npoints,
                expo_time,
                latency_time=latency_time,
                scan_info=scan_info,
                run=False,
            )
        elif self.pars.scan_type == ScanType.INTERLACED:
            tomo_scan = runner(
                fast_motor,
                fast_start_pos,
                fast_end_pos,
                fast_npoints,
                expo_time,
                scan_info=scan_info,
                run=False,
            )
        else:
            tomo_scan = runner(
                fast_motor,
                fast_start_pos,
                fast_end_pos,
                fast_npoints,
                slow_motor,
                slow_start_pos,
                slow_step_size,
                slow_npoints,
                expo_time,
                scan_info=scan_info,
                run=False,
            )

        if self.pars.scan_type == ScanType.INTERLACED:
            self._inpars.proj_time = runner._estimate_scan_duration(
                self._inpars
            ) + runner.mot_disp_time(
                slow_motor, slow_step_size, slow_motor.velocity
            ) * (
                slow_npoints - 1
            )
        else:
            self._inpars.proj_time = runner._estimate_scan_duration(self._inpars)

        if run:
            tomo_scan.run()

        return tomo_scan

    def build_sequence(self):
        """
        Build topotomo sequence according to scan option
        parameters
        """
        if self.pars.dark_at_start:
            self.add_dark()
        if self.pars.flat_at_start:
            self.add_flat()

        if self.pars.projection_groups:
            self.add_projections_group(
                self.pars.start_pos,
                self.pars.start_pos + self.pars.range,
                self.pars.tomo_n,
                self.pars.exposure_time,
                self.pars.nested_start_pos,
                self.pars.nested_end_pos,
                self.pars.nested_npoints,
                self.pars.flat_on,
            )
        else:
            self.add_proj(
                self.pars.start_pos,
                self.pars.start_pos + self.pars.range,
                self.pars.tomo_n,
                self.pars.exposure_time,
                self.pars.nested_start_pos,
                self.pars.nested_end_pos,
                self.pars.nested_npoints,
            )
        if self.pars.images_on_return:
            self.add_return()
        if self.pars.flat_at_end:
            self.add_flat()
        if self.pars.dark_at_end:
            self.add_dark()

    def full_turn_scan(
        self,
        nested_start_pos,
        nested_end_pos,
        nested_npoints,
        dataset_name=None,
        collection_name=None,
        tomo_start_pos=None,
        scan_info=None,
        run=True,
    ):
        """
        Build and run tomo of 360 angular range
        """
        self.pars.nested_start_pos = nested_start_pos
        self.pars.nested_end_pos = nested_end_pos
        self.pars.nested_npoints = nested_npoints

        if dataset_name is not None:
            setup_globals.newdataset(dataset_name)
        if collection_name is not None:
            setup_globals.newcollection(collection_name)
        if tomo_start_pos is not None:
            self.pars.start_pos = start_pos
        self.pars.range = 360

        self._setup_sequence("topotomo:fullturn", run=run, scan_info=scan_info)

    def half_turn_scan(
        self,
        nested_start_pos,
        nested_end_pos,
        nested_npoints,
        dataset_name=None,
        collection_name=None,
        tomo_start_pos=None,
        scan_info=None,
        run=True,
    ):
        """
        Build and run tomo of 180 angular range
        """
        self.pars.nested_start_pos = nested_start_pos
        self.pars.nested_end_pos = nested_end_pos
        self.pars.nested_npoints = nested_npoints

        if dataset_name is not None:
            setup_globals.newdataset(dataset_name)
        if collection_name is not None:
            setup_globals.newcollection(collection_name)
        if tomo_start_pos is not None:
            self.pars.start_pos = start_pos
        self.pars.range = 180

        self._setup_sequence("topotomo:halfturn", run=run, scan_info=scan_info)

    def basic_scan(
        self,
        nested_start_pos,
        nested_end_pos,
        nested_npoints,
        tomo_start_pos=None,
        tomo_end_pos=None,
        tomo_n=None,
        expo_time=None,
        dataset_name=None,
        collection_name=None,
        scan_info=None,
        run=True,
    ):
        """
        Build and run tomo using given parameters
        """
        self.pars.nested_start_pos = nested_start_pos
        self.pars.nested_end_pos = nested_end_pos
        self.pars.nested_npoints = nested_npoints

        if dataset_name is not None:
            setup_globals.newdataset(dataset_name)
        if collection_name is not None:
            setup_globals.newcollection(collection_name)
        if tomo_start_pos is not None:
            self.pars.start_pos = tomo_start_pos
        if tomo_end_pos is not None:
            self.pars.range = tomo_end_pos - self.pars.start_pos
        if tomo_n is not None:
            self.pars.tomo_n = tomo_n
        if expo_time is not None:
            self.pars.exposure_time = expo_time

        self._setup_sequence("topotomo:basic", run=run, scan_info=scan_info)

    def __info__(self):
        """
        Show information about the sequence parameters, and detector optic configuration.
        """
        info_str = f"{self.name} configuration info:\n"
        info_str += super().__info__()
        return info_str

    def _send_icat_metadata(self):
        """
        Fill metadata fields related to standard tomo
        Send them to ICAT
        """
        scan_saving = current_session.scan_saving
        if self.pars.scan_type == "STEP":
            acc_disp = 0
        elif self.pars.scan_type == "CONTINUOUS":
            scan = self.get_runner("CONTINUOUS2D")._fscan2d
            acc_disp = scan.inpars.fast_acc_size
        elif self.pars.scan_type == "SWEEP":
            scan = self.get_runner(self.pars.scan_type)._fsweep
            acc_disp = scan.inpars.acc_disp
        elif self.pars.scan_type == "INTERLACED":
            scan = self.get_runner(self.pars.scan_type)._finterlaced
            acc_disp = scan.inpars.acc_disp
        scan_saving.dataset.write_metadata_field("TOMOAcquisition_accel_disp", str(acc_disp))
        super()._send_icat_metadata()

    @cleanup_sequence
    def run(self, user_info=None):
        """
        Run acquisition
        """
        if self.pars.scan_type == ScanType.INTERLACED:
            slow_mot_pos = [
                pos
                for pos in np.linspace(
                    self.pars.start_pos,
                    self.pars.start_pos + self.pars.range,
                    self.pars.tomo_n,
                )
            ]
            start_pos = self.pars.start_pos
            for pos in slow_mot_pos:
                umv(self._rotation_axis, pos)
                self.pars.start_pos = pos
                super().run(user_info)
            self.pars.start_pos = start_pos
        else:
            super().run(user_info)
