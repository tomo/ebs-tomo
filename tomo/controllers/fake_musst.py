from __future__ import annotations
import gevent
import logging
import typing
import hashlib
import numpy
import contextlib
import time

from bliss.controllers.musst import Musst
from bliss.controllers.lima.lima_base import Lima
from bliss.common.axis import Axis
from bliss import current_session
from bliss.common.tango import DeviceProxy

_logger = logging.getLogger(__name__)


class TypedProperty:
    """Handle typed property.

    This is useful to handle fixed bit arithmetic.

    .. code-block::

        class Foo:
            t = TypedProperty("t", numpy.int32)
    """

    def __init__(self, name, dtype):
        self.__name = name
        self.__dtype = dtype

    def _stored_attr_name(self):
        return f"_TypedProperty__{self.__name}"

    def __get__(self, obj, klass):
        try:
            v = getattr(obj, self._stored_attr_name())
        except Exception:
            v = self.__dtype()
        return v

    def __set__(self, obj, value):
        v = self.__dtype(value)
        setattr(obj, self._stored_attr_name(), v)


class FakeMusstProgram:
    """Handle a Musst program written in python.

    A file program can contain many programs.

    Each of them have to be written as a dedicated method of this class using
    the program name with the right typo (usually upper case).

    Each of this programs are called by FakeMusst inside a dedicated greenlet.
    """

    _CHTIMER = "CHTIMER"
    CH0, CH1, CH2, CH3, CH4, CH5 = range(6)
    DOWN = "DOWN"
    UP = "UP"

    _HASH_SOURCES: list[str] = []
    """Used to store the hash of the mprg file sources"""

    def __init__(self, controller):
        self.__controller = controller
        self.__vars = {}
        self.__btrig = 0
        self.__data = []
        self.__store_list: list = []
        self.__target = {}
        self.__evsource_channel = None
        self.__evsource_direction = None
        self.__defevent = None
        self.__timer = 0
        self._init()

    def _init(self):
        pass

    def register_variable(self, name: str, dtype):
        """Called by the FakeMusst to update the program with var set by
        `putget`"""
        if dtype not in [numpy.uint32, numpy.int32]:
            raise ValueError(f"Unsupported dtype {dtype}")
        self.__vars[name] = dtype
        prop = TypedProperty(name, dtype=dtype)
        setattr(type(self), name, prop)

    def set_variable(self, name: str, value: object):
        """Called by the FakeMusst to update the program with var set by
        `putget`"""
        assert name in self.__vars
        dtype = self.__vars[name]
        v = dtype(value)
        setattr(self, name, v)

    def get_variable(self, name) -> str:
        """Returns a string value set by `putget`"""
        assert name in self.__vars
        return getattr(self, name)

    @property
    def TIMER(self):
        current = time.time()
        coef = self.__controller.get_timer_factor()
        delta = int((current - self.__timer_timestamp) * coef)
        return self.__timer + delta

    @TIMER.setter
    def TIMER(self, value):
        self.__timer_timestamp = time.time()
        self.__timer = value

    def ATRIG(self):
        """Called by the implemented python program to simulate a ATRIG."""
        self.__controller.trig_devices("atrig")

    def BTRIG(self, value: typing.Optional[int] = None):
        """Called by the implemented python program to simulate a BTRIG.

        Arguments:
            value: Logic level to set to BTRIG (0 or 1). If none, the current
                   BTRIG level is swapped.
        """
        assert value in [0, 1, None]
        if value is None:
            value = 1 - self.__btrig
        self._set_btrig(value)

    def _set_btrig(self, value):
        if self.__btrig == value:
            return
        self.__btrig = value
        if value == 0:
            self.__controller.trig_devices("btrig_down")
        elif value == 1:
            self.__controller.trig_devices("btrig_up")

    def EVSOURCE(self, channel, direction):
        self.__evsource_channel = channel
        self.__evsource_direction = direction

    def DEFEVENT(self, channel):
        self.__defevent = channel

    def setup_target(self, channel, target):
        self.__target[channel] = target

    def STORELIST(self, *channels):
        self.__store_list = list(channels)

    def STORE(self):
        for channel_id in self.__store_list:
            if channel_id == self._CHTIMER:
                v = self.TIMER
            else:
                channel = self.__controller.get_channel_by_id(channel_id)
                if channel is None:
                    raise RuntimeError(
                        f"Channel id {channel_id} not defined in the Musst"
                    )
                v = channel.value
            self.__data.append(v)

    def CTSTOP_TIMER(self):
        pass

    def CTSTART_TIMER(self):
        pass

    def EMEM_0_AT_0(self):
        self.__data = []

    def stored_data(self):
        return self.__data

    def read_channel(self, channel_id):
        if channel_id == self._CHTIMER:
            return self.TIMER
        else:
            channel = self.__controller.get_channel_by_id(channel_id)
            return channel.value

    @contextlib.contextmanager
    def wait_for_defevent(self):
        _logger.debug("wait_for_defevent")
        if self.__defevent == self._CHTIMER:
            with self.wait_for_timer():
                yield
            return
        channel = self.__controller.get_channel_by_id(self.__defevent)
        target = self.__target[self.__defevent]
        while True:
            v = channel.value
            if self.__evsource_direction == self.DOWN:
                if v <= target:
                    break
            if self.__evsource_direction == self.UP:
                if v >= target:
                    break
            _logger.debug("wait...")
            gevent.sleep(0.01)
        yield

    @contextlib.contextmanager
    def wait_for_timer(self):
        _logger.debug("wait for timer...")
        target = self.__target[self.__defevent]
        while True:
            if self.TIMER >= target:
                break
            _logger.debug("wait...")
            gevent.sleep(0.01)
        yield

    @contextlib.contextmanager
    def wait_for_target(self, channel_id):
        if channel_id == self._CHTIMER:
            with self.wait_for_timer():
                yield
            return
        _logger.debug("wait for target...")
        channel = self.__controller.get_channel_by_id(self.__defevent)
        target = self.__target[self.__defevent]
        while True:
            v = channel.value
            if self.__evsource_direction == self.DOWN:
                if v <= target:
                    break
            if self.__evsource_direction == self.UP:
                if v >= target:
                    break
            _logger.debug("wait...")
            gevent.sleep(0.01)
        yield

    def EXIT(self, result):
        self.__result = result


class FakeMusstChannel:
    COUNTER, ENCODER, SSI, ADC10, ADC5, SWITCH = list(range(6))

    def __init__(self, config):
        self._config = config
        self._obj = None
        self.__channel_id = config["channel"]

        self._string2mode = {
            "CNT": self.COUNTER,
            "ENCODER": self.ENCODER,
            "SSI": self.SSI,
            "ADC10": self.ADC10,
            "ADC5": self.ADC5,
            "SWITCH": self.SWITCH,
            "ENC": self.ENCODER,
        }

        mode = config.get("type", "CNT")
        self.__mode = self._string2mode[mode.upper()]

    def _get_object(self):
        if self._obj is None:
            self._obj = self._find_object()
        return self._obj

    def _find_object(self):
        obj = self._config.get("obj")
        if obj is not None:
            return obj

        label = self._config["label"]
        bliss_session = current_session
        blissobj = bliss_session.config.get(label)
        return blissobj

    @property
    def name(self):
        return self._config["label"]

    @property
    def value(self):
        obj = self._get_object()
        if isinstance(obj, Axis):
            return obj.user2dial(obj.position)
        return obj.value()

    @value.setter
    def value(self, value):
        pass

    @property
    def channel_id(self):
        return self.__channel_id

    @property
    def mode(self):
        return self.__mode


class FakeMusst:
    """Simulate a Musst program using a python code.

    This can be used to trigger devices in soft.

    For now it is only used to trigger the external trigger of a Lima simulator.
    """

    # From the Musst BLISS controller implementation
    (
        NOPROG_STATE,
        BADPROG_STATE,
        IDLE_STATE,
        RUN_STATE,
        BREAK_STATE,
        STOP_STATE,
        ERROR_STATE,
    ) = list(range(7))

    def __init__(self, name, config_tree):
        self._name = name
        self._config_tree = config_tree
        self._channels = {}
        for config in self._config_tree.get("channels", []):
            channel_id = config["channel"]
            self._channels[channel_id] = FakeMusstChannel(config)

        self.__tmrcfg_id = Musst.F_1KHZ
        self.__program: FakeMusstProgram | None = None
        self.__error = False

        self.__latency = 0.01
        """Latency for each call to the controller"""

        self.__program_classes = {}

        self.__run = None
        """Greenlet with the current running program"""

        # From the Musst BLISS controller implementation
        self.__frequency_conversion = {
            Musst.F_1KHZ: ("1KHZ", 1e3),
            Musst.F_10KHZ: ("10KHZ", 10e3),
            Musst.F_100KHZ: ("100KHZ", 100e3),
            Musst.F_1MHZ: ("1MHZ", 1e6),
            Musst.F_10MHZ: ("10MHZ", 10e6),
            Musst.F_50MHZ: ("50MHZ", 50e6),
            "1KHZ": Musst.F_1KHZ,
            "10KHZ": Musst.F_10KHZ,
            "100KHZ": Musst.F_100KHZ,
            "1MHZ": Musst.F_1MHZ,
            "10MHZ": Musst.F_10MHZ,
            "50MHZ": Musst.F_50MHZ,
        }

        self._register_programs()

    @property
    def __class__(self):
        return Musst

    def _register_programs(self):
        """Can be inherited to describe supported programs"""
        pass

    def register_program(self, filename, program_class):
        self.__program_classes[filename] = program_class
        for h in program_class._HASH_SOURCES:
            self.__program_classes[h] = program_class

    def trig_devices(self, trig_kind):
        """Trig devices defined in the configuration"""
        assert trig_kind in ("atrig", "btrig_up", "btrig_down")
        for obj in self._config_tree.get(trig_kind, []):
            if isinstance(obj, str):
                bliss_session = current_session
                device = bliss_session.config.get(obj)
                self._trig_device(device)
            elif callable(obj):
                obj()
            else:
                _logger.error("Unsupported kind object %s", type(obj))

    def _trig_device(self, device):
        _logger.debug("Trig device %s", device)
        if isinstance(device, Lima):
            tango_limaccds = device._proxy
            ccd_type = tango_limaccds.camera_type
            if ccd_type == "Simulator":
                device_name = tango_limaccds.name()
                # FIXME: I think this name with '/limaccds/' is not mandatory
                simulator_name = device_name.replace("/limaccds/", "/simulator/")
                tango_sim = DeviceProxy(simulator_name)
                try:
                    tango_sim.trigExternal()
                except Exception:
                    _logger.error(
                        "Error while sending external trigger to device %s",
                        device,
                        exc_info=True,
                    )
                return

        _logger.error("Unsupported detector '%s'", type(device))

    @property
    def name(self):
        return self._name

    def upload_file(self, fname, prg_root=None, template_replacement=None):
        gevent.sleep(self.__latency)

        program_class = self.__program_classes.get(fname)
        if program_class is None:
            _logger.error("No fake program registered for filename '%s'", fname)
        if program_class is not None:
            program = program_class(self)
        else:
            program = None
        self._set_program(program)

    @classmethod
    def program_hash(self, program: str):
        h = hashlib.sha1()
        h.update(program.encode("ascii"))
        return h.hexdigest()

    def upload_program(self, program_data, template_replacement={}):
        program = program_data
        for k, v in template_replacement.items():
            program = program.replace(k, v)
        h = self.program_hash(program)

        program_class = self.__program_classes.get(h)
        if program_class is None:
            _logger.error("Unsupported upload_program")
            _logger.error("Program:\n%s", program_data)
            _logger.error("Replacement:\n%s", template_replacement)
            _logger.error("Hash identifier: %s", h)
            raise RuntimeError(f"Unsupported program (hash: {h})")

        program = program_class(self)
        self._set_program(program)

    def _set_program(self, program):
        if self.__program is not None:
            # In case there is stuffs to clean up
            try:
                self._abort()
            except Exception:
                _logger.error("Error while aborting program", exc_info=True)
        self.__program = program

    def _run_program(self, method):
        self.__error = False
        try:
            method()
        except Exception:
            _logger.error("Error while running fake program %s", method, exc_info=True)
            self.__error = True
        finally:
            self.__running = False

    def run(self, entryPoint=None, wait=False):
        gevent.sleep(self.__latency)
        if self.__program is None:
            raise RuntimeError("No program loaded.")

        method_name = f"PROG_{entryPoint}"
        if not hasattr(self.__program, method_name):
            raise RuntimeError(
                f"Program '{entryPoint}' not found in program class {type(self.__program)}"
            )

        method = getattr(self.__program, method_name)
        self.__running = True
        self.__run = gevent.spawn(self._run_program, method)
        if wait:
            self._wait()

    def _wait(self):
        while self.STATE == self.RUN_STATE:
            gevent.sleep(0)

    def putget(self, msg, ack=False):
        content = msg.split(" ")
        if content[0] == "VAR" and len(content) == 3:
            assert self.__program is not None
            self.__program.set_variable(content[1], content[2])
            return
        _logger.error("Unused putget '%s'", msg)

    def get_channel_by_id(self, channel_id):
        channel = self._channels[channel_id]
        return channel

    def get_channel_by_name(self, channel_name):
        for channel in self._channels.values():
            if channel.name == channel_name:
                return channel
        raise RuntimeError(
            f"Musst '{self._name}' doesn't have channel '{channel_name}' in its config"
        )

    def get_channel_by_names(self, *channel_names):
        result = []
        for channel_name in channel_names:
            result.append(self.get_channel_by_name(channel_name))

    def get_variable(self, name):
        return getattr(self.__program, name)

    @property
    def TMRCFG(self):
        """Set/query main timer timebase"""
        gevent.sleep(self.__latency)
        return self.__frequency_conversion[self.__tmrcfg_id]

    @TMRCFG.setter
    def TMRCFG(self, value):
        gevent.sleep(self.__latency)
        if value not in self.__frequency_conversion:
            raise ValueError("Value not allowed")

        if isinstance(value, str):
            value = self.__frequency_conversion.get(value)
        self.__tmrcfg_id = value

    def get_timer_factor(self):
        _str_freq, freq = self.TMRCFG
        return freq

    def get_data(self, nb_counters, from_event_id=0, max_data_rate=0):
        assert self.__program is not None
        program = self.__program
        data = numpy.array(program.stored_data())
        data.shape = -1, nb_counters
        return data

    @property
    def CLEAR(self):
        gevent.sleep(self.__latency)
        self._set_program(None)

    def _abort(self):
        if self.__run is not None:
            self.__run.kill()
            self.__run = None
            self.__running = False

    @property
    def ABORT(self):
        gevent.sleep(self.__latency)
        self._abort()

    @property
    def STATE(self):
        gevent.sleep(self.__latency)
        if self.__program is None:
            result = self.NOPROG_STATE
        elif self.__running:
            result = self.RUN_STATE
        elif self.__error:
            result = self.ERROR_STATE
        else:
            result = self.IDLE_STATE
        return result

    @property
    def counters(self):
        return list(self._channels.values())
