import typing
from bliss.config.beacon_object import BeaconObject


class MockedWagoValue(typing.NamedTuple):
    value: typing.Any


class FakeRevolvedPeterWago(BeaconObject):
    """Fake the wago used to control a revolved peter optic."""

    def __init__(self, name, config):
        BeaconObject.__init__(self, config)
        self.__state = {}
        self.write_attribute("opobj", [0, 1, 1])
        self.write_attribute("opep", [1, 0, 0, 0])
        self.write_attribute("oprot", [0, 0])

    name = BeaconObject.config_getter("name")

    def read_attributes(self, names):
        result = [self.__state[n] for n in names]
        return result

    def write_attribute(self, name, value):
        self.__state[name] = MockedWagoValue(value)
        if name == "oprot":
            if value == [0, 1]:
                self.write_attribute("opobj", [1, 0, 1])
            if value == [1, 0]:
                self.write_attribute("opobj", [1, 1, 0])
