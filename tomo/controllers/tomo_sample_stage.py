from __future__ import annotations

from bliss.common import event
from bliss.common.axis import Axis
from bliss.config.beacon_object import BeaconObject
from bliss.config.static import Config
from tomo.helpers.beacon_object_helper import check_unexpected_keys
from tomo.helpers import info_utils
from tabulate import tabulate
from bliss.common.utils import BOLD
from tomo.controllers.pusher import Pusher
from tomo.controllers.air_bearing import AirBearing
from tomo.helpers.locked_axis_motion_hook import expect_locked_axis_motion_hook


class TomoSampleStage(BeaconObject):
    """Sample stage used for the tomo"""

    def __init__(self, name: str, config: Config):
        self.name: str = name
        BeaconObject.__init__(self, config)

        def as_axis(obj) -> Axis | None:
            if obj is None:
                return None
            if isinstance(obj, Axis):
                return obj
            raise TypeError(f"Axis expected. Found {type(obj)}")

        def as_pusher(obj) -> Pusher | None:
            if obj is None:
                return None
            if isinstance(obj, Pusher):
                return obj
            raise TypeError(f"Pusher expected. Found {type(obj)}")

        def as_air_bearing(obj) -> AirBearing | None:
            if obj is None:
                return None
            if isinstance(obj, AirBearing):
                return obj
            raise TypeError(f"AirBearing expected. Found {type(obj)}")

        expected_keys = [
            "translation_x",
            "translation_y",
            "translation_z",
            "rotation",
            "sample_u",
            "sample_v",
            "sample_x",
            "sample_y",
            "pusher",
            "air_bearing_x",
        ]
        check_unexpected_keys(self, config, expected_keys)

        self._x_axis: Axis = as_axis(config.get("translation_x", None))
        self._y_axis: Axis = as_axis(config.get("translation_y", None))
        self._z_axis: Axis = as_axis(config.get("translation_z", None))
        self._rotation_axis: Axis = as_axis(config.get("rotation", None))
        expect_locked_axis_motion_hook(self._rotation_axis)

        self._sample_u_axis: Axis = as_axis(config.get("sample_u", None))
        self._sample_v_axis: Axis = as_axis(config.get("sample_v", None))

        self._sample_x_axis: Axis = as_axis(config.get("sample_x", None))
        self._sample_y_axis: Axis = as_axis(config.get("sample_y", None))

        self._pusher: Pusher | None = as_pusher(config.get("pusher", None))

        self._air_bearing_x: AirBearing | None = as_air_bearing(
            config.get("air_bearing_x", None)
        )

        if self._x_axis is not None:
            event.connect(self._x_axis, "position", self._xaxis_position_changed)

    @property
    def x_axis(self) -> Axis | None:
        """Motor under the rotation in the direction of the beam"""
        return self._x_axis

    @property
    def y_axis(self) -> Axis:
        """Motor under the rotation orthogonal to the beam and the vertical plan"""
        return self._y_axis

    @property
    def z_axis(self) -> Axis:
        """Motor under the rotation moving the sample vertically"""
        return self._z_axis

    @property
    def rotation_axis(self) -> Axis:
        """Motor controlling the rotation axis"""
        return self._rotation_axis

    @property
    def rotation(self) -> Axis:
        """Motor controlling the rotation axis

        Deprecated. Prefer to use `.rotation_axis`
        """
        return self._rotation_axis

    @property
    def sample_u_axis(self) -> Axis:
        """Real motor moving the sample on top of the rotation axis"""
        return self._sample_u_axis

    @property
    def sample_v_axis(self) -> Axis:
        """Real motor moving the sample on top of the rotation axis"""
        return self._sample_v_axis

    @property
    def sample_x_axis(self) -> Axis | None:
        """Motor moving the sample on top of the rotation axis and always aligned to the beam"""
        return self._sample_x_axis

    @property
    def sample_y_axis(self) -> Axis | None:
        """Motor moving the sample on top of the rotation axis and always orthogonal to the bean"""
        return self._sample_y_axis

    @property
    def pusher(self) -> Pusher | None:
        """Pusher defined to move the sample on top of the rotation"""
        return self._pusher

    @property
    def air_bearing_x(self) -> AirBearing | None:
        """Air cushion used when the sample have to move along the x-translation"""
        return self._air_bearing_x

    _x_axis_focal_pos = BeaconObject.property_setting(
        name="x_axis_focal_pos",
        default=None,
        doc="Position of the focal point into the x axis",
    )

    _y_axis_focal_pos = BeaconObject.property_setting(
        name="y_axis_focal_pos",
        default=None,
        doc="Position of the focal point into the y axis",
    )

    _z_axis_focal_pos = BeaconObject.property_setting(
        name="z_axis_focal_pos",
        default=None,
        doc="Position of the focal point into the z axis",
    )

    @property
    def focal_position(self) -> tuple[float | None, float | None, float | None]:
        """Return the focal position in x/y/z axes"""
        return self._x_axis_focal_pos, self._y_axis_focal_pos, self._z_axis_focal_pos

    @focal_position.setter
    def focal_position(self, value: tuple[float | None, float | None, float | None]):
        self._x_axis_focal_pos = value[0]
        self._y_axis_focal_pos = value[1]
        self._z_axis_focal_pos = value[2]

    _detector_center = BeaconObject.property_setting(
        name="detector_center",
        default=(None, None),
        doc="Position of the detector center in the sample stage",
    )

    @property
    def detector_center(self) -> tuple[float | None, float | None]:
        return self._detector_center

    @detector_center.setter
    def detector_center(self, value: tuple[float | None, float | None]):
        self._detector_center = value

    @property
    def source_distance(self) -> float | None:
        """
        Return source sample distance in mm
        """
        focal_pos = self._x_axis_focal_pos
        if focal_pos is None:
            return None
        pos = self.x_axis.position if self.x_axis is not None else 0
        return pos - focal_pos

    @source_distance.setter
    def source_distance(self, distance: float | None):
        """
        Set source sample distance in mm
        """
        if distance is None:
            self._x_axis_focal_pos = None
        else:
            pos = self.x_axis.position if self.x_axis is not None else 0
            self._x_axis_focal_pos = pos - distance
        self._send_source_distance(distance)

    def _xaxis_position_changed(self, position, signal=None, sender=None):
        """Callback called when the x_axis position change"""
        focal_pos = self._x_axis_focal_pos
        if focal_pos is not None:
            distance = position - focal_pos
        else:
            distance = None
        self._send_source_distance(distance)

    def _send_source_distance(self, distance):
        """Called to emit a new distance position"""
        event.send(self, "source_distance", distance)

    def __info__(self):
        """
        Show information about tomo configuration
        Expose configured scan and chain presets on runners
        """
        detector_center = self.detector_center

        info_str = f"{self.name} configuration info:\n"
        table_list = []
        table_list.append(["Role", "Ref", "Value"])
        table_list.append(
            [
                BOLD("Rotation"),
                info_utils.format_device(self.rotation_axis),
                info_utils.format_axis_position(self.rotation_axis),
            ]
        )
        table_list.append(
            ["   Direction", "", info_utils.format_direction(self.rotation_axis)]
        )

        table_list.append([BOLD("Stage translation")])
        table_list.append(
            [
                "   X",
                info_utils.format_device(self.x_axis),
                info_utils.format_axis_position(self.x_axis),
            ]
        )
        table_list.append(
            [
                "   Y",
                info_utils.format_device(self.y_axis),
                info_utils.format_axis_position(self.y_axis),
            ]
        )
        table_list.append(
            [
                "   Z",
                info_utils.format_device(self.z_axis),
                info_utils.format_axis_position(self.z_axis),
            ]
        )
        if self.air_bearing_x is not None:
            table_list.append(
                [
                    "   Air bearing x",
                    info_utils.format_device(self.air_bearing_x),
                    self.air_bearing_x.state.name,
                ]
            )

        table_list.append([BOLD("Sample alignment")])
        if self.pusher is not None:
            table_list.append(
                [
                    "   Pusher",
                    info_utils.format_device(self.pusher),
                    self.pusher.state.name,
                ]
            )

        if self.sample_u_axis is not None:
            table_list.append(
                [
                    "   U",
                    info_utils.format_device(self.sample_u_axis),
                    info_utils.format_axis_position(self.sample_u_axis),
                ]
            )

        if self.sample_v_axis is not None:
            table_list.append(
                [
                    "   V",
                    info_utils.format_device(self.sample_v_axis),
                    info_utils.format_axis_position(self.sample_v_axis),
                ]
            )

        if self.sample_x_axis is not None:
            table_list.append(
                [
                    "   X projection",
                    info_utils.format_device(self.sample_x_axis),
                    info_utils.format_axis_position(self.sample_x_axis),
                ]
            )

        if self.sample_y_axis is not None:
            table_list.append(
                [
                    "   Y projection",
                    info_utils.format_device(self.sample_y_axis),
                    info_utils.format_axis_position(self.sample_y_axis),
                ]
            )

        table_list.append([BOLD("Detector center/Rotation axis")])
        table_list.append(
            [
                "   Y projection",
                "",
                info_utils.format_quantity(detector_center[0], "mm"),
            ]
        )
        table_list.append(
            [
                "   Z projection",
                "",
                info_utils.format_quantity(detector_center[1], "mm"),
            ]
        )
        table_list.append([BOLD("Focal position")])
        table_list.append(
            [
                "   X",
                "",
                info_utils.format_quantity(self._x_axis_focal_pos, "mm"),
            ]
        )
        table_list.append(
            [
                "   Y",
                "",
                info_utils.format_quantity(self._y_axis_focal_pos, "mm"),
            ]
        )
        table_list.append(
            [
                "   Z",
                "",
                info_utils.format_quantity(self._z_axis_focal_pos, "mm"),
            ]
        )
        table_list.append([BOLD("Focal distance")])
        table_list.append(
            [
                "   X",
                "",
                info_utils.format_quantity(self.source_distance, "mm"),
            ]
        )

        table = tabulate(table_list, headers="firstrow", tablefmt="simple")
        info_str += table
        return info_str
