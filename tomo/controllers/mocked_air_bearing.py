from __future__ import annotations
import gevent
from .air_bearing import AirBearing


class MockedAirBearing(AirBearing):
    """
    Mocked air cushion device.

    .. code-block::

        - name: sxair
          plugin: bliss
          class: MockedAirBearing
          package: tomo.controllers.mocked_air_bearing
    """

    def _turn_on(self):
        gevent.sleep(3.0)

    def _turn_off(self):
        gevent.sleep(1.0)
