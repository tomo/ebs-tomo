import numpy
import logging
from . import fake_musst

_logger = logging.getLogger(__name__)


class FscanProgram(fake_musst.FakeMusstProgram):
    _HASH_SOURCES = ["170b91ed219131fc65bb3fe876f0e50d805ab7ce"]

    def _init(self):
        self.register_variable("TRIGSTART", numpy.int32)
        self.register_variable("TRIGDELTA", numpy.uint32)
        self.register_variable("GATEWIDTH", numpy.uint32)
        self.register_variable("NPULSES", numpy.uint32)
        self.register_variable("SCANDIR", numpy.int32)
        self.register_variable("SCANMODE", numpy.uint32)
        self.register_variable("GATEMODE", numpy.uint32)
        self.register_variable("POSERR", numpy.uint32)
        self.register_variable("ACCNB", numpy.uint32)
        self.register_variable("ACCTIME", numpy.uint32)
        self.register_variable("NPOINTS", numpy.uint32)
        self.register_variable("POS_TG", numpy.int32)
        self.register_variable("POSDELTA", numpy.int32)
        self.register_variable("POSGATE", numpy.int32)
        self.register_variable("TRIGTIME", numpy.int32)
        self.register_variable("CUMERR", numpy.uint32)
        self.register_variable("POSCORR", numpy.int32)
        self.register_variable("IACC", numpy.uint32)
        self.MOT1 = self.CH1  # alias
        self.DATA1 = self.CH2  # alias

    def SUB_STORE_INIT(self):
        self.STORELIST(self._CHTIMER, self.MOT1, self.DATA1)
        self.EMEM_0_AT_0()

    def SUB_POS_EVENT_INIT(self):
        if self.SCANDIR > 0:
            self.EVSOURCE(self.MOT1, self.UP)
            self.POSDELTA = self.TRIGDELTA
            self.POSGATE = self.GATEWIDTH
            self.POSCORR = 1
        else:
            self.EVSOURCE(self.MOT1, self.DOWN)
            self.POSDELTA = -self.TRIGDELTA
            self.POSGATE = -self.GATEWIDTH
            self.POSCORR = -1
        self.DEFEVENT(self.MOT1)

    def SUB_POS_INC_TARGET(self):
        self.POS_TG = self.POS_TG + self.POSDELTA
        self.CUMERR += self.POSERR
        if (self.CUMERR & 0x80000000) != 0:
            self.CUMERR &= 0x7FFFFFFF
            self.POS_TG += self.POSCORR
        self.setup_target(self.MOT1, self.POS_TG)  # self.MOT1 = ...

    def SUB_ACQ_IN_POS(self):
        with self.wait_for_defevent():  # AT DEFEVENT = ...
            self.STORE()
            self.ATRIG()
            self.BTRIG()
        self.TRIGTIME = self.read_channel(self._CHTIMER)
        if self.ACCNB > 1:
            for self.IACC in range(2, self.ACCNB):
                self.setup_target(
                    self._CHTIMER, self.read_channel(self._CHTIMER) + self.ACCTIME
                )  # self.TIMER = ...
                with self.wait_for_target(self._CHTIMER):  # AT TIMER = ...
                    self.ATRIG()
        if self.GATEMODE == 0:
            self.setup_target(
                self._CHTIMER, self.TRIGTIME + self.GATEWIDTH
            )  # self.TIMER = ...
            with self.wait_for_target(self._CHTIMER):  # AT TIMER = ...
                self.STORE()
                self.BTRIG()
            self.DEFEVENT(self.MOT1)
        else:
            self.setup_target(
                self.MOT1, self.read_channel(self.MOT1) + self.POSGATE
            )  # self.MOT1 = ...
            with self.wait_for_target(self.MOT1):  # AT MOT1 = ...
                self.STORE()
                self.BTRIG()

    def SUB_ACQ_IN_TIME(self):
        with self.wait_for_defevent():  # AT DEFEVENT = ...
            self.STORE()
            self.ATRIG()
            self.BTRIG()
        self.TRIGTIME = self.read_channel(self._CHTIMER)
        if self.ACCNB > 1:
            for self.IACC in range(2, self.ACCNB):
                self.setup_target(
                    self._CHTIMER, self.read_channel(self._CHTIMER) + self.ACCTIME
                )  # self.TIMER = ...
                with self.wait_for_target(self._CHTIMER):  # AT TIMER = ...
                    self.ATRIG()
        self.setup_target(
            self._CHTIMER, self.TRIGTIME + self.GATEWIDTH
        )  # self.TIMER = ...
        with self.wait_for_target(self._CHTIMER):  # AT TIMER = ...
            self.STORE()
            self.BTRIG()
        self.setup_target(
            self._CHTIMER, self.TRIGTIME + self.TRIGDELTA
        )  # self.TIMER = ...
        self.DEFEVENT(self._CHTIMER)

    def PROG_FSCAN(self):
        self.BTRIG(0)
        self.CTSTOP_TIMER()
        self.TIMER = 0
        self.CTSTART_TIMER()
        self.SUB_STORE_INIT()
        self.NPOINTS = 0
        self.CUMERR = 0
        self.POS_TG = self.TRIGSTART
        self.SUB_POS_EVENT_INIT()
        self.setup_target(self.MOT1, self.POS_TG)  # self.MOT1 = ...
        while self.NPOINTS < self.NPULSES:
            if self.SCANMODE == 1:
                self.SUB_ACQ_IN_POS()
                self.SUB_POS_INC_TARGET()
            else:
                self.SUB_ACQ_IN_TIME()
            self.NPOINTS += 1
        with self.wait_for_defevent():  # AT DEFEVENT = ...
            self.STORE()
        return self.EXIT(self.NPOINTS)

    def PROG_FSCAN_CLEAN(self):
        self.CTSTOP_TIMER()
        self.TIMER = 0
        self.BTRIG(0)


class FakeTomoMusst(fake_musst.FakeMusst):
    def _register_programs(self):
        """Invoked at initialization"""
        self.register_program("fscan.mprg", FscanProgram)
