from __future__ import annotations
import time
import numbers
import typing
from tabulate import tabulate

from bliss import global_map
from bliss.common.logtools import log_debug, log_warning
from bliss.common.utils import grouped
from bliss.common.axis import Axis
from bliss.controllers.motor import CalcController
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss.config.static import Config, ConfigList
from tomo.helpers import info_utils
from bliss.config import static
from bliss.physics.trajectory import LinearTrajectory

from bliss.shell.cli.user_dialog import Container
from bliss.shell.standard import umv
from bliss.config.beacon_object import (
    BeaconObject,
)


class MotionStep(typing.NamedTuple):
    """Define a position for an axis"""

    axis_name: str
    absolute_position: float | None
    relative_position: float | None


def _parse_list_axis_number(
    list_axis_number,
) -> dict[Axis, float]:
    """
    Read a lost of motor displacement and return a structured dict.

    Arguments:
        list_axis_number: A list of interleaved `motor` and `position`.)
    """
    if len(list_axis_number) % 2 != 0:
        raise ValueError(
            "Expected a sequence of motor;position, found an odd number of arguments"
        )

    result: dict[Axis, float] = {}
    for mot, pos in grouped(list_axis_number, 2):
        if not isinstance(mot, Axis):
            raise ValueError(f"Expected an Axis, found {type(mot)}")
        if not isinstance(pos, numbers.Real):
            raise ValueError(f"Expected a number as position, found {type(pos)}")
        result[mot] = float(pos)
    return result


class FlatMotion(BeaconObject):
    """
    Store the setup motion to take flat images.

    The motion consist in moving the sample out of the detector (`move_out`)
    in order to take an acquisition of the raw beam. At the termination
    of such acquisition the sample is moved back to his place (`move_in`).

    **Attributes**

    settle_time : float
        time in seconds used for motor stabilization at the end of in
        and out movement
    power_onoff : Boolean
        flag to activate power on and off on reference motors before in
        and after out movements
    always_update_pos : Boolean
        flag to activate the update of reference motors in beam position
        before each out movement
    move_in_parallel: motor group
        flag to activate simultaneous motion in axis
    """

    def __init__(self, name: str, config: Config):
        log_debug(self, "__init__() entering")

        BeaconObject.__init__(self, config=config, name=name, share_hardware=False)
        global_map.register(self, tag=name)

        available_motors = config["motor"]
        if not isinstance(available_motors, ConfigList):
            available_motors = [available_motors]
        # Ensure to dereference all the motors
        self._available_motors = [m for m in available_motors]

        self.settle_time = config["settle_time"]
        self.power_onoff = config.get("power_onoff", False)
        self.always_update_pos = config.get("always_update_pos", False)
        self.move_in_parallel = config.get("move_in_parallel", True)

        # Define the necessary set of persistent parameters.
        # Initialize the parameter set name and the necessary default values

        self._validate_settings()

        log_debug(self, "__init__() leaving")

    def _validate_settings(self):
        """Reset parameters in case of change in number of reference motors"""
        supported_axis_names = {a.name for a in self._available_motors}
        out_beam_motion: list[MotionStep] = self._out_beam_motion
        out_axis_names = {s.axis_name for s in out_beam_motion}
        unexpected_axes_names = out_axis_names.difference(supported_axis_names)
        if len(unexpected_axes_names) != 0:
            log_warning(self, "The flat motion was invalidated")
            self._out_beam_motion = []

    _in_beam_motion = BeaconObject.property_setting(
        name="in_beam_motion", default=[], doc="In beam motion"
    )
    _out_beam_motion = BeaconObject.property_setting(
        name="out_beam_motion", default=[], doc="Out beam motion"
    )

    def setup(self, submenu=False):
        """
        Set-up reference motor displacement or absolute position
        """
        from bliss.shell.cli.user_dialog import UserCheckBox, UserMsg, UserInput

        ui = []
        body = []

        out_beam_motion = self._out_beam_motion
        out_beam_ref = {s.axis_name: s for s in out_beam_motion}

        for i, mot in enumerate(self._available_motors):
            out = out_beam_ref.get(mot.name)
            absolute_move = False if out is None else out.absolute_position is not None

            dlg_use_for_flat = UserCheckBox(
                label="Use this axis", defval=out is not None
            )
            dlg_absolute_move = UserCheckBox(
                label="Absolute move", defval=absolute_move
            )
            if absolute_move:
                pos = 0.0 if out is None else out.absolute_position
                dlg_current_out_value = UserMsg(
                    label=f"Out of beam position:                   {pos}"
                )
            else:
                pos = 0.0 if out is None else out.relative_position
                dlg_current_out_value = UserMsg(
                    label=f"Out of beam displacement: {pos}"
                )

            dlg_new_out_value = UserInput(
                label="New out of beam position/displacement: ",
                defval=pos,
            )

            motor_panel = Container(
                [
                    dlg_use_for_flat,
                    dlg_absolute_move,
                    dlg_current_out_value,
                    dlg_new_out_value,
                ],
                title=f"{mot.name}",
            )
            body.append([motor_panel])
            ui.append((dlg_use_for_flat, dlg_absolute_move, dlg_new_out_value))

        cancel_text = "Back" if submenu else "Cancel"
        ret = BlissDialog(
            body, title="Reference Displacement Setup", cancel_text=cancel_text
        ).show()

        if ret is not False:
            new_out_beam_motion: list[MotionStep] = []
            for i, (
                dlg_use_for_flat,
                dlg_absolute_move,
                dlg_new_out_value,
            ) in enumerate(ui):
                axis = self._available_motors[i]
                use_for_flat = ret[dlg_use_for_flat]
                if not use_for_flat:
                    continue
                absolute_move = ret[dlg_absolute_move]
                new_pos = float(ret[dlg_new_out_value])

                if absolute_move:
                    self.disp_inside_limits(axis, new_pos)
                else:
                    pos = axis.position + new_pos
                    self.disp_inside_limits(axis, pos)

                step = MotionStep(
                    axis_name=axis.name,
                    absolute_position=None if not absolute_move else new_pos,
                    relative_position=None if absolute_move else new_pos,
                )
                new_out_beam_motion.append(step)
            self._out_beam_motion = new_out_beam_motion

            # Summary
            for step in new_out_beam_motion:
                if step.absolute_position is not None:
                    print(
                        f"Out of beam position for reference motor {step.axis_name}: {step.absolute_position}"
                    )
                else:
                    print(
                        f"Out of beam displacement for reference motor {step.axis_name}: {step.relative_position}"
                    )

    def disp_inside_limits(self, motor, out_of_beam_position):
        """
        Check displacement value is inside axis limits.

        Raises:
            ValueError: In case the motion is not achievable.
        """
        motor._get_motion(out_of_beam_position)

    def __info__(self):
        config = static.get_config()

        info_str = f"Flat displacement info: {self.name}"
        motor_name = ", ".join([motor.name for motor in self._available_motors])

        table_list = [["Role", "", ""]]
        table_list.append(["motor", motor_name])
        table_list.append(
            ["settle_time", info_utils.format_quantity(self.settle_time, "s")]
        )
        table_list.append(["power_onoff", self.power_onoff])
        table_list.append(["always_update_pos", self.always_update_pos])
        table_list.append(["move_in_parallel", self.move_in_parallel])

        table = tabulate(table_list, headers="firstrow", tablefmt="simple")
        info_str += "\n\n" + table

        table_list = [["Axis name", "Relative motion", "Absolute motion"]]
        out_beam_motion: list[MotionStep] = self._out_beam_motion
        for step in out_beam_motion:
            axis = config.get(step.axis_name)
            table_list.append(
                [
                    info_utils.format_device(axis),
                    info_utils.format_axis_displacement(axis, step.relative_position),
                    info_utils.format_axis_absolute_position(
                        axis, step.absolute_position
                    ),
                ]
            )

        table = tabulate(table_list, headers="firstrow", tablefmt="simple")
        info_str += "\n\nOut beam motion\n\n"
        info_str += table

        return info_str

    def move_in(self):
        """
        Move sample to in-beam position.

        After the motion, power off motors if `power_onoff` is set
        to `True`, and sleep `settle_time` seconds for stabilization.
        """
        log_debug(self, "move_in() entering")

        config = static.get_config()
        in_beam_motion: list[MotionStep] = self._in_beam_motion

        def prepare_absolute_motion() -> dict[Axis, float]:
            motion: dict[Axis, float] = {}
            for step in in_beam_motion:
                axis = config.get(step.axis_name)
                assert step.relative_position is None
                assert step.absolute_position is not None
                motion[axis] = step.absolute_position
            return motion

        motion = prepare_absolute_motion()
        if len(motion) == 0:
            raise RuntimeError("No motion defined to move back in the beam")

        if self.move_in_parallel:
            axes_and_pos = [v for p in zip(motion.keys(), motion.values()) for v in p]
            umv(*axes_and_pos)
        else:
            for axis, pos in motion.items():
                umv(axis, pos)

        # power off the motors
        if self.power_onoff is True:
            for step in in_beam_motion:
                axis = config.get(step.axis_name)
                axis.off()

        # wait for motor stabilization
        time.sleep(self.settle_time)

        log_debug(self, "move_in() leaving")

    def move_out(self):
        """
        Move sample to out-beam position.

        Before the motion, save the motors position (`update_in_beam_position`)
        if `always_update_pos` is set to `True`. And power on motors if
        `power_onoff` is set to `True`.

        After the motion, sleep `settle_time` seconds for stabilization.
        """
        log_debug(self, "move_out() entering")

        config = static.get_config()
        if self.always_update_pos is True:
            self.update_in_beam_position()
        else:
            in_beam_motion = self._in_beam_motion
            if len(in_beam_motion) == 0:
                raise ValueError(
                    "In beam possition undefined. When 'always_update_pos' is false, the in beam position have to be set manually."
                )

        out_beam_motion = self._out_beam_motion

        # power on the motors
        if self.power_onoff is True:
            for step in out_beam_motion:
                axis = config.get(step.axis_name)
                axis.on()

        def prepare_absolute_motion() -> dict[Axis, numbers.Real]:
            in_beam_motion = self._in_beam_motion
            in_beam_map: dict[str, numbers.Real] = {
                m.axis_name: m.absolute_position for m in in_beam_motion
            }
            motion: dict[Axis, numbers.Real] = {}
            for step in out_beam_motion:
                axis = config.get(step.axis_name)
                assert (step.absolute_position is None) != (
                    step.relative_position is None
                )
                if step.absolute_position is not None:
                    pos = step.absolute_position
                elif step.relative_position is not None:
                    pos = in_beam_map[step.axis_name] + step.relative_position
                motion[axis] = pos
            return motion

        motion = prepare_absolute_motion()
        if len(motion) == 0:
            raise RuntimeError("No motion defined to move out of the beam")

        if self.move_in_parallel:
            axes_and_pos = [v for p in zip(motion.keys(), motion.values()) for v in p]
            umv(*axes_and_pos)
        else:
            for axis, pos in motion.items():
                umv(axis, pos)

        # wait for motor stabilization
        time.sleep(self.settle_time)

        log_debug(self, "move_out() leaving")

    def IN(self):
        """Alias to `move_in`"""
        self.move_in()

    def OUT(self):
        """Alias to `move_out`"""
        self.move_out()

    def update_in_beam_position(self):
        """
        Update in-beam position with current motor position.
        """
        log_debug(self, "update_in_beam_position() entering")

        out_beam_motion = self._out_beam_motion
        if len(out_beam_motion) == 0:
            raise ValueError("Out position have to be set first")

        config = static.get_config()
        in_beam_motion: list[MotionStep] = []
        for step in reversed(out_beam_motion):
            axis = config.get(step.axis_name)
            in_beam_motion.append(
                MotionStep(
                    axis_name=axis.name,
                    absolute_position=axis.position,
                    relative_position=None,
                )
            )

        self._in_beam_motion = in_beam_motion

        log_debug(self, "update_in_beam_position() leaving")

    def set_in_beam_position(self, *list_motor_positions):
        """
        Define in-beam position.

        Arguments:
            list_motor_positions: A list of interleaved `motor` and `position`.
        """
        log_debug(self, "set_in_beam_position() entering")

        motion = _parse_list_axis_number(list_motor_positions)

        out_beam_motion = self._out_beam_motion
        if len(out_beam_motion) == 0:
            raise ValueError("Out position have to be set first")

        axis_names = {s.axis_name for s in out_beam_motion}
        arg_axis_names = {k.name for k in motion.keys()}
        if axis_names != arg_axis_names:
            raise ValueError(
                f"Axis positions are mandatory for {', '.join(axis_names)}"
            )

        in_beam_motion: list[MotionStep] = []
        for mot, pos in motion.items():
            in_beam_motion.append(
                MotionStep(
                    axis_name=mot.name,
                    absolute_position=pos,
                    relative_position=None,
                )
            )
        self._in_beam_motion = in_beam_motion

        log_debug(self, "set_in_beam_position() leaving")

    def set_out_of_beam_position(self, *list_motor_positions):
        """
        Define out-of-beam absolute position.

        Arguments:
            list_motor_positions: A list of interleaved `motor` and `position`.
        """
        log_debug(self, "set_out_of_beam_position() entering")

        motion = _parse_list_axis_number(list_motor_positions)
        supported_axis_names = {a.name for a in self._available_motors}
        arg_axis_names = {k.name for k in motion.keys()}
        unexpected_axes_names = arg_axis_names.difference(supported_axis_names)
        if len(unexpected_axes_names) != 0:
            raise ValueError(
                f"Unexpected axes {', '.join(unexpected_axes_names)}. Only the following axes are supported: {', '.join(supported_axis_names)}"
            )

        out_beam_motion: list[MotionStep] = []
        for axis, position in motion.items():
            out_beam_motion.append(
                MotionStep(
                    axis_name=axis.name,
                    absolute_position=position,
                    relative_position=None,
                )
            )

        self._out_beam_motion = out_beam_motion

        log_debug(self, "set_out_of_beam_position() leaving")

    def set_out_of_beam_displacement(self, *list_motor_displacements):
        """
        Define out-of-beam displacement.

        Arguments:
            list_motor_positions: A list of interleaved `motor` and `position`.
        """
        log_debug(self, "set_out_of_beam_displacement() entering")

        motion = _parse_list_axis_number(list_motor_displacements)
        supported_axis_names = {a.name for a in self._available_motors}
        arg_axis_names = {k.name for k in motion.keys()}
        unexpected_axes_names = arg_axis_names.difference(supported_axis_names)
        if len(unexpected_axes_names) != 0:
            raise ValueError(
                f"Unexpected axes {', '.join(unexpected_axes_names)}. Only the following axes are supported: {', '.join(supported_axis_names)}"
            )

        out_beam_motion: list[MotionStep] = []
        for axis, position in motion.items():
            out_beam_motion.append(
                MotionStep(
                    axis_name=axis.name,
                    absolute_position=None,
                    relative_position=position,
                )
            )

        self._out_beam_motion = out_beam_motion

        log_debug(self, "set_out_of_beam_displacement() leaving")

    def get_relative_motion(self, axis: Axis) -> float:
        """
        Helper to return the relative motion from an Axis.

        Raises:
            ValueError: If the axis is not used in the motion
            ValueError: If the axis is not the only one used in the motion
            ValueError: If the axis is not a relative motion
        """
        out_beam_motion = self._out_beam_motion
        if len(out_beam_motion) != 1:
            raise ValueError(
                f"Axis {axis.name} is not or not only involved in the flat motion"
            )

        step = out_beam_motion[0]
        if step.axis_name != axis.name:
            raise ValueError(
                f"Axis {axis.name} is not the one involved in the flat motion. Found {step.axis_name}"
            )

        if step.absolute_position is not None:
            raise ValueError(f"Axis {axis.name} is involved but in an absolute motion")

        if step.relative_position is None:
            assert False, "Unexpected state"
        return step.relative_position

    @property
    def involved_axes(self) -> list[Axis]:
        """Return the axis involved in this flat motion."""
        config = static.get_config()
        out_beam_motion = self._out_beam_motion
        return [config.get(s.axis_name) for s in out_beam_motion]

    def get_nx_motion(self) -> dict:
        """Returns a NX like metadata dict related to this flat motion"""
        meta = {}
        config = static.get_config()
        out_beam_motion = self._out_beam_motion
        for step in out_beam_motion:
            axis = config.get(step.axis_name)
            motion: dict
            if step.relative_position is not None:
                motion = {
                    "relative_position": step.relative_position,
                    "relative_position@units": axis.unit,
                }
            elif step.absolute_position is not None:
                motion = {
                    "absolute_position": step.absolute_position,
                    "absolute_position@units": axis.unit,
                }
            else:
                assert False, "Unexpected state"
            meta[step.axis_name] = motion
        return meta

    def estimation_time(self) -> float:
        """Estimation time in seconds of the back and forth flat motion.

        It includes motion of the involved aces, and the settle time.
        """
        config = static.get_config()
        in_beam_motion = self._in_beam_motion
        in_beam_map: dict[str, float] = {
            m.axis_name: m.absolute_position for m in in_beam_motion
        }
        out_beam_motion = self._out_beam_motion
        if len(out_beam_motion) == 0:
            raise ValueError(
                "Flat motion is not porperly configured. No motion defined."
            )

        motion_times = []
        for step in out_beam_motion:
            axis = config.get(step.axis_name)

            if isinstance(axis.controller, CalcController):
                # There is no guess for now
                mot_time = 0
            else:
                if self.always_update_pos:
                    init_pos = axis.position
                else:
                    init_pos = in_beam_map[step.axis_name]
                if step.absolute_position is not None:
                    target_pos = step.absolute_position
                else:
                    target_pos = init_pos + step.relative_position

                # back and forth
                disp = target_pos - init_pos
                lt = LinearTrajectory(
                    pi=0,
                    pf=disp,
                    velocity=axis.velocity,
                    acceleration=axis.acceleration,
                    ti=0,
                )
                mot_time = 2 * lt.instant(disp)

            motion_times.append(mot_time)

        result = self.settle_time * 2
        if self.move_in_parallel:
            result += max(motion_times)
        else:
            result += sum(motion_times)
        return result

    def update_axis(
        self,
        axis_name: str,
        absolute_position: numbers.Real | None = None,
        relative_position: numbers.Real | None = None,
    ):
        """Update the displacement of one of the axis already part of the motion.

        If `absolute_position` and `relative_position` are both None, the axis is dropped.

        Arguments:
            axis_name: Name of the axis
            absolute_position: If not None, the absolute position to use
            relative_position: If not None, the relative position to use

        Raises:
            ValueError: If both `absolute_position` are defined `relative_position`.
        """
        if absolute_position is not None and relative_position is not None:
            raise ValueError(
                "Both absolute_position and relative_position defined. Only one can be set at a time"
            )

        def create_new_step():
            if absolute_position is not None:
                return MotionStep(
                    axis_name=axis_name,
                    absolute_position=float(absolute_position),
                    relative_position=None,
                )
            if relative_position is not None:
                return MotionStep(
                    axis_name=axis_name,
                    absolute_position=None,
                    relative_position=float(relative_position),
                )
            return None

        out_beam_motion = self._out_beam_motion
        result = []
        found = False

        for step in out_beam_motion:
            if step.axis_name == axis_name:
                found = True
                newstep = create_new_step()
                if newstep is not None:
                    result.append(newstep)
            else:
                result.append(step)

        if not found:
            newstep = create_new_step()
            if newstep is None:
                raise ValueError(f"{axis_name} was already not part of the motion")
            result.append(newstep)

        self._out_beam_motion = result
