import gevent
from tomo.controllers.tomo_detectors import TomoDetectors


class FakeTomoDetectors(TomoDetectors):
    def _mount(self, unmount_detector, mount_detector):
        # Wait for the fake mounting
        gevent.sleep(4)
