# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from __future__ import annotations

from .tomo_sample_stage import TomoSampleStage
import numpy
import pint
import tabulate
from bliss.common.axis import Axis
from bliss.common import event

from bliss.controllers.motor import CalcController
from bliss.config.beacon_object import BeaconObject
from bliss.config.static import ConfigNode


class BeamTracker(BeaconObject):
    """
    Track the beam with motors from the sample stage.

    Few parameters allow to tune the geometry.

    - `sy_angle` in radian, to change the ratio of `sy/sx`.
    - `sz_angle` in radian, to change the ratio of `sz/sx`.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._sample_stage: TomoSampleStage | None = None

    _sy_angle = BeaconObject.property_setting(
        name="sy_angle",
        default=None,
        doc="Position of the focal point into the x axis",
    )

    _sz_angle = BeaconObject.property_setting(
        name="sz_angle",
        default=None,
        doc="Position of the focal point into the x axis",
    )

    def _set_sample_stage(self, sample_stage: TomoSampleStage):
        """Called by tomo config after the initialization"""
        assert self._sample_stage is None
        self._sample_stage = sample_stage
        x_axis = self._sample_stage.x_axis
        if x_axis is not None:
            event.connect(x_axis, "position", self._on_x_axis_position_changed)
            event.connect(
                sample_stage, "x_axis_focal_pos", self._on_focal_position_changed
            )
            event.connect(
                sample_stage, "y_axis_focal_pos", self._on_focal_position_changed
            )
            event.connect(
                sample_stage, "z_axis_focal_pos", self._on_focal_position_changed
            )
            self._update_detector_center()

    @property
    def sy_angle(self) -> float:
        return self._sy_angle

    @sy_angle.setter
    def sy_angle(self, value):
        self._sy_angle = value
        self._update_detector_center()

    @property
    def sz_angle(self) -> float:
        return self._sz_angle

    @sz_angle.setter
    def sz_angle(self, value):
        self._sz_angle = value
        self._update_detector_center()

    def compute_correction(self, prev_x: float, x: float) -> tuple[float, float]:
        """Return the relative correction to apply to `y` and `z` from delta `x`"""
        sy_angle = self.sy_angle
        sz_angle = self.sz_angle

        dx = x - prev_x
        dy: float
        dz: float
        if sy_angle is not None:
            dy = numpy.tan(sy_angle) * dx
        else:
            dy = 0.0
        if sz_angle is not None:
            dz = numpy.tan(sz_angle) * dx
        else:
            dz = 0.0

        return dy, dz

    def _compute_detector_center(
        self, translation_x: float | None
    ) -> tuple[float | None, float | None]:
        ss = self._sample_stage
        if ss is None:
            return None, None
        focal_pos = ss.focal_position
        sx0 = focal_pos[0] or 0
        sy0 = focal_pos[1] or 0
        sz0 = focal_pos[2] or 0
        sy_angle = self.sy_angle
        sz_angle = self.sz_angle

        if translation_x is None:
            x_axis = ss.x_axis
            assert x_axis is not None
            x_pos = x_axis.position
        else:
            x_pos = translation_x

        x = x_pos - sx0
        y: float | None
        z: float | None
        if sy_angle is not None:
            y = sy0 + numpy.tan(sy_angle) * x
        else:
            y = None
        if sz_angle is not None:
            z = sz0 + numpy.tan(sz_angle) * x
        else:
            z = None

        return y, z

    def _on_focal_position_changed(self, value, *args, **kwargs):
        self._update_detector_center()

    def _on_x_axis_position_changed(self, value, *args, **kwargs):
        self._update_detector_center(translation_x=value)

    def _update_detector_center(self, translation_x: float | None = None):
        ss = self._sample_stage
        assert ss is not None
        detector_center = self._compute_detector_center(translation_x=translation_x)
        ss.detector_center = detector_center

    def __info__(self):
        ss = self._sample_stage
        if ss is None:
            return "Sample stage not setup"
        focal_pos = ss.focal_position
        sx0 = focal_pos[0] or 0
        sx_axis = ss.x_axis
        if sx_axis is None:
            return "No x-axis defined in the sample stage"
        sy_axis = ss.y_axis
        sz_axis = ss.z_axis

        dsx = pint.Quantity(1, "m").to(sx_axis.unit).magnitude - sx0
        sy_angle = self.sy_angle or 0
        sz_angle = self.sz_angle or 0
        sy_disp = numpy.tan(sy_angle) * dsx
        sz_disp = numpy.tan(sz_angle) * dsx

        def format_disp(axis: Axis, disp: float):
            unit = axis.unit
            if unit is None:
                unit = ""
            else:
                unit = f" {unit}"
            return f"{axis.axis_rounder(disp)}{unit}"

        string = ""
        string += f"BeamTracker: {self.name}\n"
        string += "\n"

        table = []
        table += [["", "Angle", "Displacement/meter"]]
        table += [["sy_angle", f"{sy_angle:0.4f} rad", format_disp(sy_axis, sy_disp)]]
        table += [["sz_angle", f"{sz_angle:0.4f} rad", format_disp(sz_axis, sz_disp)]]

        string += tabulate.tabulate(table, headers="firstrow", tablefmt="simple")
        return string


class BeamTrackerCalcController(CalcController):
    """
    .. code-block::

        controller:
        - module: tomo.controllers.beam_tracker
            class: BeamTrackerCalc
            beam_tracker: $hr_beam_tracker
            plugin: generic

            axes:
            # sample stage
            - name: $sx
                tags: real sx
            - name: $sy
                tags: real sy
            - name: $sz
                tags: real sz

            # beam tracking
            - name: sxbeam
                tags: sxbeam
    """

    def __init__(self, config: ConfigNode):
        super().__init__(config)
        self._beam_tracker = config["beam_tracker"]

    def calc_from_real(self, user_positions: dict[str, float]):
        dial_result: dict[str, float] = {"sxbeam": user_positions["sx"]}
        return dial_result

    def calc_to_real(self, dial_positions: dict[str, float]):
        sxbeam = dial_positions["sxbeam"]

        # This is not anymore a pure function
        sx_axis = self._tagged["sx"][0]
        sy_axis = self._tagged["sy"][0]
        sz_axis = self._tagged["sz"][0]

        sx = sx_axis.position
        sy = sy_axis.position
        sz = sz_axis.position
        dy, dz = self._beam_tracker.compute_correction(sx, sxbeam)

        user_result: dict[str, float] = {
            "sx": sxbeam,
            "sy": sy + dy,
            "sz": sz + dz,
        }

        return user_result
