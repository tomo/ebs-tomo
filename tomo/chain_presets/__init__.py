"""
Presets for the chain.

.. autosummary::
   :toctree:

    default_chain_fast_shutter
    default_chain_image_corr_on_off
    image_no_saving
"""
