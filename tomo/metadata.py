from __future__ import annotations
import typing
from bliss import current_session
from bliss.common.logtools import log_info
try:
    # python >= 3.10
    from importlib.metadata import version as metadata_version
except ImportError:
    # python <= 3.9
    from importlib_metadata import version as metadata_version
from tomo.helpers.nexuswriter_utils import hdf5_device_names
from bliss.controllers.lima.lima_base import Lima
from bliss.scanning.toolbox import ChainBuilder
from tomo.helpers.axis_utils import is_user_clockwise


class TomoMetaData:
    """
    Class for tomo metadata object.
    The class implements methods to handle metadata in tomo acquisition.

    The metadata is built as a nested dictionary.
    For every tomo scan the dictionary gets injected as scan info in the Bliss scan object.
    Metadata can be retrieve in the HDF5 data file under 'technique' section.

    **Attributes**

    name : str
        The Bliss object name
    rotation : Bliss controller object
        axis used for tomo acquisition
    detector : Bliss controller object
        detector used for tomo acquisition
    detectors : Tomo object
        containing all tomo detectors and optic associated to each one
        method to calculate image pixel size is also attach to this object
    tomo_config : Tomo object
        containing description of tomograph (which rotation axis is used, etc...)
    """

    def __init__(self, tomo_config, detector):
        # init logging

        log_info(self, "TomoMetaData:__init__() entering")

        self.rotation = tomo_config.rotation_axis
        self.detector = detector
        self.detectors = tomo_config.detectors
        self.tomo_config = tomo_config

    def tomo_scan_info(self, pars):
        """Deprecated"""
        s = {}
        self.update_tomo_scan_info(s, pars)
        return s

    def update_tomo_scan_info(self, scan_info, pars):
        """
        Build the nested metadata dictionary and update scan info technique
        field of scan sequence object with it
        Metadata are divided into several categories: scan, scan flags (for options),
        detector, optic, saving and tomoconfig to describe beamline setup
        """
        tomoconfig = self.tomo_config
        tomo_detector = self.detectors.get_tomo_detector(self.detector)

        technique = scan_info.setdefault("technique", {})
        technique["@tomo_version"] = metadata_version("bliss-tomo")

        # Basic tomo scan parameters
        scan = technique.setdefault("scan", {})
        try:
            scan["name"] = current_session.scan_saving.dataset.name
        except AttributeError:
            # BasicScanSaving dont have dataset attribute
            scan["name"] = "data"

        ALIASES = current_session.env_dict["ALIASES"]
        # alias_name = str(setup_globals.ALIASES.get_alias(motor.name))
        alias_name = str(ALIASES.get_alias(self.rotation.name))
        scan["motor"] = ["rotation", self.rotation.name, alias_name]
        scan["scan_type"] = pars.scan_type
        scan["scan_range"] = pars.range
        scan["dark_n"] = pars.dark_n
        scan["flat_n"] = pars.flat_n
        scan["tomo_n"] = pars.tomo_n

        # for step scan one additional image is taken for each ref group
        # asked by DAU to adapt tomo_n value for coherency
        if pars.scan_type == "STEP":
            scan["tomo_n"] += pars.nb_proj_groups if pars.nb_proj_groups > 0 else 1

        scan["flat_on"] = pars.points_proj_groups
        scan["exposure_time"] = pars.exposure_time * 1000.0
        scan["exposure_time@units"] = "ms"

        # Only in continuous hard runner!!!!!
        scan["latency_time"] = pars.latency_time * 1000.0
        scan["latency_time@units"] = "ms"

        scan["energy"] = pars.energy
        scan["energy@units"] = "keV"
        scan["sample_pixel_size"] = self.detectors.get_image_pixel_size(self.detector)
        scan["sample_pixel_size@units"] = "um"
        scan["source_sample_distance"] = tomoconfig.sample_stage.source_distance
        scan["source_sample_distance@units"] = "mm"
        scan["sample_detector_distance"] = tomo_detector.sample_detector_distance
        scan["sample_detector_distance@units"] = "mm"
        scan["source_detector_distance"] = (
            scan["sample_detector_distance"] + scan["source_sample_distance"]
        )
        scan["source_detector_distance@units"] = "mm"
        scan["effective_propagation_distance"] = (
            scan["sample_detector_distance"]
            * scan["source_sample_distance"]
            / scan["source_detector_distance"]
        )
        scan["effective_propagation_distance@units"] = "mm"
        scan["sequence"] = pars.sequence
        scan["field_of_view"] = pars.field_of_view
        scan["half_acquisition"] = pars.half_acquisition
        if pars.half_acquisition:
            scan["shift_in_mm"] = pars.shift_in_mm
        scan["comment"] = pars.comment

        # Scan configuration flags
        scan_flags = technique.setdefault("scan_flags", {})
        scan_flags["dark_images_at_start"] = pars.dark_at_start
        scan_flags["dark_images_at_end"] = pars.dark_at_end
        scan_flags["ref_images_at_start"] = pars.flat_at_start
        scan_flags["ref_images_at_end"] = pars.flat_at_end
        scan_flags["no_projection_groups"] = True if pars.nb_proj_groups == 0 else False
        scan_flags["no_return_images"] = not pars.images_on_return
        scan_flags["return_images_aligned_to_flats"] = (
            pars.return_images_aligned_to_flats
        )
        scan_flags["return_to_start_pos"] = pars.return_to_start_pos

        # Detector image parameters

        # read image parameters
        technique["detector"] = self.get_detectors_parameters(self.detector)

        # optic parameters
        optic = technique.setdefault("optic", {})
        used_optic = self.detectors.get_optic(self.detector)
        optic["name"] = used_optic.description
        optic["type"] = used_optic.type
        optic["magnification"] = used_optic.magnification
        optic["scintillator"] = used_optic.scintillator

        ### Deprecated in the future. Moved to scan section ###
        optic["sample_pixel_size"] = tomo_detector.sample_pixel_size
        optic["sample_pixel_size@units"] = "um"
        ### Deprecated in the future. Moved to scan section ###

        optic["optics_pixel_size"] = tomo_detector.optics_pixel_size
        optic["optics_pixel_size@units"] = "um"

        # saving parameters
        saving = technique.setdefault("saving", {})
        saving["path"] = current_session.scan_saving.get_path()
        saving["beamline"] = current_session.scan_saving.beamline
        saving["image_file_format"] = self.detector.saving.file_format
        saving["frames_per_file"] = self.detector.saving.frames_per_file

        # Structural information from the beamline
        sample_stage = tomoconfig.sample_stage
        model_mapping = {
            "translation_x": sample_stage.x_axis,
            "translation_y": sample_stage.y_axis,
            "translation_z": sample_stage.z_axis,
            "sample_x": sample_stage.sample_x_axis,
            "sample_y": sample_stage.sample_y_axis,
            "sample_u": sample_stage.sample_u_axis,
            "sample_v": sample_stage.sample_v_axis,
            "rotation": sample_stage.rotation_axis,
            "detector": self.detector,
            "optic": used_optic,
        }
        model: dict[str, typing.Any] = {
            k: hdf5_device_names(obj)
            for k, obj in model_mapping.items()
            if obj is not None
        }
        model["rotation_is_clockwise"] = is_user_clockwise(sample_stage.rotation_axis)
        detector_center = sample_stage.detector_center
        if detector_center[0]:
            model["detector_center_y"] = detector_center[0]
            model["detector_center_y@units"] = sample_stage.y_axis.unit
        if detector_center[1]:
            model["detector_center_z"] = detector_center[1]
            model["detector_center_z@units"] = sample_stage.z_axis.unit

        technique["tomoconfig"] = model
        technique["active_tomo_config"] = hdf5_device_names(tomoconfig)

    def get_detectors_parameters(self, *detectors):
        """
        Return a dictionary with the current image parameters for a list of detectors
        """
        detectors_par = dict()

        detectors = list(detectors)
        if len(detectors) == 0:
            builder = ChainBuilder([])

            detectors = list()
            for node in builder.get_nodes_by_controller_type(Lima):
                detectors.append(node.controller)

        for detector in detectors:
            detector_par = dict()
            # simulation camera has no name! Do all cameras have the name attribute?
            try:
                detector_par["name"] = detector.camera.name
            except Exception:
                pass

            detector_par["type"] = detector.camera_type
            # Not found in Bliss parameters
            detector_par["pixel_size"] = list(detector.proxy.camera_pixelsize)
            detector_par["size"] = [detector.image.width, detector.image.height]
            # Need to read from Lima, because accumulation mode is applied with proxy!
            detector_par["depth"] = [int(detector.image.depth)]
            # Binning not yet added to parameters in Bliss
            detector_par["binning"] = detector.image.binning
            detector_par["flipping"] = detector.image.flip
            detector_par["accumulation"] = (
                True if detector.acquisition.mode == "ACCUMULATION" else False
            )
            if detector_par["accumulation"]:
                expo_time = self.tomo_config.pars.exposure_time
                detector_par["acc_nb_frames"] = int(
                    expo_time / detector.accumulation.expo_time
                )
                detector_par["acc_expo_time"] = detector.accumulation.expo_time
            detector_par["roi"] = detector.image.roi
            if "pco" in detector.camera_type.lower():
                if detector.camera.rolling_shutter == 1:
                    detector_par["shutter_type"] = "rolling shutter"
                if detector.camera.rolling_shutter == 2:
                    detector_par["shutter_type"] = "global shutter"

            detectors_par[detector.name] = detector_par

        return detectors_par
