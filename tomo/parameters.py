import sys
import gevent
import numpy as np
import time
from bliss.common.standard import *
from bliss.common.standard import info
from bliss.common.logtools import log_info, log_debug
from bliss.config.wardrobe import ParametersWardrobe


class TomoParameters:
    """
    Class to handle persistant parameters.
    A parameter set is stored in Redis under the specified name during initialization.
    It can be initialised to a set of default parameters with default values.
    The default values musst be passed as a dictionary in the form {"parameter_name" : default_value, ...}

    **Attributes**

    param_name : string
        name of parameter object saved in Redis
    parameters : Bliss parameter wardrobe object
        parameter object saved in Redis
    """

    def __init__(self, param_name=None, param_defaults=None):
        self.param_name = param_name

        if self.param_name is not None:
            self.parameters = ParametersWardrobe(self.param_name)

            if param_defaults is not None:
                parameters_dict = self.parameters.to_dict(export_properties=True)
                for k, v in param_defaults.items():
                    if k not in parameters_dict:
                        self.parameters.add(k, v)

    def save_scan_config(self, directory):
        """
        Save in a file a parameter set.
        """
        if self.param_name != None:
            file_name = directory + "/" + self.param_name
            self.parameters.to_file(file_name)

    def load_scan_config(self, directory):
        """
        Load from a file a parameter set.
        """
        if self.param_name != None:
            file_name = directory + "/" + self.param_name
            self.parameters.from_file(file_name, "default")

    def show_config(self):
        """
        Print the parameters of the object.
        """

        if self.param_name != None:
            print(info(self.parameters))

    def reset(self):
        """
        Delete all parameters of the object from Redis.
        to re-start with the default values.
        """

        if self.param_name != None:
            #
            # remove all parameters one by one.
            # Cannot remove all parameters with the name given at object creation!
            # self.parameters.remove(self.tomo_name+':saving_parameters')
            #
            parameters_dict = self.parameters.to_dict()
            for k in parameters_dict.items():
                self.parameters.remove("." + k[0])
