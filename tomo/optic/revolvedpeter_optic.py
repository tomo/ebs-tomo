# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import typing
import gevent
import numpy

from bliss.common.logtools import log_critical
from bliss.shell.cli.user_dialog import (
    UserMsg,
    UserChoice,
    Container,
    UserCheckBoxList,
)
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss.common.logtools import log_warning
from bliss.config.beacon_object import BeaconObject
from tomo.optic.base_optic import BaseOptic, OpticState
from bliss.common.tango import DeviceProxy


class RevolvedPeterOptic(BaseOptic):
    """
    Class to handle OptiquePeter in BLISS.

    It is compound by:
    - A revolver of 3 optics
    - A rotation motor which allow to select one of this optics
    - An optional eyepiece

    The control is done by a wago.

    The optics can be chosen by the scientists from a box of optics an mounted
    manually. The device have no way to know what was mounted by the people.

    The eyepiece can be chosen by the scientists from a list of 4 parts.
    Only one of them can be installed at a time, or nothing.

    The class has three parts:

    - The standard methods implemented for every optic,
    - methods to handle the three objectives and four eyepieces and
    - methods to extract the used focus motor and the necessary focus scan parameters.

    **Attributes**:

    name : str
        The Bliss object name
    config : dictionary
        The Bliss configuration dictionary
    wago_device : string
        The name of the Wago server Tango device to use
    wago_proxy : Tango device proxy
        Proxy object used to communicate with wago device
    image_flipping_hor : boolean
        Implied horizontal image flipping by the objective
    image_flipping_vert : boolean
        Implied vertical image flipping by the objective

    rotc_motor : Bliss Axis object
        The camera rotation motor used for all three objectives
    focus_motor : Bliss Axis object
        The focus motor used for all three objectives
    focus_type : string
        The motion type of the focus motor. Can be "translation" or "rotation".
    focus_scan_steps : int
        The number of scan steps for the focus scan. The scan range depends
        on the magnification used.
    focus_lim_pos : float
        Positive soft limit for the focus motor
    focus_lim_neg : float
        Negative soft limit for the focus motor

    Example yml file:

    .. code-block:: yaml

        - name:  hrpeter
          plugin: bliss
          class: RevolvedPeterOptic
          package: tomo.optic.revolvedpeter_optic

          wago_device: "id19/wcid19c/tg"
          image_flipping_hor:  False
          image_flipping_vert: False

          rotc_motor:  $hrrotc
          focus_motor: $focrev
          focus_type: "translation"     # translation or rotation
          focus_scan_range: 0.02
          focus_scan_steps: 20
          focus_lim_pos:  0.5
          focus_lim_neg: -0.5
    """

    EXISTING_OPTIC_MAGNIFICATIONS = [2.0, 4.0, 5.0, 10.0, 20.0, 40.0]
    """
    List of available objectives which can be mounted.

    FIXME: UPDATE THE LIST OF AVAILABLE OPTICS
    """

    EXISTING_EYEPIECE_MAGNIFICATIONS = [2.0, 2.5, 3.3, 4.0]
    """
    4 different eyepieces can be mounted.

    Which one is mounted is indicated by the Wago.
    """

    def __init__(self, name, config):
        self.__name = name
        self.__config = config

        self._wago_tango_url = config["wago_device"]
        self._current_objective: int = None
        self._current_eyepiece: int = None

        param_name = self.__name + ":parameters"
        param_defaults = {}

        # Initialize the TomoOptic class
        super().__init__(name, config, param_name, param_defaults)

        self._update_state(OpticState.READY)
        self.__move_task = None
        self.__move_done = gevent.event.Event()
        self.__move_done.set()

        # Connect to the wago device
        if isinstance(self._wago_tango_url, str):
            self._wago_proxy = DeviceProxy(self._wago_tango_url)
        else:
            self._wago_proxy = self._wago_tango_url
            assert self._wago_proxy is not None

        self.sync_hard()

    _ = """Optic which was manually mounted on this device"""
    _mounted_magnifications = BeaconObject.property_setting(
        name="mounted_magnifications", default=[None, None, None], doc=_
    )

    def list_motors(self):
        return []

    @property
    def description(self) -> str:
        """
        The name string the current optics
        """
        try:
            if self._current_eyepiece is not None:
                e = self.EXISTING_EYEPIECE_MAGNIFICATIONS[self._current_eyepiece]
            else:
                e = 1
            m = self._mounted_magnifications[self.objective]
            name = f"Revolved_Peter_{m}_{e}"
        except Exception as e:
            name = "Revolved_Peter: " + str(e)
        return name

    #
    # Standard optics methods every optics has to implement
    #

    def calculate_magnification(self, iobjective: int, ieyepiece: typing.Optional[int]):
        """Calculate the magnification from the index location of the objective
        and the eyepiece.

        The eyepiece can be omitted. In this case it is `None`.
        """
        try:
            m = self._mounted_magnifications[iobjective]
        except:
            m = None
        if ieyepiece is not None:
            e = self.EXISTING_EYEPIECE_MAGNIFICATIONS[ieyepiece]
        else:
            e = 1
        if m is None:
            return e
        return e * m

    @property
    def _eyepiece_magnification(self) -> typing.Optional[float]:
        ieyepiece = self._current_eyepiece
        if ieyepiece is None:
            return None
        return self.EXISTING_EYEPIECE_MAGNIFICATIONS[ieyepiece]

    @property
    def _objective_magnification(self) -> typing.Optional[float]:
        iobjective = self._current_objective
        if iobjective is None:
            return None
        return self._mounted_magnifications[iobjective]

    @property
    def magnification(self) -> typing.Optional[float]:
        """Returns the actual magnification of the device"""
        iobj, ieye = self._read_objectives()
        return self.calculate_magnification(iobj, ieye)

    def move_magnification(self, magnification: float, wait: bool = True):
        """Move the device to automatically mount a specific available magnification.

        Arguments:
            magnification: The requested magnification
            wait: If `True` the function returns when the new
                  state is reached. Else the request is send to controller
                  and returns before the end.

        Raises:
            ValueError: If the magnification is not reachable
        """
        self.sync_hard()
        objective = self._get_objective_from_magnification(magnification)
        self._update_target_magnification(magnification)
        self._move_objective(objective, wait=wait)

    def _get_objective_from_magnification(self, magnification) -> int:
        """Returns the closest objective from the expected magnification.

        Raises an exception if no magnification are found or if magnification if
        not very close (epsilon: 0.001)

        Raises:
            ValueError: If no available objective are close enough to the requested magnification
        """
        ieyepiece = self._current_eyepiece
        if ieyepiece is not None:
            e = self.EXISTING_EYEPIECE_MAGNIFICATIONS[ieyepiece]
        else:
            e = 1
        mag = self._mounted_magnifications
        if mag == [None, None, None]:
            raise ValueError(f"No objective was setup")
        mag = [m if m is not None else numpy.nan for m in self._mounted_magnifications]
        mag = numpy.array(mag) * e
        closest_objective = numpy.nanargmin(numpy.abs(mag - magnification))
        closest_magnification = mag[closest_objective]

        epsilon = 0.001
        if (
            not numpy.isfinite(closest_magnification)
            or abs(closest_magnification - magnification) > epsilon
        ):
            raise ValueError(
                f"No objective found for magnification {magnification} (closest is {closest_magnification})"
            )

        return closest_objective

    def _move_objective(self, value: int, wait: bool):
        """
        Move the optic to a specific objective index.

        Arguments:
            value: Index of the objective from 0 to 2
            wait: If true, the call is blocking, else it is spawned in a greenlet
        """

        def do_move():
            try:
                self._execute_pre_move_hooks()
                self.objective = value
                self._execute_post_move_hooks()
                self._update_target_magnification(None)
            except Exception:
                log_critical(self, "Error while moving objective", exc_info=True)
                raise
            finally:
                self._update_state(OpticState.READY)
                if self.__move_task:
                    self.__move_done.set()
                    self.__move_task = None

        if wait:
            self._update_state(OpticState.MOVING)
            do_move()
        else:
            if self.__move_task is not None:
                raise RuntimeError(f"Optic {self.name} already about to move")
            self.__move_done.clear()
            self._update_state(OpticState.MOVING)
            self.__move_task = gevent.spawn(do_move)

    def wait_move(self):
        """Block until the actual move request is not finished"""
        if self.__move_task:
            self.__move_done.wait()

    #
    # Specific objective handling
    #

    def optic_setup(self, submenu=False):
        """
        Set-up the magnification for the two objectives mounted.
        They can be chosen from the list of possible magnifications.
        """
        while True:

            def optic_label(index):
                mag = self._mounted_magnifications[index]
                if mag is None:
                    return f"       (slot {index+1}) No objective mounted/declared"
                return f"×{mag:5.2f} (slot {index+1})"

            value_list = []
            for iobj, _ in enumerate(self._mounted_magnifications):
                value_list.append((iobj, optic_label(iobj)))

            # get the actual magnification value as default
            default1 = 0
            for i in range(0, len(value_list)):
                if self._current_objective == value_list[i][0]:
                    default1 = i

            dlg1 = UserChoice(values=value_list, defval=default1)

            if self._current_eyepiece is None:
                msg = "No eyepiece mounted"
            else:
                ieye = self._current_eyepiece
                eye = self.EXISTING_EYEPIECE_MAGNIFICATIONS[ieye]
                msg = f"Eyepiece with a magnification ×{eye}"
            dlg2 = UserMsg(label=msg)

            dlg3 = UserCheckBoxList(
                values=[("config", "Configure mounted magnifications")]
            )

            ct1 = Container([dlg1], title="Objective")
            ct2 = Container([dlg2], title="Eyepiece")
            ct3 = Container([dlg3], title="Configuration")
            cancel_text = "Back" if submenu else "Cancel"
            ret = BlissDialog(
                [[ct1], [ct2], [ct3]], title="Peter Setup", cancel_text=cancel_text
            ).show()

            # returns False on cancel
            if ret != False:
                # get the objective chosen
                sel_objective = ret[dlg1]
                # move to objective
                if self.objective != sel_objective:
                    self.objective = sel_objective
                self.sync_hard()

                if ret[dlg3] == ["config"]:
                    self.mount_setup(submenu=True)
                    continue

            break

    def mount_setup(self, submenu=False):
        """
        Set-up the mounted magnifications for the objective slots.
        """
        from tomo.helpers.user_optional_float_input import (
            UserOptionalFloatInput,
            float_or_empty,
        )

        mags = self._mounted_magnifications
        mags = ["" if m is None else m for m in mags]

        mag_input1 = UserOptionalFloatInput(label="Magnification 1", defval=mags[0])
        mag_input2 = UserOptionalFloatInput(label="Magnification 2", defval=mags[1])
        mag_input3 = UserOptionalFloatInput(label="Magnification 3", defval=mags[2])

        ct1 = Container([mag_input1, mag_input2, mag_input3], title="Objective")
        cancel_text = "Back" if submenu else "Cancel"
        ret = BlissDialog([[ct1]], title="Peter Setup", cancel_text=cancel_text).show()

        # returns False on cancel
        if ret != False:
            mag1 = float_or_empty(ret[mag_input1])
            mag2 = float_or_empty(ret[mag_input2])
            mag3 = float_or_empty(ret[mag_input3])

            self.set_mounted_objective(1, mag1)
            self.set_mounted_objective(2, mag2)
            self.set_mounted_objective(3, mag3)

            # Let's sync the eyepiece too
            self.sync_hard()

    def status(self):
        """
        Prints the current objective in use and its magnification.

        If an objective cannot be determined, the reason gets printed.
        """
        m = self.magnification
        i = self._current_objective + 1

        # test if eyepiece is connected
        if self._current_eyepiece is None:
            print("No eyepiece selected")
            print("The wago key opep does not indicate a selected eyepiece!")
            print(f"Objective {i} selected with NO eyepiece : magnification = X{m}")
        else:
            e = self._current_eyepiece + 1
            print(f"Objective {i} selected with eyepiece {e} : magnification = X{m}")

    @property
    def objective(self):
        """
        Reads and sets the current objective (0, 1 or 2)
        """
        self.sync_hard()
        if self._current_objective is None:
            return None
        return self._current_objective

    @objective.setter
    def objective(self, value):
        """
        Moves to the objective 0, 1 or 2
        """
        if value < 0 or value > 2:
            raise ValueError("Only the objectives 0, 1 and 2 can be chosen!")

        if self.objective == value:
            # Objective already at the right location
            return

        rotation = [0, 0]
        if (
            value > self._current_objective
            and not (self._current_objective == 0 and value == 2)
        ) or (self._current_objective == 2 and value == 0):
            # rotate in the positive direction
            rotation[1] = 1
            # print ("positive")
        else:
            # rotate in the negative direction
            rotation[0] = 1
            # print ("negative")

        self._wago_proxy.write_attribute("oprot", rotation)
        gevent.sleep(0.1)

        rotation[0] = 0
        rotation[1] = 0

        self._wago_proxy.write_attribute("oprot", rotation)
        gevent.sleep(5.0)

        iobj, ieye = self._read_objectives()
        magnification = self.calculate_magnification(iobj, ieye)
        self._update_magnification(magnification)

    def _read_objectives(
        self,
    ) -> typing.Tuple[typing.Optional[int], typing.Optional[int]]:
        """Read from the wago which objectives are actually mounted.

        Returns respectively the index of the optic, and the index of the eyepiece.
        Each of them can be `None`.
        """
        current_objective = None
        current_eyepiece = None

        # read objective and eyepiece arrays from wago
        attributes = self._wago_proxy.read_attributes(["opobj", "opep"])

        # The default value for objectives is 1. O indicates the objective is chosen.
        ojective_states = attributes[0].value

        # After some network errors, objective_states was None
        if ojective_states is None:
            log_warning(self, "Wago have returned None. Use the last known state.")
            return self._current_objective, self._current_eyepiece

        for i in range(0, len(ojective_states)):
            if ojective_states[i] == 0:
                current_objective = i
                break

        # The default value for eypieces is 0. 1 indicates the eyepiece is chosen.
        eyepiece_states = attributes[1].value
        for i in range(0, len(eyepiece_states)):
            if eyepiece_states[i] == 1:
                current_eyepiece = i
                break

        return current_objective, current_eyepiece

    def sync_hard(self):
        """Synchronize the state of the device from the state of the hardware."""
        iobj, ieye = self._read_objectives()
        if self._current_eyepiece != ieye:
            self._current_eyepiece = ieye
            self._update_actual_available_magnifications()
        if self._current_objective != iobj:
            self._current_objective = iobj
            magnification = self.calculate_magnification(iobj, ieye)
            self._update_magnification(magnification)

    def _update_actual_available_magnifications(self):
        """Calculate and update the magnifications which can be used actually."""
        ieyepiece = self._current_eyepiece
        if ieyepiece is not None:
            e = self.EXISTING_EYEPIECE_MAGNIFICATIONS[ieyepiece]
        else:
            e = 1
        mag = [m * e for m in self._mounted_magnifications if m is not None]
        mag = sorted(mag)
        self._update_available_magnifications(mag)

    def set_mounted_objective(self, slot: int, magnification: typing.Optional[float]):
        """
        Set the magnification of the mounted objective at this specific slot.

        Arguments:
            slot: The index of the slot from 1 to 3
            magnification: The magnification of the optic. This can be None.
        """
        assert 1 <= slot <= 3
        m = self._mounted_magnifications
        m[slot - 1] = magnification
        self._mounted_magnifications = m
        self._update_actual_available_magnifications()
        if self._current_objective == slot - 1:
            iobj, ieye = self._current_objective, self._current_eyepiece
            magnification = self.calculate_magnification(iobj, ieye)
            self._update_magnification(magnification)

    #
    # Focus related methods
    #

    def focus_scan_parameters(self):
        """
        Returns a dictionary with the parameters for a focus scan
        """
        scan_params = super().focus_scan_parameters()

        # the focus scan range is dependent on the magnification
        if self.objective == 0:
            scan_params["focus_scan_range"] = 0.05
        else:
            scan_params["focus_scan_range"] = 0.025

        return scan_params
