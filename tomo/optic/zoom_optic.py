# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import logging
import gevent

from bliss.shell.cli.user_dialog import UserMsg
from bliss.shell.cli.user_dialog import Container
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss.shell.standard import umv
from bliss.common.logtools import log_critical
from tomo.optic.base_optic import BaseOptic, OpticState
from bliss.common import event

_logger = logging.getLogger(__name__)


class ZoomOptic(BaseOptic):
    """
    Class to handle standard optics with one objective.
    The class has two parts:
    The standard methods implemented for every optic and
    methods to extract the used focus motor and the necessary focus scan parameters.

    **Attributes**:

    name : str
        The Bliss object name
    config : dictionary
        The Bliss configuration dictionary
    magnification : float
        Optic magnification
    image_flipping_hor : boolean
        Implied horizontal image flipping by the objective
    image_flipping_vert : boolean
        Implied vertical image flipping by the objective

    rotc_motor : Bliss Axis object
        The camera rotation motor for the optics
    zc_motor : Bliss axis object
        Z translation of the camera
    yc_motor : Bliss axis object
        Y translation of the camera
    focus_motor : Bliss Axis object of focus motor
        The focus motor for the optics
    focus_type : string
        The motion type of the focus motor. Can be "translation" or "rotation".

    **Example yml file**::

        - name:  Zoom
          plugin: bliss
          class: ZoomOptic
          package: tomo.fixedmag_optic

          # Operating region for the magnification
          magnification_range: [0.24, 0.985]

          image_flipping_hor:  False
          image_flipping_vert: False

          zc_motor: $zc1
          yc_motor: $yc1
          focus_motor: $zz
          focus_type: "translation"     # translation or rotation

    """

    def __init__(self, name, config):
        param_name = f"optic:{name}"
        param_defaults = {}
        super().__init__(name, config, param_name, param_defaults)
        self.__move_task = None
        self.__move_done = gevent.event.Event()
        self.__move_done.set()

        self.objective = None
        self._focus_motor = config["focus_motor"]
        self._zc_motor = config["zc_motor"]
        self._yc_motor = config["yc_motor"]
        self._zmag = config["zmag"]
        self._scint_motor = config.get("scint_motor", None)
        self._zfocus_motor = config.get("zfocus_motor", None)
        self._magnification_range = tuple(
            config.get("magnification_range", [None, None])
        )
        self.coef = config["coef"]
        self.cm = config["cm"]
        self.cf = config["cf"]
        self.cz = config["cz"]
        self.cy = config["cy"]

        self._update_magnification_range(self._magnification_range)
        try:
            mag = self._read_magnification_from_motors()
            self._update_magnification(mag)
        except Exception:
            _logger.error("Error while initializing magnification", exc_info=True)
            self._update_state(OpticState.INVALID)
        else:
            self._update_state(OpticState.READY)

        if self._scint_motor:
            event.connect(self._scint_motor, "position", self._update_scintillator)

    def list_motors(self):
        loptics = [self._focus_motor, self._zmag, self._zc_motor]
        if self._zfocus_motor:
            loptics.append(self._zfocus_motor)
        if self._scint_motor:
            loptics.append(self._scint_motor)
        return loptics

    @property
    def description(self):
        """
        The name string the current optics
        """
        name = "Zoom_X" + str(self.magnification)
        return name

    #
    # fixed otics methods every otics has to implement
    #

    @property
    def magnification(self):
        # FIXME: It would be better to listen events from motors
        mag = self._read_magnification_from_motors()
        self._update_magnification(mag)

        return super(ZoomOptic, self).magnification

    @magnification.setter
    def magnification(self, magnification):
        """
        Sets the magnification of the current objective used
        """
        self.move_magnification(magnification, wait=True)

    def is_valid_magnification(self, magnification):
        """Returns true if the magnification is inside the operating region of
        the device"""
        vmin, vmax = self._magnification_range
        if vmin is not None and magnification < vmin:
            return False
        if vmax is not None and magnification > vmax:
            return False
        return True

    def move_magnification(self, magnification, wait=False):
        """
        Move the device to reach the expected magnification.

        **Attributes**:

            magnification : float
                The requested magnification
            wait : float
                If `True` the function returns when the new
                state is reached. Else the request is send to the controller
                and returns before the end.

        **Raises**:

            ValueError : Exception
                If the magnification is outside of the operating region
                of this device.
        """
        if not self.is_valid_magnification(magnification):
            raise ValueError(
                f"The requested magnification {magnification} is outside of {self._magnification_range}"
            )

        if wait:
            self._update_state(OpticState.MOVING)
            self._update_target_magnification(magnification)
            try:
                self._move_motors(magnification)
            finally:
                new_magnification = self._read_magnification_from_motors()
                self._update_magnification(new_magnification)
                self._update_target_magnification(None)
                self._update_state(OpticState.READY)
        else:

            def run():
                # FIXME: that's basic implementation, it would be better
                #        to have a real async processing
                try:
                    self.move_magnification(magnification, wait=True)
                except Exception:
                    log_critical(
                        self, "Error while moving magnification", exc_info=True
                    )
                    raise
                finally:
                    self.__move_done.set()
                    self.__move_task = None

            self.__move_done.clear()
            self._update_state(OpticState.MOVING)
            self.__move_task = gevent.spawn(run)

    def wait_move(self):
        """Block until the actual move request is not finished"""
        if self.__move_task:
            self.__move_done.wait()

    def _read_magnification_from_motors(self):
        mag = 0
        zmag_pos = self._zmag.position
        c = len(self.coef) - 1
        for i in range(0, len(self.coef)):
            mag += self.coef[i] * pow(zmag_pos, c - i)
        return mag

    def _move_motors(self, magnification):
        """Move motors to the expected magnification"""
        # calculation of zmag_pos from the optical magnification
        value = magnification
        zmag_pos = 0
        c = len(self.cm) - 1
        for i in range(0, len(self.cm)):
            zmag_pos += self.cm[i] * pow(value, c - i)

        # calculation of zz_pos from optical magnification

        zz_pos = 0
        c = len(self.cf) - 1
        for i in range(0, len(self.cf)):
            zz_pos += self.cf[i] * pow(value, c - i)

        if zmag_pos > self._zmag.position:
            # print(f"Move {self._focus_motor.name} from {self._focus_motor.position} to {zz_pos:.3f}")
            umv(self._focus_motor, zz_pos)
            # print(f"Move {self._zmag.name} from {self._zmag.position} to {zmag_pos:.3f}")
            umv(self._zmag, zmag_pos)

        else:
            # print(f"Move {self._zmag.name} from {self._zmag.position} to {zmag_pos:.3f}")
            umv(self._zmag, zmag_pos)
            # print(f"Move {self._focus_motor.name} from {self._focus_motor.position} to {zz_pos:.3f}")
            umv(self._focus_motor, zz_pos)

        # calculation of zc1_pos from optical magnification

        zc_pos = 0
        c = len(self.cz) - 1
        for i in range(0, len(self.cz)):
            zc_pos += self.cz[i] * pow(value, c - i)
        # print(
        #    f"Move {self._zc_motor.name} from {self._zc_motor.position} to {zc_pos:.3f}"
        # )
        # umv(self._zc_motor, zc_pos)

        # calculation of yc1_pos from optical magnification

        yc_pos = 0
        c = len(self.cy) - 1
        for i in range(0, len(self.cy)):
            yc_pos += self.cy[i] * pow(value, c - i)
        # print(
        #    f"Move {self._yc_motor.name} from {self._yc_motor.position} to {yc_pos:.3f}"
        # )
        # umv(self._yc_motor, yc_pos)

    def _update_scintillator(self, position):
        if position <= 50 and position >= 40:
            self.scintillator = self.scintillators[2]
        elif position >= -50 and position <= -40:
            self.scintillator = self.scintillators[0]
        elif position >= -5 and position <= 5:
            self.scintillator = self.scintillators[1]
        else:
            self.scintillator = ""

    def optic_setup(self, submenu=False):
        """
        Set-up the optic by choosing from the list of available fixed optics
        """

        msg = f"Zoom optic with a magnification of X{self.magnification}"
        dlg1 = UserMsg(label=msg)

        ct1 = Container([dlg1], title="Optic")
        cancel_text = "Back" if submenu else "Cancel"
        BlissDialog([[ct1]], title="Zoom Optic Setup", cancel_text=cancel_text).show()

    def status(self):
        """
        Prints the current objective in use and its magnification.
        If an objective cannot be determined, the reason gets printed.
        """
        print("Zoom objective : magnification = X%.4f" % (self.magnification))
