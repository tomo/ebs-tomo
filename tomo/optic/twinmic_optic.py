# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

# from gevent import Timeout, sleep


import gevent
from bliss.shell.cli.user_dialog import (
    UserChoice,
    Container,
    UserInput,
    Validator,
    UserCheckBox,
)
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss.common import event
from bliss.common.axis import AxisState
from bliss.common.logtools import log_critical

from tomo.optic.base_optic import BaseOptic, OpticState


class TwinOptic(BaseOptic):
    """
    Class to handle double optics of type TwinMic.
    The class has three parts:
    The standart methods implemented for every optic,
    methods to handle the two objectives and
    methods to extract the used focus motor and the necessary focus scan parameters.

    **Attributes**:

    magnifications : list of float
        A list with the possible magnification lenses for this objective type
    magnification : float
        magnification value corresponding to current objective used
    selection_motor : Bliss Axis object
        The motor used to switch the objectives.
    image_flipping_hor : boolean
        Implied horizontal image flipping by the objective
    image_flipping_vert : boolean
        Implied verticalal image flipping by the objective

    focus_motor : Bliss Axis object
        The focus motor for the ojective
    focus_type : string
        The motion type of the focus motor. Can be "translation" or "rotation".
    focus_scan_steps : int
        The number of scan steps for the focus scan. The scan range depends
        on the maginification used.
    focus_lim_pos : float
        Positive soft limit for the focus motor
    focus_lim_neg : float
        Negative soft limit for the focus motor

    **Example yml file**::

        - name:  twinmic
          plugin: bliss
          class: TwinOptic
          package: tomo.optic.twinmic_optic

          # Two objectives can be mounted and dynamically chosen.
          # The magnifications given are used in the order [obj1, obj2]
          # Possible lens magnification values are [2, 5, 7.5, 10]
          magnifications: [2, 5]

          selection_motor: $twobjsel
          image_flipping_hor:  True
          image_flipping_vert: False

          focus_motor: $twfocus
          focus_type: "translation"     # translation or rotation
          focus_scan_steps: 20
          focus_lim_pos:  0.5
          focus_lim_neg: -0.5

    """

    def __init__(self, name, config):
        self.__name = name
        self.__config = config

        self._twinmic_magnifications = config["magnifications"]
        self._selection_motor = config["selection_motor"]
        self._focus_motor = config["focus_motor"]
        self._rotc_motor = config["rotc_motor"]
        self._mounted_objectives = [
            self._twinmic_magnifications[0],
            self._twinmic_magnifications[1],
        ]

        param_name = self.__name + ":parameters"
        param_defaults = {}

        # Initialise the TomoOptic class
        super().__init__(name, config, param_name, param_defaults)

        event.connect(self._selection_motor, "state", self.__motor_state_updated)
        # __motor_state_updated is not triggered at connection
        self.__motor_state_updated(self._selection_motor.state)
        self._update_available_magnifications(self._mounted_objectives)

    @property
    def description(self):
        """
        The name string the current optics
        """
        name = "TwinMic_" + str(self.magnification)
        return name

    #
    # standart otics methods every otics has to implement
    #
    def list_motors(self):
        return [self._focus_motor, self._rotc_motor]

    def __motor_state_updated(self, state: AxisState, *args, **kwargs):
        if state.MOVING:
            new_state = OpticState.MOVING
            new_magnification = None
        elif state.READY:
            try:
                objective = self.objective
            except ValueError:
                new_state = OpticState.INVALID
                new_magnification = None
            else:
                new_state = OpticState.READY
                new_magnification = self._mounted_objectives[objective - 1]
            self._update_target_magnification(None)
        else:
            new_state = OpticState.UNKNOWN
            new_magnification = None
        self._update_magnification(new_magnification)
        self._update_state(new_state)

    def calculate_magnification(self):
        """
        Returns magnification value according to current objective used
        """
        try:
            objective = self.objective
        except ValueError:
            return None

        return self._mounted_objectives[objective - 1]

    def _tune_magnification(self, user_magnification):
        """
        Sets the magnification of the current objective used
        """
        objective = self.objective
        self._mounted_objectives[objective - 1] = user_magnification
        self._update_magnification(user_magnification)
        self._update_available_magnifications(self._mounted_objectives)

    #
    # Specific objective handling
    #

    def move_magnification(self, magnification, wait=True):
        """Move the device to automatically mount a specific available magnification

        Attributes:
            magnification: The requested magnification
            wait: If `True` the function returns when the new
                  state is reached. Else the request is send to controller
                  and returns before the end.

        Raises:
            ValueError: If the magnification is not reachable
        """
        objective = self._get_objective_from_magnification(magnification)
        self._update_target_magnification(magnification)
        self._move_objective(objective, wait=wait)

    def optic_setup(self, submenu=False):
        """
        Set-up the objective to be used by chosing the objective's magnification
        """

        value_list = []
        for i in self._mounted_objectives:
            value_list.append((i, "X" + str(i)))

        # get the actual magnification value as default
        default1 = 0
        for i in range(0, len(value_list)):
            if self.magnification == value_list[i][0]:
                default1 = i

        dlg1 = UserChoice(values=value_list, defval=default1)
        ct1 = Container([dlg1], title="Objective")
        dlg2 = UserCheckBox(label="Expert settings", defval=False)
        cancel_text = "Back" if submenu else "Cancel"
        ret = BlissDialog(
            [[ct1], [dlg2]], title="TwinMic Setup", cancel_text=cancel_text
        ).show()

        # returns False on cancel
        if ret != False:
            # get the objective chosen
            sel_objective = self._mounted_objectives.index(float(ret[dlg1])) + 1
            # move to objective
            if self.objective != sel_objective:
                self.objective = sel_objective

            new_magnification = self.calculate_magnification()
            self._update_magnification(new_magnification)
            if ret[dlg2]:
                check_obj = Validator(self._objective_exists)
                dlg3 = UserInput(
                    label="objective 1",
                    defval=self._mounted_objectives[0],
                    validator=check_obj,
                )
                dlg4 = UserInput(
                    label="objective 2",
                    defval=self._mounted_objectives[1],
                    validator=check_obj,
                )
                ret = BlissDialog([[dlg3], [dlg4]], title="TwinMic Expert Setup").show()
                if ret != False:
                    self._mounted_objectives = [int(ret[dlg3]), int(ret[dlg4])]
                    new_magnification = self.calculate_magnification()
                    self._update_magnification(new_magnification)
                    self._update_available_magnifications(self._mounted_objectives)
                    for mag in  self._twinmic_magnifications:
                        if mag not in self._mounted_objectives:
                            self.config['magnifications'] = self._mounted_objectives
                            self.config['magnifications'].append(mag)
                            break
                    self.config.save()

    def _objective_exists(self, str_input):
        if int(str_input) not in self._twinmic_magnifications:
            raise ValueError(
                f"Only these objectives are currently configured for this optic: {self._twinmic_magnifications}"
            )

    def status(self):
        """
        Prints the current ojective in use and its magnification.
        If an objective cannot be determined, the reason gets printed.
        """
        try:
            ojective = self.objective
            magnification = self.magnification
            print(
                "Objective %d selected with a magnification of X%s"
                % (ojective, str(magnification))
            )

        except ValueError as err:
            print("Optics indicates a problem:\n", err)

    @property
    def objective(self):
        """
        Reads and sets the current objective (1, 2 or 3)
        """
        return self._objective_state()

    @objective.setter
    def objective(self, value):
        """
        Moves to the objective 1, 2 or 3
        """
        self._move_objective(value, wait=True)

    def _move_objective(self, value, wait):
        if value < 1 or value > 2:
            raise ValueError("Only the objectives 1 and 2 can be chosen!")
        target_magnification = self._mounted_objectives[value - 1]
        if target_magnification == 0:
            raise ValueError("Cannot choose zero magnification")
        current = self.objective
        if current == value:
            return

        if value == 1:
            target_position = -1  # negative limit
        elif value == 2:
            target_position = 1  # positive limit
        if wait:
            self._execute_pre_move_hooks()
            self._selection_motor.hw_limit(target_position)
            new_magnification = self._mounted_objectives[value - 1]
            self._update_magnification(new_magnification)
            self._execute_post_move_hooks()
        else:

            def run():
                try:
                    self._execute_pre_move_hooks()
                    self._selection_motor.hw_limit(target_position, wait=False)
                    self._execute_post_move_hooks()
                except Exception:
                    log_critical(self, "Error while moving objective", exc_info=True)
                    raise

            self._task = gevent.spawn(run)

    def _objective_state(self):
        """
        Evaluates which objective is currently used and returns its value (1or 2)
        """
        current_objective = None

        # Get current objective
        lim_high = self._selection_motor.hw_state.LIMPOS
        lim_low = self._selection_motor.hw_state.LIMNEG

        if lim_high is True and lim_low is False:
            current_objective = 2
        else:
            if lim_high is False and lim_low is True:
                current_objective = 1
            else:
                if lim_high is True and lim_low is True:
                    raise ValueError(
                        "No objective selected\nBoth limits are active at the same time, very strange!"
                    )
                else:
                    if lim_high is False and lim_low is False:
                        raise ValueError("No objective selected\nNo active limit!")

        return current_objective

    def _get_objective_from_magnification(self, magnification):
        for obj_id, obj_magnification in enumerate(self._mounted_objectives):
            if magnification == obj_magnification:
                return obj_id + 1
        raise ValueError("No objective found for magnification %s", magnification)

    def focus_scan_parameters(self):
        """
        Returns a dictionary with the paramters for a focus scan
        """
        scan_params = super().focus_scan_parameters()
        # the focus scan range is dependent on the magnification
        if self.magnification == 2:
            scan_params["focus_scan_range"] = 0.2
        else:
            scan_params["focus_scan_range"] = 0.1

        return scan_params
