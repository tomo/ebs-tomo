""" Detector optic

.. autosummary::
   :toctree:

    align
    base_optic
    dzoom_optic
    fixedmag_optic
    revolvedhasselblad_optic
    revolvedpeter_optic
    triplemic_optic
    twinmic_optic
    user_optic
    zoom_optic
"""
