# Compatibility with tomo <= 2.5.0

from tomo.align import *  # noqa
from bliss.common import deprecation

deprecation.deprecated_warning(
    "Module",
    "tomo.optic.align",
    replacement="tomo.align",
    since_version="2.5.0",
)
