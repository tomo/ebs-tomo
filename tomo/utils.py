import os
from enum import Enum

from bliss.shell.cli.user_dialog import (
    UserIntInput,
    UserFloatInput,
    UserCheckBox,
    UserChoice,
    Container,
    UserMsg,
    Validator,
    UserInput,
)
from bliss.shell.cli.pt_widgets import message_dialog
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss.controllers.lima.lima_base import Lima
from bliss.common.logtools import log_info
from bliss.shell.standard import umv
from bliss import current_session
from tomo.sequence.pcotomo import PcoTomo
from tomo.sequence.multitomo import MultiTurnsTomo
from tomo.helpers.user_optional_float_input import UserOptionalFloatInput


class ScanType(Enum):
    CONTINUOUS = 0
    SWEEP = 1
    INTERLACED = 2
    STEP = 3


class ShiftType(Enum):
    FOV_FACTOR = "Field-of-view factor"
    """The shift is expected as a factor of the half detector width.

    - `0` is the center
    - `1` is the right side of the detector
    - `-1` is the left side of the detector
    """

    MM = "Millimeters"
    """The shift is expected as an offset in millimeter"""

    PIXEL = "Pixels"
    """The shift is expected as an offset in pixel of the detector"""


def setup(sequence):
    """
    Set-up the tomo scan and all its sub objects like detector,
    optic, shutter ,saving, etc.
    """
    from tomo.helpers.select_dialog2 import select_dialog2

    tomo_config = sequence._tomo_config
    choice = None
    while True:
        value_list = []

        if tomo_config.detectors.active_detector:
            value_list.extend(
                [
                    (
                        "detector",
                        f"Setup detector/optic {tomo_config.detectors.active_detector.detector.name}",
                    )
                ]
            )
        value_list.extend([("active_detector", "Select the active detector")])

        value_list.extend(
            [
                (""),
                ("scan", "Scan Parameter Setup"),
                ("flags", "Scan Option Setup"),
                ("type", "Scan Type Setup"),
                ("half", "Half Tomo Setup"),
                (""),
                ("reference", "Reference Setup"),
                ("saving", "Saving Setup"),
                (""),
                ("detectors", "Available detectors"),
            ]
        )

        choice = select_dialog2(
            title="Tomo Setup", values=value_list, selection=choice, cancel_text="Close"
        )

        if choice is None:
            break

        if choice == "scan":
            scan_setup(sequence, submenu=True)
            if isinstance(sequence, MultiTurnsTomo):
                multiturns_setup(sequence, submenu=True)
        elif choice == "active_detector":
            active_detector_setup(sequence, submenu=True)
        elif choice == "flags":
            flag_setup(sequence, submenu=True)
        elif choice == "type":
            type_setup(sequence, submenu=True)
        elif choice == "reference":
            reference_setup(sequence, submenu=True)
        elif choice == "saving":
            saving_setup(submenu=True)
        elif choice == "half":
            half_setup(sequence, submenu=True)
        elif choice == "detector":
            tomo_config.detectors.active_detector.setup(submenu=True)
        elif choice == "detectors":
            tomo_config.detectors.setup(submenu=True)
        else:
            print(f"Unsupported choice {choice}")

    sequence.init_sequence()
    try:
        sequence._detector.camera_type
    except AttributeError:
        return
    sequence.build_sequence()
    sequence.validate()
    sequence.show_info()


def scan_setup(sequence, submenu=False):
    """
    Set-up the main tomo scan parameters
    """
    sequence.init_sequence()
    parameters = sequence.pars.to_dict()
    source_sample_distance = sequence._tomo_config.sample_stage.source_distance
    sample_detector_distance = sequence._detectors.get_tomo_detector(
        sequence._detector
    ).sample_detector_distance

    dlg_range = UserFloatInput(label="Tomo Range?", defval=parameters["range"])

    dlg_expo = UserFloatInput(
        label="Detector Exposure Time [s]?", defval=parameters["exposure_time"]
    )
    dlg_latency = UserFloatInput(
        label="Latency Time [s]?", defval=parameters["latency_time"]
    )

    dlg_dark_n = UserIntInput(
        label="Number of Dark Images? (> 0)", defval=parameters["dark_n"]
    )
    dlg_ref_n = UserIntInput(
        label="Number of Flat Images? (> 0)", defval=parameters["flat_n"]
    )
    # Only ask for ref_on if reference groups are enabled
    if parameters["projection_groups"]:
        dlg_flat_on = UserIntInput(
            label="Flat Images after how many projections? (> 0)",
            defval=parameters["flat_on"],
        )
    else:
        dlg_flat_on = UserMsg(label="Projection Groups are Disabled")

    dlg_sdd = UserFloatInput(
        label="Sample to Detector Distance [mm]?", defval=sample_detector_distance
    )
    dlg_ssd = UserOptionalFloatInput(
        label="Source to Sample Distance [mm]?", defval=source_sample_distance
    )
    dlg_en = UserFloatInput(label="Energy [keV] ?", defval=parameters["energy"])
    dlg_comment = UserInput(label="Comment", defval=parameters["comment"])

    if "helical" in sequence.name:
        dlg_tomo_n = UserIntInput(
            label="Number of Projections per turn?", defval=parameters["tomo_n"]
        )
        dlg_n_turns = UserIntInput(
            label="Number of turns?", defval=parameters["nturns"]
        )
        dlg_zstep = UserFloatInput(
            label="Z step per turn", defval=parameters["slave_step_size"]
        )
        ct1 = Container(
            [dlg_range, dlg_tomo_n, dlg_n_turns, dlg_zstep, dlg_expo, dlg_latency],
            title="Main Parameters",
        )
    else:
        dlg_tomo_n = UserIntInput(
            label="Number of Projections?", defval=parameters["tomo_n"]
        )
        ct1 = Container(
            [dlg_range, dlg_tomo_n, dlg_expo, dlg_latency], title="Main Parameters"
        )

    ct2 = Container([dlg_dark_n, dlg_ref_n, dlg_flat_on], title="Dark and Flat Images")
    ct3 = Container([dlg_sdd, dlg_ssd, dlg_en, dlg_comment], title="Others")

    cancel_text = "Back" if submenu else "Cancel"
    ret = BlissDialog(
        [[ct1], [ct2], [ct3]], title="Tomo Scan Setup", cancel_text=cancel_text
    ).show()

    # returns False on cancel
    if ret is not False:
        new_params = {
            "range": float(ret[dlg_range]),
            "tomo_n": int(ret[dlg_tomo_n]),
            "exposure_time": float(ret[dlg_expo]),
            "latency_time": float(ret[dlg_latency]),
            "dark_n": int(ret[dlg_dark_n]),
            "flat_n": int(ret[dlg_ref_n]),
            "energy": float(ret[dlg_en]),
            "comment": ret[dlg_comment],
        }
        if parameters["projection_groups"]:
            new_params["flat_on"] = int(ret[dlg_flat_on])
        sequence.pars.from_dict(new_params)
        sample_detector_distance = ret[dlg_sdd]
        source_sample_distance = dlg_ssd.input_to_value(ret[dlg_ssd])
        energy = float(ret[dlg_en])
        tomoconfig = sequence._tomo_config
        tomoconfig.sample_stage.source_distance = source_sample_distance
        tomoconfig.energy = energy
        tomodetector = sequence._detectors.get_tomo_detector(sequence._detector)
        tomodetector.sample_detector_distance = sample_detector_distance


def multiturns_setup(sequence, submenu=False):
    parameters = sequence.pars.to_dict()
    dlg_waiting_turns = UserIntInput(
        label="Number of Waiting Turns?", defval=parameters["waiting_turns"]
    )
    min_acc_turns = sequence._check_acc_turns()
    dlg_min_acc_turns = UserMsg(
        label=f"Minimum turns for the motor to accelerate is {min_acc_turns}"
    )
    dlg_starting_turns = UserIntInput(
        label="Number of Starting Turns?", defval=min_acc_turns
    )
    if isinstance(sequence, PcoTomo):
        max_scans = sequence._check_max_images()
        dlg_max_scans = UserMsg(
            label=f"Maximum number of scans in memory is {max_scans}"
        )
    dlg_tomo_loop = UserIntInput(
        label="Number of Scans ?",
        defval=int(parameters["tomo_loop"] * parameters["ntomo"]),
    )

    sequence_trigger_mode_available = sequence.pars.sequence_trigger_mode_available
    sequence_trigger_mode_values = []
    for i, value in enumerate(sequence_trigger_mode_available):
        sequence_trigger_mode_values.append((i, value))
        if value == parameters["sequence_trigger_mode"]:
            defval = i
    dlg_sequence_trigger_mode = UserChoice(
        label="Sequence Trigger Mode",
        values=sequence_trigger_mode_values,
        defval=defval,
    )

    musst_shutter_ctrl = True if sequence.pars.shutter_mode == "SYNCHRONIZED" else False
    dlg_musst_shutter_ctrl = UserCheckBox(
        label="Shutter Controlled by Musst",
        defval=musst_shutter_ctrl,
    )
    shutter_closing_mode = sequence.get_runner("MULTITURNS").pars.shutter_closing_mode
    shutter_closing_mode_available = sequence.get_runner(
        "MULTITURNS"
    ).pars.shutter_closing_mode_available
    shutter_mode_closing_values = []
    for i, value in enumerate(shutter_closing_mode_available):
        shutter_mode_closing_values.append((i, value))
        if value == shutter_closing_mode:
            defval = i
    dlg_shutter_closing_mode = UserChoice(
        label="Shutter Closing Mode",
        values=shutter_mode_closing_values,
        defval=defval,
    )
    dlg_shutter_time = UserFloatInput(
        label="Shutter Opening Time [s]", defval=parameters["shutter_time"]
    )

    if isinstance(sequence, PcoTomo):

        dlg_download_after_ntomo = UserIntInput(
            label="Download images after scan number?",
            defval=parameters["download_after_ntomo"],
        )
        ct = Container(
            [
                dlg_waiting_turns,
                dlg_min_acc_turns,
                dlg_starting_turns,
                dlg_max_scans,
                dlg_tomo_loop,
                dlg_download_after_ntomo,
                dlg_sequence_trigger_mode,
                dlg_musst_shutter_ctrl,
                dlg_shutter_closing_mode,
                dlg_shutter_time,
            ]
        )
    else:
        trig_mode_values = []
        loop_trig_mode = sequence.get_runner("MULTITURNS").pars.loop_trig_mode
        loop_trig_mode_available = sequence.get_runner(
            "MULTITURNS"
        ).pars.loop_trig_mode_available
        for i, value in enumerate(loop_trig_mode_available):
            trig_mode_values.append((i, value))
            if value == loop_trig_mode:
                defval = i
        dlg_loop_trig_mode = UserChoice(
            label="Loop Trig Mode", values=trig_mode_values, defval=defval
        )
        ct = Container(
            [
                dlg_tomo_loop,
                dlg_loop_trig_mode,
                dlg_waiting_turns,
                dlg_min_acc_turns,
                dlg_starting_turns,
                dlg_sequence_trigger_mode,
                dlg_musst_shutter_ctrl,
                dlg_shutter_closing_mode,
                dlg_shutter_time,
            ]
        )
    cancel_text = "Back" if submenu else "Cancel"
    ret = BlissDialog(
        [[ct]], title="Multi Turns Scan Setup", cancel_text=cancel_text
    ).show()

    # returns False on cancel

    if ret is not False:
        if ret[dlg_musst_shutter_ctrl]:
            shutter_mode = "SYNCHRONIZED"
        else:
            shutter_mode = "SOFT"
        new_params = {
            "waiting_turns": ret[dlg_waiting_turns],
            "start_turns": ret[dlg_starting_turns],
            "sequence_trigger_mode": sequence_trigger_mode_values[
                ret[dlg_sequence_trigger_mode]
            ][1],
            "shutter_mode": shutter_mode,
            "shutter_time": ret[dlg_shutter_time],
        }

        sequence.get_runner(
            "MULTITURNS"
        ).pars.shutter_closing_mode = shutter_mode_closing_values[
            ret[dlg_shutter_closing_mode]
        ][
            1
        ]

        if isinstance(sequence, PcoTomo):
            if (
                ret[dlg_tomo_loop] == ret[dlg_download_after_ntomo]
                and ret[dlg_tomo_loop] <= max_scans
            ):
                loop_mode = "FILL_MEMORY"
                if ret[dlg_waiting_turns] > 0:
                    ntomo = 1
                    tomo_loop = int(ret[dlg_tomo_loop])
                else:
                    ntomo = int(ret[dlg_tomo_loop])
                    tomo_loop = 1
            else:
                loop_mode = "DOWNLOADING"
                if ret[dlg_waiting_turns] > 0:
                    ntomo = 1
                else:
                    ntomo = ret[dlg_download_after_ntomo]
                    ret[dlg_tomo_loop] /= ret[dlg_download_after_ntomo]
                tomo_loop = int(ret[dlg_tomo_loop])

            add_params = {
                "loop_mode": loop_mode,
                "download_after_ntomo": ret[dlg_download_after_ntomo],
                "tomo_loop": tomo_loop,
                "ntomo": ntomo,
            }
            new_params.update(add_params)

        else:
            add_params = {
                "loop_mode": trig_mode_values[ret[dlg_loop_trig_mode]][1],
                "tomo_loop": ret[dlg_tomo_loop],
            }
            new_params.update(add_params)

        sequence.pars.from_dict(new_params)


def flag_setup(sequence, submenu=False):
    """
    Set-up the tomo scan options like dark images, reference images, return images and half acquisition.
    All options can be True or False
    """

    parameters = sequence.pars.to_dict()
    while True:
        dlg1 = UserCheckBox(
            name="dark_at_start",
            label="Dark Images at Start",
            defval=parameters["dark_at_start"],
        )
        dlg2 = UserCheckBox(
            name="dark_at_end",
            label="Dark Images at End",
            defval=parameters["dark_at_end"],
        )

        dlg3 = UserCheckBox(
            name="flat_at_start",
            label="Flat Images at Start",
            defval=parameters["flat_at_start"],
        )
        dlg4 = UserCheckBox(
            name="flat_at_end",
            label="Flat Images at End",
            defval=parameters["flat_at_end"],
        )
        dlg5 = UserCheckBox(
            name="projection_groups",
            label="Projection Groups",
            defval=parameters["projection_groups"],
        )

        dlg6 = UserCheckBox(
            name="images_on_return",
            label="Images on Scan Return",
            defval=parameters["images_on_return"],
        )
        dlg7 = UserCheckBox(
            name="return_images_aligned_to_flats",
            label="Return Images aligned with Projection Groups",
            defval=parameters["return_images_aligned_to_flats"],
        )
        dlg8 = UserCheckBox(
            name="beam_check", label="Beam check", defval=parameters["beam_check"]
        )
        dlg9 = UserCheckBox(
            name="refill_check", label="Refill check", defval=parameters["refill_check"]
        )
        dlg10 = UserCheckBox(
            name="wait_for_beam",
            label="Wait for beam",
            defval=parameters["wait_for_beam"],
        )
        dlg12 = UserCheckBox(
            name="return_to_start_pos",
            label="Return to Start Position",
            defval=parameters["return_to_start_pos"],
        )
        dlg13 = UserCheckBox(
            name="activate_sinogram",
            label="Sinogram",
            defval=parameters["activate_sinogram"],
        )

        ct1 = Container([dlg1, dlg2], title="Dark Images")
        ct2 = Container([dlg3, dlg4, dlg5], title="Flat Images")
        ct3 = Container([dlg6, dlg7], title="Return Images")
        ct4 = Container([dlg8, dlg9, dlg10], title="Securities")
        ct5 = Container([dlg12, dlg13], title="Others")

        cancel_text = "Back" if submenu else "Cancel"
        ret = BlissDialog(
            [[ct1, ct2, ct3], [ct4], [ct5]],
            title="Tomo Option Setup",
            cancel_text=cancel_text,
        ).show()

        # returns False on cancel
        if ret is False:
            return

        def have_changed(name):
            value = ret[name]
            previous = parameters[name]
            return value != previous

        messages = []

        def force_param(name, value, message):
            nonlocal messages
            if ret[name] == value:
                return
            print(name, ret[name], value, "FIXED")
            messages.append(message)
            ret[name] = value

        if have_changed("activate_sinogram") and ret["activate_sinogram"]:
            force_param("dark_at_start", True, "- dark at start will be forced")
            force_param("flat_at_start", True, "- flat at start will be forced")
        if have_changed("dark_at_start") and not ret["dark_at_start"]:
            force_param("activate_sinogram", False, "sinogram will be disabled")
        if have_changed("flat_at_start") and not ret["flat_at_start"]:
            force_param("activate_sinogram", False, "sinogram will be disabled")

        if len(messages) > 0:
            msg = "\n".join(messages)
            dlg1 = UserMsg(label=msg)
            cancel_text = "Back" if submenu else "Cancel"
            warn_res = BlissDialog(
                [[dlg1]], title="Warning", cancel_text=cancel_text
            ).show()
            if warn_res is False:
                parameters = ret
                continue

        keys = [
            "dark_at_start",
            "dark_at_end",
            "flat_at_start",
            "flat_at_end",
            "projection_groups",
            "images_on_return",
            "return_images_aligned_to_flats",
            "beam_check",
            "refill_check",
            "wait_for_beam",
            "return_to_start_pos",
            "activate_sinogram",
        ]
        new_params = {k: ret[k] for k in keys}
        sequence.pars.from_dict(new_params)
        break


def type_setup(sequence, submenu=False):
    """
    Set-up the tomo scan type.
    The possibilities are:
    1.) Hardware triggered continuous scan
    2.) Sweep scan
    3.) Interlaced scan
    4.) Step scan

    Set-up also type of musst triggers for continuous hard scans.
    The possibilities are:
    1.) Time
    2.) Position
    3.) Camera
    """

    parameters = sequence.pars.to_dict()

    if "helical" in sequence.name:
        scan_runner = sequence.get_runner("HELICAL")
        values = [(0, "CONTINUOUS_HARD")]
    else:
        scan_runner = sequence.get_runner(parameters.get("scan_type"))
        values = [(0, "CONTINUOUS_HARD"), (1, "SWEEP"), (2, "INTERLACED"), (3, "STEP")]

    dlg_scan_type = UserChoice(
        label="Scan Type?",
        values=values,
        defval=ScanType[parameters["scan_type"]].value,
    )

    values = [(0, "Position"), (1, "Time"), (2, "Camera")]
    scan_runner_pars = getattr(scan_runner, "pars", None)
    scan_mode = getattr(scan_runner_pars, "scan_mode", None)
    defval = 1
    if scan_runner_pars is not None and scan_mode is not None:
        for i, trigger_type in values:
            if trigger_type.upper() == scan_mode:
                defval = i

    dlg_trigger_type = UserChoice(
        label="Musst Triggered In?", values=values, defval=defval
    )

    cancel_text = "Back" if submenu else "Cancel"
    ret = BlissDialog(
        [[dlg_scan_type], [dlg_trigger_type]],
        title="Tomo Scan Type Setup",
        cancel_text=cancel_text,
    ).show()

    # returns False on cancel
    if ret is not False:
        sequence.pars.scan_type = ScanType(ret[dlg_scan_type]).name
        if scan_runner_pars is not None and scan_mode is not None:
            scan_runner_pars.scan_mode = values[ret[dlg_trigger_type]][1]


def reference_setup(sequence, submenu=False):
    """
    Set-up reference motors displacement.
    """
    # Keep it like this or not ?
    sequence._reference.setup(submenu=submenu)


def saving_setup(submenu=False):
    """
    Set-up scan saving parameters.
    User can select storage space (visitor, inhouse, tmp) and storage system (nfs, lbs).
    Then user defines proposal name, collection name and dataset name.
    """
    scan_saving_config = current_session.scan_saving.scan_saving_config
    scan_saving = current_session.scan_saving

    values = [("visitor", "visitor"), ("inhouse", "inhouse"), ("tmp", "tmp")]
    proposal_type = scan_saving.proposal_type
    # get the current proposal type as default value
    index = 0
    for i in values:
        if proposal_type == i[0]:
            type_index = index
            break
        else:
            index = index + 1

    dlg_disk = UserChoice(values=values, defval=type_index)
    ct1 = Container([dlg_disk], title="Storage Space")
    i = 0
    mp_values = []
    for mount_point in scan_saving.mount_points:
        if mount_point != "":
            mp_values.append((i, mount_point))
            i += 1

    if len(mp_values) > 1:
        defval = [
            index for index, value in mp_values if value == scan_saving.mount_point
        ]
        dlg_mount_point = UserChoice(values=mp_values, defval=defval[0])
        ct2 = Container([dlg_mount_point], title="Mount Points")
        cancel_text = "Back" if submenu else "Cancel"
        ret = BlissDialog(
            [[ct1], [ct2]], title="Saving Setup", cancel_text=cancel_text
        ).show()
    else:
        cancel_text = "Back" if submenu else "Cancel"
        ret = BlissDialog([[ct1]], title="Saving Setup", cancel_text=cancel_text).show()

    # returns False on cancel
    if ret is not False:
        proposal_type = ret[dlg_disk]
        if len(mp_values) > 1:
            scan_saving.mount_point = mp_values[ret[dlg_mount_point]][1]

        if proposal_type == "visitor":
            if len(mp_values) > 1:
                index = [
                    index
                    for index, value in mp_values
                    if value == scan_saving.mount_point
                ]
                base_path = scan_saving_config["visitor_data_root"][
                    scan_saving.mount_point
                ].replace("{beamline}", scan_saving.beamline)
            else:
                base_path = scan_saving_config["visitor_data_root"].replace(
                    "{beamline}", scan_saving.beamline
                )
            proposal_tips = "(user experiment)"
        elif proposal_type == "inhouse":
            if len(mp_values) > 1:
                index = [
                    index
                    for index, value in mp_values
                    if value == scan_saving.mount_point
                ]
                base_path = scan_saving_config["inhouse_data_root"][
                    scan_saving.mount_point
                ].replace("{beamline}", scan_saving.beamline)
            else:
                base_path = scan_saving_config["inhouse_data_root"].replace(
                    "{beamline}", scan_saving.beamline
                )
            proposal_tips = (
                "(Empty string for default proposal name or must start with blc or ih)"
            )
        elif proposal_type == "tmp":
            base_path = scan_saving_config["tmp_data_root"].replace(
                "{beamline}", scan_saving.beamline
            )
            proposal_tips = "(Musst start with test, temp or tmp)"

        current_proposal = scan_saving.proposal
        current_collection = scan_saving.collection.name
        current_dataset = scan_saving.dataset

        # if the base path has changed, clean-up proposal, collection and dataset
        if scan_saving.base_path != base_path:
            current_proposal = ""
            current_collection = ""
            current_dataset = ""

        dlg_path = UserMsg(label=f"Base Path {base_path}")
        v_proposal = Validator(proposal_validator, proposal_type)
        dlg_proposal = UserInput(
            label="Proposal Name ", defval=current_proposal, validator=v_proposal
        )
        dlg_tips = UserMsg(label=proposal_tips)
        dlg_collection = UserInput(label="Collection Name", defval=current_collection)
        dlg_dataset = UserInput(label="Dataset Name", defval=current_dataset)

        cancel_text = "Back" if submenu else "Cancel"
        ret = BlissDialog(
            [[dlg_path], [dlg_proposal], [dlg_tips], [dlg_collection], [dlg_dataset]],
            title="Saving Setup",
            cancel_text=cancel_text,
        ).show()

        # returns False on cancel
        if ret is not False:
            # get main parameters
            new_proposal = ret[dlg_proposal]
            new_collection = ret[dlg_collection]
            new_dataset = ret[dlg_dataset]

            configure(proposal_type, new_proposal, new_collection, new_dataset)


def configure_shift(sequence, shift: float, unit: ShiftType):
    """
    Get lateral axis shift value defined by the user and deduce
    other shift type values from it
    The shift values are set to 0 each time half acquisition setup
    is launched so the active shift
    differs from 0

    Arguments:
        sequence: The tomo sequence
        shift: The value of the shift
        unit:
    """

    def get_detector():
        if sequence._detector is not None:
            return sequence._detector
        return sequence._detectors.active_detector.detector

    detector = get_detector()
    pixel_size = sequence._detectors.get_image_pixel_size(detector) / 1000.0
    ccd_cols = detector.image.width

    if unit == ShiftType.MM:
        sequence.pars.shift_in_mm = shift
        sequence.pars.shift_in_pixels = shift / pixel_size
        sequence.pars.shift_in_fov_factor = sequence.pars.shift_in_pixels / ccd_cols * 2
    elif unit == ShiftType.PIXEL:
        sequence.pars.shift_in_pixels = shift
        sequence.pars.shift_in_fov_factor = shift / ccd_cols * 2
        sequence.pars.shift_in_mm = shift * pixel_size
    elif unit == ShiftType.FOV_FACTOR:
        sequence.pars.shift_in_fov_factor = shift
        sequence.pars.shift_in_pixels = shift * ccd_cols * 0.5
        sequence.pars.shift_in_mm = sequence.pars.shift_in_pixels * pixel_size
    else:
        raise ValueError(f"Unit '{unit}' is not supported")
    sequence.pars.shift_type = unit.value
    sequence.pars.acquisition_position = (
        sequence.pars.full_frame_position + sequence.pars.shift_in_mm
    )


def half_setup(sequence, submenu=False):
    """
    Set-up the lateral axis shift and the number of acquired images
    for half-acquisition tomo
    Move lateral axis to acquisition position if
    move_to_acquisition_position parameter is set to True
    """
    while True:
        values = [ShiftType.FOV_FACTOR, ShiftType.MM, ShiftType.PIXEL]
        shift_type = ShiftType(sequence.pars.shift_type)
        defval = values.index(shift_type)
        values = [(u, u.value) for u in values]

        half_acquisition = sequence.pars.half_acquisition

        dlg_enable = UserCheckBox(
            label="Half Acquisition Enabled",
            defval=half_acquisition,
        )

        dlg_size_type = UserChoice(label="Shift Type?", values=values, defval=defval)

        shifts = {
            ShiftType.FOV_FACTOR: sequence.pars.shift_in_fov_factor,
            ShiftType.MM: sequence.pars.shift_in_mm,
            ShiftType.PIXEL: sequence.pars.shift_in_pixels,
        }
        dlg_size_value = UserFloatInput(label="Shift Value?", defval=shifts[shift_type])

        v_nb_images = Validator(nb_images_validator)
        dlg_nb_images = UserInput(
            label="Number of images?",
            defval=sequence.pars.tomo_n,
            validator=v_nb_images,
        )

        dlg_move = UserCheckBox(
            label="Move to acquisition position now",
            defval=sequence.pars.move_to_acquisition_position,
        )

        cancel_text = "Back" if submenu else "Cancel"
        if half_acquisition:
            desc = [
                [dlg_enable],
                [dlg_size_type],
                [dlg_size_value],
                [dlg_nb_images],
                [dlg_move],
            ]
        else:
            desc = [[dlg_enable]]

        ret = BlissDialog(
            desc,
            title="Tomo Half Acquisition Setup",
            cancel_text=cancel_text,
        ).show()

        if ret is False:
            # Menu was cancelled
            break

        sequence.pars.half_acquisition = ret[dlg_enable]
        if half_acquisition != sequence.pars.half_acquisition:
            if sequence.pars.half_acquisition:
                continue

        if not sequence.pars.half_acquisition:
            return

        # FIXME: This should not be setup here
        # need to check with scientists if it is always the case
        sequence.pars.full_frame_position = 0.0

        shift_unit = ret[dlg_size_type]
        if shift_unit not in ShiftType:
            message_dialog(
                "Error",
                f"Shift factor {shift_unit} not supported!",
            )
            continue

        shift = ret[dlg_size_value]
        # FIXME: It would be better not to change the sequence before to be sure it is fine.
        #        Else cancelling the dialog do not means anything
        configure_shift(sequence, shift, shift_unit)

        if sequence.pars.shift_in_fov_factor == 0:
            message_dialog(
                "Error",
                "Whole sample fits into CCD field of view!\nNo need for half-acquisition!",
            )
            continue

        elif abs(sequence.pars.shift_in_fov_factor) > 0.98:
            message_dialog(
                "Error",
                "Sample too big!\nHalf of the sample does not fit in CCD field of view!",
            )
            continue

        def get_detector():
            if sequence._detector is not None:
                return sequence._detector
            return sequence._detectors.active_detector.detector

        detector = get_detector()
        ccd_cols = detector.image.width

        sequence.pars.tomo_n = int(ret[dlg_nb_images])
        fov_factor = (abs(sequence.pars.shift_in_fov_factor) * 0.5 + 0.5) * 2
        slice_size = int(fov_factor * ccd_cols)

        print(
            f"Stored {sequence._y_axis.name} full-frame pos. reference: {sequence.pars.full_frame_position }\n"
        )
        print(
            f"{sequence._y_axis.name} shift: {sequence.pars.shift_in_mm:.3f} mm - {sequence.pars.shift_in_pixels:.1f} pixels - {sequence.pars.shift_in_fov_factor:.3f} FOV\n"
        )
        print(
            f"Your reconstructed slice size will be approx. {slice_size}x{slice_size} px\n"
        )
        sequence.pars.move_to_acquisition_position = ret[dlg_move]
        if sequence.pars.move_to_acquisition_position:
            umv(sequence._y_axis, sequence.pars.acquisition_position)

        break


def nb_images_validator(str_input):
    """
    Verify number of images in half acquisition mode is even (needed for reconstruction)
    """
    if int(str_input) % 2 != 0:
        raise ValueError("Number of images must be even (for nabu reconstruction)")


def proposal_validator(str_input, proposal_type):
    """
    Verify proposal name is consistent with proposal type.
    """
    str_input = str_input.lower()

    if proposal_type == "tmp":
        if (
            not str_input.startswith("tmp")
            and not str_input.startswith("temp")
            and not str_input.startswith("test")
        ):
            raise ValueError("Proposal name does not start with test, temp or tmp")

    if proposal_type == "inhouse":
        if not str_input == "" and not str_input.startswith(
            current_session.scan_saving.beamline
        ):
            raise ValueError(
                f"Proposal name is not empty and does not start with {current_session.scan_saving.beamline}"
            )

    if proposal_type == "visitor":
        if (
            str_input.startswith("tmp")
            or str_input.startswith("temp")
            or str_input.startswith("test")
            or str_input.startswith(current_session.scan_saving.beamline)
        ):
            raise ValueError(
                f"Proposal name should not start with test, temp, tmp or {current_session.scan_saving.beamline}"
            )

    return str_input


def configure(proposal_type, proposal=None, collection=None, dataset=None):
    """
    Configure proposal name, collection name, dataset name and the path extension when using a LBS.
    """
    scan_saving_config = current_session.scan_saving.scan_saving_config
    scan_saving = current_session.scan_saving

    log_info("tomo_setup", "configure() entering")

    if proposal_type == "visitor":
        root_path = "visitor_data_root"
    elif proposal_type == "inhouse":
        root_path = "inhouse_data_root"
    elif proposal_type == "tmp":
        root_path = "tmp_data_root"

    if scan_saving.mount_point == "lbs":
        check_lbs_free_space()

    # configure data policy
    if proposal is not None:
        if scan_saving.proposal != proposal:
            proposal_validator(proposal, proposal_type)
            scan_saving.newproposal(proposal)

    if collection is not None:
        if scan_saving.collection.name != collection:
            scan_saving.newcollection(collection)

    if dataset is not None:
        if scan_saving.dataset != dataset:
            scan_saving.newdataset(dataset)

    log_info("tomo_setup", "configure() leaving")


def check_lbs_free_space():
    """
    Verify storage free space is sufficient on lbs pc.
    A threshold value is used for the check.
    If memory used is > 80%, raise exception by asking user to free space
    """
    scan_saving = current_session.scan_saving
    current_dir = scan_saving.get_path()
    # non_lbs_path = '/' + '/'.join(current_dir.split('/')[2:])

    # TO ADD ?
    # if self.parameters.lbs_active:

    df = os.popen("df -h /lbsram").readlines()
    mem_use = [item[:-1] for item in df[1].split(" ") if "%" in item]
    mem_use = int(mem_use[0])

    if mem_use > 80:
        mem_available = 100 - mem_use
        msg = f"\n\033[1mIt remains only {mem_available}% of free space on lbs\033[0m\n"
        msg += "\033[1mPlease free up some space on lbs pc\033[0m\n"
        raise Exception(msg)


def active_detector_setup(sequence, submenu: bool = False) -> Lima:
    """
    Setup the active detector used by a sequence
    """
    from tomo.helpers.select_dialog2 import select_dialog2

    tomo_config = sequence._tomo_config
    tomo_detectors = tomo_config.detectors

    value_list = []
    for d in tomo_detectors.detectors:
        label = d.detector.name
        if d is tomo_detectors.active_detector:
            label += " (active)"
        value_list.append((d, label))

    choice = None
    cancel_text = "Back" if submenu else "Cancel"
    choice = select_dialog2(
        title="Active Detector Setup",
        values=value_list,
        selection=choice,
        cancel_text=cancel_text,
    )
    if choice is None:
        return

    active_detector = choice
    from .standard import tomoccdselect

    tomoccdselect(active_detector.detector)
